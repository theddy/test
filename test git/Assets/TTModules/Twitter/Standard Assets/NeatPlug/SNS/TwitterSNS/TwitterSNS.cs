/**
 * TwitterSNS.cs
 * 
 * A Singleton class encapsulating publicly accessible methods of Twitter SNS.
 * 
 * Please read the code comments carefully, or visit 
 * http://www.neatplug.com/integration-guide-unity3d-twitter-sns-plugin to find information how 
 * to use this program.
 * 
 * End User License Agreement: http://www.neatplug.com/eula
 * 
 * (c) Copyright 2012 NeatPlug.com All rights reserved.
 * 
 * @version 1.4.1
 * @api_version(android) 1.1
 * @api_version(ios) 1.1
 *
 */

using UnityEngine;
using System;
using System.Collections;

public class TwitterSNS  {
	
	#region Fields		

#if UNITY_ANDROID
	private class TwitterSNSNativeHelper : ITwitterSNSNativeHelper {	
		private AndroidJavaObject _plugin = null;
		public TwitterSNSNativeHelper()
		{			
		}		
		public void CreateInstance(string className, string instanceMethod)
		{			
			AndroidJavaClass jClass = new AndroidJavaClass(className);
			_plugin = jClass.CallStatic<AndroidJavaObject>(instanceMethod);		
		}		
		public void Call(string methodName, params object[] args)
		{
			_plugin.Call(methodName, args);	
		}		
		public void Call(string methodName, string signature, object arg)
		{
			var method = AndroidJNI.GetMethodID(_plugin.GetRawClass(), methodName, signature);			
			AndroidJNI.CallObjectMethod(_plugin.GetRawObject(), method, AndroidJNIHelper.CreateJNIArgArray(new object[] {arg}));
		}
		public ReturnType Call<ReturnType> (string methodName, params object[] args)
		{
			return _plugin.Call<ReturnType>(methodName, args);				
		}	
	};	
#endif	

	private static TwitterSNS _instance = null;
	
	#endregion
	
	#region Functions
	
	/**
	 * Constructor.
	 */
	private TwitterSNS()
	{	
#if UNITY_ANDROID		
		TwitterSNSAndroid.Instance().SetNativeHelper(new TwitterSNSNativeHelper());
#endif	
#if UNITY_IPHONE
		TwitterSNSIOS.Instance();
#endif
	}
	
	/**
	 * Instance method.
	 */
	public static TwitterSNS Instance()
	{		
		if (_instance == null) 
		{
			_instance = new TwitterSNS();
		}
		
		return _instance;
	}	
	
	/**
	 * Initialize the environment.
	 * 
	 * @param consumerKey
	 *            string - The Consumer Key got from twitter dev site.
	 * 
	 * @param consumerSecret
	 *            string - The Consumer Secret got from twitter dev site.	
	 * 	
	 */
	public void Init(string consumerKey, string consumerSecret)
	{
#if UNITY_ANDROID		
		TwitterSNSAndroid.Instance().Init(consumerKey, consumerSecret);		
#endif	
#if UNITY_IPHONE
		TwitterSNSIOS.Instance().Init(consumerKey, consumerSecret);
#endif		
	}
	
	/**
	 * Show up a twitter login box.
	 * 
	 * You may need to call this function if you want the user to login whenever you want, 
	 * instead of logging in at the time when posting a message (status update) or screenshot.
	 * 
	 * A usecase is that you let the user login every time your game starts.
	 * 
	 */
	public void Login()
	{
#if UNITY_ANDROID		
		TwitterSNSAndroid.Instance().Login();		
#endif	
#if UNITY_IPHONE
		TwitterSNSIOS.Instance().Login();
#endif			
	}
	
	/**
	 * Log out the user.
	 */
	public void Logout()
	{
#if UNITY_ANDROID		
		TwitterSNSAndroid.Instance().Logout();		
#endif	
#if UNITY_IPHONE
		TwitterSNSIOS.Instance().Logout();
#endif			
	}	

	/**
	 * Capture and post the screenshot of your App on the user's wall.
	 * 
	 * NOTE: Screenshot capturing is having issue on Android with Unity3.4.x, 
	 * you may want to upgrade to Unity4 to get it work. That is a Unity bug.
	 * 	
	 */
	public void PostScreenShot()
	{
#if UNITY_ANDROID		
		TwitterSNSAndroid.Instance().PostScreenShot(null);		
#endif	
#if UNITY_IPHONE
		TwitterSNSIOS.Instance().PostScreenShot(null);
#endif
	}		
	
	/**
	 * Capture and post the screenshot of your App on the user's wall.
	 * 
	 * NOTE: Screenshot capturing is having issue on Android with Unity3.4.x, 
	 * you may want to upgrade to Unity3.5 or Unity4 to get it work. That is a Unity bug.
	 * 
	 * @param description
	 *            string - Description for the screenshot.
	 * 	
	 * 
	 */
	public void PostScreenShot(string description)
	{
#if UNITY_ANDROID		
		TwitterSNSAndroid.Instance().PostScreenShot(description);		
#endif	
#if UNITY_IPHONE
		TwitterSNSIOS.Instance().PostScreenShot(description);
#endif
	}	
	
	/**
	 * Post an image for the user.
	 * 
	 * @param imgBytes
	 *          byte[] - The image data.
	 * 	
	 */	
	public void PostImage(byte[] imgBytes)
	{
#if UNITY_ANDROID		
		TwitterSNSAndroid.Instance().PostImage(imgBytes, null);		
#endif	
#if UNITY_IPHONE
		TwitterSNSIOS.Instance().PostImage(imgBytes, null);
#endif
	}	
	
	/**
	 * Post an image for the user.
	 * 
	 * @param imgBytes
	 *          byte[] - The image data.
	 * 
	 * @param description
	 *          string - Description for the image.
	 */	
	public void PostImage(byte[] imgBytes, string description)
	{
#if UNITY_ANDROID		
		TwitterSNSAndroid.Instance().PostImage(imgBytes, description);		
#endif	
#if UNITY_IPHONE
		TwitterSNSIOS.Instance().PostImage(imgBytes, description);
#endif
	}
	
	/**
	 * Post a message (status update) for the user.
	 * 
	 * NOTE: The text to be posted must not exceed Twitter's maximum characters
	 * limit. (As of this document is written, the limit is 140.)
	 * 
	 * @param message
	 *           string - The message to be posted.
	 * 	
	 */
	public void PostMessage(string message)
	{
#if UNITY_ANDROID		
		TwitterSNSAndroid.Instance().PostMessage(message, null);		
#endif	
#if UNITY_IPHONE
		TwitterSNSIOS.Instance().PostMessage(message, null);
#endif
	}	
	
	/**
	 * Post a message (status update) for the user.
	 * 
	 * NOTE: The text to be posted must not exceed Twitter's maximum characters
	 * limit, please be aware of that the link will also take up some room although
	 * it would be shortened.
	 * (As of this document is written, the limit is 140.)
	 * 
	 * @param message
	 *           string - The message to be posted.
	 * 
	 * @param link
	 *           string - The link to be posted.
	 */
	public void PostMessage(string message, string link)
	{
#if UNITY_ANDROID		
		TwitterSNSAndroid.Instance().PostMessage(message, link);		
#endif	
#if UNITY_IPHONE
		TwitterSNSIOS.Instance().PostMessage(message, link);
#endif
	}
	
	/**
	 * Fetch all past tweets from the user's home timeline on twitter.
	 * 
	 * Your app will be notified with the fetched tweets by 
	 * TwitterSNSListener::OnUserTimelineDataArrived() event.
	 * 
	 */
	public void GetUserHomeTimeline()
	{
#if UNITY_ANDROID		
		TwitterSNSAndroid.Instance().GetUserHomeTimeline();	
#endif	
#if UNITY_IPHONE
		TwitterSNSIOS.Instance().GetUserHomeTimeline();
#endif
	}
	
	/**
	 * Follow a twitter user.
	 * 
	 * @param userScreenName
	 *             string - The screen name of the user.
	 */
	public void FollowUser(string userScreenName)
	{
#if UNITY_ANDROID		
		TwitterSNSAndroid.Instance().FollowUser(userScreenName);
#endif	
#if UNITY_IPHONE
		TwitterSNSIOS.Instance().FollowUser(userScreenName);
#endif	
	}

	/**
	 * Check if the user is logged in.
	 * 
	 * @return bool - True for logged in, false for not.
	 */
	public bool IsUserLoggedIn()
	{
		bool result = false;
#if UNITY_ANDROID		
		result = TwitterSNSAndroid.Instance().IsUserLoggedIn();
#endif	
#if UNITY_IPHONE
		result = TwitterSNSIOS.Instance().IsUserLoggedIn();
#endif
		return result;
	}
	
	#endregion
}
