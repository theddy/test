using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Touchten.Analytics;

namespace Touchten.TestScene
{
	public class AnalyticsMenuButton : MonoBehaviour
	{
		StateIndicator indicator;

		// Use this for initialization
		void Start ()
		{
			indicator = GetComponent<StateIndicator> ();
			TestSceneEvent.OnTTCoreInitCompleteE += TestSceneEvent_OnTTCoreInitCompleteE;
			GetComponent<Button> ().onClick.AddListener (delegate() {
				TTAnalytics.SendCustomEvent ("CustomEvent",

					new Dictionary<string, object> () {
						{ "data1","content1" },
						{ "data2","content2" }

					}

				);
			});
	
		}

		void TestSceneEvent_OnTTCoreInitCompleteE (bool success)
		{
			indicator.SetStatus (success);
		}

		void OnDestroy ()
		{
			TestSceneEvent.OnTTCoreInitCompleteE -= TestSceneEvent_OnTTCoreInitCompleteE;

		}
	}
}