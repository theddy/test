using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Touchten.Analytics;

namespace Touchten.TestScene
{
	public class AnalyticsTestModule : ITestModule
	{

		static AnalyticsTestModule _instance;

		static AnalyticsTestModule Instance {
			get {
				if (_instance == null) {
					_instance = new AnalyticsTestModule ();
				}
				return _instance;
			}
		}

		public static void Init ()
		{
			Instance.MenuButtonResourcePath = new List<string> (){ "AnalyticsMenuButton" };
			Instance.ModuleMenuResourcePath = new List<string> ();

		}

		public static IList<string> GetMenuButtonResourcePath ()
		{
			return Instance.MenuButtonResourcePath;
		}

		public static IList<string> GetModuleMenuResourcePath ()
		{
			return Instance.ModuleMenuResourcePath;
		}

		public IList<string>  MenuButtonResourcePath {
			get ;
			private set;
		}

		public IList<string>  ModuleMenuResourcePath {
			get;
			private set;
		}
	}

}