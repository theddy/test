using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Touchten.Ads.Sandbox
{
	public class PlaceholderAdsBehaviour : MonoBehaviour
	{

		public Image image;
		public System.Action<AdsType> OnClosed;
		public AdsType adsType;

		public void CloseAds ()
		{
			OnClosed (adsType);
			gameObject.SetActive (false);
		}


	}
}