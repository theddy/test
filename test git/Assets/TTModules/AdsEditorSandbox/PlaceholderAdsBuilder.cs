using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Touchten.Ads;
using System;

namespace Touchten.Ads.Sandbox
{
	public class PlaceholderAdsBuilder : MonoBehaviour
	{

		public GameObject InstantiatedCanvas;
		public GameObject InstantiatedBanner;
		public GameObject InstantiatedVideo;
		public GameObject InstantiatedRewardedVideo;

		private AdsType currentAdsType;

		private Action bannerAdsCallback;
		private Action interstitialCallback;

		Action onVideoCompleted;
		Action onVideoClosed;
		Action onRewardedVideoCompleted;
		Action onRewardedVideoClosed;

		private string InterstitialAdsPrefabName = "CanvasAdsInterstitial";
		private string BannerAdsPrefabName = "CanvasAdsBanner";

		void Start ()
		{
			InitInterstitial ();
			InitBanner ();
			InitRewardedVideo();
			InitVideoAds();
		}

		#region IAdsViewBuilder implementation

		public void CacheInterstitial ()
		{
			IsInterstitialReady = true;
		}

		public void CacheVideoAds ()
		{
			IsVideoReady = true;
		}

		public void CacheBanner ()
		{
			IsBannerReady = true;
		}

		public void CacheRewardedVideoAds ()
		{
			IsRewardedVideoReady = true;
		}

		public bool IsRewardedVideoReady {
			get ;
			private set;
		}

		public bool IsBannerReady {
			get ;
			private set;
		}

		public bool IsVideoReady {
			get ;
			private set;
		}

		public bool IsInterstitialReady {
			get ;
			private set;
		}

		public void ShowBannerAds (Action callback = null)
		{
			bannerAdsCallback = callback;
			InstantiatedBanner.SetActive (true);
			currentAdsType = AdsType.Banner;
			IsBannerReady = false;
		
		}
		public void ShowVideoAds (System.Action OnVideoComplete, System.Action OnVideoClosed)
		{
			onVideoCompleted = OnVideoComplete;
			onVideoClosed = OnVideoClosed;
			InstantiatedVideo.SetActive (true);
			IsVideoReady = false;
			currentAdsType = AdsType.Video;
		}
		public void ShowRewardedVideo (System.Action OnVideoComplete, System.Action OnVideoClosed)
		{
			onRewardedVideoCompleted = OnVideoComplete;
			onRewardedVideoClosed = OnVideoClosed;
			InstantiatedRewardedVideo.SetActive (true);
			currentAdsType = AdsType.RewardedVideo;
			IsRewardedVideoReady = false;
		}

		public void ShowInterstitial (Action OnInteract, Action OnClosed)
		{
			interstitialCallback = OnClosed;
			InstantiatedCanvas.SetActive (true);
			currentAdsType = AdsType.Intersitital;
			IsInterstitialReady = false;
		}

		public void DestroyBannerAds ()
		{
			InstantiatedBanner.SetActive (false);
		}

		#endregion



		public void CloseAds (AdsType closedAdsType)
		{
			InstantiatedCanvas.SetActive (false);
			switch (closedAdsType) {
			case AdsType.Intersitital:
				if (interstitialCallback != null)
					interstitialCallback ();
				break;
			case AdsType.Banner:
				if (bannerAdsCallback != null)
					bannerAdsCallback ();
				break;
			case AdsType.Video:
				if(onVideoClosed!=null)onVideoClosed();
				if(onVideoCompleted!=null)onVideoCompleted();
				break;
			case AdsType.RewardedVideo:
				if(onRewardedVideoClosed!=null) onRewardedVideoClosed();
				if(onRewardedVideoCompleted!=null) onRewardedVideoCompleted();
				break;
			}
		}

		void InitInterstitial ()
		{
			InstantiatedCanvas = GameObject.Instantiate (Resources.Load (InterstitialAdsPrefabName)as GameObject);
			InstantiatedCanvas.GetComponent<PlaceholderAdsBehaviour> ().OnClosed = CloseAds;
			InstantiatedCanvas.GetComponent<PlaceholderAdsBehaviour> ().adsType = AdsType.Intersitital;
			InstantiatedCanvas.SetActive (false);
			DontDestroyOnLoad (InstantiatedCanvas);
		}

		void InitBanner ()
		{
			InstantiatedBanner = GameObject.Instantiate (Resources.Load (BannerAdsPrefabName)as GameObject);
			InstantiatedBanner.GetComponent<PlaceholderAdsBehaviour> ().OnClosed = CloseAds;
			InstantiatedBanner.SetActive (false);
			DontDestroyOnLoad (InstantiatedBanner);
		}

		void InitVideoAds ()
		{
			InstantiatedVideo = GameObject.Instantiate (Resources.Load (InterstitialAdsPrefabName)as GameObject);
			InstantiatedVideo.GetComponent<PlaceholderAdsBehaviour> ().OnClosed = CloseAds;
			InstantiatedVideo.GetComponent<PlaceholderAdsBehaviour> ().adsType = AdsType.Video;
			InstantiatedVideo.SetActive (false);
			DontDestroyOnLoad (InstantiatedCanvas);
		}

		void InitRewardedVideo ()
		{
			InstantiatedRewardedVideo = GameObject.Instantiate (Resources.Load (InterstitialAdsPrefabName)as GameObject);
			InstantiatedRewardedVideo.GetComponent<PlaceholderAdsBehaviour> ().OnClosed = CloseAds;
			InstantiatedRewardedVideo.GetComponent<PlaceholderAdsBehaviour> ().adsType = AdsType.RewardedVideo;
			InstantiatedRewardedVideo.SetActive (false);
			DontDestroyOnLoad (InstantiatedCanvas);
		}

		class PlaceholderAdsResult :AdsResult
		{
			public PlaceholderAdsResult (bool isSuccess)
			{
				this.IsSuccess = isSuccess;
			}
		}

		class PlaceholderVideoAdsResult: AdsResult
		{
			public PlaceholderVideoAdsResult (bool isClosed)
			{
				this.IsSuccess = isClosed;
			}
		}
	}



	public enum AdsType
	{
		Banner,
		Intersitital,
		Video,
		RewardedVideo
	}

}



