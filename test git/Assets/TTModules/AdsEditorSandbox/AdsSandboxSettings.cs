using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System;
using System.Linq;

namespace Touchten.Ads.Sandbox
{
	#if UNITY_EDITOR
	using UnityEditor;

	[InitializeOnLoad]
	#endif
	public class AdsSandboxSettings :ScriptableObject
	{

		public static float CachingDuration{get{return Instance._cachingDuration;}}
		public static CacheResultState CacheResult{get{return Instance._cacheResult;}}


		public static VideoEndState VideoAdsEndState{get{return Instance._videoAdsEndState;}}
		public static VideoEndState RewardedVideoAdsEndState{get{return Instance._rewardedVideoAdsEndState;}}


		const string AdsSandboxSettingsAssetName = "AdsSandboxSettings";
		const string AdsSandboxSettingsPath = "Touchten Configuration/Resources";
		const string AdsSandboxSettingsAssetExtension = ".asset";


		private static AdsSandboxSettings _instance;

		static AdsSandboxSettings Instance {
			get {
				if (_instance == null) {
					_instance = Resources.Load (AdsSandboxSettingsAssetName) as AdsSandboxSettings;
					if (_instance == null) {
						_instance = CreateInstance<AdsSandboxSettings> ();
						#if UNITY_EDITOR
						string properPath = Path.Combine (Application.dataPath, AdsSandboxSettingsPath);
						if (!Directory.Exists (properPath)) {
							AssetDatabase.CreateFolder ("Assets", "Touchten Configuration");
							AssetDatabase.CreateFolder ("Assets/Touchten Configuration", "Resources");
						}

						string fullPath = Path.Combine (Path.Combine ("Assets", AdsSandboxSettingsPath),
							                  AdsSandboxSettingsAssetName + AdsSandboxSettingsAssetExtension);
						AssetDatabase.CreateAsset (_instance, fullPath);
						#endif
					}
				}
				return _instance;
			}
		}

		#if UNITY_EDITOR
		[MenuItem ("Touchten/Edit Sandbox Settings/Ads Sandbox Settings", false, 201)]
		public static void EditSettings ()
		{
			Selection.activeObject = Instance;
			EditorApplication.ExecuteMenuItem ("Window/Inspector");
		}
		#endif

		#region Settings

		[Header("Settings for ALL Advertisement type")]
		[SerializeField] float _cachingDuration = 5f;
		[SerializeField] CacheResultState _cacheResult = CacheResultState.Success;

		[Header("Settings video result callback")]
		[SerializeField] VideoEndState _videoAdsEndState = VideoEndState.ClosedOnly;
		[SerializeField] VideoEndState _rewardedVideoAdsEndState = VideoEndState.FinishedAndClosed;

		#endregion

	}

	public enum CacheResultState
	{
		Success,
		Failed
	}

	public enum VideoEndState
	{
		ClosedOnly,
		FinishedAndClosed
	}
}

