using UnityEngine;
using System.Collections;
using System;

namespace Touchten.Ads.Sandbox
{
	public class AdsSanboxService : MonoBehaviour, IAdsBannerService, IAdsInterstitialService, IAdsVideoService, IAdsRewardedVideoService
	{

		static GameObject _gameObject;

		public static AdsSanboxService Instance {
			get {
				if (_gameObject == null) {
					_gameObject = new GameObject ("Vmax");
					MonoBehaviour.DontDestroyOnLoad (_gameObject);
				}
				var instance = _gameObject.GetComponent<AdsSanboxService> ();
				if (instance == null) {
					instance = _gameObject.AddComponent<AdsSanboxService> ();
				}
				return instance;
			}
		}

		PlaceholderAdsBuilder placehodlerAds;


		#region IAdsRewardedVideoService implemetation

		public event EventHandler<AdsResult> onCachedRewardedVideo;

		public event EventHandler<AdsResult> onShownRewardedVideo;

		public event Action<AdsReward> onFinishedRewardedVideo;

		public event Action onClosedRewardedVideo;

		public void CacheRewardedVideo (string adsId)
		{
			WaitAndDo (AdsSandboxSettings.CachingDuration, () => {
				placehodlerAds.CacheRewardedVideoAds ();
				switch (AdsSandboxSettings.CacheResult) {
				case CacheResultState.Success:
					if (onCachedRewardedVideo != null) {
						onCachedRewardedVideo (this, new AdsSanboxResult ());
					}
					break;

				case CacheResultState.Failed:
					if (onCachedRewardedVideo != null) {
						onCachedRewardedVideo (this, new AdsSanboxResult (new Exception ("Cache result set to fail in settings")));
					}
					break;
				}
			});
		}

		public void ShowRewardedVideo (string adsId)
		{
			if (IsRewardedVideoCached) {
				if (onShownRewardedVideo != null) {
					onShownRewardedVideo (this, new AdsSanboxResult ());
				}
				placehodlerAds.ShowRewardedVideo (() => {
					if (onFinishedRewardedVideo != null) {
						onFinishedRewardedVideo (new AdsSandboxReward ("sandbox reward"));
					}
				}, () => {
					if (onClosedRewardedVideo != null) {
						onClosedRewardedVideo ();
					}
				});
			}
		}

		public bool IsRewardedVideoCached {
			get {
				return placehodlerAds.IsRewardedVideoReady;
			}
		}



		#endregion

		#region IAdsVideoService implementation

		public event EventHandler<AdsResult> onCachedVideo;

		public event EventHandler<AdsResult> onShownVideo;

		public event Action onFinishedVideo;

		public event Action onClosedVideo;

		public void CacheVideo (string adsId)
		{			
			WaitAndDo (AdsSandboxSettings.CachingDuration, () => {
				placehodlerAds.CacheVideoAds ();
				switch (AdsSandboxSettings.CacheResult) {
				case CacheResultState.Success:
					if (onCachedVideo != null) {
						onCachedVideo (this, new AdsSanboxResult ());
					}
					break;

				case CacheResultState.Failed:
					if (onCachedVideo != null) {
						onCachedVideo (this, new AdsSanboxResult (new Exception ("Cache result set to fail in settings")));
					}
					break;
				}
			});
		}

		public void ShowVideo (string adsId)
		{
			if (IsVideoCached) {
				AdsDebug.Log("Show Video");
				if (onShownVideo != null) {
					AdsDebug.Log("Shown Video");
					onShownVideo (this, new AdsSanboxResult ());
				}
				AdsDebug.Log("Show Video2");

				placehodlerAds.ShowVideoAds (() => {
					if (AdsSandboxSettings.VideoAdsEndState == VideoEndState.FinishedAndClosed) {
						if (onFinishedVideo != null) {
							onFinishedVideo ();
						}
					}
				}, () => {
					if (onClosedVideo != null) {
						onClosedVideo ();
					}
				});
			}
		}

		public bool IsVideoCached {
			get {
				return placehodlerAds.IsVideoReady;
			}
		}



		#endregion

		#region IAdsInterstitialService implementation

		public event EventHandler<AdsResult> onCachedInterstitial;

		public event EventHandler<AdsResult> onShownInterstitial;

		public event Action onClosedInterstitial;

		public event Action onInteractInterstitial;

		public void CacheInterstitial (string adsId)
		{
			WaitAndDo (AdsSandboxSettings.CachingDuration,
				() => {
					placehodlerAds.CacheInterstitial ();
					if (onCachedInterstitial != null)
						onCachedInterstitial (this, new AdsSanboxResult ());
				});
		}

		public void ShowInterstitial (string adsId)
		{
			if (IsInterstitialCached) {
				placehodlerAds.ShowInterstitial (() => {
					
				},
					() => {
						if (onInteractInterstitial != null) {
							onInteractInterstitial ();
						}
						if (onClosedInterstitial != null) {
							onClosedInterstitial ();
						}
					});
				if (onShownInterstitial != null)
					onShownInterstitial (this, new AdsSanboxResult ());
			}
		}

		public bool IsInterstitialCached {
			get {
				return placehodlerAds.IsInterstitialReady;
			}
		}

		#endregion

		#region IAdsBannerService implementation

		public event System.EventHandler<AdsResult> onCachedBanner;
		public event System.EventHandler<AdsResult> onShownBanner;
		public event System.Action onInteractBanner;

		public void CacheBanner (string adsId)
		{
			WaitAndDo (AdsSandboxSettings.CachingDuration,
				() => {
					placehodlerAds.CacheBanner ();
					if (onCachedBanner != null)
						onCachedBanner (this, new AdsSanboxResult ());
				});
		}

		public void ShowBanner (string adsId)
		{
			if (IsBannerCached) {
				placehodlerAds.ShowBannerAds (() => {
					if (onInteractBanner != null) {
						onInteractBanner ();
					}
				});
				if (onShownBanner != null) {
					onShownBanner (this, new AdsSanboxResult ());
				}
			}
		}

		public void DestroyBanner (string adsId)
		{
			placehodlerAds.DestroyBannerAds ();
		}

		public bool IsBannerCached {
			get {
				return placehodlerAds.IsBannerReady;
			}
		}

		#endregion

		#region IAdsService implementation

		public event System.EventHandler<AdsResult> onInitComplete;

		public void InitAds ()
		{
			placehodlerAds = new GameObject ("EditorAds").AddComponent<PlaceholderAdsBuilder> ();
			if (onInitComplete != null)
				onInitComplete (this, new AdsSanboxResult ());
			IsInitialized = true;
		}

		public bool IsInitialized {
			get ;
			private set;
		}

		#endregion


		void WaitAndDo (float waitTime, Action onWaitComplete)
		{
			StartCoroutine (WaitAndDoCoroutine (waitTime, onWaitComplete));
		}

		IEnumerator WaitAndDoCoroutine (float waitTime, Action onWaitComplete)
		{
			yield return new WaitForSeconds (waitTime);
			if (onWaitComplete != null) {
				onWaitComplete ();
			}
		}
	}
}