using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Touchten.Ads.Sandbox {

	public class CoroutineManager : MonoBehaviour {


		private static CoroutineManager m_instance;
		protected static CoroutineManager _Instance {
			get {
				if (m_instance == null) {
					var instances = GameObject.FindObjectsOfType<CoroutineManager> ();
					if (instances != null && instances.Length > 0) {
						if (instances.Length == 1) {
							m_instance = instances[0];
						} else {
							m_instance = instances[0];
							for (int i = 1; i < instances.Length; i++)
								Destroy (instances[i].gameObject);
						}
					} else {
						var go = new GameObject (typeof (CoroutineManager).Name, typeof (CoroutineManager));
						m_instance = go.GetComponent<CoroutineManager> ();
					}

					DontDestroyOnLoad (m_instance);
				}

				return m_instance;
			}
			set {
				m_instance = value as CoroutineManager;
			}
		}


		class RealtimeDelegate {
			public float start { get; private set; }
			public float duration { get; private set; }
			System.Action callback;

			public RealtimeDelegate (float start, float duration, System.Action callback) {
				this.start = start;
				this.duration = duration;
				this.callback = callback;
			}

			public bool DoWhenValid () {
				if (Time.unscaledTime - start >= duration) {
					callback ();
					return true;
				} else {
					return false;
				}
			}
		}

		List<RealtimeDelegate> realtimeDelegates;
		List<RealtimeDelegate> toBeDeleted;

		void Awake () {
			realtimeDelegates = new List<RealtimeDelegate> ();
			toBeDeleted = new List<RealtimeDelegate> ();
		}

		void Update () {
			for (int i = 0; i < realtimeDelegates.Count; i++) {
				var rd = realtimeDelegates[i];
				if (rd.DoWhenValid ())
					toBeDeleted.Add (rd);
			}

			for (int i = 0; i < toBeDeleted.Count; i++) {
				var rd = toBeDeleted[i];
				realtimeDelegates.Remove (rd);
			}

			toBeDeleted.Clear ();
		}

		static CoroutineManager Instance { get { return _Instance as CoroutineManager; } }

		static IEnumerator DoAfter (float time, System.Action callback) {
			yield return new WaitForSeconds (time);
			
			callback ();
		}

		static IEnumerator DoAfter<T> (float time, System.Action<T> callback, T argument) {
			yield return new WaitForSeconds (time);
			
			callback (argument);
		}

		static IEnumerator DoAfter<T, U> (float time, System.Func<T, U> callback, T argument) {
			yield return new WaitForSeconds (time);
			
			callback (argument);
		}

		public static Coroutine StartCoroutineDelegate (IEnumerator coroutineDelegate) {
			return Instance.StartCoroutine (coroutineDelegate);
		}

		public static void StopCoroutineDelegate (IEnumerator coroutineDelegate) {
			Instance.StopCoroutine (coroutineDelegate);
		}

		public static Coroutine WaitAndDo (float time, System.Action callback) {
			return Instance.StartCoroutine (DoAfter (time, callback));
		}

		public static Coroutine WaitAndDo<T> (float time, System.Action<T> callback, T argument) {
			return Instance.StartCoroutine (DoAfter<T> (time, callback, argument));
		}

		public static Coroutine WaitAndDo<T, U> (float time, System.Func<T, U> callback, T argument) {
			return Instance.StartCoroutine (DoAfter<T, U> (time, callback, argument));
		}

		public static void WaitAndDoRealtime (float time, System.Action callback) {
			Instance.realtimeDelegates.Add (new RealtimeDelegate (Time.unscaledTime, time, callback));
		}
	}

}