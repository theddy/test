namespace Touchten.Social.Sandbox
{

	using UnityEngine;
	using System.Collections.Generic;
	using System.Collections;
	using System.IO;
	using System;
	using System.Linq;

	#if UNITY_EDITOR
	using UnityEditor;

	[InitializeOnLoad]
	#endif
	public class SocialSandboxSettings : ScriptableObject
	{
		#region singleton

		const string SocialSandboxSettingsAssetName = "SocialSandboxSettings";
		const string SocialSandboxSettingsPath = "Touchten Configuration/Resources";
		const string SocialSandboxSettingsAssetExtension = ".asset";


		private static SocialSandboxSettings _instance;

		static SocialSandboxSettings Instance {
			get {
				if (_instance == null) {
					_instance = Resources.Load (SocialSandboxSettingsAssetName) as SocialSandboxSettings;
					if (_instance == null) {
						_instance = CreateInstance<SocialSandboxSettings> ();
						#if UNITY_EDITOR
						string properPath = Path.Combine (Application.dataPath, SocialSandboxSettingsPath);
						if (!Directory.Exists (properPath)) {
							AssetDatabase.CreateFolder ("Assets", "Touchten Configuration");
							AssetDatabase.CreateFolder ("Assets/Touchten Configuration", "Resources");
						}

						string fullPath = Path.Combine (Path.Combine ("Assets", SocialSandboxSettingsPath),
							                  SocialSandboxSettingsAssetName + SocialSandboxSettingsAssetExtension);
						AssetDatabase.CreateAsset (_instance, fullPath);
						#endif
					}
				}
				return _instance;
			}
		}

		#endregion


		#region private variables

		[Header("Facebook")]
		[SerializeField]public  CallbackState _facebookInitResult = CallbackState.Success;
		[SerializeField]float _facebookInitDuration = 1;

		[SerializeField]CallbackState _facebookCallbackResult = CallbackState.Success;
		[SerializeField]float _facebookDurationUntilCallback = 1;


		[Header("Twitter")]
		[SerializeField]CallbackState _twitterInitResult = CallbackState.Success;
		[SerializeField]float _twitterInitDuration = 1;

		[SerializeField]CallbackState _twitterCallbackResult = CallbackState.Success;
		[SerializeField]float _twitterDurationUntilCallback = 1;


		[Header("Instagram")]
		[SerializeField]CallbackState _instagramInitResult = CallbackState.Success;
		[SerializeField]float _instagramInitDuration = 1;

		[SerializeField]CallbackState _instagramCallbackResult = CallbackState.Success;
		[SerializeField]float _instagramDurationUntilCallback = 1;

		#endregion

		#region Properties

		#region Facebook

		public static CallbackState FacebookInitResult{ get { return Instance._facebookInitResult; } set { Instance._facebookInitResult = value; } }

		public static float FacebookInitDuration { get { return Instance._facebookInitDuration; } }

		public static CallbackState FacebookCallbackResult { get { return Instance._facebookCallbackResult; } set { Instance._facebookCallbackResult = value; } }

		public static float FacebookDurationUntilCallback  { get { return Instance._facebookDurationUntilCallback; } }

		#endregion

		#region Twitter

		public static CallbackState TwitterInitResult{ get { return Instance._twitterInitResult; } set { Instance._twitterInitResult = value; } }

		public static float TwitterInitDuration  { get { return Instance._twitterInitDuration; } }

		public static CallbackState TwitterCallbackResult { get { return Instance._twitterCallbackResult; } set { Instance._twitterCallbackResult = value; } }

		public static float TwitterDurationUntilCallback { get { return Instance._twitterDurationUntilCallback; } }

		#endregion

		#region Instagram

		public static CallbackState InstagramInitResult{ get { return Instance._instagramInitResult; } set { Instance._instagramInitResult = value; } }

		public static float InstagramInitDuration { get { return Instance._instagramInitDuration; } }

		public static CallbackState InstagramCallbackResult{ get { return Instance._instagramCallbackResult; } set { Instance._instagramCallbackResult = value; } }

		public static float InstagramDurationUntilCallback  { get { return Instance._instagramDurationUntilCallback; } }

		#endregion

		#endregion

		#if UNITY_EDITOR
		[MenuItem ("Touchten/Edit Sandbox Settings/Social Sandbox Settings", false, 200)]
		public static void EditSettings ()
		{
			Selection.activeObject = Instance;
			EditorApplication.ExecuteMenuItem ("Window/Inspector");
		}
		#endif
	}

	public enum CallbackState
	{
		Success = 1,
		Fail = 2,
	}
}