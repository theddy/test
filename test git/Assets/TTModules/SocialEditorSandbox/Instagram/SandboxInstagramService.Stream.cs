using System;
using UniRx;
using Instagram;
namespace Touchten.Social.Sandbox.Instagram
{
	public partial class SandboxInstagramService
	{
		private InstagramAPI igAPI { get { return InstagramAPI.Instance; }}
		UniRx.IObservable<UserDetail[]> GetSelfFollowsStream()
		{
			var stream = Observable.FromEvent<InstagramAPI.UserSelfFollowsHandler, User[]>(
				handler => (InstagramAPI sender, User[] users) => handler(users),
				_ => igAPI.OnUserFollowsRetrieved += _,
				_ => igAPI.OnUserFollowsRetrieved -= _
			);

			return Observable.Create<UserDetail[]> (
				observer =>
				{
					stream.Take(1).Timeout(TimeSpan.FromSeconds(20)).Subscribe
					(
						users =>
						{
							UserDetail[] socialUser = new UserDetail[users.Length];
							for (int i = 0; i < users.Length; i++) {
								UserDetail detail = new UserDetail(users[i].Id,
									users[i].Username,
									users[i].Fullname);
								socialUser[i] = detail;
							}
							observer.OnNext(socialUser);
							observer.OnCompleted();
						},
						ex => observer.OnError(ex)
					);

					igAPI.GetUserFollows();

					return Disposable.Empty;
				}
			);
		}
		UniRx.IObservable<UserDetail[]> GetSelfFollowedByStream()
		{
			var stream = Observable.FromEvent<InstagramAPI.UserSelfFollowedByHandler, User[]>(
				handler => (InstagramAPI sender, User[] users) => handler(users),
				_ => igAPI.OnUserFollowedByRetrieved += _,
				_ => igAPI.OnUserFollowedByRetrieved -= _
			);

			return Observable.Create<UserDetail[]> (
				observer =>
				{
					stream.Take(1).Timeout(TimeSpan.FromSeconds(20)).Subscribe
					(
						users =>
						{
							UserDetail[] socialUser = new UserDetail[users.Length];
							for (int i = 0; i < users.Length; i++) {
								UserDetail detail = new UserDetail(users[i].Id,
									users[i].Username,
									users[i].Fullname);
								socialUser[i] = detail;
							}
							observer.OnNext(socialUser);
							observer.OnCompleted();
						},
						ex => observer.OnError(ex)
					);

					igAPI.GetUserFollowedBy();

					return Disposable.Empty;
				}
			);
		}

		UniRx.IObservable<bool> CheckInstagramUserFollowsStream(string userId)
		{
			var stream = Observable.FromEvent<InstagramAPI.UserSelfFollowsHandler, User[]>(
				handler => (InstagramAPI sender, User[] users) => handler(users),
				_ => igAPI.OnUserFollowsRetrieved += _,
				_ => igAPI.OnUserFollowsRetrieved -= _
			);

			return Observable.Create<bool> (
				observer =>
				{
					stream.Take(1).Timeout(TimeSpan.FromSeconds(20)).Subscribe
					(
						users =>
						{
							bool isFollows = false;
							for (int i = 0; i < users.Length; i++)
							{
								if (users[i].Id.Equals(userId))
								{
									isFollows = true;
									break;
								}
							}

							observer.OnNext(isFollows);
							observer.OnCompleted();
						},
						ex => observer.OnError(ex)
					);

					igAPI.GetUserFollows();

					return Disposable.Empty;
				}
			);
		}

		public UniRx.IObservable<Unit> LoginInstagramStream()
		{

			if(igAPI.AccessToken!=null)
			{
				return Observable.Create<Unit>(delegate(UniRx.IObserver<Unit> observer) {
					observer.OnNext(Unit.Default);
					observer.OnCompleted();
					return Disposable.Empty;
				});
			}
			var loginCompletedStream = Observable.FromEvent<InstagramAPI.LoginHandler, InstagramAPI>( 
				handler => (InstagramAPI sender) => {handler(sender);},
				x => igAPI.OnLogin += x,
				x => igAPI.OnLogin -= x
			);

			var loginCanceledStream = Observable.FromEvent<InstagramAPI.LoginHandler, InstagramAPI>( 
				handler => (InstagramAPI sender) => {handler(sender);},
				_ => igAPI.OnLoginCanceled += _,
				_ => igAPI.OnLoginCanceled -= _
			);

			return Observable.Create<Unit> (
				observer => 
				{
					loginCompletedStream.Take(1).Timeout(TimeSpan.FromSeconds(20)).Subscribe(
						res =>
						{
							observer.OnNext(Unit.Default);
							observer.OnCompleted();
						},
						ex => observer.OnError(ex));

					loginCanceledStream.Take(1).Timeout(TimeSpan.FromSeconds(20)).Subscribe(
						res => observer.OnError(new Exception("Login Canceled")),
						ex => observer.OnError(new Exception("Login Canceled"))
					);

					igAPI.Auth("http://www.touchten.com", new string[] {"basic", "follower_list", "relationships"});

					return Disposable.Empty;	
				}
			);
		}

		UniRx.IObservable<Unit> FollowUserStream(string userId)
		{
			var stream = Observable.FromEvent<InstagramAPI.UserRelationshipHandler, UserRelationship>(
				handler => (InstagramAPI sender, UserRelationship relationship) => handler(relationship),
				_ => igAPI.OnUserRelationshipRetrieved += _,
				_ => igAPI.OnUserRelationshipRetrieved -= _
			);

			return Observable.Create<Unit> (
				observer =>
				{
					stream.Take(1).Timeout(TimeSpan.FromSeconds(20)).Subscribe
					(
						users =>
						{
							observer.OnNext(Unit.Default);
							observer.OnCompleted();
						},
						ex => observer.OnError(ex)
					);

					igAPI.SetUserRelationship(userId, "follow");


					return Disposable.Empty;
				}
			);

		}
		UniRx.IObservable<Unit> UnfollowUserStream(string userId)
		{
			var stream = Observable.FromEvent<InstagramAPI.UserRelationshipHandler, UserRelationship>(
				handler => (InstagramAPI sender, UserRelationship relationship) => handler(relationship),
				_ => igAPI.OnUserRelationshipRetrieved += _,
				_ => igAPI.OnUserRelationshipRetrieved -= _
			);

			return Observable.Create<Unit> (
				observer =>
				{
					stream.Take(1).Timeout(TimeSpan.FromSeconds(20)).Subscribe
					(
						users =>
						{
							observer.OnNext(Unit.Default);
							observer.OnCompleted();
						},
						ex => observer.OnError(ex)
					);

					igAPI.SetUserRelationship(userId, "unfollow");

					return Disposable.Empty;
				}
			);

		}
	}
}

