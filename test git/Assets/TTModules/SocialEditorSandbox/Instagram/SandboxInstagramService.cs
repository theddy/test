using System;
using UnityEngine;
using UniRx;
using System.Linq;
using Instagram;
namespace Touchten.Social.Sandbox.Instagram
{
	public partial class SandboxInstagramService : IInstagramService
	{
		#region ISocialShareService implementation
		public void Post (string message, SocialDelegate<IActionResult> postCompleteCallback)
		{
			Debug.LogWarning("[Instagram] No Post Implementation");
		}

		public void Post (string title, string message, SocialDelegate<IActionResult> postCompleteCallback)
		{
			Debug.LogWarning("[Instagram] No Post Implementation");
		}

		public void PostImageFromURL (string message, string imageURL, SocialDelegate<IActionResult> postCompleteCallback)
		{
			Debug.LogWarning("[Instagram] No Post Image From URL Implementation");
		}
		public void PostImage (string message, Texture2D image, SocialDelegate<IActionResult> postCompleteCallback)
		{
			MakeFakeCallback(postCompleteCallback,"Instagram PostImage success");
		}

		public void PostScreenShot (string message, SocialDelegate<IActionResult> postCompleteCallback)
		{
			MakeFakeCallback(postCompleteCallback,"Instagram PostScreenShot success");
		}
		#endregion
		#region ISocialLoginService implementation
		public void Login (SocialDelegate<ITTLoginResult> loginCompleteCallback)
		{
			Debug.LogWarning("[Instagram] No Login Implementation");
		}

		public void Logout (SocialDelegate<IActionResult> logoutCompleteCallback)
		{
			Debug.LogWarning("[Instagram] No Logout Implementation");
		}
		public bool IsLoggedIn {
			get {
				return true;
			}
		}
		#endregion
		#region ISocialCoreService implementation
		public void Init (SocialDelegate<IActionResult> initCompleteCallback)
		{
			WaitAndDo (SocialSandboxSettings.InstagramInitDuration, () => {
				if (initCompleteCallback != null) {
					switch (SocialSandboxSettings.InstagramCallbackResult) {
					case CallbackState.Success:
						SocialDebug.Log ("Init succeeded");
						initCompleteCallback (new ActionResult ());
						isInitComplete = true;
						new GameObject().AddComponent<InstagramAPI>();
						break;
					case CallbackState.Fail:
						SocialDebug.Log ("Init failed");
						initCompleteCallback (new ActionResult ("Callback result set to fail", false));
						isInitComplete = false;
						break;
					}
				}
			});

		}

		public bool IsInitComplete {
			get {
				if (isInitComplete == false) {
					SocialDebug.Log ("Module not initialized");
				}
				return isInitComplete;
			}
		}
		#endregion

		private bool isInitComplete = false;
		private bool isLoggedIn = false;

		void WaitAndDo (float waitTime, Action onWaitComplete)
		{
			Observable.Timer (TimeSpan.FromSeconds ((double)waitTime)).SubscribeOnMainThread ().Subscribe (delegate(long obj) {
				if (onWaitComplete != null) {
					onWaitComplete ();
				}
			});

		}
		void MakeFakeCallback (SocialDelegate<IActionResult> callback, string successMessage)
		{
			if (IsInitComplete) {
				WaitAndDo (SocialSandboxSettings.InstagramDurationUntilCallback, () => {
					if (callback != null) {
						switch (SocialSandboxSettings.InstagramCallbackResult) {
						case CallbackState.Success:
							SocialDebug.Log (successMessage);
							callback (new ActionResult ());
							break;
						case CallbackState.Fail:
							callback (new ActionResult ("Instagram callback action result set to fail", false));
							break;
						}

					}
				});

			}

		}

		public void CheckFollowUser (string id, SocialDelegate<ICheckFollowResult> callback)
		{
			var query = from login in LoginInstagramStream()
				from isFollowing in CheckInstagramUserFollowsStream(id)
				select (bool)isFollowing;

			query.Subscribe(delegate(bool result) {
				if(callback!=null)callback(new CheckFollowResult(result));
			},delegate(Exception obj) {
				if(callback!=null)callback(new CheckFollowResult(obj.Message,false));
			});
		}

		public void FollowUser (string id, SocialDelegate<IActionResult> callback)
		{
			var query = from login in LoginInstagramStream()
				from action in FollowUserStream(id)
				select action;

			query.Subscribe(delegate(Unit result) {
				if(callback!=null)callback(new ActionResult());
			},delegate(Exception obj) {
				if(callback!=null)callback(new ActionResult(obj.Message,false));
			});
		}

		public void UnfollowUser (string id, SocialDelegate<IActionResult> callback)
		{			
			var query = from login in LoginInstagramStream()
				from action in UnfollowUserStream(id)
				select action;

			query.Subscribe(delegate(Unit result) {
				if(callback!=null)callback(new ActionResult());
			},delegate(Exception obj) {
				if(callback!=null)callback(new ActionResult(obj.Message,false));
			});
		}

		public void GetFollowing (SocialDelegate<IUserListResult> callback)
		{
			SocialDebug.Log("GetFollowingInstagram");
			var query = from login in LoginInstagramStream()
				from following in GetSelfFollowsStream()
				select following;

			query.Subscribe(delegate(UserDetail[] result) {
				if(callback!=null)callback(new UserListResult(result));
			},delegate(Exception obj) {
				if(callback!=null)callback(new UserListResult(obj.Message,false));
			});
		}

		public void GetFollower (SocialDelegate<IUserListResult> callback)
		{
			var query = from login in LoginInstagramStream()
				from following in GetSelfFollowedByStream()
				select following;

			query.Subscribe(delegate(UserDetail[] result) {
				if(callback!=null)callback(new UserListResult(result));
			},delegate(Exception obj) {
				if(callback!=null)callback(new UserListResult(obj.Message,false));
			});
		}
			
	}
}