using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;


namespace Touchten.Social.Sandbox.Facebook
{
	internal class SandboxFacebookService : IFacebookService
	{


		#region IFacebookService implementation

		public void ShowInviteFriendsView (string title, string message, SocialDelegate<IActionResult> callback)
		{
			var query = from init in InitStream ()
			            from cb in MakeFakeCallbackStream (callback, "Facebook ShowInviteFriendsView called successfully")
			            select cb;
			query.Subscribe ();
		}

		public void ShowPostView (string message, SocialDelegate<IActionResult> callback)
		{
			var query = from init in InitStream ()
			            from cb in MakeFakeCallbackStream (callback, "Facebook ShowPostView called successfully")
			            select cb;
			query.Subscribe ();
		}

		public void ShowPostView (string title, string message, SocialDelegate<IActionResult> callback)
		{
			var query = from init in InitStream ()
			            from cb in MakeFakeCallbackStream (callback, "Facebook ShowPostView called successfully")
			            select cb;
			query.Subscribe ();
		}

		public void PostScreenshotWithDialog (string message, SocialDelegate<IActionResult> callback)
		{
			var query = from init in InitStream ()
			            from cb in MakeFakeCallbackStream (callback, "Facebook PostScreenshotWithDialog called successfully")
			            select cb;
			query.Subscribe ();
		}

		public void PostImageWithDialog (string message, Texture2D image, SocialDelegate<IActionResult> callback)
		{
			var query = from init in InitStream ()
			            from cb in MakeFakeCallbackStream (callback, "Facebook PostImageWithDialog called successfully")
			            select cb;
			query.Subscribe ();
		}

		public void PostImageFromURLWithDialog (string message, string imageURL, SocialDelegate<IActionResult> callback)
		{
			var query = from init in InitStream ()
			            from cb in MakeFakeCallbackStream (callback, "Facebook PostImageFromURLWithDialog called successfully")
			            select cb;
			query.Subscribe ();
		}

		public void GetSocialProfile (System.Action<string> callback)
		{
			if (IsInitComplete && IsLoggedIn) {
				Observable.Timer (TimeSpan.FromSeconds (SocialSandboxSettings.FacebookDurationUntilCallback)).SubscribeOnMainThread ().Subscribe (delegate(long obj) {
					if (callback != null) {
						switch (SocialSandboxSettings.FacebookCallbackResult) {
						case CallbackState.Success:
							callback ("Fake social profile returned successfully");
							break;
						case CallbackState.Fail:
							callback ("Fake social profile returns error");
							break;
						}
					}
				});
			}
		}

		#endregion

		#region ISocialFriendsService implementation

		public void GetFriends (System.Action<System.Collections.Generic.IEnumerable<string>> callback)
		{
			List<string> friendList = new List<string> () {
				"You",
				"have",
				"no",
				"friends",
				"here"
			};

			if (IsInitComplete && IsLoggedIn) {
				Observable.Timer (TimeSpan.FromSeconds (SocialSandboxSettings.FacebookDurationUntilCallback)).SubscribeOnMainThread ().Subscribe (delegate(long obj) {

					if (callback != null) {
						switch (SocialSandboxSettings.FacebookCallbackResult) {
						case CallbackState.Success:
							SocialDebug.Log ("Get Friends succeeded");
							callback (friendList);
							break;
						case CallbackState.Fail:
							SocialDebug.Log ("Get Friends failed");
							callback (null);
							break;
						}
					}

				});
			}

		}

		#endregion

		#region ISocialShareService implementation

		public void Post (string message, SocialDelegate<IActionResult> postCompleteCallback)
		{
			var query = from init in InitStream ()
			            from cb in MakeFakeCallbackStream (postCompleteCallback, "Facebook Post called successfully")
			            select cb;
			query.Subscribe ();
		}

		public void Post (string title, string message, SocialDelegate<IActionResult> postCompletedCallback)
		{
			var query = from init in InitStream ()
			            from cb in MakeFakeCallbackStream (postCompletedCallback, "Facebook Post called successfully")
			            select cb;
			query.Subscribe ();
		}

		public void PostImage (string message, Texture2D image, SocialDelegate<IActionResult> postCompleteCallback)
		{
			var query = from init in InitStream ()
			            from cb in MakeFakeCallbackStream (postCompleteCallback, "Facebook PostImage called successfully")
			            select cb;
			query.Subscribe ();
		}

		public void PostImageFromURL (string message, string imageURL, SocialDelegate<IActionResult> postCompleteCallback)
		{
			var query = from init in InitStream ()
			            from cb in MakeFakeCallbackStream (postCompleteCallback, "Facebook PostImageFromURL called successfully")
			            select cb;
			query.Subscribe ();
		}

		public void PostScreenShot (string message, SocialDelegate<IActionResult> postCompleteCallback)
		{
			var query = from init in InitStream ()
			            from cb in MakeFakeCallbackStream (postCompleteCallback, "Facebook PostScreenShot called successfully")
			            select cb;
			query.Subscribe ();
		}

		#endregion

		#region ISocialLoginService implementation

		public void Login (SocialDelegate<ITTLoginResult> loginCompleteCallback)
		{

			if (IsInitComplete) {
				isLoggedIn = true;
			}
		}

		public void Logout (SocialDelegate<IActionResult> logoutCompleteCallback)
		{
			if (IsInitComplete) {
				isLoggedIn = false;
			}
		}

		public bool IsLoggedIn {
			get {
				if (isLoggedIn == false) {
					SocialDebug.Log ("Not logged in");
				}
				return isLoggedIn;
			}
		}

		#endregion

		#region ISocialCoreService implementation

		public void Init (SocialDelegate<IActionResult> initCompleteCallback)
		{
			InitStream ().Subscribe (x => {
				SocialDebug.Log ("Login succeeded");
				initCompleteCallback (new ActionResult ());
			},
				ex => {
					initCompleteCallback (new ActionResult (ex.Message, false));
				}
			);
		}

		public bool IsInitComplete {
			get {
				if (isInitComplete == false) {
					SocialDebug.Log ("Module not initialized");
				}
				return isInitComplete;
			}
		}

		#endregion


		private bool isInitComplete = false;
		private bool isLoggedIn = false;

		public void MakeFakeCallback (SocialDelegate<IActionResult> callback, string successMessage)
		{
			if (IsInitComplete && IsLoggedIn) {
				Observable.Timer (TimeSpan.FromSeconds (SocialSandboxSettings.FacebookDurationUntilCallback)).SubscribeOnMainThread ().Subscribe (delegate(long obj) {
					if (callback != null) {
						switch (SocialSandboxSettings.FacebookCallbackResult) {
						case CallbackState.Success:
							SocialDebug.Log (successMessage);
							callback (new ActionResult ());
							break;
						case CallbackState.Fail:
							callback (new ActionResult ("Callback result set to fail", false));
							break;
						}
					}
				
				});
			}
		}


		IObservable<Unit> MakeFakeCallbackStream (SocialDelegate<IActionResult> callback, string successMessage)
		{
			return Observable.Create<Unit> (delegate(IObserver<Unit> arg) {				
				MakeFakeCallback (callback, successMessage);
				arg.OnNext (Unit.Default);
				arg.OnCompleted ();
				return Disposable.Empty;
			});

		}

		IObservable<Unit> InitStream ()
		{
			return Observable.Create<Unit> (delegate(IObserver<Unit> arg) {	
				Observable.Timer (TimeSpan.FromSeconds (SocialSandboxSettings.FacebookDurationUntilCallback)).SubscribeOnMainThread ().Subscribe (delegate(long obj) {
		
					switch (SocialSandboxSettings.FacebookCallbackResult) {
					case CallbackState.Success:
						
						arg.OnNext (Unit.Default);
						arg.OnCompleted ();
						isInitComplete = true;
						break;
					case CallbackState.Fail:

						arg.OnError (new Exception ("callback set to error on setting"));
						isInitComplete = false;
						break;
					default:
						SocialDebug.Log (" state error ");
						break;
					}
				});				
				return Disposable.Empty;
			});

		}

	}
}