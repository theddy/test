using UnityEngine;
using System.Collections;
using System;
using UniRx;

namespace Touchten.Social.Sandbox.Twitter
{
	internal class SandboxTwitterService : ITwitterService
	{
		#region ISocialShareService implementation

		public void Post (string message, SocialDelegate<IActionResult> postCompleteCallback)
		{
			MakeFakeCallback (postCompleteCallback, "Twitter Post called successfully");
		}

		public void Post (string title, string message, SocialDelegate<IActionResult> postCompletedCallback)
		{
			MakeFakeCallback (postCompletedCallback, "Twitter Post called successfully");
		}

		public void PostImage (string message, Texture2D image, SocialDelegate<IActionResult> postCompleteCallback)
		{
			MakeFakeCallback (postCompleteCallback, "Twitter PostImage called successfully");
		}

		public void PostImageFromURL (string message, string imageURL, SocialDelegate<IActionResult> postCompleteCallback)
		{
			MakeFakeCallback (postCompleteCallback, "Twitter PostImageFromURL called successfully");
		}

		public void PostScreenShot (string message, SocialDelegate<IActionResult> postCompleteCallback)
		{
			MakeFakeCallback (postCompleteCallback, "Twitter PostScreenShot called successfully");
		}

		#endregion


		#region ISocialLoginService implementation

		public void Login (SocialDelegate<ITTLoginResult> loginCompleteCallback)
		{
			SocialDebug.Log("TryLogin");
			if (IsInitComplete) {
				WaitAndDo (SocialSandboxSettings.TwitterDurationUntilCallback, () => {
					if (loginCompleteCallback != null) {
						if (SocialSandboxSettings.TwitterCallbackResult == CallbackState.Success) {
							isLoggedIn = true;
							loginCompleteCallback (new LoginResult ("Sandbox access Token here"));
						} else {
							isLoggedIn = false;
							loginCompleteCallback (new LoginResult ("Twitter callback set to error", false));
						}
					}

				});
			}
		}

		public void Logout (SocialDelegate<IActionResult> logoutCompleteCallback)
		{
			if (IsInitComplete && IsLoggedIn) {
				WaitAndDo (SocialSandboxSettings.TwitterDurationUntilCallback, () => {
					logoutCompleteCallback (new ActionResult ());
					isLoggedIn = false;
				});
			}
		}

		public bool IsLoggedIn {
			get {
				if (isLoggedIn == false) {
					SocialDebug.Log ("Not logged in");
				}
				return isLoggedIn;
			}
		}
		#endregion

		#region ISocialCoreService implementation

		public void Init (SocialDelegate<IActionResult> initCompleteCallback)
		{
			WaitAndDo (SocialSandboxSettings.TwitterInitDuration, () => {
				if (initCompleteCallback != null) {
					switch (SocialSandboxSettings.TwitterCallbackResult) {
					case CallbackState.Success:
						SocialDebug.Log ("Init success");
						initCompleteCallback (new ActionResult ());
						isInitComplete = true;
						break;
					case CallbackState.Fail:
						SocialDebug.Log ("init failed");
						initCompleteCallback (new ActionResult ("Callback result set to fail", false));
						isInitComplete = false;
						break;
					}
				}
			});
		}



		public bool IsInitComplete {
			get {
				if (isInitComplete == false) {
					SocialDebug.Log ("Module not initialized");
				}
				return isInitComplete;
			}
		}

		#endregion

		private bool isInitComplete = false;
		private bool isLoggedIn = false;

		void WaitAndDo (float waitTime, Action onWaitComplete)
		{

			Observable.Timer (TimeSpan.FromSeconds ((double)waitTime)).SubscribeOnMainThread ().Subscribe (delegate(long obj) {
				if (onWaitComplete != null) {
					onWaitComplete ();
				}
			});

		}


		void MakeFakeCallback (SocialDelegate<IActionResult> callback, string successMessage)
		{
			if (IsInitComplete ) {
				WaitAndDo (SocialSandboxSettings.TwitterDurationUntilCallback, () => {
					if (callback != null) {
						switch (SocialSandboxSettings.TwitterCallbackResult) {
						case CallbackState.Success:
							SocialDebug.Log (successMessage);
							callback (new ActionResult ());
							break;
						case CallbackState.Fail:
							callback (new ActionResult ("Twitter callback action result set to fail", false));
							break;
						}

					}
				});
			}
		}
	}
}