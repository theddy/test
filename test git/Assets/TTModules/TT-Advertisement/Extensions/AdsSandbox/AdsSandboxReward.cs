using System;

namespace  Touchten.Ads.Sandbox
{
	public class AdsSandboxReward:AdsReward
	{
		public AdsSandboxReward (string rewardName)
		{
			Quantity = 1;
			Item = rewardName;
		}

		public AdsSandboxReward (int quantity, string rewardName)
		{
			Quantity = quantity;
			Item = rewardName;
		}

		public override string ToString ()
		{
			return string.Format ("[AdsSandboxReward: Quantity={0}, Item={1}]", Quantity, Item);
		}
	}
}

