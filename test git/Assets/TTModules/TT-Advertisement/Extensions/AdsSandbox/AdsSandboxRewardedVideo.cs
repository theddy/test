using System;
using System.Collections.Generic;
using RSG;

namespace Touchten.Ads.Sandbox
{
	public class AdsSandboxRewardedVideo:AdsRewardedVideoBase
	{
		Dictionary<int, EventHandler<AdsResult>> initHandlers;
		Dictionary<int, EventHandler<AdsResult>> cacheHandlers;
		Dictionary<int, EventHandler<AdsResult>> showHandlers;
		Action<AdsReward> finishHandler;
		Action closeHandler;

		public AdsSandboxRewardedVideo ()
		{
			_rewardedVideoService = AdsSanboxService.Instance;
			initHandlers = new Dictionary<int, EventHandler<AdsResult>> ();
			cacheHandlers = new Dictionary<int, EventHandler<AdsResult>> ();
			showHandlers = new Dictionary<int, EventHandler<AdsResult>> ();

			_rewardedVideoService.onFinishedRewardedVideo += OnFinish;
			_rewardedVideoService.onClosedRewardedVideo += OnClose;
		}

		#region implemented abstract members of AdsRewardedVideoBase

		public override bool IsRewardedVideoReady { 
			get {
				return _rewardedVideoService.IsRewardedVideoCached;
			}
		}

		public override void InitRewardedVideo (EventHandler<AdsResult> callback)
		{
			Init ()
				.Done (delegate(AdsResult obj) {
					if(callback != null) callback(this, new AdsSanboxResult ());
				}, delegate(Exception obj) {
					if(callback != null) callback(this, new AdsSanboxResult (obj));	
				});
		}

		public override void CacheRewardedVideo (EventHandler<AdsResult> callback)
		{
			Init ()
				.Then(x => Cache())
				.Done (delegate(AdsResult obj) {
					if(callback != null) callback(this, new AdsSanboxResult ());
				}, delegate(Exception obj) {
					if(callback != null) callback(this, new AdsSanboxResult (obj));	
				});
		}

		public override void ShowRewardedVideo (Action<AdsReward> OnVideoComplete, Action OnVideoClosed, EventHandler<AdsResult> callback)
		{	
			ListenToFinish (OnVideoComplete);
			ListenToClose (OnVideoClosed);
			Init ()
				.Then(x => Cache())
				.Then(x => Show())
				.Done (delegate(AdsResult obj) {
					if(callback != null) callback(this, new AdsSanboxResult ());
				}, delegate(Exception obj) {
					finishHandler = null;
					closeHandler = null;
					if(callback != null) callback(this, new AdsSanboxResult (obj));	
				});
		}

		#endregion

		void ListenToFinish(Action<AdsReward> onFinish){
			if(onFinish != null){
				finishHandler = onFinish;
			}
		}

		void ListenToClose(Action onClose){
			if(onClose != null){
				closeHandler = onClose;
			}
		}

		void OnFinish (AdsReward reward)
		{
			AdsDebug.Log("Rewarded video FINISH Handler: " + finishHandler);
			if(finishHandler != null){
				finishHandler (reward);
			}
		}

		void OnClose ()
		{
			AdsDebug.Log("Rewarded video CLOSE Handler: " + closeHandler);
			if(closeHandler != null){
				closeHandler ();
			}
		}

		#region Stream
		IPromise<AdsResult> Init (){
			var promise = new Promise<AdsResult> ();

			if (!_rewardedVideoService.IsInitialized) {
				EventHandler<AdsResult> initHandler = (object sender, AdsResult e) => {
					AdsDebug.Log ("Rewarded video INIT Handler ID: " + promise.Id);
					if (e.Error != null)
						promise.Reject (e.Error);
					else
						promise.Resolve (e);

					//Unsubscribe event
					_rewardedVideoService.onInitComplete -= initHandlers [promise.Id];
					initHandlers.Remove (promise.Id);
				};

				initHandlers.Add (promise.Id, initHandler);
				_rewardedVideoService.onInitComplete += initHandler;

				_rewardedVideoService.InitAds ();
			}
			else{
				AdsDebug.Log ("Rewarded video already INIT");
				promise.Resolve (new AdsSanboxResult());
			}

			return promise;
		}

		IPromise<AdsResult> Cache(){
			var promise = new Promise<AdsResult> ();

			if (!_rewardedVideoService.IsRewardedVideoCached) {
				EventHandler<AdsResult> cacheHandler = (object sender, AdsResult e) => {
					AdsDebug.Log ("Rewarded video CACHE Handler ID: " + promise.Id);
					if (e.Error != null)
						promise.Reject (e.Error);
					else
						promise.Resolve (e);

					//Unsubscribe event
					_rewardedVideoService.onCachedRewardedVideo -= cacheHandlers [promise.Id];
					cacheHandlers.Remove (promise.Id);
				};

				cacheHandlers.Add (promise.Id, cacheHandler);
				_rewardedVideoService.onCachedRewardedVideo += cacheHandler;
				_rewardedVideoService.CacheRewardedVideo (null);
			}
			else{
				AdsDebug.Log ("Rewarded video already CACHE");
				promise.Resolve (new AdsSanboxResult());
			}

			return promise;
		}

		IPromise<AdsResult> Show(){
			var promise = new Promise<AdsResult> ();

			EventHandler<AdsResult> showHandler = (object sender, AdsResult e) => {
				AdsDebug.Log("Rewarded video SHOW Handler ID: " + promise.Id);
				_rewardedVideoService.onShownRewardedVideo -= showHandlers[promise.Id];
				if(e.Error != null)
					promise.Reject (e.Error);
				else
					promise.Resolve (e);

				//Unsubscribe event
				showHandlers.Remove (promise.Id);
			};

			showHandlers.Add (promise.Id, showHandler);
			_rewardedVideoService.onShownRewardedVideo += showHandler;
			_rewardedVideoService.ShowRewardedVideo (null);

			return promise;
		}

		#endregion
	}
}

