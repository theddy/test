using System;
using System.Collections.Generic;
using RSG;

namespace  Touchten.Ads.Sandbox
{
	public class AdsSandboxInterstitial: AdsInterstitialBase
	{
		Dictionary<int, EventHandler<AdsResult>> initHandlers;
		Dictionary<int, EventHandler<AdsResult>> cacheHandlers;
		Dictionary<int, EventHandler<AdsResult>> showHandlers;
		Action interactHandler;
		Action closeHandler;
		bool isCaching;

		public AdsSandboxInterstitial ()
		{			
			_interstitialService = AdsSanboxService.Instance;
			initHandlers = new Dictionary<int, EventHandler<AdsResult>> ();
			cacheHandlers = new Dictionary<int, EventHandler<AdsResult>> ();
			showHandlers = new Dictionary<int, EventHandler<AdsResult>> ();
			isCaching = false;

			_interstitialService.onInteractInterstitial += OnInteract;
			_interstitialService.onClosedInterstitial += OnClose;
		}
		#region implemented abstract members of AdsInterstitialBase

		public override bool IsInterstitialReady { 
			get {
				return _interstitialService.IsInterstitialCached;
			}
		}

		public override void InitInterstitial (EventHandler<AdsResult> callback)
		{
			Init ()
				.Done (delegate(AdsResult obj) {
					if(callback != null) callback(this, new AdsSanboxResult ());
				}, delegate(Exception obj) {
					if(callback != null) callback(this, new AdsSanboxResult (obj));	
				});
		}

		public override void CacheInterstitial (EventHandler<AdsResult> callback)
		{
			Init ()
				.Then(x => Cache())
				.Done (delegate(AdsResult obj) {
					if(callback != null) callback(this, new AdsSanboxResult ());
				}, delegate(Exception obj) {
					if(callback != null) callback(this, new AdsSanboxResult (obj));	
				});
		}

		public override void ShowInterstitial (Action OnInteract, Action OnClosed, EventHandler<AdsResult> callback)
		{
			ListenToInteract (OnInteract);
			ListenToClose (OnClosed);
			Init ()
				.Then(x => Cache())
				.Then(x => Show())
				.Done (delegate(AdsResult obj) {
					if(callback != null) callback(this, new AdsSanboxResult ());
				}, delegate(Exception obj) {
					interactHandler = null;
					closeHandler = null;
					if(callback != null) callback(this, new AdsSanboxResult (obj));	
				});
		}

		#endregion
		#region Stream
		IPromise<AdsResult> Init (){
			var promise = new Promise<AdsResult> ();

			if(!_interstitialService.IsInitialized){
				EventHandler<AdsResult> initHandler = (object sender, AdsResult e) => {
					AdsDebug.Log("Interstitial INIT Handler ID: " + promise.Id);
					if (e.Error != null)
						promise.Reject (e.Error);
					else
						promise.Resolve (e);

					//Unsubscribe event
					_interstitialService.onInitComplete -= initHandlers[promise.Id];
					initHandlers.Remove (promise.Id);
				};

				initHandlers.Add (promise.Id, initHandler);
				_interstitialService.onInitComplete += initHandler;

				_interstitialService.InitAds ();
			}
			else{
				AdsDebug.Log ("Interstitial already INIT");
				promise.Resolve (new AdsSanboxResult());
			}

			return promise;
		}

		IPromise<AdsResult> Cache(){
			var promise = new Promise<AdsResult> ();

			AdsDebug.Log ("Interstitial CACHE Status: " + _interstitialService.IsInterstitialCached);
			if (isCaching){
				AdsDebug.Log ("Interstitial is CACHING");
				promise.Reject (new Exception ("Interstitial is being cached, please wait until caching is done"));
			}
			else if (!_interstitialService.IsInterstitialCached){
				isCaching = true;

				EventHandler<AdsResult> cacheHandler = (object sender, AdsResult e) => {
					AdsDebug.Log("Interstitial CACHE Handler ID: " + promise.Id);
					if(e.Error != null)
						promise.Reject (e.Error);
					else
						promise.Resolve (e);
					isCaching = false;

					//Unsubscribe event
					_interstitialService.onCachedInterstitial -= cacheHandlers[promise.Id];
					cacheHandlers.Remove (promise.Id);
				};

				cacheHandlers.Add (promise.Id, cacheHandler);
				_interstitialService.onCachedInterstitial += cacheHandler;

				_interstitialService.CacheInterstitial (null);
			}
			else{
				AdsDebug.Log ("Insterstitial already CACHE");
				promise.Resolve (new AdsSanboxResult());
			}

			return promise;
		}

		IPromise<AdsResult> Show(){
			var promise = new Promise<AdsResult> ();

			EventHandler<AdsResult> showHandler = (object sender, AdsResult e) => {
				AdsDebug.Log("Interstitial SHOW Handler ID: " + promise.Id);
				isCaching = false;
				if(e.Error != null)
					promise.Reject (e.Error);
				else
					promise.Resolve (e);

				//Unsubscribe event
				_interstitialService.onShownInterstitial -= showHandlers[promise.Id];
				showHandlers.Remove (promise.Id);
			};

			showHandlers.Add (promise.Id, showHandler);
			_interstitialService.onShownInterstitial += showHandler;

			_interstitialService.ShowInterstitial (null);

			return promise;
		}
		#endregion

		void ListenToInteract(Action onInteract){
			if(onInteract != null){
				interactHandler = onInteract;
			}
		}

		void ListenToClose(Action onClose){
			if(onClose != null){
				closeHandler = onClose;
			}
		}

		void OnInteract ()
		{
			AdsDebug.Log("Interstitial INTERACT Handler: " + interactHandler);
			if(interactHandler != null){
				interactHandler ();
			}
		}

		void OnClose ()
		{
			AdsDebug.Log("Interstitial CLOSE Handler: " + closeHandler);
			if(closeHandler != null){
				closeHandler ();
			}
		}
	}
}

