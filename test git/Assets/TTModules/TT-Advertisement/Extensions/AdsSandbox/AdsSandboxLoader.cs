using System;
using System.Collections.Generic;

namespace Touchten.Ads.Sandbox
{
	internal class AdsSandboxLoader :AdsLoader
	{
		public override AdsGameObject AdsGO {
			get {
				AdsGameObject go = ComponentFactory.GetComponent<AdsGameObject>();
				go.AdsBanner = new AdsSanboxBanner();
				go.AdsInterstitial = new AdsSandboxInterstitial();
				go.AdsVideo = new AdsSandboxVideo();
				go.AdsRewardedVideo = new AdsSandboxRewardedVideo();
				return go;
			}
		}
	}
}

