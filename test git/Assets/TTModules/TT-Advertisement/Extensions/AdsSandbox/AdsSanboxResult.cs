using System;

namespace Touchten.Ads.Sandbox
{
	public class AdsSanboxResult:AdsResult
	{
		public AdsSanboxResult ()
		{
			IsSuccess = true;	
		}

		public AdsSanboxResult (System.Exception error)
		{
			IsSuccess = false;
			Error = error;
		}

		public override string ToString ()
		{
			return string.Format ("[AdsSanboxResult: IsSuccess={0}, Error={1}]", IsSuccess, Error);
		}
	}
}

