using System;
using System.Collections.Generic;
using RSG;

namespace Touchten.Ads.AppODeal
{
	public class AODAdsVideo : AdsVideoBase
	{
		Dictionary<int, EventHandler<AdsResult>> initHandlers;
		Dictionary<int, EventHandler<AdsResult>> cacheHandlers;
		Dictionary<int, EventHandler<AdsResult>> showHandlers;
		Action finishHandler;
		Action closeHandler;

		public AODAdsVideo ()
		{
			_videoService = AODAdsService.Instance;
			initHandlers = new Dictionary<int, EventHandler<AdsResult>> ();
			cacheHandlers = new Dictionary<int, EventHandler<AdsResult>> ();
			showHandlers = new Dictionary<int, EventHandler<AdsResult>> ();

			_videoService.onFinishedVideo += OnFinish;
			_videoService.onClosedVideo += OnClose;
		}

		IPromise<AdsResult> Init (){
			var promise = new Promise<AdsResult> ();

			if (!_videoService.IsInitialized) {
				EventHandler<AdsResult> initHandler = (object sender, AdsResult e) => {
					AdsDebug.Log ("Video INIT Handler ID: " + promise.Id);
					if (e.Error != null)
						promise.Reject (e.Error);
					else
						promise.Resolve (e);

					//Unsubscribe event
					_videoService.onInitComplete -= initHandlers [promise.Id];
					initHandlers.Remove (promise.Id);
				};

				initHandlers.Add (promise.Id, initHandler);
				_videoService.onInitComplete += initHandler;

				_videoService.InitAds ();
			}
			else{
				AdsDebug.Log ("Video already INIT");
				promise.Resolve (new AODResult());
			}

			return promise;
		}

		IPromise<AdsResult> Cache(){
			var promise = new Promise<AdsResult> ();

			if (!_videoService.IsVideoCached) {
				EventHandler<AdsResult> cacheHandler = (object sender, AdsResult e) => {
					AdsDebug.Log ("Video CACHE Handler ID: " + promise.Id);
					if (e.Error != null)
						promise.Reject (e.Error);
					else
						promise.Resolve (e);

					//Unsubscribe event
					_videoService.onCachedVideo -= cacheHandlers [promise.Id];
					cacheHandlers.Remove (promise.Id);
				};

				cacheHandlers.Add (promise.Id, cacheHandler);
				_videoService.onCachedVideo += cacheHandler;

				_videoService.CacheVideo (null);
			}
			else{
				AdsDebug.Log ("Video already CACHE");
				promise.Resolve (new AODResult());
			}

			return promise;
		}

		IPromise<AdsResult> Show(){
			var promise = new Promise<AdsResult> ();

			EventHandler<AdsResult> showHandler = (object sender, AdsResult e) => {
				AdsDebug.Log("Video SHOW Handler ID: " + promise.Id);
				if(e.Error != null)
					promise.Reject (e.Error);
				else
					promise.Resolve (e);

				//Unsubscribe event
				_videoService.onShownVideo -= showHandlers[promise.Id];
				showHandlers.Remove (promise.Id);
			};

			showHandlers.Add (promise.Id, showHandler);
			_videoService.onShownVideo += showHandler;

			_videoService.ShowVideo (null);

			return promise;
		}

		void ListenToFinish(Action onFinish){
			if(onFinish != null){
				finishHandler = onFinish;
			}
		}

		void ListenToClose(Action onClose){
			if(onClose != null){
				closeHandler = onClose;
			}
		}

		void OnFinish ()
		{
			AdsDebug.Log("Video FINISH Handler: " + finishHandler);
			if(finishHandler != null){
				finishHandler ();
			}
		}

		void OnClose ()
		{
			AdsDebug.Log("Video CLOSE Handler: " + closeHandler);
			if(closeHandler != null){
				closeHandler ();
			}
		}
		
		#region implemented abstract members of AdsVideoBase

		public override bool IsVideoReady { 
			get {
				return _videoService.IsVideoCached;
			}
		}

		public override void InitVideo (EventHandler<AdsResult> callback)
		{
			Init ()
				.Done (delegate(AdsResult obj) {
					if(callback != null) callback(this, new AODResult ());
				}, delegate(Exception obj) {
					if(callback != null) callback(this, new AODResult (obj));	
				});
		}
		
		public override void CacheVideo (EventHandler<AdsResult> callback)
		{
			Init ()
				.Then(x => Cache())
				.Done (delegate(AdsResult obj) {
					if(callback != null) callback(this, new AODResult ());
				}, delegate(Exception obj) {
					if(callback != null) callback(this, new AODResult (obj));	
				});
		}
		
		public override void ShowVideo (Action OnFinished, Action OnClosed, EventHandler<AdsResult> callback)
		{	
			ListenToFinish (OnFinished);
			ListenToClose (OnClosed);
			Init ()
				.Then(x => Cache())
				.Then(x => Show())
				.Done (delegate(AdsResult obj) {
					if(callback != null) callback(this, new AODResult ());
				}, delegate(Exception obj) {
					finishHandler = null;
					closeHandler = null;
					if(callback != null) callback(this, new AODResult (obj));	
				});
		}

		#endregion
	}
}