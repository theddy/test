using System;
using RSG;
using System.Collections.Generic;

namespace Touchten.Ads.AppODeal
{
	public class AODAdsBanner : AdsBannerBase
	{
		Dictionary<int, EventHandler<AdsResult>> initHandlers;
		Dictionary<int, EventHandler<AdsResult>> cacheHandlers;
		Dictionary<int, EventHandler<AdsResult>> showHandlers;

		public AODAdsBanner ()
		{
			_bannerService = AODAdsService.Instance;
			initHandlers = new Dictionary<int, EventHandler<AdsResult>> ();
			cacheHandlers = new Dictionary<int, EventHandler<AdsResult>> ();
			showHandlers = new Dictionary<int, EventHandler<AdsResult>> ();
		}

		IPromise<AdsResult> Init(){
			var promise = new Promise<AdsResult> ();

			if (!_bannerService.IsInitialized) {
				EventHandler<AdsResult> initHandler = (object sender, AdsResult e) => {
					AdsDebug.Log ("Banner INIT Handler ID: " + promise.Id);
					if (e.Error != null)
						promise.Reject (e.Error);
					else
						promise.Resolve (e);

					//Unsubscribe event
					_bannerService.onInitComplete -= initHandlers [promise.Id];
					initHandlers.Remove (promise.Id);
				};

				initHandlers.Add (promise.Id, initHandler);
				_bannerService.onInitComplete += initHandler;

				_bannerService.InitAds ();
			}
			else{
				AdsDebug.Log ("Banner already INIT");
				promise.Resolve (new AODResult());
			}

			return promise;
		}
			
		IPromise<AdsResult> Cache(){
			var promise = new Promise<AdsResult> ();

			if (!_bannerService.IsBannerCached) {
				EventHandler<AdsResult> cacheHandler = (object sender, AdsResult e) => {
					AdsDebug.Log ("Banner CACHE Handler ID: " + promise.Id);
					if (e.Error != null)
						promise.Reject (e.Error);
					else
						promise.Resolve (e);

					//Unsubscribe event
					_bannerService.onCachedBanner -= cacheHandlers [promise.Id];
					cacheHandlers.Remove (promise.Id);

				};
				cacheHandlers.Add (promise.Id, cacheHandler);
				_bannerService.onCachedBanner += cacheHandler;

				_bannerService.CacheBanner (null);
			}
			else{
				AdsDebug.Log ("Banner already CACHE");
				promise.Resolve (new AODResult());
			}

			return promise;
		}

		IPromise<AdsResult> Show(){
			var promise = new Promise<AdsResult> ();

			EventHandler<AdsResult> showHandler = (object sender, AdsResult e) => {
				AdsDebug.Log("Banner SHOW Handler ID: " + promise.Id);
				if(e.Error != null)
					promise.Reject (e.Error);
				else
					promise.Resolve (e);

				//Unsubscribe event
				_bannerService.onShownBanner -= showHandlers[promise.Id];
				showHandlers.Remove (promise.Id);

			};
			showHandlers.Add (promise.Id, showHandler);
			_bannerService.onShownBanner += showHandler;

			_bannerService.ShowBanner (null);

			return promise;
		}

		#region implemented abstract members of AdsBannerBase

		public override bool IsBannerReady { 
			get {
				return _bannerService.IsBannerCached;
			}
		}

		public override void InitBanner (EventHandler<AdsResult> callback)
		{
			Init ()
				.Done (delegate(AdsResult obj) {
					if(callback != null) callback(this, new AODResult ());
				}, delegate(Exception obj) {
					if(callback != null) callback(this, new AODResult (obj));	
				});
		}

		public override void CacheBanner (EventHandler<AdsResult> callback)
		{
			Init ()
			#if UNITY_ANDROID
				.Then(x => Cache()) //In iOS, Cache is not working
			#endif
				.Done (delegate(AdsResult obj) {
					if(callback != null) callback(this, new AODResult ());
				}, delegate(Exception obj) {
					if(callback != null) callback(this, new AODResult (obj));	
				});
		}

		public override void ShowBanner (EventHandler<AdsResult> callback)
		{
			Init ()
			#if UNITY_ANDROID
				.Then(x => Cache()) //In iOS, Cache is not working
			#endif
				.Then(x => Show())
				.Done (delegate(AdsResult obj) {
					if(callback != null) callback(this, new AODResult ());
				}, delegate(Exception obj) {
					if(callback != null) callback(this, new AODResult (obj));	
				});
		}

		public override void DestroyBanner ()
		{
			_bannerService.DestroyBanner (string.Empty);
		}

		#endregion
	}
}