namespace Touchten.Ads.AppODeal
{
	public class AODResult : AdsResult
	{
		public AODResult ()
		{
			IsSuccess = true;	
		}
		
		public AODResult (System.Exception error)
		{
			IsSuccess = false;
			Error = error;
		}
		
		public override string ToString ()
		{
			return string.Format ("[AODResult: IsSuccess={0}, Error={1}]", IsSuccess, Error);
		}
	}
}