using System.Collections.Generic;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;

namespace Touchten.Ads.AppODeal
{
	internal class AODLoader : AdsLoader
	{
		#region implemented abstract members of AdsLoader
		public override AdsGameObject AdsGO {
			get {
				AdsGameObject aodGo = ComponentFactory.GetComponent<AdsGameObject> ();
				aodGo.AdsBanner = new AODAdsBanner();
				aodGo.AdsInterstitial = new AODAdsInterstitial();
				aodGo.AdsVideo = new AODAdsVideo();
				aodGo.AdsRewardedVideo = new AODAdsRewardedVideo();
				return aodGo;
			}
		}
		#endregion
	}
}