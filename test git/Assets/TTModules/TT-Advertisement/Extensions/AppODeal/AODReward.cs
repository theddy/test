namespace Touchten.Ads.AppODeal
{
	public class AODAdsReward : AdsReward
	{
		public AODAdsReward (string rewardName)
		{
			Quantity = 1;
			Item = rewardName;
		}
		
		public AODAdsReward (int quantity, string rewardName)
		{
			Quantity = quantity;
			Item = rewardName;
		}
		
		public override string ToString ()
		{
			return string.Format ("[AdsReward: Quantity={0}, Item={1}]", Quantity, Item);
		}
	}
}