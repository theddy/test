using System;

namespace Touchten.Ads
{
	public interface IAdsInterstitialService : IAdsService
	{
		bool IsInterstitialCached {get;}

		void CacheInterstitial(string adsId);
		void ShowInterstitial(string adsId);

		event EventHandler<AdsResult> onCachedInterstitial;
		event EventHandler<AdsResult> onShownInterstitial;
		event Action onClosedInterstitial;
		event Action onInteractInterstitial;
	}
}