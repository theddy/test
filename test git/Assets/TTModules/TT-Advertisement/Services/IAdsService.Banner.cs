using System;
using UnityEngine.EventSystems;

namespace Touchten.Ads
{
	public interface IAdsBannerService : IAdsService
	{
		bool IsBannerCached {get;}

		void CacheBanner(string adsId);
		void ShowBanner(string adsId);
		void DestroyBanner(string adsId);

		event EventHandler<AdsResult> onCachedBanner;
		event EventHandler<AdsResult> onShownBanner;
		event Action onInteractBanner;
	}
}