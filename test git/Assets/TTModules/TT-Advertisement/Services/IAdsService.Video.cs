using System;

namespace Touchten.Ads
{
	public interface IAdsVideoService : IAdsService
	{
		bool IsVideoCached {get;}

		void CacheVideo(string adsId);
		void ShowVideo(string adsId);

		event EventHandler<AdsResult> onCachedVideo;
		event EventHandler<AdsResult> onShownVideo;
		event Action onFinishedVideo;
		event Action onClosedVideo;
	}
}