using System;

namespace Touchten.Ads
{
	public interface IAdsRewardedVideoService : IAdsService
	{
		bool IsRewardedVideoCached {get;}

		void CacheRewardedVideo(string adsId);
		void ShowRewardedVideo(string adsId);

		event EventHandler<AdsResult> onCachedRewardedVideo;
		event EventHandler<AdsResult> onShownRewardedVideo;
		event Action<AdsReward> onFinishedRewardedVideo;
		event Action onClosedRewardedVideo;
	}
}