using System;

namespace Touchten.Ads
{
	public interface IAdsService
	{
		bool IsInitialized {get;}

		void InitAds();

		event EventHandler<AdsResult> onInitComplete;
	}
}