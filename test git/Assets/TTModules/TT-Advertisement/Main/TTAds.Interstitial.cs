using System;

namespace Touchten.Ads
{
	public sealed partial class TTAds
	{
		// Get Ads Interstitial implementation from the game object
		internal static IAdsInterstitial IAdsInterstitialImpl {
			get {
				if (ads == null)
					throw new NullReferenceException ("Did you call TTAds.Init()?");
				if (ads.AdsInterstitial == null) 
					throw new NotImplementedException ("Interstitial not implemented yet");
				return ads.AdsInterstitial;
			}
		}

		public static class Interstitial
		{
			public static bool IsInterstitialReady {
				get {
					return IAdsInterstitialImpl.IsInterstitialReady;
				}
			}

			public static void Cache (EventHandler<AdsResult> callback = null)
			{
				IAdsInterstitialImpl.CacheInterstitial (callback);
			}
			
			public static void Show (Action OnInteract, Action OnClosed, EventHandler<AdsResult> callback = null)
			{
				IAdsInterstitialImpl.ShowInterstitial (OnInteract, OnClosed, callback);
			}

		}
	}
}