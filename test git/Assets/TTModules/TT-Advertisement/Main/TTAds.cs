using UnityEngine;
using System;
using System.Reflection;
using System.Collections.Generic;
using RSG;

namespace Touchten.Ads
{
	public sealed partial class TTAds : ScriptableObject
	{
		// Game object containing instance of Ads implementation
		private static AdsGameObject ads;
		// Collects the status of initialization of all ad types
		private static Dictionary<string, bool> moduleInitList;

		public static Action<bool> InitAdsComplete;

		public static bool Init ()
		{
			AdsDebug.Log ("init all type of Ads");
			AdsProvider loadedProvider;
			moduleInitList = new Dictionary<string, bool> ();

			#if UNITY_EDITOR
			Type aod_module = Type.GetType("Touchten.Ads.Sandbox.AdsSandboxLoader");
			loadedProvider = AdsProvider.EditorSandbox;
			#else

			Type aod_module = Type.GetType("Touchten.Ads.AppODeal.AODLoader");
			loadedProvider = AdsProvider.AppODeal;
			if (aod_module == null) {
				aod_module = Type.GetType ("Touchten.Ads.Vmax.VmaxLoader");
				loadedProvider = AdsProvider.Vmax;
			}
			#endif
			if (aod_module != null) {
				ads = getAdsModules(aod_module);
			}

			if (ads != null && aod_module != null) 
			{
				Promise.Sequence (
					() => InitAdUnit (moduleInitList, AdsType.Banner, loadedProvider),
					() => InitAdUnit (moduleInitList, AdsType.Interstitial, loadedProvider),
					() => InitAdUnit (moduleInitList, AdsType.Video, loadedProvider),
					() => InitAdUnit (moduleInitList, AdsType.RewardedVideo, loadedProvider)
				).Done (delegate() {
					if(InitAdsComplete != null){
						bool allAdsInit = true;
						foreach(KeyValuePair<string, bool> initStatus in moduleInitList){
							Debug.Log(initStatus);
							if(!initStatus.Value){
								allAdsInit = false;
								break;
							}
						}
						InitAdsComplete(allAdsInit);
					}
				});
			} else {
				throw new NotSupportedException ("No plugin Ads installed");
			}

			return true;
		}

		static IPromise InitAdUnit(Dictionary<string, bool> moduleInitList, AdsType adsType, AdsProvider provider){
			var promise = new Promise ();

			switch(adsType){
			case AdsType.Banner:
				ads.AdsBanner.InitBanner (delegate(object sender, AdsResult e) {
					moduleInitList.Add ("banner_" + provider, e.IsSuccess);
					promise.Resolve ();
				});
				break;
			case AdsType.Interstitial:
				ads.AdsInterstitial.InitInterstitial (delegate(object sender, AdsResult e) {
					moduleInitList.Add ("interstitial_" + provider, e.IsSuccess);
					promise.Resolve ();
				});
				break;
			case AdsType.RewardedVideo:
				ads.AdsRewardedVideo.InitRewardedVideo (delegate(object sender, AdsResult e) {
					moduleInitList.Add ("rewardedvideo_" + provider, e.IsSuccess);
					promise.Resolve ();
				});
				break;
			case AdsType.Video:
				ads.AdsVideo.InitVideo (delegate(object sender, AdsResult e) {
					moduleInitList.Add ("video_" + provider, e.IsSuccess);
					promise.Resolve ();
				});
				break;
			}

			return promise;
		}

		private static AdsGameObject getAdsModules(Type modulesPackage){
			MethodInfo getComponent = typeof(ComponentFactory).GetMethod ("GetComponent").MakeGenericMethod (modulesPackage);
			object adsGameObject = getComponent.Invoke (null, null);
			return  adsGameObject.GetType ().GetProperty ("AdsGO").GetValue (adsGameObject, null) as AdsGameObject;
		}
	}
}