using UnityEngine;
using System;

namespace Touchten.Ads
{
	public sealed partial class TTAds : ScriptableObject
	{
		// Get Ads Banner implementation from the game object
		internal static IAdsBanner IAdsBannerImpl {
			get {
				if (ads == null)
					throw new NullReferenceException ("Did you call TTAds.Init()?");
				if (ads.AdsBanner == null) 
					throw new NotImplementedException ("Banner not implemented yet");
				return ads.AdsBanner;
			}
		}

		public static class Banner
		{
			public static bool IsBannerReady {
				get {
					return IAdsBannerImpl.IsBannerReady;
				}
			}

			public static void Destroy ()
			{
				IAdsBannerImpl.DestroyBanner ();
			}
			
			public static void Show (EventHandler<AdsResult> callback = null)
			{
				IAdsBannerImpl.ShowBanner (callback);
			}

			public static void Cache(EventHandler<AdsResult> callback = null)
			{
				IAdsBannerImpl.CacheBanner (callback);
			}
		}
	}
}