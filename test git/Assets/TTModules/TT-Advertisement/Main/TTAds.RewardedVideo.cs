using UnityEngine;
using System;

namespace Touchten.Ads
{
	public sealed partial class TTAds : ScriptableObject
	{
		// Get Ads Video implementation from the game object
		internal static IAdsRewardedVideo IAdsRewardedVideoImpl {
			get {
				if (ads == null)
					throw new NullReferenceException ("Did you call TTAds.Init()?");
				if (ads.AdsRewardedVideo == null) 
					throw new NotImplementedException ("RewardedVideo not implemented yet");
				return ads.AdsRewardedVideo;
			}
		}

		public static class RewardedVideo
		{
			public static bool IsRewardedVideoReady {
				get {
					return IAdsRewardedVideoImpl.IsRewardedVideoReady;
				}
			}

			public static void Cache (EventHandler<AdsResult> callback = null)
			{
				IAdsRewardedVideoImpl.CacheRewardedVideo (callback);
			}

			public static void Show (Action<AdsReward> onVideoComplete, Action onVideoClosed, EventHandler<AdsResult> callback = null)
			{
				IAdsRewardedVideoImpl.ShowRewardedVideo (onVideoComplete, onVideoClosed, callback);
			}
		}
	}
}