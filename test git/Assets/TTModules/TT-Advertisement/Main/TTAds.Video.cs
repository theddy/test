using System;

namespace Touchten.Ads
{
	public sealed partial class TTAds
	{
		// Get Ads Video implementation from the game object
		internal static IAdsVideo IAdsVideoImpl {
			get {
				if (ads == null)
					throw new NullReferenceException ("Did you call TTAds.Init()?");
				if (ads.AdsVideo == null) 
					throw new NotImplementedException ("Video not implemented yet");
				return ads.AdsVideo;
			}
		}

		public static class Video
		{
			public static bool IsVideoReady {
				get {
					return IAdsVideoImpl.IsVideoReady;
				}
			}

			public static void Cache (EventHandler<AdsResult> callback = null)
			{
				IAdsVideoImpl.CacheVideo (callback);
			}

			public static void Show (Action OnFinished, Action OnClosed, EventHandler<AdsResult> callback = null)
			{
				IAdsVideoImpl.ShowVideo (OnFinished, OnClosed, callback);
			}
		}
	}
}