using System;

namespace Touchten.Ads
{
	public abstract class AdsVideoBase : IAdsVideo
	{
		protected IAdsVideoService _videoService;
		public abstract bool IsVideoReady { get; }

		#region IAdsVideo implementation
		public abstract void InitVideo (EventHandler<AdsResult> callback);
		public abstract void CacheVideo (EventHandler<AdsResult> callback);
		public abstract void ShowVideo (Action OnFinished, Action OnClosed, EventHandler<AdsResult> callback);
		#endregion
	}
}