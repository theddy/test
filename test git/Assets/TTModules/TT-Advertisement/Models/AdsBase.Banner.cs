using System;

namespace Touchten.Ads
{
	public abstract class AdsBannerBase : IAdsBanner
	{
		public abstract bool IsBannerReady { get; }
		protected IAdsBannerService _bannerService;

		#region IAdsBanner implementation
		public abstract void InitBanner (EventHandler<AdsResult> callback);
		public abstract void CacheBanner (EventHandler<AdsResult> callback);
		public abstract void ShowBanner (EventHandler<AdsResult> callback);
		public abstract void DestroyBanner ();
		#endregion
	}
}