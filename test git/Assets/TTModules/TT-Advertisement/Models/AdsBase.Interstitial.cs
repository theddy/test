using System;

namespace Touchten.Ads
{
	public abstract class AdsInterstitialBase : IAdsInterstitial
	{
		protected IAdsInterstitialService _interstitialService;
		public abstract bool IsInterstitialReady { get; }

		#region IAdsInterstitial implementation
		public abstract void InitInterstitial (EventHandler<AdsResult> callback);
		public abstract void CacheInterstitial (EventHandler<AdsResult> callback);
		public abstract void ShowInterstitial (Action OnInteract, Action OnClosed, EventHandler<AdsResult> callback);
		#endregion
	}
}