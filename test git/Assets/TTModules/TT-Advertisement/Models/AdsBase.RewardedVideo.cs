using System;

namespace Touchten.Ads
{
	public abstract class AdsRewardedVideoBase : IAdsRewardedVideo
	{
		protected IAdsRewardedVideoService _rewardedVideoService;
		public abstract bool IsRewardedVideoReady { get; }

		#region IAdsVideo implementation
		public abstract void InitRewardedVideo (EventHandler<AdsResult> callback);
		public abstract void CacheRewardedVideo (EventHandler<AdsResult> callback);
		public abstract void ShowRewardedVideo (Action<AdsReward> OnFinished, Action OnClosed, EventHandler<AdsResult> callback);
		#endregion
	}
}