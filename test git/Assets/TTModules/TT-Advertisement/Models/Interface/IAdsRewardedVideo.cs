using UnityEngine;
using System;

namespace Touchten.Ads
{
	public delegate void EventRewardedVideoHandler ();

	public interface IAdsRewardedVideo
	{
		bool IsRewardedVideoReady { get; }

		void InitRewardedVideo (EventHandler<AdsResult> callback);
		void CacheRewardedVideo (EventHandler<AdsResult> callback);
		void ShowRewardedVideo (Action<AdsReward> OnFinished, Action OnClosed, EventHandler<AdsResult> callback = null);
	}
}