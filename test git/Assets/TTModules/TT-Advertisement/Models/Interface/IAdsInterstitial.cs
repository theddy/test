using System;

namespace Touchten.Ads
{
	public interface IAdsInterstitial
	{
		bool IsInterstitialReady { get; }

		void InitInterstitial(EventHandler<AdsResult> callback = null);
		void CacheInterstitial(EventHandler<AdsResult> callback = null);
		void ShowInterstitial(Action OnInteract, Action OnClosed, EventHandler<AdsResult> callback = null);
	}
}