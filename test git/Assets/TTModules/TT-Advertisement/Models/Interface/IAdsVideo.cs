using System;

namespace Touchten.Ads
{
	public interface IAdsVideo
	{
		bool IsVideoReady { get; }

		void InitVideo (EventHandler<AdsResult> callback = null);
		void CacheVideo (EventHandler<AdsResult> callback = null);
		void ShowVideo (Action OnFinished, Action OnClosed, EventHandler<AdsResult> callback = null);
	}
}