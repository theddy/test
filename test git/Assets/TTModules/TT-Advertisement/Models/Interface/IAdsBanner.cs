using System;

namespace Touchten.Ads
{
	public interface IAdsBanner
	{
		bool IsBannerReady { get; }

		void InitBanner(EventHandler<AdsResult> callback = null);
		void CacheBanner (EventHandler<AdsResult> callback = null);
		void ShowBanner(EventHandler<AdsResult> callback = null);
		void DestroyBanner();
	}
}