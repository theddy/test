namespace Touchten.Ads
{
	public abstract class AdsReward
	{
		public int Quantity { get; set; }
		public string Item { get; set; }
	}
}