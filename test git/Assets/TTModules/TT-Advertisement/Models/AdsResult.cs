using System;

namespace Touchten.Ads
{
	public abstract class AdsResult : EventArgs
	{
		public bool IsSuccess { get; set; }
		public Exception Error { get; set; }
	}
}