using UnityEngine;
using System.Collections;

namespace Touchten.Ads
{
	internal abstract class AdsLoader : MonoBehaviour
	{
		public abstract AdsGameObject AdsGO { get; }

		public void Start ()
		{
			MonoBehaviour.Destroy (this);
		}
	}
}