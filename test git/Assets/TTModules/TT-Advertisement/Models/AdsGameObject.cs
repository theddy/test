using UnityEngine;
using System.Collections.Generic;

namespace Touchten.Ads
{
	internal class AdsGameObject : MonoBehaviour
	{
		public IAdsBanner AdsBanner { get; set; }
		public IAdsInterstitial AdsInterstitial { get; set; }
		public IAdsVideo AdsVideo { get; set; }
		public IAdsRewardedVideo AdsRewardedVideo { get; set; }

		public bool Initialized { get; private set; }

		public void Awake ()
		{
			MonoBehaviour.DontDestroyOnLoad (this);
			this.OnAwake ();
		}
		// use this to call the rest of the Awake function
		protected virtual void OnAwake ()
		{

		}
	}
}