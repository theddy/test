using UnityEngine;
using System.Collections;
using Touchten.Ads;
namespace Touchten.TestScene{
[RequireComponent(typeof(AdsMenuView))]
public class AdsMenuController : MonoBehaviour {



	AdsMenuView menuView;

	void Start()
	{
		menuView = GetComponent<AdsMenuView>();
	}

	//Banner
	public void ShowBanner()
	{
		TTAds.Banner.Show(delegate(object sender, AdsResult e) {
			
		});
	}

	public void CloseBanner()
	{
		TTAds.Banner.Destroy();
	}


	//intestitial
	public void CacheInterstitial()
	{
		menuView.CachingIntersitial();
		TTAds.Interstitial.Cache(delegate(object sender, AdsResult e) {
			menuView.OnInterstitialCached(e.IsSuccess);
		});
	}

	public void ShowInterstitial()
	{
		TTAds.Interstitial.Show(delegate() {			
		},delegate() {
				#if KADOSAKU_TEST
			UniRx.MainThreadDispatcher.Post(delegate(object obj) {
				menuView.OnInterstitialCached(false);	
			},null);
				#endif
		},delegate(object sender, AdsResult e) {
			
		});
	}



	//video
	public void CacheVideo()
	{
		menuView.CachingVideo();
		TTAds.Video.Cache(delegate(object sender, AdsResult e) {
			menuView.OnVideoCached(e.IsSuccess);
		});
	}

	public void ShowVideo()
	{
		TTAds.Video.Show(delegate() {
			
		},delegate() {
				#if KADOSAKU_TEST
			UniRx.MainThreadDispatcher.Post(delegate(object obj) {
				menuView.OnVideoCached(false);	
			},null);
				#endif
		},delegate(object sender, AdsResult e) {
			
		});
	}


	//rewarded video

	public void CacheRewardedVideo()
	{
		menuView.CachingRewardedVideo();
		TTAds.RewardedVideo.Cache(delegate(object sender, AdsResult e) {
			menuView.OnRewardedVideoCached(e.IsSuccess);
		});
	}

	public void ShowRewardedVideo()
	{
		TTAds.RewardedVideo.Show(delegate(AdsReward obj) {
			
		},delegate() {
				#if KADOSAKU_TEST
			UniRx.MainThreadDispatcher.Post(delegate(object obj) {
				menuView.OnRewardedVideoCached(false);	
			},null);

				#endif
		},delegate(object sender, AdsResult e) {
			
		});
	}
}
}