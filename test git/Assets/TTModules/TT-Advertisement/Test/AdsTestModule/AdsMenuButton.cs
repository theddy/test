using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Touchten.TestScene
{
	public class AdsMenuButton : MonoBehaviour
	{

		StateIndicator indicator;
		// Use this for initialization
		void Start ()
		{
			indicator = GetComponent<StateIndicator>();
			TestSceneEvent.OnAdsInitCompleteE+= TestSceneEvent_OnAdsInitCompleteE;
			GetComponent<Button>().onClick.AddListener(delegate() {
				MenuEvent.OpenAdsMenu();	
			});
		}
		void OnDestroy()
		{
			TestSceneEvent.OnAdsInitCompleteE-= TestSceneEvent_OnAdsInitCompleteE;

		}
		void TestSceneEvent_OnAdsInitCompleteE (bool success)
		{
			indicator.SetStatus(success);
		}
	

	}
}