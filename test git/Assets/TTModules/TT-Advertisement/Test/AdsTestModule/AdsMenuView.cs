using UnityEngine;
using System.Collections;
namespace Touchten.TestScene{
public class AdsMenuView : MenuBase
{
	void Awake()
	{
		MenuEvent.OpenAdsMenuE += MenuEvent_OpenAdsMenuE;
	}

	void MenuEvent_OpenAdsMenuE ()
	{
		ActivateMenu();
	}

	void OnDestroy()
	{
		MenuEvent.OpenAdsMenuE -= MenuEvent_OpenAdsMenuE;
	}


	public StateIndicator InterstitialCacheIndicator;
	public StateIndicator VideoCacheIndicator;
	public StateIndicator RewardedVideoCacheIndicator;


	public void CachingIntersitial ()
	{
		InterstitialCacheIndicator.SetStatus (IndicatorStatus.Idle);
	}

	public void CachingVideo ()
	{
		VideoCacheIndicator.SetStatus (IndicatorStatus.Idle);
	}

	public void CachingRewardedVideo ()
	{
		RewardedVideoCacheIndicator.SetStatus (IndicatorStatus.Idle);
	}


	public void OnInterstitialCached (bool status)
	{
		InterstitialCacheIndicator.SetStatus (status);
	}


	public void OnVideoCached (bool status)
	{
		VideoCacheIndicator.SetStatus (status);

	}

	public void OnRewardedVideoCached (bool status)
	{
		RewardedVideoCacheIndicator.SetStatus (status);

	}		
}
}