using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Touchten.Ads;
namespace Touchten.TestScene
{
	public class AdsTestModule : ITestModule
	{
		static AdsTestModule _instance;

		static AdsTestModule Instance {
			get {
				if (_instance == null) {
					_instance = new AdsTestModule ();
				}
				return _instance;
			}
		}
		public static void Init ()
		{
			TTAds.InitAdsComplete = delegate(bool obj) {
				TestSceneEvent.OnAdsInitComplete (obj);	
			};

			Instance.MenuButtonResourcePath = new List<string> (){ "AdsMenuButton" };
			Instance.ModuleMenuResourcePath = new List<string> (){ "AdsTestMenu" };


		}
		public static IList<string> GetMenuButtonResourcePath ()
		{
			return Instance.MenuButtonResourcePath;
		}

		public static IList<string> GetModuleMenuResourcePath ()
		{
			return Instance.ModuleMenuResourcePath;
		}

		public IList<string>  MenuButtonResourcePath {
			get ;
			private set;
		}

		public IList<string>  ModuleMenuResourcePath {
			get;
			private set;
		}
	}
}