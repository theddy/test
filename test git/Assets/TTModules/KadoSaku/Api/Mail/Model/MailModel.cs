using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace KadoSaku.Api
{
	internal class MailModel : IMailModel
	{
		[JsonConstructor]
		internal MailModel (string id,
		                    string title,
		                    string customData,
		                    string status,
		                    string description,
		                    DateTime sendDate,
		                    DateTime expiration,
		                    string type) 
		{
			this.Id = id;
			this.Title = title;
			this.CustomData = customData;
			this.Status = status;
			this.Description = description;
			this.SendDate = sendDate;
			this.Expiration = expiration;
			this.Type = type;
		}

		[JsonProperty("_id")]
		public string Id { get; private set; }
		public string Title { get; private set; }
		public string CustomData { get; private set; }
		public string Status { get; private set; }
		public string Description { get; private set; }
		public DateTime SendDate { get; private set; }
		public DateTime Expiration { get; private set; }
		public string Type { get; private set; }

		public override string ToString ()
		{
			return string.Format ("[MailModel: Id={0}, Title={1}, CustomData={2}, Status={3}, Description={4}, SendDate={5}, Expiration={6}, Type={7}]", Id, Title, CustomData, Status, Description, SendDate, Expiration, Type);
		}
	}
}

