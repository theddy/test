using System;

namespace KadoSaku.Api
{
	public interface IMailModel
	{
		string Id { get; }
		string Title { get; }
		string CustomData { get; }
		string Status { get; }
		string Description { get; }
		DateTime SendDate { get; }
		DateTime Expiration { get; }
		string Type { get; }
	}
}

