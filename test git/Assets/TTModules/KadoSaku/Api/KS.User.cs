using System;
using KadoSaku.Core.Communication;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	public partial class KS
	{
		public static class User
		{
			public static void UpdatePlayerProfile (KadoSakuDelegate<IPlayerProfileResult> callback) 
			{
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.UpdatePlayerProfile);
				KS.CallApi(param, callback);
			}
		}
	}
}

