using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	internal class PlayerModel : IPlayerProfileModel
	{
		internal PlayerModel (string responseString) : this(JToken.Parse(responseString)) {}

		internal PlayerModel (JToken response) {
			Id = KSUtility.GetValue<string>(response, "_id", string.Empty);
			Name = KSUtility.GetValue<string>(response, "name", string.Empty);
			Email = KSUtility.GetValue<string>(response, "email", string.Empty);
			PhotoURL = KSUtility.GetValue<string>(response, "photoURL", string.Empty);
			Credits = KSUtility.GetValue<int>(response, "credits", 0);
		} 

		public string Id 	{ get; private set; }
		public string Name 		{ get; private set; }
		public string Email 	{ get; private set; }
		public string PhotoURL 	{ get; private set; }
		public int Credits 		{ get; private set; }

		public override string ToString ()
		{
			return string.Format ("[PlayerModel: ObjectId={0}, Name={1}, Email={2}, PhotoURL={3}, Credits={4}]", Id, Name, Email, PhotoURL, Credits);
		}
	}
}

