using System;

namespace KadoSaku.Api
{
	public interface IPlayerProfileModel : IPlayerModel
	{
		string Email		{ get; }
		int Credits 		{ get; }
	}
}

