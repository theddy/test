using System;

namespace KadoSaku.Api
{
	public interface IPlayerModel
	{
		string Id 	{ get; }
		string Name 		{ get; }
		string PhotoURL		{ get; }
	}
}

