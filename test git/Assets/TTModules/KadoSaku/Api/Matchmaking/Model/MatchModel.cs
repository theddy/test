using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using KadoSaku.Utility;
using System.Text;

namespace KadoSaku.Api
{
	internal class MatchModel : IMatchModel
	{
		internal MatchModel (JToken response) {
			Id = KSUtility.GetValue<string>(response, "_id", string.Empty);
			LeaderboardId = KSUtility.GetValue<string>(response["leaderboard"], "_id", string.Empty);
			Attacker = KSUtility.GetValue<FriendModel>(response, "attacker", null);
			Defender = KSUtility.GetValue<FriendModel>(response, "defender", null);
			CustomData = GetCustomData(response["customData"]);
			Status = GetStatus(response);
			AttackerScore = KSUtility.GetValue<float>(response, "attackerScore", 0);
			DefenderScore = KSUtility.GetValue<float>(response, "defenderScore", 0);
			Winner = GetWinner(response);
		}

		public string Id 				{ get; private set; }
		public string LeaderboardId 	{ get; private set; }
		public IFriendModel Attacker 	{ get; private set; }
		public IFriendModel Defender 	{ get; private set; }
		public IList<string> CustomData { get; private set; }
		public MatchStatus Status 		{ get; private set; }
		public float AttackerScore 		{ get; private set; }
		public float DefenderScore 		{ get; private set; }
		public MatchWinner Winner 		{ get; private set; }

		const bool ignoreCase = true;

		static MatchStatus GetStatus (JToken response) {
			return (MatchStatus) Enum.Parse(typeof(MatchStatus),
				KSUtility.GetValue<string>(response, "status", MatchStatus.Invalid.ToString()),
				ignoreCase);
		}

		static IList<string> GetCustomData (JToken response) {
			IList<string> customDataList = new List<string>();
			if (response == null) return customDataList;

			foreach (JToken item in response) {
				
				string data = item.ToObject<string>();
				customDataList.Add(KSUtility.DecodeCustomData(data));
			}
			return customDataList;
		}

		static MatchWinner GetWinner (JToken response) {
			return (MatchWinner) Enum.Parse(typeof(MatchWinner),
				KSUtility.GetValue<string>(response, "winner", MatchWinner.Invalid.ToString()),
				ignoreCase);
		}

		public override string ToString ()
		{	
			StringBuilder sb = new StringBuilder();
			foreach (var data in CustomData) {
				sb.Append(data);
			}
			string customDataListString = sb.ToString();
			return string.Format ("[MatchModel: Id={0}, LeaderboardId={1}, Attacker={2}, Defender={3}, CustomData={4}, Status={5}, AttackerScore={6}, DefenderScore={7}, Winner={8}]", Id, LeaderboardId, Attacker, Defender, customDataListString, Status, AttackerScore, DefenderScore, Winner);
		}
	}
}

