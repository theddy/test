using System;

namespace KadoSaku.Api
{
	public struct MatchSubmitModel
	{
		public string MatchId { get; private set; }
		public int Score { get; private set; }
		public string CustomData { get; private set; }

		public MatchSubmitModel (string matchId,
								 int score,
								 string customData) 
		{
			this.MatchId = matchId;
			this.Score = score;
			this.CustomData = customData;
		}
	}
}

