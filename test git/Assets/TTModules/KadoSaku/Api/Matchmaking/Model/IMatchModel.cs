using System.Collections.Generic;

namespace KadoSaku.Api
{
	public interface IMatchModel
	{
		string Id { get; }
		string LeaderboardId { get; }
		IFriendModel Attacker { get; }
		IFriendModel Defender { get; }
		IList<string> CustomData { get; }
		MatchStatus Status { get; }
		float AttackerScore { get; }
		float DefenderScore { get; }
		MatchWinner Winner { get; }
	}
}

