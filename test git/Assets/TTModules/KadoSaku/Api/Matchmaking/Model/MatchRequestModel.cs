using System;

namespace KadoSaku.Api
{
	public struct MatchRequestModel
	{
		public string DefenderId { get; private set; }
		public string LeaderboardId { get; private set; }
		public string CustomData { get; private set; }

		public MatchRequestModel (string defenderId,
								  string leaderboardId,
								  string customData)
		{
			this.DefenderId = defenderId;
			this.LeaderboardId = leaderboardId;
			this.CustomData = customData;
		}
	}
}

