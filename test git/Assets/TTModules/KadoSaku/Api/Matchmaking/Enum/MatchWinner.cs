using System;

namespace KadoSaku.Api
{
	public enum MatchWinner
	{
		Invalid,
		Attacker,
		Defender,
		Tie
	}
}

