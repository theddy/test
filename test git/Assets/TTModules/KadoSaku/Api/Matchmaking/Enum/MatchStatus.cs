using System;

namespace KadoSaku.Api
{
	public enum MatchStatus
	{
		Invalid,
		Created,
		Pending,
		Ended,
		Cancelled
	}
}

