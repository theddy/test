using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	internal class AchievementModel : IAchievementModel
	{
		internal AchievementModel (JToken token)
		{
			Id = GetId(token);
			Name = GetName(token);
			Description = GetDescription(token);
			AchievementActionList = GetAchievementActions(token);
		}

		public string Id			{ get; private set; }
		public string Name 			{ get; private set; }
		public string Description 	{ get; private set; }
		public IList<IAchievementActionModel> AchievementActionList { get; private set; }
	
		static string GetId (JToken token) {
			return KSUtility.GetValue<string>(token["achievement"], "_id", string.Empty);
		}

		static string GetName (JToken token) {
			return KSUtility.GetValue<string>(token["achievement"], "name", string.Empty); 
		}	

		static string GetDescription (JToken token) {
			return KSUtility.GetValue<string>(token["achievement"], "description", string.Empty);	
		}

		static IList<IAchievementActionModel> GetAchievementActions (JToken token) {
			IList<IAchievementActionModel> actions = new List<IAchievementActionModel>();
			JToken achievementActionToken = token["achievementActions"];
			foreach (JToken item in achievementActionToken) {
				IAchievementActionModel action = new AchievementActionModel(item);
				actions.Add(action);
			}
			return actions;
		}

		public override string ToString ()
		{
			return string.Format ("[AchievementModel: Name={0}, Description={1}, AchievementActionList={2}]", 
				Name, 
				Description, 
				AchievementActionList);
		}
	}
}

