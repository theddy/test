using System;
using Newtonsoft.Json.Linq;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	internal class AchievementActionModel : IAchievementActionModel
	{
		const bool ignoreCase = true;

		internal AchievementActionModel (JToken token) {
			Limit = KSUtility.GetValue<int>(token, "limit", 0);
			Language = KSUtility.GetValue<string>(token, "language", string.Empty);
			Message = KSUtility.GetValue<string>(token, "message", string.Empty);
			Type = (AchievementActionType) Enum.Parse(typeof(AchievementActionType), 
				KSUtility.GetValue<string>(token, "type", AchievementActionType.Point.ToString()),
				ignoreCase);

			ReceivedReward = null;
			ReceivedPoint = null;

			switch (Type) {
			case AchievementActionType.Point:
				ReceivedPoint = new AchievementActionPoint(token);
				break;
			case AchievementActionType.Reward:
				ReceivedReward = new AchievementActionReward(token);
				break;
			}
		}

		public int Limit					{ get; private set; }
		public string Language				{ get; private set; }
		public string Message				{ get; private set; }
		public AchievementActionType Type 	{ get; private set; }	
		public IAchievementActionReward ReceivedReward { get; private set; }
		public IAchievementActionPoint ReceivedPoint { get; private set; }
	}

	internal class AchievementActionPoint : IAchievementActionPoint 
	{
		internal AchievementActionPoint (JToken token) {
			Point = KSUtility.GetValue<int>(token, "localValue", 0);
		}
		
		public int Point { get; private set; }	
	}

	internal class AchievementActionReward : IAchievementActionReward 
	{
		internal AchievementActionReward (JToken token) {
			RewardTransactionId = KSUtility.GetValue<string>(token, "rewardTransaction", string.Empty);
			PhotoURL = KSUtility.GetValue<string>(token, "photoURL", string.Empty);
			Name = KSUtility.GetValue<string>(token, "name", string.Empty);
		}	

		public string RewardTransactionId { get; private set; }
		public string PhotoURL { get; private set; }
		public string Name { get; private set; }
	}

}

