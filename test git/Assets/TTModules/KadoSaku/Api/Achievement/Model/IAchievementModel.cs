using System;
using System.Collections.Generic;

namespace KadoSaku.Api
{
	public interface IAchievementModel
	{
		string Id			{ get; }
		string Name 		{ get; }
		string Description 	{ get; }
		IList<IAchievementActionModel> AchievementActionList { get; }		
	}
}