using System;

namespace KadoSaku.Api
{
	public interface IAchievementActionModel
	{
		int Limit					{ get; }
		string Language				{ get; }
		string Message				{ get; }
		AchievementActionType Type 	{ get; }	
		IAchievementActionReward ReceivedReward { get; }
		IAchievementActionPoint ReceivedPoint { get; }
	}

	public interface IAchievementActionPoint 
	{
		int Point { get; }
	}

	public interface IAchievementActionReward
	{
		string RewardTransactionId { get; }
		string PhotoURL { get; }
		string Name { get; }
	}
}

