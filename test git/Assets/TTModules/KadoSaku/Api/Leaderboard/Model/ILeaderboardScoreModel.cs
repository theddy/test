using System;

namespace KadoSaku.Api
{
	public interface ILeaderboardScoreModel
	{
		IPlayerModel Player { get; }
		int Score			{ get; }
	}
}

