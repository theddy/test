using System;
using Newtonsoft.Json.Linq;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	internal class LeaderboardScoreModel : ILeaderboardScoreModel
	{
		internal LeaderboardScoreModel (JToken token)  {
			Player = token["player"] != null ? new PlayerModel(token["player"]) : null;
			Score = KSUtility.GetValue<int>(token, "value", 0);
		}

		public IPlayerModel Player 	{ get; private set; }
		public int Score 			{ get; private set; }

		public override string ToString ()
		{
			return string.Format ("[LeaderboardScoreModel: Player={0}, Score={1}]", Player, Score);
		}
	}
}

