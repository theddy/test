using System;

namespace KadoSaku.Api
{
	public enum LeaderboardTimeFrame
	{
		AllTime = 0,
		Monthly,
		Weekly,
		Daily
	}
}

