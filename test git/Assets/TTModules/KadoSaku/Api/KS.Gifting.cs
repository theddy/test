using System;
using System.Linq;
using KadoSaku.Core.Communication;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	public partial class KS
	{
		public static class Gifting
		{
			const string GIFT_ID_KEY = "giftId";
			const string FRIEND_IDS_KEY = "friendIds";
			const string FRIEND_TYPE_KEY = "friendType";
			const string GIFT_TYPE_KEY = "type";
			const string ITEM_ID_KEY = "itemId";
			const string QUANTITY_KEY = "quantity";

			public static void GetGiftNotification (KadoSakuDelegate<IGiftNotificationResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.GetGiftNotification);
				KS.CallApi(param, callback);
			}

			public static void SendGift (GiftRequestModel giftRequest, KadoSakuDelegate<IActionResult> callback) {
				CreateItemRequest(giftRequest, callback);
			}

			public static void AskGift (GiftRequestModel giftRequest, KadoSakuDelegate<IActionResult> callback) {
				CreateItemRequest(giftRequest, callback);
			}

			public static void AcceptGift (string giftId, KadoSakuDelegate<IActionResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.AcceptGift);
				param.AddQuery(GIFT_ID_KEY, giftId);
				KS.CallApi(param, callback);
			}

			public static void DenyGift (string giftId, KadoSakuDelegate<IActionResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.DenyGift);
				param.AddQuery(GIFT_ID_KEY, giftId);
				KS.CallApi(param, callback);
			}

			public static void ArchiveGift (string giftId, KadoSakuDelegate<IActionResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.ArchiveGift);
				param.AddQuery(GIFT_ID_KEY, giftId);
				KS.CallApi(param, callback);
			}
				
			/////////////////////

			static void CreateItemRequest (GiftRequestModel giftRequest, KadoSakuDelegate<IActionResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.CreateGiftRequest);
				param.AddBody(FRIEND_IDS_KEY, giftRequest.FriendIds.ToArray());
				param.AddBody(FRIEND_TYPE_KEY, giftRequest.FriendType.ToString().ToLower());
				param.AddBody(ITEM_ID_KEY, giftRequest.ItemId);
				param.AddBody(QUANTITY_KEY, giftRequest.Quantity);
				param.AddBody(GIFT_TYPE_KEY, giftRequest.GiftType.ToString().ToLower());
				KS.CallApi(param, callback);
			}
		}
	}
}

