using System;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	public delegate void DelegateViewState (bool isViewShown);

	public partial class KS
	{
		public static bool IsLogin {
			get { return KSConfig.IsLogin; }
		}

		public static bool IsKadoSakuViewShown {
			get { return ViewManagerImpl.IsShown; }
		}

		public static bool IsInitComplete { get; private set; }

		public static IPlayerProfileModel PlayerProfile {
			get { return KSConfig.PlayerProfile; }
		}

		public static event DelegateViewState onViewStateChangedEvent {
			add {
				ViewManagerImpl.OnViewIsShownEvent += value;
			}
			remove {
				ViewManagerImpl.OnViewIsShownEvent -= value;
			}
		}
	}
}

