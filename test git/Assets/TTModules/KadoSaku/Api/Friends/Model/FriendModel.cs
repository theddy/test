using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	internal class FriendModel : IFriendModel
	{
		[JsonConstructor]
		internal FriendModel (string _id, string name, string photoURL) {
			this.Id = _id;
			this.Name = name;
			this.PhotoURL = photoURL;
		}

		internal FriendModel (JToken response) {
			Id = KSUtility.GetValue<string>(response, "_id", string.Empty);
			Name = KSUtility.GetValue<string>(response, "name", string.Empty);
			PhotoURL = KSUtility.GetValue<string>(response, "photoURL", string.Empty);
		}

		public string Id 		{ get; private set; }
		public string Name 		{ get; private set; }
		public string PhotoURL 	{ get; private set; }

		public override string ToString () {
			return string.Format ("[FriendModel: ObjectId={0}, Name={1}, PhotoURL={2}]", Id, Name, PhotoURL);
		}
	}
}

