using System;
using KadoSaku.Core.View;
using KadoSaku.Core.Communication;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	public partial class KS
	{
		public static class Achievement
		{
			const string ACHIEVEMENT_ID_KEY = "achievementId";

			public static void TriggerAchievement (string achievementId, KadoSakuDelegate<IAchievementTransactionResult> callback) {
				KadoSakuDelegate<IAchievementTransactionResult> internalCallback = delegate (IAchievementTransactionResult result) {
					if (result.Error != null) { 
						Sync.SaveAchievement(achievementId); 
					}
					callback(result);
				};

				ParamRequest param = new ParamRequest(KSAction.Api.TriggerAchievement);
				param.AddQuery(ACHIEVEMENT_ID_KEY, achievementId);
				KS.CallApi(param, internalCallback);
			}

			public static void GetAchievementList (KadoSakuDelegate<IAchievementListResult> callback) {
				ParamRequest param = new ParamRequest(KSAction.Api.GetAchievementList);
				KS.CallApi(param, callback);
			}

			public static void ShowAchivementListView () {
				ParamView param = new ParamView(KadoSakuView.AchievementListView);
				KS.ShowView(param);
			}

			public static void ShowAchievementResultView () {
				ParamView param = new ParamView(KadoSakuView.AchievementResultView);
				KS.ShowView(param);
			}
		}
	}
}

