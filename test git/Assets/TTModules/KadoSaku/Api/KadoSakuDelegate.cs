using System;

namespace KadoSaku.Api
{
	public delegate void KadoSakuDelegate<T>(T result) where T : IResult;
}

