using System;
using KadoSaku.Core.Communication;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	public partial class KS
	{
		public static class Mail 
		{
			public static void CheckInbox (KadoSakuDelegate<IMailResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.CheckInbox);
				KS.CallApi(param, callback);
			}

			public static void CheckAnnouncements (KadoSakuDelegate<IMailResult> callback) {
				ParamRequest param = new ParamRequest(KSAction.Api.CheckAnnouncements);
				KS.CallApi(param, callback);
			}

			public static void MarkMailAsRead (string mailId, KadoSakuDelegate<IActionResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.MarkMailAsRead);
				param.AddBody("mailId", mailId);
				KS.CallApi(param, callback);
			}
		}
	}
}

