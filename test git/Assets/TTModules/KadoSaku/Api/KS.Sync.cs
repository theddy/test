using UnityEngine;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using KadoSaku.Core.Crypto;
using KadoSaku.Leaderboard.Sync;
using KadoSaku.Achievement.Sync;
using KadoSaku.Matchmaking.Sync;
using KadoSaku.CloudSave;
using KadoSaku.Utility;
using KadoSaku.Sync;
using KadoSaku.Core.Communication;

namespace KadoSaku.Api
{
	public partial class KS
	{
		public static class Sync 
		{
			static string playerId { 
				get { 
					if (KSConfig.PlayerProfile == null) return "";
					else return KSConfig.PlayerProfile.Id; 
				}
			}

			static ScoreSyncController scoreSync = new ScoreSyncController(new KadoSakuSyncUploader<IActionResult>(KS.CallApi, KSAction.Api.SyncScore));
			static AchievementSyncController achievementSync = new AchievementSyncController(new KadoSakuSyncUploader<IActionResult>(KS.CallApi,  KSAction.Api.SyncAchievement));

			#region Leaderboard Score
			public static void SaveLeaderboardScore (string leaderboardId, double score) {
				TTScore newScore = new TTScore(leaderboardId, score, 0);
				scoreSync.Save(newScore, playerId, LogSave);
			}

			public static void SyncLeaderboardScore (KadoSakuDelegate<IActionResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				scoreSync.Sync(playerId, delegate(Exception scoreError, TTScore[] scores) {
					if (scoreError == null) callback(new ActionResult());
					else callback(new ActionResult(KSString.CreateError(500, scoreError.Message), false));
				});
			}
			#endregion


			#region Achievement
			public static void SaveAchievement (string achievementId) {
				TTAchievement newAchievement = new TTAchievement(achievementId, 0);
				achievementSync.Save(newAchievement, playerId, LogSave);
			}

			public static void SyncAchievement (KadoSakuDelegate<IActionResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}
					
				achievementSync.Sync(playerId, delegate(Exception achievementError, TTAchievement[] achievements) {
					if (achievementError == null) callback(new ActionResult());
					else callback(new ActionResult(KSString.CreateError(500, achievementError.Message), false));
				});
			}
			#endregion

			public static void SyncAll () {
				if (!KS.IsLogin) return;
				scoreSync.Sync(playerId, delegate(Exception scoreError, TTScore[] scores) {
					if (scoreError == null) {
						achievementSync.Sync(playerId, delegate(Exception achievementError, TTAchievement[] achievements) {
							if (achievementError == null) KadoSaku.Utility.KSDebug.Log("success");
							else KadoSaku.Utility.KSDebug.LogError(achievementError.Message);
						});
					}
					else {
						KadoSaku.Utility.KSDebug.LogError(scoreError.Message);
					}
				});
			}

			static void LogSave (Exception err, IEnumerable array) {
				if (!Debug.isDebugBuild) return;
				if (array == null) return;

				StringBuilder sb = new StringBuilder();
				foreach (var item in array) {
					sb.Append(item.ToString());
				}
				string message = sb.ToString();
				KadoSaku.Utility.KSDebug.Log("Save: " + message);
			}
		}
	}
}

