using System;
using System.Collections;
using System.Collections.Generic;

namespace KadoSaku.Api
{
	public interface IMailResult : IResult
	{
		IList<IMailModel> Mails { get; }
	}
}

