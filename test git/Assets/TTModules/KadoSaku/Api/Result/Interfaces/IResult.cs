using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace KadoSaku.Api
{
	public interface IResult
	{
		IError Error { get; }
		bool? Cancelled { get; }
		string RawResult { get; }
	}
}

