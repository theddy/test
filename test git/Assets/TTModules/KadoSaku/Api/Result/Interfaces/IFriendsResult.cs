using System.Collections.Generic;

namespace KadoSaku.Api
{
	public interface IFriendsResult : IResult
 	{
		IList<IFriendModel> FriendList { get; }
	}
}

