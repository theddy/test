using System.Collections.Generic;

namespace KadoSaku.Api
{
	public interface IMatchNotificationResult : IResult
	{
		IList<IMatchModel> MatchList { get; }
	}
}

