using System;
using System.Collections.Generic;

namespace KadoSaku.Api
{
	public interface IAchievementListResult : IResult
	{
		IList<IAchievementModel> AchievementList { get; }
	}
}

