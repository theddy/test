using System;

namespace KadoSaku.Api
{
	public interface IDownloadMetaDataResult : IResult
	{
		DateTime? Date { get; }
		int FileLength { get; }
		string FileType { get; }
		DateTime? LastCreated { get; }
		DateTime? LastModified { get; }
	}
}

