using System;

namespace KadoSaku.Api
{
	public interface ILoginResult : IResult
	{
		string MembershipToken { get; }
		IPlayerProfileModel PlayerProfile { get; }
	}
}

