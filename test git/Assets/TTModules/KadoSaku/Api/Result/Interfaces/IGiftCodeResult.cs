using System;

namespace KadoSaku.Api
{
	public interface IGiftCodeResult : IResult
	{
		IItemModel Item { get; }
		int Quantity { get; }
	}
}

