using System;
using System.Collections.Generic;

namespace KadoSaku.Api
{
	public interface IGiftNotificationResult : IResult
	{
		IList<IGiftModel> GiftList { get; }		
	}
}

