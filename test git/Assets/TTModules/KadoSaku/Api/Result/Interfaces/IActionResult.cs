using System;

namespace KadoSaku.Api
{
	public interface IActionResult : IResult
	{
		bool IsSuccess { get; }
	}
}

