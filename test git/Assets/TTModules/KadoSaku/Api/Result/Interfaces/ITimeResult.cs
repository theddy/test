using System;

namespace KadoSaku.Api
{
	public interface ITimeResult : IResult
	{
		DateTime? UTCTime { get; }
	}
}

