using System;

namespace KadoSaku.Api
{
	public interface IDownloadSaveResult : IResult 
	{
		string DownloadedFile { get; }
	}
}

