using System;
using KadoSaku.Core.Communication;
using Newtonsoft.Json.Linq;

namespace KadoSaku.Api
{
	public interface IInternalResult : IResult
	{
		string CallbackId { get; }
		string Action { get; }
		JToken Response { get; }
	}
}

