using System;

namespace KadoSaku.Api
{
	public interface IError
	{
		int Code { get; }
		string Message { get; }
	}
}

