using System;
using System.Collections.Generic;

namespace KadoSaku.Api
{
	public interface IAchievementTransactionResult : IResult
	{
		IAchievementModel ReceivedAchievement { get; }	
	}
}
