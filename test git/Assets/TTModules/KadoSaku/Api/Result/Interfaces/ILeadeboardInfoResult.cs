using System;
using System.Collections;
using System.Collections.Generic;

namespace KadoSaku.Api
{
	public interface ILeadeboardInfoResult : IResult
	{
		string Name { get; }
		string Status { get; }
		bool EnableMatchmaking { get; }
	}
}

