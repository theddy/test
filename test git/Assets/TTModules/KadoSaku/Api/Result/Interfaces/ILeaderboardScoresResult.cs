using System;
using System.Collections;
using System.Collections.Generic;

namespace KadoSaku.Api
{
	public interface ILeaderboardScoresResult : IResult
	{
		IList<ILeaderboardScoreModel> Scores { get; }
	}
}

