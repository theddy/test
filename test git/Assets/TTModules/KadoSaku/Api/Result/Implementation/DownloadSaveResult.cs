using System;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace KadoSaku.Api
{
	public class DownloadSaveResult : BaseResult, IDownloadSaveResult
	{
		public DownloadSaveResult (string result) : base (result) {
			if (this.Response != null) {
				this.DownloadedFile = GetDownloadedFile(this.Response);
			}
		}

		public DownloadSaveResult (IError error, bool cancelled) : base (error, cancelled) {}

		public string DownloadedFile { get; private set; }

		static string GetDownloadedFile (JToken token) {
			if (token["file"] != null) {
				return (string)token["file"];
			}
			return null;
		}

		public override string ToString ()
		{
			return string.Format ("[DownloadSaveResult: DownloadedFile={0}, Error={1}, Cacelled={2}]", DownloadedFile, Error, Cancelled);
		}
	}
}

