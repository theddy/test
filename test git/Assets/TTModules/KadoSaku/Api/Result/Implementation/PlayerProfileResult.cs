using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	public class PlayerProfileResult: BaseResult, IPlayerProfileResult
	{
		public PlayerProfileResult (string result) : base(result)
		{
			if (this.Response != null) {
				IPlayerProfileModel player = SaveProfile(this.Response);
				this.Id = player.Id;
				this.Name = player.Name;
				this.Email = player.Email;
				this.PhotoURL = player.PhotoURL;
				this.Credits = player.Credits;
			}
		}

		public PlayerProfileResult (IError error, bool cancelled) : base(error, cancelled) {
			this.Id = null;
			this.Name = null;
			this.Email = null;
			this.PhotoURL = null;
			this.Credits = 0;
		}

		public string Id { get; private set; }
		public string Name { get; private set; }
		public string Email { get; private set; }
		public string PhotoURL { get; private set; }
		public int Credits { get; private set; }
	
		IPlayerProfileModel SaveProfile (JToken token) {
			IPlayerProfileModel player = new PlayerModel(token["playerProfile"]);
			KSConfig.PlayerProfile = player;
			return player;
		}

		public override string ToString ()
		{
			return string.Format ("[PlayerProfileResult: ObjectId={0}, Name={1}, Email={2}, PhotoURL={3}, Credits={4}, Error={5}, Cancelled={6}]", Id, Name, Email, PhotoURL, Credits, Error, Cancelled);
		}
	}
}

