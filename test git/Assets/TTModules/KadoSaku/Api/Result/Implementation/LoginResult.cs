using System;
using Newtonsoft.Json.Linq;

namespace KadoSaku.Api
{
	public class LoginResult : BaseResult, ILoginResult
	{
		public LoginResult (string responseString) : base (responseString) {
			if (this.Response != null) {
				this.MembershipToken = GetMembershipToken(this.Response);
				this.PlayerProfile = GetPlayerProfile(this.Response);
			}
		}

		public LoginResult (IError error, bool cancelled) : base (error, cancelled) {}

		public string MembershipToken { get; private set; }
		public IPlayerProfileModel PlayerProfile { get; private set; }

		static string GetMembershipToken (JToken token) {
			if (token["membershipToken"] != null) {
				return (string)token["membershipToken"];
			}
			return null;
		}

		static IPlayerProfileModel GetPlayerProfile (JToken token) {
			if (token["playerProfile"] != null) {
				return new PlayerModel(token["playerProfile"]);
			}
			return null;
		}

		public override string ToString ()
		{
			return string.Format ("[LoginResult: MembershipToken={0}, PlayerProfile={1}, Error={2}, Cancelled={3}]", MembershipToken, PlayerProfile, Error, Cancelled);
		}
	}
}

