using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;

namespace KadoSaku.Api
{
	public class GiftNotificationResult : BaseResult, IGiftNotificationResult
	{
		public IList<IGiftModel> GiftList { get; private set; } 

		public GiftNotificationResult (string result) : base (result)
		{
			if (this.Response != null) {
				GiftList = GetGiftList(this.Response);
			}
			else {
				HandleErrorResult();
			}
		}

		public GiftNotificationResult (IError error, bool cancelled) : base (error, cancelled) {
			HandleErrorResult();
		} 

		static IList<IGiftModel> GetGiftList (JToken token) {
			IList<IGiftModel> gifts = new List<IGiftModel>();
			foreach (JToken item in token) {
				IGiftModel gift = new GiftModel(item);
				gifts.Add(gift);
			}
			return gifts;
		}

		void HandleErrorResult () {
			GiftList = new List<IGiftModel>();
		}

		public override string ToString ()
		{
			StringBuilder sb = new StringBuilder();
			foreach (var gift in GiftList) {
				sb.Append(gift.ToString());
			}
			string giftString = sb.ToString();
			return string.Format("[GiftNotification: Gifts={0}, Error={1}, Cancelled={2}]", giftString, Error, Cancelled);
		}
	}
}

