using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	public class AchievementTransactionResult : BaseResult, IAchievementTransactionResult
	{
		public AchievementTransactionResult (string result) : base (result)
		{
			if (this.Response != null) {
				ReceivedAchievement = GetAchievement(this.Response);
			}
		}

		public AchievementTransactionResult (IError error, bool cancelled) : base(error, cancelled) {}

		public IAchievementModel ReceivedAchievement { get; private set; }

		static IAchievementModel GetAchievement (JToken token) {
			return new AchievementModel(token);
		} 

		public override string ToString ()
		{
			return string.Format ("[AchievementResult: ReceivedAchievement={0} Error={1} Cancelled={2}]", 
				ReceivedAchievement, Error, Cancelled);
		}
	}
}

