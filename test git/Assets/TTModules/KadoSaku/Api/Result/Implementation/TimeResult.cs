using System;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace KadoSaku.Api
{
	public class TimeResult : BaseResult, ITimeResult
	{
		public TimeResult (string result) : base (result) {
			if (this.Response != null) {
				this.UTCTime = GetDateTime(this.Response);
			}
		}

		public TimeResult (IError error, bool cancelled) : base (error, cancelled) {}

		public DateTime? UTCTime { get; private set; }

		static DateTime? GetDateTime (JToken token) {
			if (token["utcTime"] != null) {
				string time = (string)token["utcTime"];
				return DateTime.Parse(time).ToUniversalTime();
			}
			return null;
		}

		public override string ToString ()
		{
			return string.Format ("[TimeResult: UTCTime={0}, Error={1}, Cancelled={2}]", UTCTime, Error, Cancelled);
		}
	}
}

