using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;

namespace KadoSaku.Api
{
	public class LeaderboardScoreResult : BaseResult, ILeaderboardScoresResult
	{
		public LeaderboardScoreResult (string result) : base(result)
		{
			if (this.Response != null) {
				Scores = GetScores(this.Response);
			}
			else {
				HandleErrorResult();
			}
		}

		public LeaderboardScoreResult (IError error, bool cancelled) : base (error, cancelled) {
			HandleErrorResult();
		}

		public IList<ILeaderboardScoreModel> Scores { get; private set; }

		static IList<ILeaderboardScoreModel> GetScores (JToken token) {
			IList<ILeaderboardScoreModel> scores = new List<ILeaderboardScoreModel>();
			foreach (JToken item in token) {
				ILeaderboardScoreModel score = new LeaderboardScoreModel(item);
				scores.Add(score);
			}
			return scores;
		}

		void HandleErrorResult () {
			this.Scores = new List<ILeaderboardScoreModel>();
		}

		public override string ToString ()
		{
			StringBuilder sb = new StringBuilder();
			foreach (var score in Scores) {
				sb.Append(score.ToString());
			}
			string scoreString = sb.ToString();
			return string.Format ("[LeaderboardScoreResult: Scores={0}, Error={1}, Cancelled={2}]", scoreString, Error, Cancelled);
		}
	}
}

