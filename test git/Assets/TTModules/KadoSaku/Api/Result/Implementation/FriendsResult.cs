using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using KadoSaku.Utility;
using System.Text;

namespace KadoSaku.Api
{
	public class FriendsResult : BaseResult, IFriendsResult
	{
		public FriendsResult (string result) : base (result)
		{
			if (this.Response != null) {
				this.FriendList = GetFriendList(this.Response);		
			}
			else {
				HandleErrorResult();
			}
		}

		public FriendsResult (IError error, bool cancelled) : base (error, cancelled) {
			HandleErrorResult();
		}

		public IList<IFriendModel> FriendList { get; private set; }

		static IList<IFriendModel> GetFriendList (JToken response) {
			IList<IFriendModel> friends = new List<IFriendModel>();
			foreach (JToken item in response) {
				IFriendModel friend = new FriendModel(item);
				friends.Add(friend);
			}
			return friends;
		} 

		void HandleErrorResult () {
			this.FriendList = new List<IFriendModel>();
		}

		public override string ToString ()
		{
			StringBuilder sb = new StringBuilder();
			foreach (var friend in FriendList) {
				sb.Append(friend.ToString());
			}
			string friendString = sb.ToString();
			return string.Format ("[FriendsResult: FriendList={0}, Error={1}, Cancelled={2}]", friendString, Error, Cancelled);
		}
	}
}

