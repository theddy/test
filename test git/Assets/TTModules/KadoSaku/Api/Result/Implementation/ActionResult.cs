using System;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace KadoSaku.Api
{
	public class ActionResult : BaseResult, IActionResult
	{
		public ActionResult () : base (null, false) {
			Response = new JObject();
			Response["status"] = "ok";
		}

		public ActionResult (string result) : base (result) {}

		public ActionResult (IError error, bool isCancelled) : base (error, isCancelled) {}

		public bool IsSuccess { 
			get {
				if (this.Error != null) return false;
				if (this.Cancelled != null && this.Cancelled.Value == true) return false;
				
				if (Response["status"] != null) {
					if ((string)Response["status"] == "ok") 
					{
						return true;
					}
				}
				return false;
			}
		}

		public override string ToString ()
		{
			return string.Format ("[ActionResult: IsSuccess={0}, Error={1}, Cancelled={2}]", IsSuccess, Error, Cancelled);
		}
	}
}

