using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using UnityEngine;
using KadoSaku.Core.Communication;

namespace KadoSaku.Api
{
	public class BaseResult : IInternalResult
	{
		public virtual string RawResult { get; protected set; }
		public virtual IError Error { get; protected set; }
		public virtual bool? Cancelled { get; protected set; }
		public virtual JToken Response { get; protected set; }
		public virtual string Action { get; protected set; }
		public virtual string CallbackId { get; protected set; }

		protected KSString ksString { get; private set; }

		public BaseResult (string responseJSON) {
			ksString = new KSString(responseJSON);
			this.Error = ksString.Error;
			this.Cancelled = ksString.Cancelled;
			this.Response = ksString.Response;
			this.Action = ksString.Action;
			this.CallbackId = ksString.CallbackId;
			this.RawResult = responseJSON;
		}

		public BaseResult (IError error, bool cancelled) {
			ksString = new KSString();
			this.Error = ksString.Error = error;
			this.Cancelled = ksString.Cancelled = cancelled;
		}

		public override string ToString ()
		{
			return string.Format ("[BaseResult: Response={0}, Error={1}, Cancelled={2}]", 
			                      Response, 
			                      Error, 
			                      Cancelled);
		}
	}
}

