using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using UnityEngine;


namespace KadoSaku.Api
{
	public class DownloadMetaDataResult : BaseResult, IDownloadMetaDataResult
	{
		public DownloadMetaDataResult (string result) : base(result) { 
			if (this.Response != null) {
				this.Date = GetDate(this.Response);
				this.FileLength = GetFileLength(this.Response);
				this.FileType = GetFileType(this.Response);
				this.LastCreated = GetLastCreated(this.Response);
				this.LastModified = GetLastModified(this.Response);
			}
		}

		public DownloadMetaDataResult (IError error, bool cancelled) : base(error, cancelled) {
			this.Date = null;
			this.FileLength = -1;
			this.FileType = string.Empty;
			this.LastCreated = null;
			this.LastModified = null;
		}

		public DateTime? Date { get; private set; }
		public int FileLength { get; private set; }
		public string FileType { get; private set; }
		public DateTime? LastCreated { get; private set; }
		public DateTime? LastModified { get; private set; }

		static DateTime? GetDate (JToken token) {
			if (token["date"] != null) {
				return token["date"].ToObject<DateTime?>();
			}
			return null;
		}

		static int GetFileLength (JToken token) {
			if (token["fileLength"] != null) {
				string lengthString = token["fileLength"].ToObject<string>();
				int lengthInt;
				if (Int32.TryParse(lengthString, out lengthInt)) return lengthInt;
				else return -1;
			}
			return -1;
		}

		static string GetFileType (JToken token) {
			if (token["fileType"] != null) {
				return token["fileType"].ToObject<string>();
			}
			return string.Empty;
		}

		static DateTime? GetLastCreated (JToken token) {
			if (token["lastCreated"] != null) {
				return token["lastCreated"].ToObject<DateTime?>();
			}
			return null;
		}

		static DateTime? GetLastModified (JToken token) {
			if (token["lastModified"] != null) {
				return token["lastModified"].ToObject<DateTime?>();
			}
			return null;
		}

		public override string ToString ()
		{
			return string.Format ("[DownloadMetaDataResult: Date={0}, FileLength={1}, FileType={2}, LastCreated={3}, LastModified={4}] Error={5} Cancelled={6}", Date, FileLength, FileType, LastCreated, LastModified, Error, Cancelled);
		}
	}
}

