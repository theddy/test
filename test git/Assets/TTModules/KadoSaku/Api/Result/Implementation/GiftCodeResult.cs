using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	public class GiftCodeResult : BaseResult, IGiftCodeResult
	{
		public GiftCodeResult (string result) : base (result)
		{
			if (this.Response != null) {
				this.Item = KSUtility.GetValue<ItemModel>(this.Response, "item", null);
				this.Quantity = KSUtility.GetValue<int>(this.Response["gift"], "quantity", 0);
			}
		}

		public GiftCodeResult (IError error, bool cancelled) : base (error, cancelled) {}

		public IItemModel Item 	{ get; private set; }
		public int Quantity 	{ get; private set; }

		public override string ToString ()
		{
			return string.Format ("[GiftCodeResult: Item={0}, Quantity={1}, Error={2}, Cancelled={3}]", Item, Quantity, Error, Cancelled);
		}
	}
}

