using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using KadoSaku.Utility;
using System.Text;

namespace KadoSaku.Api
{
	public class AchievementListResult : BaseResult, IAchievementListResult
	{
		public AchievementListResult (string result) : base(result)
		{
			if (this.Response != null) {
				this.AchievementList = GetAchievementList(this.Response);
			}
			else {
				HandleErrorResult();
			}
		}

		public AchievementListResult (IError error, bool cancelled) : base(error, cancelled) {
			HandleErrorResult();
		}

		public IList<IAchievementModel> AchievementList { get; private set; }

		static IList<IAchievementModel> GetAchievementList (JToken token) {
			IList<IAchievementModel> achievements = new List<IAchievementModel>();
			foreach (JToken item in token) {
				IAchievementModel achievement = new AchievementModel(item);
				achievements.Add(achievement);
			}
			return achievements;
		}

		void HandleErrorResult () {
			this.AchievementList = new List<IAchievementModel>();
		}

		public override string ToString ()
		{
			StringBuilder sb = new StringBuilder();
			foreach (var score in AchievementList) {
				sb.Append(score.ToString());
			}
			string achievementString = sb.ToString();
			return string.Format ("[AchievementListResult: AchievementList={0}]", achievementString);
		}
	}
}

