using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;

namespace KadoSaku.Api
{
	public class MatchNotificationResult : BaseResult, IMatchNotificationResult
	{
		public MatchNotificationResult (string result) : base(result) 
		{
			if (this.Response != null) {
				this.MatchList = GetMatches(this.Response);			
			}
			else {
				HandleErrorResult();
			}
		}

		public MatchNotificationResult (IError error, bool cancelled) : base (error, cancelled) {
			HandleErrorResult();
		}

		public IList<IMatchModel> MatchList { get; private set; }

		static IList<IMatchModel> GetMatches (JToken response) {
			IList<IMatchModel> matches = new List<IMatchModel>();
			foreach (JToken item in response) {
				IMatchModel match = new MatchModel(item);
				matches.Add(match);
			}
			return matches;
		}

		void HandleErrorResult () {
			this.MatchList = new List<IMatchModel>();
		}

		public override string ToString ()
		{
			StringBuilder sb = new StringBuilder();
			foreach (var match in MatchList) {
				sb.Append(match.ToString());
			}
			string matchString = sb.ToString();
			return string.Format("[MatchNotificationResult: Matches={0}, Error={1}, Cancelled={2}]", matchString, Error, Cancelled);
		}
	}
}

