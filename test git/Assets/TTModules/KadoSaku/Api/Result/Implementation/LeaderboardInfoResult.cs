using System;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace KadoSaku.Api
{
	public class LeaderboardInfoResult : BaseResult, ILeadeboardInfoResult
	{
		public LeaderboardInfoResult (string result) : base (result)
		{
			if (this.Response != null) {
				this.Name = GetName(this.Response);
				this.Status = GetStatus(this.Response);
				this.EnableMatchmaking = GetMatchmakingStatus(this.Response);
			}
		}

		public LeaderboardInfoResult (IError error, bool cancelled) : base(error, cancelled) {}

		public string Name { get; private set; }	
		public string Status { get; private set; }
		public bool EnableMatchmaking { get; private set; }
	
		static string GetName (JToken token) {
			if (token["name"] != null) {
				return (string)token["name"];
			}
			return null;
		}

		static string GetStatus (JToken token) {
			if (token["status"] != null) {
				return (string)token["status"];
			}
			return null;
		}

		static bool GetMatchmakingStatus (JToken token) {
			if (token["enableMatchmaking"] != null) {
				return (bool)token["enableMatchmaking"];
			}
			return false;
		}

		public override string ToString ()
		{
			return string.Format ("[LeaderboardGetInfoResult: Name={0}, Status={1}, EnableMatchmaking={2}, Error={3}, Cancelled={4}]", 
			                      Name, 
			                      Status, 
			                      EnableMatchmaking,
			                      Error,
			                      Cancelled);
		}
	}
}

