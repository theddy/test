using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Text;
using UnityEngine;

namespace KadoSaku.Api
{
	public class MailResult : BaseResult, IMailResult
	{
		public MailResult (string result) : base(result) 
		{
			if (this.Response != null) {
				this.Mails = GetMails(this.Response);
			}
			else {
				HandleErrorResult();
			}
		}

		public MailResult (IError error, bool cancelled) : base (error, cancelled) {
			HandleErrorResult();
		}

		public IList<IMailModel> Mails { get; private set; }

		static IList<IMailModel> GetMails (JToken token) {
			IList<IMailModel> mails = new List<IMailModel>();
			foreach (JToken item in token) {
				IMailModel mail = item.ToObject<MailModel>();
				mails.Add(mail);
			}
			return mails;
		}

		void HandleErrorResult () {
			this.Mails = new List<IMailModel>();
		}

		public override string ToString ()
		{
			StringBuilder sb = new StringBuilder();
			foreach (var mail in Mails) {
				sb.Append(mail.ToString());
			}
			string mailString = sb.ToString();
			return string.Format ("[MailResult: Mails={0}, Error={1}, Cancelled={2}]", mailString, Error, Cancelled);
		}
	}
}

