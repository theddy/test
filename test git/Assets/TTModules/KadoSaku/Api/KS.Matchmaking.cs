using System;
using System.Linq;
using System.Text;
using KadoSaku.Core.Communication;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	public partial class KS
	{
		public static class Matchmaking
		{
			const string DEFENDER_ID_KEY = "defender";
			const string LEADERBOARD_ID_KEY = "leaderboard";
			const string CUSTOM_DATA_KEY = "customData";
			const string MATCH_ID_KEY = "matchId";
			const string SCORE_KEY = "score";

			public static void GetMatchNotification (string leaderboardId, KadoSakuDelegate<IMatchNotificationResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.GetMatchNotification);
				param.AddQuery("leaderboardId", leaderboardId);
				KS.CallApi(param, callback);
			}

			public static void CreateNewMatch (MatchRequestModel matchRequest, KadoSakuDelegate<IActionResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}
					
				ParamRequest param = new ParamRequest(KSAction.Api.CreateNewMatch);
				param.AddBody(DEFENDER_ID_KEY, matchRequest.DefenderId);
				param.AddBody(LEADERBOARD_ID_KEY, matchRequest.LeaderboardId);
				param.AddBody(CUSTOM_DATA_KEY, string.IsNullOrEmpty(matchRequest.CustomData) ? string.Empty : KSUtility.EncodeCustomData(matchRequest.CustomData));
				KS.CallApi(param, callback);
			}

			public static void SubmitMatchScore (MatchSubmitModel matchSubmit, KadoSakuDelegate<IActionResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.SubmitMatchScore);
				param.AddQuery(MATCH_ID_KEY, matchSubmit.MatchId);
				param.AddBody(SCORE_KEY, matchSubmit.Score);
				param.AddBody(CUSTOM_DATA_KEY, string.IsNullOrEmpty(matchSubmit.CustomData) ? string.Empty : KSUtility.EncodeCustomData(matchSubmit.CustomData));
				KS.CallApi(param, callback);
			}

			public static void CancelMatch (string matchId, string customData, KadoSakuDelegate<IActionResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.CancelMatch);
				param.AddQuery(MATCH_ID_KEY, matchId);
				param.AddBody(CUSTOM_DATA_KEY, string.IsNullOrEmpty(customData) ? string.Empty : KSUtility.EncodeCustomData(customData));
				KS.CallApi(param, callback);
			}

			public static void ArchiveMatch (string matchId, string customData, KadoSakuDelegate<IActionResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.ArchiveMatch);
				param.AddQuery(MATCH_ID_KEY, matchId);
				param.AddBody(CUSTOM_DATA_KEY, string.IsNullOrEmpty(customData) ? string.Empty : KSUtility.EncodeCustomData(customData));
				KS.CallApi(param, callback);
			}


		}
	}
}

