using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	internal class GiftModel : IGiftModel
	{
		internal GiftModel (JToken response) {
			Id = KSUtility.GetValue<string>(response, "_id", string.Empty);
			Giver = KSUtility.GetValue<FriendModel>(response, "giver", null);
			Sender = KSUtility.GetValue<FriendModel>(response, "sender", null);
			Status = GetStatus(response);
			Type = GetType(response);
			Quantity = KSUtility.GetValue<int>(response, "quantity", 0);
			Item = KSUtility.GetValue<ItemModel>(response, "item", null);
		}

		public string Id 			{ get; private set; }
		public IFriendModel Giver 	{ get; private set; }
		public IFriendModel Sender 	{ get; private set; }
		public GiftStatus Status 	{ get; private set; }
		public GiftType Type 		{ get; private set; }
		public int Quantity 		{ get; private set; }
		public IItemModel Item 		{ get; private set; }

		const bool ignoreCase = true;

		static GiftStatus GetStatus (JToken response) {
			return (GiftStatus) Enum.Parse(typeof(GiftStatus),
				KSUtility.GetValue<string>(response, "status", GiftStatus.Invalid.ToString()),
				ignoreCase);
		}

		static GiftType GetType (JToken response) {
			return (GiftType) Enum.Parse(typeof(GiftType),
				KSUtility.GetValue<string>(response, "type", GiftType.Invalid.ToString()),
				ignoreCase);
		}

		public override string ToString ()
		{
			return string.Format ("[GiftModel: Id={0}, Target={1}, Initiator={2}, Status={3}, Type={4}, Quantity={5}, Item={6}]", Id, Giver, Sender, Status, Type, Quantity, Item);
		}
	}
}

