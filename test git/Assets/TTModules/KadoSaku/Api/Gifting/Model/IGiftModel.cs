using System;

namespace KadoSaku.Api
{
	public interface IGiftModel
	{
		string Id { get; }
		IFriendModel Sender { get; }
		IFriendModel Giver { get; }
		GiftStatus Status { get; }
		GiftType Type { get; }
		int Quantity { get; }
		IItemModel Item { get; }
	}
}

