using System;
using System.Collections.Generic;

namespace KadoSaku.Api
{
	public struct GiftRequestModel
	{
		public IList<string> FriendIds 	{ get; private set; }
		public FriendType FriendType	{ get; private set; }
		public string ItemId			{ get; private set; }
		public int Quantity				{ get; private set; }
		public GiftType GiftType 		{ get; private set; }

		public GiftRequestModel (IList<string> friendIds, 
								 FriendType friendType, 
								 string itemId, 
								 int quantity, 
								 GiftType giftType) 
		{
			this.FriendIds = friendIds;
			this.FriendType = friendType;
			this.ItemId = itemId;
			this.Quantity = quantity;
			this.GiftType = giftType;
		}
	}
}

