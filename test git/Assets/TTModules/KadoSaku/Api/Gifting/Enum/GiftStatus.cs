using System;

namespace KadoSaku.Api
{
	public enum GiftStatus
	{
		Invalid,
		Pending,
		Ended,
		Cancelled
	}
}

