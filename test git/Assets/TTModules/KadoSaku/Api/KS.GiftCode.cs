using System;
using KadoSaku.Core.Communication;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	public partial class KS
	{
		public static class GiftCode 
		{
			const string CODE_KEY = "code";

			public static void RedeemCode (string giftCode, KadoSakuDelegate<IGiftCodeResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.RedeemGiftCode);
				param.AddBody(CODE_KEY, giftCode);
				KS.CallApi(param, callback);
			}	
		}
	}
}

