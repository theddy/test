using System;
using KadoSaku.Core.Communication;
using KadoSaku.Utility;
using Touchten.Social;

namespace KadoSaku.Api
{
	public partial class KS
	{
		public static class Utility 
		{
			const string FBTOKEN_KEY = "fbToken";

			public static void GetServerTime (KadoSakuDelegate<ITimeResult> callback) {
				ParamRequest param = new ParamRequest(KSAction.Api.GetServerTime);
				KS.CallApi(param, callback);
			}

			public static void GetKadoSakuFriends (KadoSakuDelegate<IFriendsResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback)	;
					return;
				}

				socialService.LoginUsing(SocialService.Facebook, (accessToken) => {
					ParamRequest param = new ParamRequest(KSAction.Api.GetKadoSakuFriends);
					param.AddQuery(FBTOKEN_KEY, accessToken);
					KS.CallApi(param, callback);
				});
			}
		}
	}
}

