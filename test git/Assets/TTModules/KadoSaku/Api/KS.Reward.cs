using System;
using KadoSaku.Core.Communication;
using KadoSaku.Core.View;

namespace KadoSaku.Api
{
	public partial class KS
	{
		public static class Reward
		{
			public static void ShowView () {
				ParamView param = new ParamView(KadoSakuView.RewardView);
				KS.ShowView(param);
			}
		}
	}
}

