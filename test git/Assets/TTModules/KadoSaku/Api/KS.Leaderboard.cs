using System;
using System.Collections.Generic;
using KadoSaku.Core.View;
using KadoSaku.Core.Communication;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	public partial class KS
	{
		public static class Leaderboard 
		{
			const string LEADERBOARD_ID_KEY = "leaderboardId";
			const string PLAYER_SCORE_KEY = "value";
			const string TIME_FRAME_KEY = "timeFrame";
			const string FBTOKEN_KEY = "facebookToken";

			public static void ShowView (string leaderboardId) {
				ParamView param = new ParamView(KadoSakuView.LeaderboardView);
				param.AddViewQuery(LEADERBOARD_ID_KEY, leaderboardId);
				KS.ShowView(param);
			}

			public static void SubmitScore (string leaderboardId, int score, KadoSakuDelegate<IActionResult> callback) {
				KadoSakuDelegate<IActionResult> internalCallback = delegate (IActionResult result) {
					if (!result.IsSuccess) { 
						Sync.SaveLeaderboardScore(leaderboardId, Convert.ToDouble(score)); 
					}
					callback(result);
				};

				ParamRequest param = new ParamRequest(KSAction.Api.SubmitLeaderboardScore);
				param.AddQuery(LEADERBOARD_ID_KEY, leaderboardId);
				param.AddBody(PLAYER_SCORE_KEY, score);
				KS.CallApi(param, internalCallback);
			}

			public static void GetLeaderboardInfo (string leaderboardId, KadoSakuDelegate<ILeadeboardInfoResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.GetLeaderboardInfo);
				param.AddQuery(LEADERBOARD_ID_KEY, leaderboardId);
				KS.CallApi(param, callback);
			}

			public static void GetGlobalScores (string leaderboardId, LeaderboardTimeFrame timeFrame, KadoSakuDelegate<ILeaderboardScoresResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.GetLeaderboardGlobalScores);
				param.AddQuery(LEADERBOARD_ID_KEY, leaderboardId);
				param.AddQuery(TIME_FRAME_KEY, timeFrame.ToString().ToLower());
				KS.CallApi(param, callback);
			}

			public static void GetSocialScores (string leaderboardId, LeaderboardTimeFrame timeFrame, KadoSakuDelegate<ILeaderboardScoresResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.GetLeaderboardSocialScores);
				param.AddQuery(LEADERBOARD_ID_KEY, leaderboardId);
				param.AddQuery(TIME_FRAME_KEY, timeFrame.ToString().ToLower());
				param.AddBody(FBTOKEN_KEY, socialService.AccessToken);
				KS.CallApi(param, callback);
			}
		}
	}
}

