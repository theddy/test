using System;
using UnityEngine;
using KadoSaku.Api;
using KadoSaku.Utility;
using KadoSaku.Core;
using KadoSaku.Core.View;
using KadoSaku.Core.Communication;
using KadoSaku.Core.Platform;
using KadoSaku.Settings;
using Touchten.Social;
using Touchten.Core;
using Touchten.Analytics;

namespace KadoSaku.Api
{
	public partial class KS : ScriptableObject
	{
		private static IKadoSakuPlatform kadosaku;
		private static ITTSocialExtension socialService;
		private static IAnalyticsData analyticsData;
		private static ISharedPush sharedPush;

		private static PushSharedData _pushData;
		private static PushSharedData pushSharedData {
			set { _pushData = value; }
			get {
				if (_pushData == null) {
					_pushData = new PushSharedData(string.Empty, string.Empty, string.Empty);
				}
				return _pushData;
			}
		}

		public static Action<bool> InitCompleteCallback;

		private static IViewManagerService ViewManagerImpl { 
			get {
				if (kadosaku == null) {
					throw new NullReferenceException("KadoSaku Game Object is Not Loaded. Did you call KS.Init()?");
				}
				return (IViewManagerService)kadosaku;
			}
		}

		private static ICommunicationService CommunicationImpl { 
			get {
				if (kadosaku == null) {
					throw new NullReferenceException("KadoSaku Game Object is Not Loaded. Did you call KS.Init()?");
				}
				return (ICommunicationService)kadosaku;
			} 
		}

		private static CallbackManager CallbackManagerImpl {
			get {
				if (kadosaku == null) {
					throw new NullReferenceException("KadoSaku Game Object is Not Loaded. Did you call KS.Init()?");
				}
				return kadosaku.callbackManager;
			}
		}

		public static ITTSocialExtension SocialServiceImpl {
			get {
				if (socialService == null) {
					throw new NullReferenceException("No TT Social Module Injected! Init not Completed!");
				}
				return socialService;
			}
			set {
				socialService = value;
			}
		}

		public static IKadoSakuPlatform KadoSakuImpl {
			get {
				if (kadosaku == null) {
					throw new NullReferenceException("KadoSaku Game Object is Not Loaded. Did you call KS.Init()?");
				}
				return kadosaku;
			}
			set {
				kadosaku = value;
			}
		}

		public static bool Init (ITTSocialExtension ttSocialExtension, ISharedSettings sharedSettings, IAnalyticsData analyticsDataExtension, ISharedPush sharedPush) {
			if (ttSocialExtension == null) throw new ArgumentException("KadoSaku Depends to TT Social Module, Cannot be null!");
			if (sharedSettings == null) throw new ArgumentException("KadoSaku Depends to TT.Core Shared Settings, Cannot be null");
			KSConfig.IsEncrypted = true;
			KSConfig.sharedSettings = sharedSettings;
			socialService = ttSocialExtension;
			analyticsData = analyticsDataExtension;
			KS.sharedPush = sharedPush;
			KS.sharedPush.PushIdReceived += OnPushIdReceieved;
			KSComponentFactory.GetComponent<UniwebviewKadoSakuLoader>();
			return true;
		}

		static void OnKSDestroyed () {
			KS.sharedPush.PushIdReceived -= OnPushIdReceieved;
		}

		void OnDestroy () {
			OnKSDestroyed();
		}

		private static void OnLoaded () {
			IsInitComplete = true;
			if (InitCompleteCallback != null) InitCompleteCallback(IsInitComplete);
			KSConfig.PlayerProfile = null;
			KSConfig.LoadMembershipToken();
			InitWeb( _=> {
				Authentication.AutoLogin((r) => {
					RegisterCognitoId( __ => {
						RegisterSessionId( ___ => {
							RegisterPushNotif(() => {
								KadoSaku.Utility.KSDebug.Log("Auto Login Completed: " + r);
								Sync.SyncAll();
							});
						});
					});
				});
			});

			kadosaku.OnApplicationFocus = delegate {
				RegisterSessionId((result) => { KadoSaku.Utility.KSDebug.Log(result); });
			};
		}


		#region Cross Module Event Handlers
		static void OnPushIdReceieved (object sender, PushSharedData pushSharedData) {
			KS.pushSharedData = pushSharedData;
		}

		static void RegisterPushNotif (Action callback) {
			KadoSakuDelegate<IActionResult> internalCallback = delegate(IActionResult result) {
				KadoSaku.Utility.KSDebug.Log("[KS Register Push] " + result.ToString());
				callback();
			};

			KadoSaku.Utility.KSDebug.Log("[KS Register Push] registering push data");
			analyticsData.GetAmazonCognitoId((cognitoId) => {
				ParamRequest param = new ParamRequest (KSAction.Api.RegisterPushNotification);
				param.AddBody("cognitoId", cognitoId);
				param.AddBody("provider", pushSharedData.PushProvider);
				param.AddBody("identifier", pushSharedData.PushIdentifier);

				#if UNITY_ANDROID
				param.AddBody("gcmRegistrationId", pushSharedData.PushToken);
				#elif UNITY_IOS
				param.AddBody("apnsToken", pushSharedData.PushToken);
				#endif

				CallApi(param, internalCallback);
			});
		}
		#endregion


		#region Helper Methods
		static void InitWeb (KadoSakuDelegate<IActionResult> callback) {
			ParamRequest param = new ParamRequest(KSAction.Init);
			param.AddQuery("gameName", KSCoreSettings.GAME_NAME);
			param.AddQuery("gamePhoto", KSCoreSettings.GAME_PHOTO);
			CallApi(param, callback);
		}

		static void RegisterCognitoId (KadoSakuDelegate<IActionResult> callback) {
			analyticsData.GetAmazonCognitoId((cognitoId) => {
				ParamRequest param = new ParamRequest(KSAction.Api.RegisterAmazonCognitoId);
				param.AddQuery("cognitoId", cognitoId);
				param.AddBody("cognitoId", cognitoId);
				CallApi (param, callback);
			});
		}

		static void RegisterSessionId (KadoSakuDelegate<IActionResult> callback) {
			KadoSaku.Utility.KSDebug.Log("register session id");
			analyticsData.GetAmazonSessionId((sessionId) => {
				ParamRequest param = new ParamRequest(KSAction.Api.RegisterAmazonSessionId);
				param.AddQuery("sessionId", sessionId);
				CallApi (param, callback);
			});
		}

		static void ShowView (ParamView param) {
			ViewManagerImpl.ShowView(param);
		}

		static void CallApi<T> (ParamRequest param, KadoSakuDelegate<T> callback) where T : IResult {
			string callbackId = CallbackManagerImpl.AddKadoSakuDelegate(callback);
			param.AddCallbackId(callbackId);
			CommunicationImpl.CallToWeb(param);
		}
		#endregion


		public abstract class CompiledKadoSakuLoader : MonoBehaviour
		{
			protected abstract void LoadServices (KadoSakuGameObject kadoSakuGO, ITTSocialExtension socialService, IAnalyticsData analyticsData);

			private KadoSakuGameObject KadoSakuGO {
				get {
					KadoSakuGameObject ksGO = KSComponentFactory.GetComponent<KadoSakuGameObject>();
					LoadServices(ksGO, KS.socialService, KS.analyticsData);
					return ksGO;
				}
			}

			public void Awake () {
				KS.kadosaku = this.KadoSakuGO.kadoSakuPlatform;
				KS.OnLoaded();
				MonoBehaviour.Destroy(this);
			}
		}
	}
}

