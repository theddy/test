using System;
using KadoSaku.Core.Communication;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	public partial class KS
	{
		public static class CloudSave 
		{
			const string FILE_NAME_KEY = "fileName";
			const string FILE_KEY = "file";

			public static void UploadSave (string fileName, string file, KadoSakuDelegate<IActionResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.UploadSave, "application/octet-stream");
				param.AddQuery(FILE_NAME_KEY, fileName);
				param.AddBody(FILE_KEY, file);
				KS.CallApi(param, callback);
			}

			public static void DownloadSave (string fileName, KadoSakuDelegate<IDownloadSaveResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.DownloadSave);
				param.AddQuery(FILE_NAME_KEY, fileName);
				KS.CallApi(param, callback);
			}

			public static void DeleteSave (string fileName, KadoSakuDelegate<IActionResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.DeleteSave);
				param.AddQuery(FILE_NAME_KEY, fileName);
				KS.CallApi(param, callback);
			}

			public static void DownloadMetaData (string fileName, KadoSakuDelegate<IDownloadMetaDataResult> callback) {
				if (!KS.IsLogin) {
					KSUtility.CallbackNotLoggedInError(callback);
					return;
				}

				ParamRequest param = new ParamRequest(KSAction.Api.DownloadMetaData);
				param.AddQuery(FILE_NAME_KEY, fileName);
				KS.CallApi(param, callback);
			}
		}
	}
}

