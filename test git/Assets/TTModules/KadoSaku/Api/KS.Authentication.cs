using System;
using KadoSaku.Core.View;
using KadoSaku.Core.Communication;
using KadoSaku.Utility;
using KadoSaku.Settings;
using Touchten.Social;

namespace KadoSaku.Api
{
	public partial class KS
	{
		public static class Authentication 
		{
			const string SOCIAL_TOKEN_KEY = "socialToken";
			const string APP_SECRET_KEY = "appSecret";
			public static KadoSakuDelegate<IActionResult> loginCallback = null;

			#region Events
			public delegate void delegateAutoLogin (bool isInAutoLoginProcess);
			public static event delegateAutoLogin isInAutoLoginProcessEvent;
			static void FireAutoLoginEventFlag (bool isInAutoLoginProcess) {
				if (isInAutoLoginProcessEvent != null) isInAutoLoginProcessEvent (isInAutoLoginProcess);	
			}

			public static event KadoSakuDelegate<IActionResult> onAutoLoginCompleteEvent;
			static void FireAutoLoginCompleteEvent (IActionResult result) {
				if (onAutoLoginCompleteEvent != null) onAutoLoginCompleteEvent (result);
			}

			public static event KadoSakuDelegate<IActionResult> onLoginEvent;
			public static void _OnLoginEvent (IActionResult result) {
				if (KS.Authentication.onLoginEvent != null) KS.Authentication.onLoginEvent(result);
			}

			public static event KadoSakuDelegate<IActionResult> onLogoutEvent;
			public static void _OnLogoutEvent (IActionResult result) {
				if (KS.Authentication.onLogoutEvent != null) KS.Authentication.onLogoutEvent(result);
			}
			#endregion


			public static void ShowLoginView (KadoSakuDelegate<IActionResult> callback) {
				ParamView param = new ParamView(KadoSakuView.LoginView);
				KS.ShowView(param);
				loginCallback = callback;
			}

			public static void Logout (KadoSakuDelegate<IActionResult> callback) {
				socialService.Logout(SocialService.Facebook, delegate(bool obj) {
					KSConfig.DeleteMembershipToken();
					KSConfig.PlayerProfile = null;
					callback(new ActionResult());
				});
			}

			public static void UnlinkSocialAndLogout (KadoSakuDelegate<IActionResult> callback) {
				KadoSakuDelegate<IActionResult> internalCallback = (IActionResult result) =>
				{
					if (result.IsSuccess) Logout((r) => {});
					callback(result);
					return;
				};

				ParamRequest param = new ParamRequest(KSAction.Api.Unlink);
				KS.CallApi(param, internalCallback);
			}

			public static void AutoLogin (KadoSakuDelegate<IActionResult> callback) {
				KadoSakuDelegate<IActionResult> internalCallback = (IActionResult result) =>
				{
					if (!result.IsSuccess) Logout((r) => {});
					FireAutoLoginEventFlag(false);
					FireAutoLoginCompleteEvent(result);
					callback(result);
					return;
				};

				if (!KSCoreSettings.ENABLE_AUTO_LOGIN) {
					Logout((r) => {});
					IActionResult _result = new ActionResult(KSString.CreateInternalError("Auto Login Disabled"), false);
					FireAutoLoginEventFlag(false);
					FireAutoLoginCompleteEvent(_result);
					callback(_result);
					return;
				}
					
				if (string.IsNullOrEmpty(KSConfig.MembershipToken)) {
					Logout((r) => {});
					IActionResult _result = new ActionResult(KSString.CreateInternalError("No Old Token"), false);
					FireAutoLoginEventFlag(false);
					FireAutoLoginCompleteEvent(_result);
					callback(_result);
					return;
				}

				FireAutoLoginEventFlag(true);
				socialService.LoginUsing(SocialService.Facebook, (accessToken) => {
					if (string.IsNullOrEmpty(accessToken)) {
						Logout ((r) => {});
						IActionResult _result = new ActionResult(KSString.CreateInternalError("Empty FB Token"), true);
						FireAutoLoginEventFlag(false);
						FireAutoLoginCompleteEvent(_result);
						callback(_result);
						return;
					}
					else {
						KadoSaku.Utility.KSDebug.Log("access token: " + accessToken);
						ParamRequest param = new ParamRequest(action: KSAction.Api.AutoLogin,
						                                      accessToken: accessToken,
						                                      contentType: "application/json");
						KS.CallApi(param, internalCallback);
					}
				});
			}


		}
	}
}

