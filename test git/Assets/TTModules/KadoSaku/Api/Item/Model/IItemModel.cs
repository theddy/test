using System;

namespace KadoSaku.Api
{
	public interface IItemModel
	{
		string Id 			{ get; }
		string Name 		{ get; }
		string Description 	{ get; }
	}
}

