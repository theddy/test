using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using KadoSaku.Utility;

namespace KadoSaku.Api
{
	internal class ItemModel : IItemModel
	{
		[JsonConstructor]
		internal ItemModel (string _id, string name, string description) {
			this.Id = _id;
			this.Name = name;
			this.Description = description;
		}

		internal ItemModel (JToken response) {
			Id = KSUtility.GetValue<string>(response, "_id", string.Empty);
			Name = KSUtility.GetValue<string>(response, "name", string.Empty);
			Description = KSUtility.GetValue<string>(response, "description", string.Empty);
		}

		public string Id 			{ get; private set; }
		public string Name 			{ get; private set; }
		public string Description 	{ get; private set; }

		public override string ToString ()
		{
			return string.Format ("[ItemModel: Id={0}, Name={1}, Description={2}]", Id, Name, Description);
		}
	}
}

