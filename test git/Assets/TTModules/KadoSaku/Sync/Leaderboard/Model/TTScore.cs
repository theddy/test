namespace KadoSaku.Leaderboard.Sync {
	public class TTScore {
		public string leaderboardId;
		public double value;
		public int time;

		public TTScore(string leaderboardId, double value, int time){
			this.leaderboardId = leaderboardId;
			this.value = value;
			this.time = time;
		}

		public override string ToString ()
		{
			return 
				"leaderboardId: " + leaderboardId + "\n" +
				"value: " + value + "\n" +
				"time: " + time;
		}
}
}