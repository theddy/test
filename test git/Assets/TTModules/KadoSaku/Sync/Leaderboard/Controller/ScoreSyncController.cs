using System;
using KadoSaku.Core.Crypto;

namespace KadoSaku.Leaderboard.Sync
{
	public class ScoreSyncController : SyncController<TTScore>
	{
		public ScoreSyncController(ISyncUploader cloudUploader) : base (cloudUploader){
			_saveType = "score";
			_maxSave = TTCryptoConfiguration.MAX_SAVED_SCORES;
		}

		protected override void ValidateSaveParameters (TTScore saveData, string identifier)
		{
			if(saveData == null)
				throw new Exception("saveData must be filled");
			if(string.IsNullOrEmpty(saveData.leaderboardId))
				throw new Exception("leaderboardId cannot be empty");
		}

		protected override void ValidateDiskContent (TTSyncCollection<TTScore> syncCollection, string identifier)
		{
			if(!syncCollection.type.Equals(_saveType))
				throw new Exception("File contains wrong format");
			if(!syncCollection.playerId.Equals(identifier))
				throw new Exception("The saved score is not intended for this playerId");
		}

		protected override void PreprocessNewSave (ref TTScore newSave, string identifier)
		{
			newSave.time = Utils.GetEpochTime(); //Always override timestamp to make sure it is not tampered
		}

		protected override void PreprocessCollection (ref TTSyncCollection<TTScore> syncCollection, string identifier)
		{
			syncCollection.playerId = identifier;
			syncCollection.type = _saveType;
		}
	}
}