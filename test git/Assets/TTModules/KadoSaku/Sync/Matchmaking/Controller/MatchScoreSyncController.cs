using System;
using KadoSaku.Core.Crypto;

namespace KadoSaku.Matchmaking.Sync
{
	public class MatchScoreSyncController : SyncController<TTMatchScore> {
		public MatchScoreSyncController(ISyncUploader cloudUploader) : base (cloudUploader){
			_saveType = "matchscore";
			_maxSave = TTCryptoConfiguration.MAX_SAVED_MATCHSCORES;
		}
		
		protected override void ValidateSaveParameters (TTMatchScore saveData, string identifier)
		{
			if(saveData == null)
				throw new Exception("saveData must be filled");
			if(string.IsNullOrEmpty(saveData.matchId))
				throw new Exception("matchId cannot be empty");
		}
		
		protected override void ValidateDiskContent (TTSyncCollection<TTMatchScore> syncCollection, string identifier)
		{
			if(!syncCollection.type.Equals(_saveType))
				throw new Exception("File contains wrong format");
			if(!syncCollection.playerId.Equals(identifier))
				throw new Exception("The saved match score is not intended for this playerId");
		}
		
		protected override void PreprocessNewSave (ref TTMatchScore newSave, string identifier)
		{
			newSave.time = Utils.GetEpochTime(); //Always override timestamp to make sure it is not tampered
		}
		
		protected override void PreprocessCollection (ref TTSyncCollection<TTMatchScore> syncCollection, string identifier)
		{
			syncCollection.playerId = identifier;
			syncCollection.type = _saveType;
		}
	}
}