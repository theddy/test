namespace KadoSaku.Matchmaking.Sync {
	public class TTMatchScore {
		public string matchId;
		public double score;
		public int time;

		public TTMatchScore(string matchId, double score, int time){
			this.matchId = matchId;
			this.score = score;
			this.time = time;
		}

		public override string ToString ()
		{
			return 
				"matchId: " + matchId + "\n" +
				"score: " + score + "\n" +
				"time: " + time;
		}
}
}