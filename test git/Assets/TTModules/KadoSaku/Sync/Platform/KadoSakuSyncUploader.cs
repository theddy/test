using System;
using KadoSaku.Core.Crypto;
using KadoSaku.Core.Communication;
using KadoSaku.Api;
using UnityEngine;
using Newtonsoft.Json.Linq;

namespace KadoSaku.Sync
{
	public class KadoSakuSyncUploader<R> : ISyncUploader where R : IResult
	{
		public delegate void ApiCallHandler<R> (ParamRequest param, KadoSakuDelegate<R> callback);

		ApiCallHandler<IActionResult> apiCall;
		string ksAction;

		public KadoSakuSyncUploader(ApiCallHandler<IActionResult> apiCall, string ksAction) {
			this.apiCall = apiCall;
			this.ksAction = ksAction;
		}

		public void UploadToCloud (string json, Action<Exception, int> callback) {
			// Internal Callback
			KadoSakuDelegate<IActionResult> internalCallback = delegate (IActionResult result) {
				if (result.IsSuccess) { 
					KadoSaku.Utility.KSDebug.Log("[Sync] Success");
					callback(null, 200); 
				}
				else { 
					KadoSaku.Utility.KSDebug.LogError("[Sync] Error");
					callback(new Exception("Internal Error"), 500); 
				}
				return;
			};

			// Request
			JObject body = JObject.Parse(json);
			ParamRequest param = new ParamRequest(this.ksAction);
			param.AddBody("syncBody", body);
			this.apiCall(param, internalCallback);
		}
	}
}

