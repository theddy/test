using System.Collections.Generic;

namespace KadoSaku.Core.Crypto {
	public class TTSyncCollection<T> {
		public string playerId;
		public string type;
		public T[] data;

		public override string ToString ()
		{
			if (data == null) {
				return 
					"playerId: " + playerId + "\n" +
						"type: " + type;
			} else {
				return 
				"playerId: " + playerId + "\n" +
					"type: " + type + "\n" +
					"data: " + data.Length;
			}
		}
	}
}