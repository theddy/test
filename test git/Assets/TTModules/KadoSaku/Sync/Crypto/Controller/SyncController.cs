using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace KadoSaku.Core.Crypto {
	public abstract class SyncController<T> {
		/// <summary>
		/// The uploader class for uploading data to cloud.
		/// </summary>
		protected ISyncUploader _uploader;

		/// <summary>
		/// The type of the save file.
		/// </summary>
		protected string _saveType;

		/// <summary>
		/// Maximum number of save before it is truncated.
		/// </summary>
		protected int _maxSave;

		/// <summary>
		/// Initializes a new instance of the <see cref="KadoSaku.Core.Crypto.SyncController`1"/> class.
		/// </summary>
		/// <param name="uploader">Cloud uploader service.</param>
		public SyncController(ISyncUploader uploader){
			_uploader = uploader;
		}

		/// <summary>
		/// Save the specified saveData object.
		/// </summary>
		/// <param name="newSave">Data that will be saved.</param>
		/// <param name="identifier">Identifier.</param>
		/// <param name="callback">Returns the whole collection of data in the disk, or exception if error.</param>
		public virtual void Save(T newSave, string identifier, Action<Exception, T[]> callback) {
			try{
				ValidateSaveParameters(newSave, identifier);

				//Initialize variables
				string sharedKey = TTCryptoConfiguration.SharedKey;
				TTSyncCollection<T> syncCollection = new TTSyncCollection<T>();
				Queue<T> existingSaves = new Queue<T>();

				//Load existing data
				string filename = EncryptDiskService.GenerateFilename(_saveType, identifier);
				byte[] rawSaveFile = EncryptDiskService.LoadFromFile(filename);
				if(rawSaveFile != null && rawSaveFile.Length > 1){
					string JSONdata = EncryptDiskService.DecryptDiskData(rawSaveFile, sharedKey);
					syncCollection = JsonConvert.DeserializeObject<TTSyncCollection<T>>(JSONdata);
					ValidateDiskContent(syncCollection, identifier);
					existingSaves = new Queue<T>(syncCollection.data);
				}

				//Add new save
				PreprocessNewSave(ref newSave, identifier);
				existingSaves.Enqueue(newSave);

				//Trim old saves
				while(existingSaves.Count > _maxSave)
					existingSaves.Dequeue();
				syncCollection.data = existingSaves.ToArray();

				//Add extra info to the wrapper
				PreprocessCollection(ref syncCollection, identifier);

				//Encrypt saves
				rawSaveFile = EncryptDiskService.EncryptDiskData(JsonConvert.SerializeObject(syncCollection), sharedKey);
				EncryptDiskService.SaveToFile(filename, rawSaveFile);
				
				callback(null, existingSaves.ToArray());
			}
			catch(Exception e){
				callback(e, null);
			}
		}

		/// <summary>
		/// Load the saved object specified by its identifier and upload it to cloud.
		/// </summary>
		/// <param name="identifier">Identifier.</param>
		/// <param name="callback">Returns the whole collection of uploaded data, or exception if error.</param>
		public virtual void Sync(string identifier, Action<Exception, T[]> callback) {
			try{
				if(string.IsNullOrEmpty(identifier))
					throw new Exception("playerId cannot be empty");

				//Initialize variables
				string sharedKey = TTCryptoConfiguration.SharedKey;
				 
				TTSyncCollection<T> syncCollection = new TTSyncCollection<T>();
				Queue<T> existingSaves = new Queue<T>();
				T[] mergedSavesCollection = new T[0];

				//load authorized file
				string filename = EncryptDiskService.GenerateFilename(_saveType, identifier);
				byte[] rawSaveFile = EncryptDiskService.LoadFromFile(filename);

				if (rawSaveFile != null && rawSaveFile.Length > 1) {
					string JSONdata = EncryptDiskService.DecryptDiskData(rawSaveFile, sharedKey);
					syncCollection = JsonConvert.DeserializeObject<TTSyncCollection<T>>(JSONdata);
					mergedSavesCollection = syncCollection.data;
				}

				//load unauthorized file
				string filenameUnauthorized = EncryptDiskService.GenerateFilename(_saveType, string.Empty);
				byte[] rawSaveFileUnauthorized = EncryptDiskService.LoadFromFile(filenameUnauthorized);
				
				if (rawSaveFileUnauthorized != null && rawSaveFileUnauthorized.Length > 1) {
					string JSONdataUnauthorized = EncryptDiskService.DecryptDiskData(rawSaveFileUnauthorized, sharedKey);
					syncCollection = JsonConvert.DeserializeObject<TTSyncCollection<T>>(JSONdataUnauthorized);
					mergedSavesCollection = CombineData(mergedSavesCollection, syncCollection.data);
				}

				//send data t upload to cloud
				if(mergedSavesCollection.Length > 0){
					syncCollection.playerId = identifier;
					syncCollection.data = mergedSavesCollection;
					ValidateDiskContent(syncCollection, identifier);
					existingSaves = new Queue<T>(syncCollection.data);

					//Trim old saves
					while(existingSaves.Count > _maxSave)
						existingSaves.Dequeue();
					syncCollection.data = existingSaves.ToArray();
									
					_uploader.UploadToCloud(JsonConvert.SerializeObject(syncCollection), delegate(Exception err, int statusCode) {
						if (err != null) throw err;
						if (statusCode != 200) throw new Exception("Failed to sync data");
				
						EncryptDiskService.DeleteFile(filenameUnauthorized);
						EncryptDiskService.DeleteFile(filename);
						callback(null, existingSaves.ToArray());
					});
				}else{
					callback(null, existingSaves.ToArray());
				}
			}
			catch(Exception e){
				callback(e, null);
			}
		}

		private T[] CombineData(T[] signedSyncCollection, T[] unsignedSyncCollection)
		{
			T[] newData = new T[unsignedSyncCollection.Length + signedSyncCollection.Length];
			Array.Copy (unsignedSyncCollection, 0, newData, 0, unsignedSyncCollection.Length);
			Array.Copy (signedSyncCollection, 0, newData, unsignedSyncCollection.Length, signedSyncCollection.Length);
			return newData;
		}

		protected abstract void ValidateSaveParameters(T saveData, string identifier);

		protected abstract void ValidateDiskContent (TTSyncCollection<T> syncCollection, string identifier);

		protected abstract void PreprocessNewSave(ref T newSave, string identifier);

		protected abstract void PreprocessCollection(ref TTSyncCollection<T> syncCollection, string identifier);
	}
}