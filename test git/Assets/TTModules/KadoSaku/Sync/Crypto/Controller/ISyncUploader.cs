using System;

namespace KadoSaku.Core.Crypto {
	public interface ISyncUploader {
		/// <summary>
		/// Uploads encrypted data to cloud.
		/// </summary>
		/// <param name="json">Decrypted JSON to be encrypted in body.</param>
		/// <param name="callback">Callback. Includes HTTP status codes, 0 if none.</param>
		void UploadToCloud(string json, Action<Exception, int> callback);
	}
}