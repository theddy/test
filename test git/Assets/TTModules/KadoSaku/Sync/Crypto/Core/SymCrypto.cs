using System;

namespace KadoSaku.Core.Crypto
{
	public static class SymCrypto
	{
		/**
		 * Encrypt data with key
		 * @params {byte array} data
		 * @params {string} key , key to ecrypt data
		 * @params {bool} includeLength, will included encrypted length in array
		 * @params {bool} hexString, if we use hexa string as key
		 * @return {byte array} encrypted data
		 */
		public static byte[] Encrypt (byte[] data, string key, bool includeLength, bool hexString = false)
		{
			if(hexString) return Encrypt (data, Utils.HexStringToByteArray (key), includeLength);
			return Encrypt (data, StringToBytes (key), includeLength);
		}
		/**
		 * Encrypt data with key 
		 * @param {byte array} data, this array is contain data
		 * @param {byte array} key, this array is contain key
		 * @param {bool} includeLength, will included encrypted length in array
		 * @return {byte array} encrypted data
		 */
		public static byte[] Encrypt (byte[] data, byte[] key, bool includeLength)
		{
			if (!includeLength) return ToByteArray (ProcessEncrypt (ToIntArray (data), ToIntArray (key)));
			return AddDataLength (ToByteArray (ProcessEncrypt (ToIntArray (data), ToIntArray (key))));
		}

		/**
		 * Decrypt data with key
		 * @params {byte array} encrypted data
		 * @params {string} key , key to ecrypt data
		 * @params {bool} includeLength, will remove included encrypted length in array
		 * @params {bool} hexString, if we use hexa string as key
		 * @return {byte array} decrypted data
		 */
		public static byte[] Decrypt (byte[] data, string key, bool includeLength, bool hexString = false)
		{
			if(hexString) return Decrypt (data, Utils.HexStringToByteArray (key), includeLength);
			return Decrypt (data, StringToBytes (key), includeLength);
		}

		/**
		 * Decrypt data with key 
		 * @param {byte array} encrypted data, this array is contain data
		 * @param {byte array} key, this array is contain key
		 * @param {bool} includeLength, will remove included encrypted length in array
		 * @return {byte array} decrypted data
		 */
		public static byte[] Decrypt (byte[] data, byte[] key, bool includeLength)
		{
			if (!includeLength) return ToByteArray (ProcessDecrypt (ToIntArray (data), ToIntArray (key)));		 
			return ToByteArray (ProcessDecrypt (ToIntArray (RemoveDataLength(data)), ToIntArray (key)));
		}

		/**
		 * Encrypt data with key 
		 * @param {int array} data, this array is contain data
		 * @param {int array} key, this array is contain key
		 * @return {int array} encrypted data
		 */
		private static int[] ProcessEncrypt (int[] data, int[] key)
		{
			int n = data.Length - 1;
			
			if (n < 1)			
				return data;
			
			if (key.Length < 4) {
				int[] temp = new int[4];
				Array.Copy (key, 0, temp, 0, key.Length);
				key = temp;						
			}
			
			int z = data [n], y = data [0], sum = 0, e;
			int p, q = 6 + 52 / (n + 1);
			UInt32 delta = 0x9E3779B9;
			
			while (q-- > 0) {
				sum = sum + (int)delta;
				e = sum >> 2 & 3;
				for (p = 0; p < n; p++) {
					y = data [p + 1];
					z = data [p] += (z >> 5 ^ y << 2) + (y >> 3 ^ z << 4) ^ (sum ^ y) + (key [p & 3 ^ e] ^ z);
				}
				y = data [0];
				z = data [n] += (z >> 5 ^ y << 2) + (y >> 3 ^ z << 4) ^ (sum ^ y) + (key [p & 3 ^ e] ^ z);
			}
			
			return data;
		}
				
		/**
		 * Decrypt data with key 
		 * @param {int array} encrypted data, this array is contain data
		 * @param {int array} key, this array is contain key
		 * @return {int array} decrypted data
		 */
		private static int[] ProcessDecrypt (int[] data, int[] key)
		{
			int n = data.Length - 1;
					
			if (n < 1)			
				return data;
						
			if (key.Length < 4) {
				int[] temp = new int[4];
				Array.Copy (key, 0, temp, 0, key.Length);
				key = temp;	
			}
					
			int z = data [n], y = data [0], sum, e;
			int p, q = 6 + 52 / (n + 1);
			UInt32 delta = 0x9E3779B9;
					
			sum = (int)(q * delta);
					
			while (sum != 0) {
				e = sum >> 2 & 3;
				for (p = n; p > 0; p--) {
					z = data [p - 1];
					y = data [p] -= (z >> 5 ^ y << 2) + (y >> 3 ^ z << 4) ^ (sum ^ y) + (key [p & 3 ^ e] ^ z);
				}						
				z = data [n];
				y = data [0] -= (z >> 5 ^ y << 2) + (y >> 3 ^ z << 4) ^ (sum ^ y) + (key [p & 3 ^ e] ^ z);
				sum = (int)(sum - delta);
			}
			return data;
		}
		/**
		 * helper to convert string to bytes array
		 */
		private static byte[] StringToBytes (string s)
		{
			return System.Text.Encoding.UTF8.GetBytes (s);
		}
		/**
		 * helper to convert bytes array to string
		 */ 
		private static string BytesToString (byte[] byteData)
		{
			return System.Text.Encoding.UTF8.GetString (byteData);
		}
		/**
		 * helper to convert bytes array to ints array
		 */ 
		private static int[] ToIntArray (byte[] byteData)
		{
			//fixed occupied array length
			if (byteData.Length % 4 != 0) {
				byte[] temp = new byte[(int)Math.Ceiling ((double)byteData.Length / 4) * 4];
				Array.Copy (byteData, 0, temp, 0, byteData.Length);
				byteData = temp;
			}
			
			int[] intArray = new int[byteData.Length / 4];
			for (int i = 0; i < byteData.Length; i += 4)
				intArray [i / 4] = BitConverter.ToInt32 (byteData, i);
			return intArray;
		}
		/**
		 * helper to convert ints array to bytes array
		 */ 
		private static byte[] ToByteArray (int[] intData)
		{
			byte[] data = new byte[intData.Length * 4];
			for (int i = 0; i < intData.Length; i++)
				Array.Copy (BitConverter.GetBytes (intData [i]), 0, data, i * 4, 4);
			return data;
		}
		/**
		 * helper to added data length in array 
		 * @ilustration [90,87,67,34,1] became [5,0,0,0,90,87,67,34,1]
		 */
		private static byte[] AddDataLength (byte[] byteData)
		{
			byte[] data = new byte[byteData.Length + 4];
			byte[] byteLength = BitConverter.GetBytes (byteData.Length);
			Array.Copy (byteLength, 0, data, 0, byteLength.Length);
			Array.Copy (byteData, 0, data, 4, byteData.Length);
			return data;
		}
		/**
		 * helper to remove data length in array 
		 * @ilustration [5,0,0,0,90,87,67,34,1] became [90,87,67,34,1]
		 */
		private static byte[] RemoveDataLength (byte[] byteData)
		{
			byte[] data = new byte[byteData.Length - 4];		
			Array.Copy (byteData, 4, data, 0, (byteData.Length-4));
			return data;
		}
	}
}
