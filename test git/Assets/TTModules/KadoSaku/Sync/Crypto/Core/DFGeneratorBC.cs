using System;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Agreement;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;
using System.Text.RegularExpressions;

namespace KadoSaku.Core.Crypto
{	
	/**
	 * Class that will generate pair key (public and private) 
	 * and calculate sharedKey with other public key
	 */ 
	public class DFGeneratorBC
	{
		private ObscuredString _serverPrime = "5C4pF9myukWe23XA"; //DO NOT CHANGE THIS
		private DHBasicKeyPairGenerator _dhNew;
		private DHParameters _dhParams;
		private DHPublicKeyParameters _pu1;
		private DHPrivateKeyParameters _pv1;

		public DFGeneratorBC() {

			//new code
			byte[] aout = System.Text.Encoding.UTF8.GetBytes (_serverPrime);
			_dhParams = new DHParameters(new BigInteger(BitConverter.ToString (aout).Replace("-",""), 16), new BigInteger("2", 16));

			DHKeyGenerationParameters dhkgParams = new DHKeyGenerationParameters(new SecureRandom(), _dhParams);
			_dhNew = new DHBasicKeyPairGenerator();
			
			_dhNew.Init(dhkgParams);
		}

		/// <summary>
		/// Generates new keys.
		/// </summary>
		/// <returns>The new keys in as Dictionary [privateKey, publicKey, sharedKey].</returns>
		/// <param name="serverPublicKey">Server public key.</param>
		public Dictionary<string, string> GenerateNewKeys(string serverPublicKey) {
			//new code 
			AsymmetricCipherKeyPair pair = _dhNew.GenerateKeyPair();
			
			_pu1 = (DHPublicKeyParameters)pair.Public;
			_pv1 = (DHPrivateKeyParameters)pair.Private;
			Dictionary<string, string> resultNew = new Dictionary<string, string> ();
			resultNew.Add ("privateKey", _pv1.X.ToString(16));
			resultNew.Add ("publicKey", _pu1.Y.ToString(16));
			resultNew.Add ("sharedKey", ComputeSharedKey (serverPublicKey));

			return resultNew;
		}

		/// <summary>
		/// Based on existing keys, generates a shared key.
		/// </summary>
		/// <returns>The shared key.</returns>
		/// <param name="devicePublicKey">Device public key.</param>
		/// <param name="devicePrivateKey">Device private key.</param>
		/// <param name="serverPublicKey">Server public key.</param>
		public string GetSharedKey(string devicePublicKey, string devicePrivateKey, string serverPublicKey) {
			//new code

			_pu1 = new DHPublicKeyParameters(new BigInteger(devicePublicKey, 16),_dhParams);
			_pv1 = new DHPrivateKeyParameters(new BigInteger(devicePrivateKey, 16),_dhParams);

			return ComputeSharedKey (serverPublicKey);
		}

		private string ComputeSharedKey(string serverPublicKey) {				
			//new code
			DHPublicKeyParameters pu2 = new DHPublicKeyParameters(new BigInteger(serverPublicKey, 16),_dhParams);
			DHBasicAgreement e1 = new DHBasicAgreement();
			e1.Init(_pv1);
			BigInteger k1 = e1.CalculateAgreement(pu2);
			string sharedKey = BitConverter.ToString (k1.ToByteArray()).Replace("-","");

			return new Regex ("^(0{2})+").Replace (sharedKey, "");
		}
	}
}
