//using OpenSSL.Crypto;
using System;
using System.IO;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using KadoSaku.Settings;

namespace KadoSaku.Core.Crypto
{
	[Serializable]
	public class TTCryptoConfiguration
	{
		public const int MAX_SAVED_SCORES = 10;
		public const int MAX_SAVED_MATCHSCORES = 10;
		public const int MAX_SAVED_ACHIEVEMENTS = 10;

		private const string DEVICE_PUBLIC_KEY_PREF = "Touchten-DevicePublicKey";
		private const string DEVICE_PRIVATE_KEY_PREF = "Touchten-DevicePrivateKey";
		private const string DEVICE_SHARED_KEY_PREF = "Touchten-SharedKey";

		private static ObscuredString _serverPublicKey = KSCoreSettings.GAME_PUBLIC_KEY;
		private static ObscuredString _devicePublicKey = string.Empty;
		private static ObscuredString _devicePrivateKey = string.Empty;
		private static ObscuredString _sharedKey = string.Empty;

		/// <summary>
		/// Gets the device public key. Will try to load from PlayerPrefs or Generate new keys if not found.
		/// </summary>
		/// <value>The device public key.</value>
		public static string DevicePublicKey {
			get {
				if (string.IsNullOrEmpty(_devicePublicKey)){
					LoadKeysFromPrefs();
					if (string.IsNullOrEmpty(_devicePublicKey)) GenerateKeys();
				}

				return _devicePublicKey;
			}
		}

		/// <summary>
		/// Gets the shared key. Will try to load from PlayerPrefs or Generate new keys if not found.
		/// </summary>
		/// <value>The shared key.</value>
		public static string SharedKey {
			get {
				if (string.IsNullOrEmpty(_sharedKey)){
					LoadKeysFromPrefs();
					if (string.IsNullOrEmpty(_sharedKey)) GenerateKeys();
				}
				
				return _sharedKey;
			}
		}

		private static void LoadKeysFromPrefs() {
			if (string.IsNullOrEmpty (_serverPublicKey))
				throw new Exception ("Please enter Game App Public Key in Touchten Platform Settings");
			_devicePublicKey = ObscuredPrefs.GetString (DEVICE_PUBLIC_KEY_PREF);
			_devicePrivateKey = ObscuredPrefs.GetString (DEVICE_PRIVATE_KEY_PREF);
			if (!string.IsNullOrEmpty(_devicePublicKey) && !string.IsNullOrEmpty(_devicePrivateKey))
				_sharedKey = (new DFGeneratorBC ()).GetSharedKey (_devicePublicKey, _devicePrivateKey, _serverPublicKey);
		}

		private static void GenerateKeys() {
			Dictionary<string, string> keys = (new DFGeneratorBC()).GenerateNewKeys(_serverPublicKey);
			_devicePublicKey = keys ["publicKey"];
			_devicePrivateKey = keys ["privateKey"];
			_sharedKey = keys ["sharedKey"];

			ObscuredPrefs.SetString (DEVICE_PUBLIC_KEY_PREF, _devicePublicKey);
			ObscuredPrefs.SetString (DEVICE_PRIVATE_KEY_PREF, _devicePrivateKey);
		}
	}
}