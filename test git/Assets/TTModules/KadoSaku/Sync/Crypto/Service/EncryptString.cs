using System;

namespace KadoSaku.Core.Crypto
{
	public static class EncryptString
	{
		public static string Encrypt (string data) {
			return Utils.ByteArrayToBase64(SymCrypto.Encrypt (Utils.StringToByteArray (data), TTCryptoConfiguration.SharedKey, false, true));
		}
	}
}

