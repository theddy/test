using UnityEngine;
using System.Collections.Generic;
using KadoSaku.Core.Communication;
using KadoSaku.Settings;

namespace KadoSaku.Core.Crypto {
	public static class EncryptRequestService {
		public static IRequestString EncryptRequestString(IRequestString requestString){
			requestString.Headers = EncryptHeaders (requestString.Headers);
			requestString.Body = EncryptBody (requestString.Body);

			return requestString;
		}

		private static Dictionary<string, string> EncryptHeaders(Dictionary<string, string> headers){
			if (headers ["authorization"] != null)
				headers ["authorization"] = EncryptString.Encrypt(headers ["authorization"]);
			if (headers ["language"] != null)
				headers ["language"] = EncryptString.Encrypt(headers ["language"]);
			if (headers ["location"] != null)
				headers ["location"] = EncryptString.Encrypt(headers ["location"]);
			if (headers ["device-id"] != null)
				headers ["device-id"] = EncryptString.Encrypt(headers ["device-id"]);
			if (headers ["encrypted-content-type"] != null)
				headers ["encrypted-content-type"] = EncryptString.Encrypt(headers ["encrypted-content-type"]);

			headers ["timestamp"] = EncryptString.Encrypt(Utils.GetEpochTime ().ToString ());
			headers ["content-type"] = "text/plain";
			headers ["key"] = TTCryptoConfiguration.DevicePublicKey + ":" + KSCoreSettings.GAME_PUBLIC_KEY;

			return headers;
		}

		private static string EncryptBody(string body){
			if (string.IsNullOrEmpty(body)) return null;
			body = EncryptString.Encrypt(body);
			return body;
		}
	}
}	