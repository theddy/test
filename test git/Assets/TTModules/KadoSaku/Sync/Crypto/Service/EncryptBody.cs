using System;
using KadoSaku.Settings;
using KadoSaku.Core.Communication;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KadoSaku.Core.Crypto
{
	public static class EncryptBody
	{
		public static string Encrypt (JToken body) {
			string bodyString = JsonConvert.SerializeObject(body);
			return EncryptString.Encrypt(bodyString);
		} 
	}
}

