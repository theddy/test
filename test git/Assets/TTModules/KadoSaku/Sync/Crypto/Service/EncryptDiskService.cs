using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;

namespace KadoSaku.Core.Crypto
{
	public static class EncryptDiskService {
		private static string PATH_FILE = Application.persistentDataPath;

		/// <summary>
		/// Generates save filename according to the type and identifier.
		/// </summary>
		/// <returns>The filename.</returns>
		/// <param name="type">Type of file.</param>
		/// <param name="identifier">Identifier.</param>
		public static string GenerateFilename(string type, string identifier){
			if(string.IsNullOrEmpty(identifier))
				return type + ".sav";
			return type + "-" + identifier + ".sav";
		}

		/// <summary>
		/// Will load contents of the file.
		/// </summary>
		/// <returns>File contents in byte[]</returns>
		/// <param name="filename">Filename.</param>
		public static byte[] LoadFromFile(string filename){
			try{
				return File.ReadAllBytes (Path.Combine(PATH_FILE, filename));
			}
			catch(FileNotFoundException){
				return null;
			}
		}

		/// <summary>
		/// Will writes encrypted data to file.
		/// </summary>
		/// <param name="filename">Filename.</param>
		/// <param name="encryptedData">Encrypted data.</param>
		public static void SaveToFile(string filename, byte[] encryptedData){
			File.WriteAllBytes (Path.Combine (PATH_FILE, filename), encryptedData);
		}

		/// <summary>
		/// Deletes the file.
		/// </summary>
		/// <param name="filename">Filename.</param>
		public static void DeleteFile(string filename){
			try{
				File.Delete (Path.Combine (PATH_FILE, filename));
			}
			catch(FileNotFoundException){
				return;
			}
		}

		/// <summary>
		/// Decrypts the encrypted raw data and cast it back to string.
		/// </summary>
		/// <returns>String JSON containing the data.</returns>
		/// <param name="encryptedData">Encrypted data.</param>
		/// <param name="sharedKey">Shared key.</param>
		public static string DecryptDiskData(byte[] encryptedData, string sharedKey){
			return Utils.ByteArrayToString(SymCrypto.Decrypt (encryptedData, sharedKey, false, true));
		}

		/// <summary>
		/// Will convert data in string into encrypted bytes of data.
		/// </summary>
		/// <returns>The encrypted content in array.</returns>
		/// <param name="data">Decrypted data.</param>
		/// <param name="sharedKey">Shared key.</param>
		public static byte[] EncryptDiskData(string data, string sharedKey){
			return SymCrypto.Encrypt (Utils.StringToByteArray (data), sharedKey, false, true);
		}
	}
}
