using System;
using KadoSaku.Settings;
using KadoSaku.Core.Communication;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace KadoSaku.Core.Crypto
{
	public static class EncryptHeaders
	{
		public static void Encrypt (ref JObject header) {
			UpdateHeaders(ref header);
			EncryptDefaultHeaders(ref header);
		}

		static void UpdateHeaders (ref JObject header) {
			header.Add(new JProperty("Encrypted-Content-Type", EncryptString.Encrypt((string)header["Content-Type"])));
			header.Property("Content-Type").Remove();
			header.Add(new JProperty("Content-Type", "text/plain"));
			header.Add(new JProperty("Timestamp", EncryptString.Encrypt(Utils.GetEpochTime ().ToString ())));
			header.Add(new JProperty("Key", TTCryptoConfiguration.DevicePublicKey + ":" + KSCoreSettings.GAME_PUBLIC_KEY));
		}
		
		static void EncryptDefaultHeaders (ref JObject header) {
			header["Authorization"] = EncryptString.Encrypt((string)header["Authorization"]);
			header["Language"] = EncryptString.Encrypt((string)header["Language"]);
			header["Location"] = EncryptString.Encrypt((string)header["Location"]);
			header["Device-ID"] = EncryptString.Encrypt((string)header["Device-ID"]);
		}
	}
}

