using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace KadoSaku.Core.Crypto
{	
	public class Utils
	{
		public static byte[] StringToByteArray (string s)
		{
			return System.Text.Encoding.UTF8.GetBytes (s);
		}
		
		public static string ByteArrayToString (byte[] byteData)
		{
			return System.Text.Encoding.UTF8.GetString (byteData);
		}
		
		public static int[] ByteArrayToIntArray (byte[] byteData)
		{
			//fixed occupied array length
			if (byteData.Length % 4 != 0) {
				byte[] temp = new byte[(int)Math.Ceiling ((double)byteData.Length / 4) * 4];
				Array.Copy (byteData, 0, temp, 0, byteData.Length);
				byteData = temp;
			}
			
			int[] intArray = new int[byteData.Length / 4];
			for (int i = 0; i < byteData.Length; i += 4)
				intArray [i / 4] = BitConverter.ToInt32 (byteData, i);
			return intArray;
		}
		
		public static byte[] IntArrayToByteArray (int[] intData)
		{
			byte[] data = new byte[intData.Length * 4];
			for (int i = 0; i < intData.Length; i++)
				Array.Copy (BitConverter.GetBytes (intData [i]), 0, data, i * 4, 4);
			return data;
		}

		public static int ByteArrayToInt(byte[] ba){
			return BitConverter.ToInt32 (ba, 0);
		}

		public static byte[] IntToByteArray(int intData){
			return BitConverter.GetBytes (intData);
		}
		
		public static byte[] Base64toByteArray(string base64){
			return System.Convert.FromBase64String (base64);
		}
		
		public static string ByteArrayToBase64(byte[] ba){
			return System.Convert.ToBase64String (ba);
		}
		
		public static byte[] HexStringToByteArray (string hex)
		{
			int NumberChars = hex.Length;
			byte[] bytes = new byte[NumberChars / 2];
			for (int i = 0; i < NumberChars; i += 2)
				bytes [i / 2] = System.Convert.ToByte (hex.Substring (i, 2), 16);
			return bytes;
		}
		
		public static string ByteArrayToHexString (byte[] ba)
		{
			string hex = System.BitConverter.ToString (ba);
			return hex.Replace ("-", "");
		}
		
		// Convert an object to a byte array
		public static byte[] ObjectToByteArray (object obj)
		{
			BinaryFormatter bf = new BinaryFormatter ();
			using (MemoryStream ms = new MemoryStream()) {
				bf.Serialize (ms, obj);
				return ms.ToArray ();
			}
		}
		// Convert a byte array to an Object
		public static object ByteArrayToObject (byte[] arrBytes)
		{
			using (MemoryStream memStream = new MemoryStream()) {
				BinaryFormatter binForm = new BinaryFormatter ();
				memStream.Write (arrBytes, 0, arrBytes.Length);
				memStream.Seek (0, SeekOrigin.Begin);
				object obj = (object)binForm.Deserialize (memStream);
				return obj;
			}
		}
		
		public static byte[] AddDataToBundle (byte[] Bundle, byte[] addData)
		{
			byte[] newData = new byte[Bundle.Length + addData.Length];
			Array.Copy (Bundle, 0, newData, 0, Bundle.Length);
			Array.Copy (addData, 0, newData, Bundle.Length, addData.Length);
			return newData;
		}

		public static byte[] RemoveDataFromBundle (byte[] byteData, int startIndex, int lenght)
		{
			byte[] data = new byte[byteData.Length - lenght];		
			Array.Copy (byteData, lenght, data, 0, (byteData.Length - lenght));	
			return data;
		}

		public static string NameToHex(string name){
			return Utils.ByteArrayToHexString (System.Text.Encoding.UTF8.GetBytes (name));
		}
		
		public static int GetEpochTime(){
			return (int)(System.DateTime.UtcNow - new System.DateTime(1970, 1, 1)).TotalSeconds;
		}
		
		public static DateTime EpochToDateTime(int epoch) {
			return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(epoch).ToUniversalTime(); 
		}

		public static int DateTimeToEpoch(DateTime dateTime) {
			return (int)(dateTime - new DateTime(1970, 1, 1)).TotalSeconds;
		}
	}
}