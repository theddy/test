namespace KadoSaku.CloudSave {
	public class TTSaveGame {
		public string playerId;
		public string data;
		public int timestamp;
		
		public TTSaveGame(string playerId, string data, int timestamp){
			this.playerId = playerId;
			this.data = data;
			this.timestamp = timestamp;
		}
		
		public override string ToString ()
		{
			return 
				"playerId: " + playerId + "\n" +
					"data: " + data + "\n" +
					"time: " + timestamp;
		}
	}
}