using System;
using System.Collections.Generic;

namespace KadoSaku.CloudSave {
	public delegate void GetResponseHandler(Exception err, int statusCode, Dictionary<string, string> headers, string body);
	public delegate void GetPlayerChoiceHandler(SaveLocationOption playerChoice);

	public enum SaveLocationOption {
		Cloud,
		Local
	}

	public interface ISaveConnector {
		/// <summary>
		/// Uploads encrypted save data to cloud.
		/// </summary>
		/// <param name="filename">Filename.</param>
		/// <param name="data">Decrypted JSON savegame to be encrypted in body.</param>
		/// <param name="callback">Callback. Also returns the headers and body of the response if successful.</param>
		void UploadToCloud(string filename, string data, GetResponseHandler callback);

		/// <summary>
		/// Downloads the file metadata.
		/// </summary>
		/// <param name="filename">Filename.</param>
		/// <param name="callback">Callback. Only returns the headers of the response. Body should be empty here.</param>
		void DownloadMetadata(string filename, GetResponseHandler callback);

		/// <summary>
		/// Downloads save game from cloud.
		/// </summary>
		/// <param name="filename">Filename.</param>
		/// <param name="callback">Callback. Includes the actual data in body as string.</param>
		void DownloadFromCloud(string filename, GetResponseHandler callback);

		/// <summary>
		/// Prompts the user to choose save game.
		/// </summary>
		/// <param name="saveTimes">Save game timestamps.</param>
		/// <param name="callback">Callback. Returns the player's choice.</param>
		void PromptChooseSaveGame(DateTime[] saveTimes, GetPlayerChoiceHandler callback);
	}
}