using System;
using Newtonsoft.Json;
using CodeStage.AntiCheat.ObscuredTypes;
using KadoSaku.Core.Crypto;

namespace KadoSaku.CloudSave {
	public class CloudSaveController {
		private ISaveConnector _connector;
		private IPlayerChecker _playerChecker;
		private string _saveType;

		public CloudSaveController(ISaveConnector connector, IPlayerChecker playerChecker){
			_connector = connector;
			_playerChecker = playerChecker;
			_saveType = "save";
		}

		public void SaveGame(string name, string data, Action<Exception> callback){
			try{
				ObscuredString encryptedData = data;
				data = string.Empty;

				if (_playerChecker.IsLoggedIn ()) {
					_connector.UploadToCloud(name, encryptedData, delegate(Exception err, int statusCode, System.Collections.Generic.Dictionary<string, string> headers, string body) {
						if(err != null) throw err;
						if(statusCode != 201) throw new Exception("Failed to upload save game");

						SaveToFile(name, new TTSaveGame(_playerChecker.GetPlayerId(), encryptedData.GetEncrypted(), Utils.DateTimeToEpoch(DateTime.Parse(headers["last-modified"]))));
						callback(null);
					});
				}
				else {
					SaveToFile(name, new TTSaveGame(null, encryptedData.GetEncrypted(), Utils.DateTimeToEpoch(DateTime.UtcNow)));
					callback(null);
				}
			}
			catch (Exception e){
				callback(e);
			}
		}

		public void LoadGame(string name, Action<Exception, string> callback){
			try{
				TTSaveGame localSave = LoadFromFile(name);
				ObscuredString localSaveData = localSave == null ? null : localSave.data;

				//Don't load if savegame is marked for another user
				if (localSave != null && !string.IsNullOrEmpty (localSave.playerId) && !localSave.playerId.Equals (_playerChecker.GetPlayerId()))
					localSaveData = null;

				if (_playerChecker.IsLoggedIn()) {
					_connector.DownloadMetadata(name, delegate(Exception err, int statusCode, System.Collections.Generic.Dictionary<string, string> headers, string body) {
						if(err != null) throw err;

						//Cloud save exist
						if(statusCode == 200){
							//Local save does not exist or is for other player
							if(localSave == null || localSaveData == null) LoadFromCloud(name, callback);
							//Local save exist and valid
							else{
								int cloudSaveEpoch = Utils.DateTimeToEpoch(DateTime.Parse(headers["last-modified"]));

								//Local save game is from anonymous user
								if(string.IsNullOrEmpty(localSave.playerId)) PromptSaveGameToUser(name, localSave, cloudSaveEpoch, callback); 
								//Local save is newer
								else if(localSave.timestamp > cloudSaveEpoch) PromptSaveGameToUser(name, localSave, cloudSaveEpoch, callback);
								//Cloud save is newer
								else LoadFromCloud(name, callback);
							}
						}
						//Cloud save does not exist
						else
							callback(null, localSaveData);
					});
				}
				else
					callback(null, localSaveData);
			}
			catch (Exception e){
				callback(e, null);
			}
		}

		private void SaveToFile(string name, TTSaveGame newSave){
			byte[] rawSaveFile = EncryptDiskService.EncryptDiskData (JsonConvert.SerializeObject (newSave), TTCryptoConfiguration.SharedKey);
			string filename = EncryptDiskService.GenerateFilename (_saveType, name);
			EncryptDiskService.SaveToFile (filename, rawSaveFile);

			return;
		}

		private TTSaveGame LoadFromFile(string name){
			string filename = EncryptDiskService.GenerateFilename (_saveType, name);
			byte[] rawSaveFile = EncryptDiskService.LoadFromFile (filename);
			TTSaveGame localSavegame = null;

			if (rawSaveFile != null && rawSaveFile.Length > 1) {
				localSavegame = JsonConvert.DeserializeObject<TTSaveGame> (EncryptDiskService.DecryptDiskData (rawSaveFile, TTCryptoConfiguration.SharedKey));

				ObscuredString decryptedData = string.Empty;
				decryptedData.SetEncrypted (localSavegame.data);
				localSavegame.data = decryptedData;
			}

			return localSavegame;
		}

		private void LoadFromCloud(string name, Action<Exception, string> callback){
			_connector.DownloadFromCloud (name, delegate(Exception err, int statusCode, System.Collections.Generic.Dictionary<string, string> headers, string body) {
				if(err != null) throw err;
				if(statusCode != 200) throw new Exception("There is an error when downloading your save game");

				callback(null, body);
			});
		}

		private void PromptSaveGameToUser(string name, TTSaveGame localSavegame, int cloudSaveEpoch, Action<Exception, string> callback){
			DateTime[] saveTimestamps = new DateTime[2];
			saveTimestamps [0] = Utils.EpochToDateTime (cloudSaveEpoch);
			saveTimestamps [1] = Utils.EpochToDateTime (localSavegame.timestamp);

			_connector.PromptChooseSaveGame (saveTimestamps, delegate(SaveLocationOption playerChoice) {
				switch(playerChoice){
				case SaveLocationOption.Cloud:
					LoadFromCloud(name, callback);
					break;
				case SaveLocationOption.Local:
					callback(null, localSavegame.data);
					break;
				default:
					throw new Exception("Unknown response from user");
				}
			});
		}
	}
}