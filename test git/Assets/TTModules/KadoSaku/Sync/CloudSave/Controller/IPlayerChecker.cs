namespace KadoSaku.CloudSave {
	public interface IPlayerChecker{
		bool IsLoggedIn();
		string GetPlayerId();
	}
}