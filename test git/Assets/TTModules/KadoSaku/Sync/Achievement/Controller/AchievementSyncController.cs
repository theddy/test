using System;
using KadoSaku.Core.Crypto;

namespace KadoSaku.Achievement.Sync
{
	public class AchievementSyncController : SyncController<TTAchievement> {
		public AchievementSyncController(ISyncUploader cloudUploader) : base (cloudUploader){
			_saveType = "achievement";
			_maxSave = TTCryptoConfiguration.MAX_SAVED_ACHIEVEMENTS;
		}
		
		protected override void ValidateSaveParameters (TTAchievement saveData, string identifier)
		{
			if(saveData == null)
				throw new Exception("saveData must be filled");
			if(string.IsNullOrEmpty(saveData.achievementId))
				throw new Exception("achievementId cannot be empty");
		}
		
		protected override void ValidateDiskContent (TTSyncCollection<TTAchievement> syncCollection, string identifier)
		{
			if(!syncCollection.type.Equals(_saveType))
				throw new Exception("File contains wrong format");
			if(!syncCollection.playerId.Equals(identifier))
				throw new Exception("The saved match score is not intended for this playerId");
		}
		
		protected override void PreprocessNewSave (ref TTAchievement newSave, string identifier)
		{
			newSave.time = Utils.GetEpochTime(); //Always override timestamp to make sure it is not tampered
		}
		
		protected override void PreprocessCollection (ref TTSyncCollection<TTAchievement> syncCollection, string identifier)
		{
			syncCollection.playerId = identifier;
			syncCollection.type = _saveType;
		}
	}
}