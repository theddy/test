namespace KadoSaku.Achievement.Sync {
	public class TTAchievement {
		public string achievementId;
		public int time;

		public TTAchievement(string achievementId, int time){
			this.achievementId = achievementId;
			this.time = time;
		}

		public override string ToString ()
		{
			return 
				"achievementId: " + achievementId + "\n" +
				"time: " + time;
		}
	}
}