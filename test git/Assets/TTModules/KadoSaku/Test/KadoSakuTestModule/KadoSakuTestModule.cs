#define KADOSAKU_TEST
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Touchten.TestScene
{
	public class KadoSakuTestModule : ITestModule
	{
		static KadoSakuTestModule _instance;

		static KadoSakuTestModule Instance {
			get {
				if (_instance == null) {
					_instance = new KadoSakuTestModule ();
				}
				return _instance;
			}
		}

		public static void Init()
		{
			KadoSaku.Api.KS.InitCompleteCallback = delegate(bool obj) {
				TestSceneEvent.OnKadoSakuInitComplete (obj);
			};
			Instance.MenuButtonResourcePath = new List<string> (){ "KadoSakuMenuButton" };
			Instance.ModuleMenuResourcePath = new List<string> (){ "AchievementMenu","KadoSakuMenu","LeaderboardMenu",
			"MatchmakingMenu"};

		}
		public static IList<string> GetMenuButtonResourcePath ()
		{
			return Instance.MenuButtonResourcePath;
		}

		public static IList<string> GetModuleMenuResourcePath ()
		{
			return Instance.ModuleMenuResourcePath;
		}

		public IList<string>  MenuButtonResourcePath {
			get ;
			private set;
		}

		public IList<string>  ModuleMenuResourcePath {
			get;
			private set;
		}
	}
}