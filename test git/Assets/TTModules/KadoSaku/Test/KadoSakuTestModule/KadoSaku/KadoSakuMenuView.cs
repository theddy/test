using UnityEngine;
using System.Collections;
namespace Touchten.TestScene{
public class KadoSakuMenuView : MenuBase {

	void Awake()
	{
		MenuEvent.OpenKadoSakuMenuE+= MenuEvent_OpenKadoSakuMenuE;
	}

	void MenuEvent_OpenKadoSakuMenuE ()
	{
		ActivateMenu();
	}

	void OnDestroy()
	{
		MenuEvent.OpenKadoSakuMenuE-= MenuEvent_OpenKadoSakuMenuE;

	}
}
}