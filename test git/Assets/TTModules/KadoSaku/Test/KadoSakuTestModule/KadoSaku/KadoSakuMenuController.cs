using UnityEngine;
using System.Collections;
using KadoSaku.Api;
namespace Touchten.TestScene{
[RequireComponent(typeof(KadoSakuMenuView))]
public class KadoSakuMenuController : MonoBehaviour
{

	public void Login ()
	{
		KS.Authentication.ShowLoginView (delegate(IActionResult result) {
			KadoSaku.Utility.KSDebug.Log ("Login Result = " + result.ToString ());	
		});
	}

	public void Logout()
	{
		KS.Authentication.Logout(delegate(IActionResult result) {
			KadoSaku.Utility.KSDebug.Log("Logout Result = "+result.ToString());	
		});
	}

	public void Unlink()
	{
		KS.Authentication.UnlinkSocialAndLogout(delegate(IActionResult result) {
			KadoSaku.Utility.KSDebug.Log("Unlink Result = "+result.ToString());	
		});
	}

	public void ShowReward()
	{
		KS.Reward.ShowView();
	}

	public void SendAchievement()
	{
		MenuEvent.OpenAchievementMenu();
	}

	public void UpdatePlayerProfile()
	{
		KS.User.UpdatePlayerProfile(delegate(IPlayerProfileResult result) {
			KadoSaku.Utility.KSDebug.Log("Update player profile result : "+result.ToString());	
		});
	}

	public void GetKadoSakuFriends()
	{
		KadoSaku.Api.KS.Utility.GetKadoSakuFriends(delegate(IFriendsResult result) {
			KadoSaku.Utility.KSDebug.Log("Get kadosaku friend result = " +result.ToString());	
		});
	}

	public void Leaderboard()
	{
		MenuEvent.OpenLeaderboardMenu();
	}

	public void GetAnnouncement()
	{
		KS.Mail.CheckAnnouncements(delegate(IMailResult result) {
			KadoSaku.Utility.KSDebug.Log("Announcement Result : "+result.ToString());	
		});
	}

	public void GetMail()
	{
		KS.Mail.CheckInbox(delegate(IMailResult result) {
			KadoSaku.Utility.KSDebug.Log("Mail Result : " +result.ToString());	
		});
	}


	public void CreateMatchmaking()
	{
		MenuEvent.OpenMatchmakingMenu();

	}

	public void GetServerTime()
	{
		KS.Utility.GetServerTime(delegate(ITimeResult result) {
			KadoSaku.Utility.KSDebug.Log("Get server time : " +result.ToString());	
		});
	}

	#region CloudSave
	const string fileName = "cloudsavefile";

	string fileContent = "lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ";
	public void UploadSave()
	{
		KS.CloudSave.UploadSave(fileName,fileContent,delegate(IActionResult result) {
			KadoSaku.Utility.KSDebug.Log("Upload Save Result : "+ result.ToString());	
		});
	}


	public void DownloadSave()
	{
		KS.CloudSave.DownloadSave(fileName,delegate(IDownloadSaveResult result) {
			KadoSaku.Utility.KSDebug.Log("Download result : " +result.ToString());
		});
	}

	public void DownloadMetaData()
	{
		KS.CloudSave.DownloadMetaData(fileName,delegate(IDownloadMetaDataResult result) {
			KadoSaku.Utility.KSDebug.Log("Meta Data result : " +result.ToString());
		});
	}

	public void DeleteSave()
	{
		KS.CloudSave.DeleteSave(fileName,delegate(IActionResult result) {
			KadoSaku.Utility.KSDebug.Log("Delete Result : "+result.ToString());	
		});
	}
	#endregion
}
}
