using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using KadoSaku.Api;
namespace Touchten.TestScene{
public class MatchmakingAccept : MonoBehaviour
{

	string id;
	string name;
	string leaderboardID;

	public Text textId;
	public Text textName;


	void Start ()
	{
		GetComponent<Button> ().onClick.AddListener (delegate() {
			AcceptMatchmaking ();	
		});
	}

	public void Init (string name, string id, string leaderboardId)
	{
		this.id = id;
		this.name = name;
		this.leaderboardID = leaderboardID;

		textId.text = id;
		textName.text = name;
	}

	public void AcceptMatchmaking ()
	{
		KS.Matchmaking.ArchiveMatch (id, "", delegate(IActionResult result) {
			KadoSaku.Utility.KSDebug.Log ("Accept match result : " + result.ToString ());	
		});
	}


}
}