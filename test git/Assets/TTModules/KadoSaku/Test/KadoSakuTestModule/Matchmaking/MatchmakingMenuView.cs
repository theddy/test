using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Touchten.TestScene{
public class MatchmakingMenuView : MenuBase
{

	public Transform buttonParent;
	public Transform acceptParent;

	void Start ()
	{
		MenuEvent.OpenMatchmakingMenuE += MenuEvent_OpenMatchmakingMenuE;
	}

	void OnDestroy ()
	{
		MenuEvent.OpenMatchmakingMenuE -= MenuEvent_OpenMatchmakingMenuE;
	}

	void MenuEvent_OpenMatchmakingMenuE ()
	{
		ActivateMenu ();
	}




	public void SetMatchmakingItem (List<GameObject> goList)
	{
		foreach (var go in goList) {
			go.transform.SetParent (buttonParent);
			go.transform.localScale = Vector3.one;
		}
	}


	public void SetAcceptItem (List<GameObject> goList)
	{
		foreach (var go in goList) {
			go.transform.SetParent (acceptParent);
			go.transform.localScale = Vector3.one;
		}
	}
}
}