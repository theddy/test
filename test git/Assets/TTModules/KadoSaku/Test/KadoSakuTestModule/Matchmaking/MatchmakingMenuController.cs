using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using KadoSaku.Api;
namespace Touchten.TestScene{
[RequireComponent (typeof(MatchmakingMenuView))]
public class MatchmakingMenuController : MonoBehaviour
{

	public string LeaderboardID = "123123123123123123123";
	MatchmakingMenuView menuView;
	public GameObject CreateMatchmakingPrefab;
	public GameObject AcceptMatchmakingPrefab;


	List<GameObject> createMatchmakingItemList;
	void Awake ()
	{
		menuView = GetComponent<MatchmakingMenuView>();
		menuView.OnActivated = delegate() {
			InitCreateMatchmaking ();
		};
	}

	void InitCreateMatchmaking ()
	{
		if(createMatchmakingItemList!=null)
		{
			foreach(var item in createMatchmakingItemList)
			{
				GameObject.Destroy(item);
			}
		}
		List<GameObject> goList = new List<GameObject> ();
		KS.Utility.GetKadoSakuFriends (delegate(IFriendsResult result) {
			if (result.Error == null) {
				foreach (var friend in result.FriendList) {
					GameObject go = Instantiate<GameObject> (CreateMatchmakingPrefab);
					go.GetComponent<MatchmakingItem> ().Init (friend.Name, friend.Id, LeaderboardID);
					goList.Add (go);
				}
				menuView.SetMatchmakingItem (goList);
				createMatchmakingItemList = goList;
			}
		});

	}

	void InitAcceptMatchmaking ()
	{
		List<GameObject> goList = new List<GameObject> ();
		KS.Matchmaking.GetMatchNotification (LeaderboardID, delegate(IMatchNotificationResult result) {
			if (result.Error == null) {
				foreach (var match in result.MatchList) {
					
					GameObject go = Instantiate<GameObject> (CreateMatchmakingPrefab);
					go.GetComponent<MatchmakingAccept> ().Init (match.Attacker.Name, match.Id, LeaderboardID);
					goList.Add (go);

				}
				menuView.SetAcceptItem (goList);
			}
		});

	}
}
}