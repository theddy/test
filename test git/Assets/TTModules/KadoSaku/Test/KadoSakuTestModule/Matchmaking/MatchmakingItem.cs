using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using KadoSaku.Api;
namespace Touchten.TestScene{
[RequireComponent(typeof(Button))]
public class MatchmakingItem : MonoBehaviour {

	string id;
	string name;
	string leaderboardID;

	public Text textId;
	public Text textName;


	void Start()
	{
		GetComponent<Button>().onClick.AddListener(delegate() {
			CreateMatchmaking();	
		});
	}
	public void Init(string name, string id,string leaderboardId)
	{
		this.id = id;
		this.name = name;
		this.leaderboardID = leaderboardID;

		textId.text = id;
		textName.text = name;
	}


	public void CreateMatchmaking()
	{
		MatchRequestModel req = new MatchRequestModel(id,leaderboardID,"");
		KS.Matchmaking.CreateNewMatch(req,delegate(IActionResult result) {
			KadoSaku.Utility.KSDebug.Log("Create Matchmaking result : "+ result.ToString());	
		});
//		KS.Matchmaking.c

	}

}
}