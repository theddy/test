using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Touchten.TestScene{
[RequireComponent (typeof(LeaderboardMenuView))]
public class LeaderboardMenuController : MonoBehaviour
{
	public GameObject leaderboardButtonPrefab;

	LeaderboardMenuView menuView;
	// Use this for initialization
	void Start ()
	{
		menuView = GetComponent<LeaderboardMenuView> ();
		TestSceneEvent.OnTTCoreInitCompleteE += TestSceneEvent_OnTTCoreInitCompleteE;
	}

	void TestSceneEvent_OnTTCoreInitCompleteE (bool success)
	{
		List<GameObject> goList = new List<GameObject>();
		foreach(var record in KadoSaku.Settings.KSLeaderboardSettings.GetAllLeaderboard())
		{
			GameObject go = Instantiate<GameObject>(leaderboardButtonPrefab);
			go.GetComponent<LeaderboardItem>().Init(record.leaderboardKey,record.leaderboardID);
			goList.Add(go);
		}
		menuView.SetLeaderboardItem(goList);
	}
	
	// Update is called once per frame
	void OnDestroy ()
	{
		TestSceneEvent.OnTTCoreInitCompleteE -= TestSceneEvent_OnTTCoreInitCompleteE;
	
	}
}
}