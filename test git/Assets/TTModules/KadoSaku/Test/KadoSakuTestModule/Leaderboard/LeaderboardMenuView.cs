using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Touchten.TestScene{
public class LeaderboardMenuView : MenuBase {

	public Transform buttonParent;

	void Awake()
	{
		MenuEvent.OpenLeaderboardMenuE+= MenuEvent_OpenLeaderboardMenuE;
	}

	void MenuEvent_OpenLeaderboardMenuE ()
	{
		ActivateMenu();
	}

	void OnDestroy()
	{
		MenuEvent.OpenLeaderboardMenuE-= MenuEvent_OpenLeaderboardMenuE;

	}


	public void SetLeaderboardItem(List<GameObject> goList)
	{
		foreach(var go in goList)
		{
			go.transform.SetParent(buttonParent);
			go.transform.localScale = Vector3.one;
		}
	}
}
}