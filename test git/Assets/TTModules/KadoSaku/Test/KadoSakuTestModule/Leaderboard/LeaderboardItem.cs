using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using KadoSaku.Api;
namespace Touchten.TestScene{
[RequireComponent(typeof(Button))]
public class LeaderboardItem : MonoBehaviour {

	public Text textKey;
	public Text textId;

	string key;
	string id;

	void Start()
	{
		GetComponent<Button>().onClick.AddListener(delegate() {
			GetData();	
		});
	}
	public void Init(string key, string id)
	{
		this.key = key;
		this.id = id;

		textKey.text = key;
		textId.text = id;
	}


	public void GetData()
	{
		KS.Leaderboard.GetGlobalScores(id,LeaderboardTimeFrame.AllTime,delegate(ILeaderboardScoresResult result) {
			KadoSaku.Utility.KSDebug.Log("Leaderboard Result = " +result.ToString());
		});
	}
}
}