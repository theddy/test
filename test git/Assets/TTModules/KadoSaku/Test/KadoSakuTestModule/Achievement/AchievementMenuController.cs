using UnityEngine;
using System.Collections;
using KadoSaku.Api;
using System.Collections.Generic;

namespace Touchten.TestScene{
[RequireComponent(typeof(AchievementMenuView))]
public class AchievementMenuController : MonoBehaviour
{

	public GameObject AchievementTriggerPrefab;
	AchievementMenuView menuView;

	void Awake ()
	{
		menuView = GetComponent<AchievementMenuView>();
		TestSceneEvent.OnTTCoreInitCompleteE += TestSceneEvent_OnTTCoreInitCompleteE;

	}

	void OnDestroy ()
	{
		TestSceneEvent.OnTTCoreInitCompleteE -= TestSceneEvent_OnTTCoreInitCompleteE;

	}

	void TestSceneEvent_OnTTCoreInitCompleteE (bool success)
	{
		List<GameObject> goList = new List<GameObject>();
		foreach(var record in KadoSaku.Settings.KSAchievementSettings.GetAllAchievement())
		{
			GameObject go = Instantiate<GameObject>(AchievementTriggerPrefab);
			go.GetComponent<AchievementTriggerButton>().Init(record.achievementKey,record.achievementId);
			goList.Add(go);
		}
		menuView.SetAchievementTriggers(goList);


	}


}
}