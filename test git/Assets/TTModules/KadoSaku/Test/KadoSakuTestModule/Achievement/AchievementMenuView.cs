using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace Touchten.TestScene{
public class AchievementMenuView : MenuBase {

	void Awake()
	{
		MenuEvent.OpenAchievementMenuE += MenuEvent_OpenAchievementMenuE;
	}

	void MenuEvent_OpenAchievementMenuE ()
	{
		ActivateMenu();
	}


	void OnDestroy()
	{
		MenuEvent.OpenAchievementMenuE -= MenuEvent_OpenAchievementMenuE;

	}




	public Transform triggerParent;
	public void SetAchievementTriggers(List<GameObject> goList)
	{
		foreach(var go in goList)
		{
			go.transform.SetParent(triggerParent);
			go.transform.localScale = Vector3.one;
		}
	}
}
}