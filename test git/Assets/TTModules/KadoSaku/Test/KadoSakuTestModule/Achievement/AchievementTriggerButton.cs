using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using KadoSaku.Api;
namespace Touchten.TestScene{
[RequireComponent (typeof(Button))]
public class AchievementTriggerButton : MonoBehaviour
{
	public Text textKey;
	public Text textID;

	string id;
	string key;
	void Start()
	{
		GetComponent<Button>().onClick.AddListener(delegate() {
			Trigger();
		});

	}
	public void Init (string key, string id)
	{
		this.id = id;
		this.key = key;

		textKey.text = "Key : " + key;
		textID.text = "ID : " + id;

	}

	public void Trigger ()
	{
		KS.Achievement.TriggerAchievement (id, delegate(IAchievementTransactionResult result) {
			KadoSaku.Utility.KSDebug.Log ("Achievement Trigger Result = " + result.ToString ());	
		});
	}
}
}