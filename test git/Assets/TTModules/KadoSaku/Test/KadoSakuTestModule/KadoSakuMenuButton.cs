using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Touchten.TestScene
{
	public class KadoSakuMenuButton : MonoBehaviour
	{

		StateIndicator indicator;

		void Start ()
		{
			indicator = GetComponent<StateIndicator> ();
			TestSceneEvent.OnKadoSakuInitCompleteE += TestSceneEvent_OnKadoSakuInitCompleteE;
			GetComponent<Button>().onClick.AddListener(delegate() {
				MenuEvent.OpenKadoSakuMenu();	
			});
		}

		void TestSceneEvent_OnKadoSakuInitCompleteE (bool success)
		{
			indicator.SetStatus(success);
		}

		void OnDestroy ()
		{
			TestSceneEvent.OnKadoSakuInitCompleteE -= TestSceneEvent_OnKadoSakuInitCompleteE;
		}
	}
}