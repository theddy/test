using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.IO;

using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace KadoSaku.Settings
{
	#if UNITY_EDITOR
	[InitializeOnLoad]
	#endif
	public class KSLeaderboardSettings : BaseSettings<KSLeaderboardSettings>
	{
		#if UNITY_EDITOR
		[MenuItem("Touchten/KadoSaku/Edit Leaderboard Settings", false, 1)]
		public static void EditSettings () {
			Selection.activeObject = Instance;
			EditorApplication.ExecuteMenuItem("Window/Inspector");
		}
		#endif
		
		[SerializeField] List<KadoSakuLeaderboardRecord> _kadosakuLeaderboard = new List<KadoSakuLeaderboardRecord>();
		
		public static bool TryGetLeaderboardId (string leaderboardKey, out string leaderboardId) {
			try {
				leaderboardId = Instance._kadosakuLeaderboard.FirstOrDefault
					(item => item.leaderboardKey.Equals(leaderboardKey, StringComparison.CurrentCultureIgnoreCase)).leaderboardID;
			}
			catch {
				leaderboardId = string.Empty;
				return false;
			}
			return true;
		}	

		public static List<KadoSakuLeaderboardRecord> GetAllLeaderboard()
		{
			return Instance.kadosakuLeaderboard;
		}

		public List<KadoSakuLeaderboardRecord> kadosakuLeaderboard {
			get { return _kadosakuLeaderboard; }
		}
	}
	
	[Serializable]
	public class KadoSakuLeaderboardRecord 
	{
		public string leaderboardKey;
		public string leaderboardID;
		public string leaderboardMatchmakingLoadedSceneName;
	}
}