using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.IO;

using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace KadoSaku.Settings
{
	public abstract class BaseSettings<T> : ScriptableObject where T : BaseSettings<T>
	{
		const string AssetPath = "KadoSaku Configuration/Resources";
		const string AssetExtension = ".asset";

		private static string AssetName { 
			get {
				return typeof(T).Name;
			}
		}

		static T _instance;
		protected static T Instance {
			get {
				if (_instance == null) 
				{
					_instance = Resources.Load(AssetName) as T;
					if (_instance == null) 
					{
						_instance = CreateInstance<T>();
						#if UNITY_EDITOR
						string properPath = Path.Combine(Application.dataPath, AssetPath);
						if (!Directory.Exists(properPath))
						{
							AssetDatabase.CreateFolder("Assets", "KadoSaku Configuration");
							AssetDatabase.CreateFolder("Assets/KadoSaku Configuration", "Resources");
						}
						
						string fullPath = Path.Combine(Path.Combine("Assets", AssetPath), AssetName + AssetExtension);
						AssetDatabase.CreateAsset(_instance, fullPath);
						#endif
					}
				}
				return _instance;
			}
		}

		protected static void DirtyEditor() {
			#if UNITY_EDITOR
			EditorUtility.SetDirty(Instance);
			#endif
		}
	}
}

