using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.IO;

using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace KadoSaku.Settings
{
	#if UNITY_EDITOR
	[InitializeOnLoad]
	#endif
	public class KSAchievementSettings : BaseSettings<KSAchievementSettings>
	{
		#if UNITY_EDITOR
		[MenuItem("Touchten/KadoSaku/Edit Achievement Settings", false, 1)]
		public static void EditSettings () {
			Selection.activeObject = Instance;
			EditorApplication.ExecuteMenuItem("Window/Inspector");
		}
		#endif
	
		[SerializeField] List<KadoSakuAchievementRecord> _kadosakuAchievements = new List<KadoSakuAchievementRecord>();
	
		public static bool TryGetAchievementId (string achievementKey, out string achievementId) {
			try {
				achievementId = Instance._kadosakuAchievements.Single
					(item => item.achievementKey.Equals(achievementKey, StringComparison.CurrentCultureIgnoreCase)).achievementId;
			}
			catch {
				achievementId = string.Empty;
				return false;
			}
			return true;
		}

		public static List<KadoSakuAchievementRecord> GetAllAchievement()
		{
			return Instance.kadosakuAchievements;
		}

		public List<KadoSakuAchievementRecord> kadosakuAchievements {
			get { return _kadosakuAchievements; }
		}
	}

	[Serializable]
	public class KadoSakuAchievementRecord {
		public string achievementKey;
		public string achievementId;
	}
}

