using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.IO;

using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace KadoSaku.Settings
{
	#if UNITY_EDITOR
	[InitializeOnLoad]
	#endif
	public class KSCoreSettings : BaseSettings<KSCoreSettings>
	{
		#if UNITY_EDITOR
		[MenuItem("Touchten/KadoSaku/Edit Core Settings", false, 0)]
		public static void EditSettings () {
			Selection.activeObject = Instance;
			EditorApplication.ExecuteMenuItem("Window/Inspector");
		}
		#endif


		public const string SDK_VER = "1.6.7";
		public const string API_URL = "https://s3-ap-southeast-1.amazonaws.com/touchten-client/v1/index.html#/main";

		[SerializeField] string _gameAppId = string.Empty;
		[SerializeField] string _gameAppSecret = string.Empty;
		[SerializeField] string _gameName = string.Empty;
		[SerializeField] string _gamePhotoLink = string.Empty;
		[SerializeField] string _gameLink = string.Empty;
		
		[SerializeField] string _gameDefaultLanguage = "english";
		[SerializeField] string _gameDefaultCountry = "en";

		[SerializeField] string _bundleId = string.Empty;
		[SerializeField] string _bundleVersion = string.Empty;

		[SerializeField] bool _enableAutoLogin = true;
		[SerializeField] bool _enableKadoSakuLogging = false;

		[SerializeField] string _gamePublicKey = string.Empty;

		[SerializeField] string _inviteTitle = string.Empty;
		[SerializeField] string _inviteMessage = string.Empty;

		public static string GAME_PUBLIC_KEY {
			get { return Instance._gamePublicKey; }
		}

		public static string GAME_APP_ID {
			get { return Instance._gameAppId; }
		}
		
		public static string GAME_APP_SECRET {
			get { return Instance._gameAppSecret; }
		}
		
		public static string GAME_NAME {
			get { return Instance._gameName; }
		}
		
		public static string GAME_PHOTO {
			get { return Instance._gamePhotoLink; }
		}
		
		public static string GAME_LINK {
			get { return Instance._gameLink; }
		}
		
		public static string GAME_DEFAULT_LANGUAGE {
			get { return Instance._gameDefaultLanguage; }
		}
		
		public static string GAME_DEFAULT_COUNTRY {
			get { return Instance._gameDefaultCountry; }
		}
		
		public static bool ENABLE_AUTO_LOGIN {
			get { return Instance._enableAutoLogin; }
			set { Instance._enableAutoLogin = value; }
		}
		
		public static string BUNDLE_ID {
			get { return Instance._bundleId; }
		}
		
		public static string BUNDLE_VERSION {
			get { return Instance._bundleVersion; }
		}

		public static bool ENABLE_KADOSAKU_LOGGING {
			get { return Instance._enableKadoSakuLogging; }
		}

		public static string INVITE_TITLE {
			get { return Instance._inviteTitle; }
		}
		
		public static string INVITE_MESSAGE {
			get { return Instance._inviteMessage; }
		}

		#region Setter
		public string GamePublicKey {
			get { return _gamePublicKey; }
			set {
				if (_gamePublicKey != value) {
					_gamePublicKey = value;
					DirtyEditor();
				}
			}
		}

		public string GameAppID {
			get { return _gameAppId; }
			set {
				if (_gameAppId != value) {
					_gameAppId = value;
					DirtyEditor();
				}
			}
		}
		
		public string GameAppSecret {
			get { return _gameAppSecret; }
			set {
				if (_gameAppSecret != value) {
					_gameAppSecret = value;
					DirtyEditor();
				}
			}
		}
		
		public string GameName {
			get { return _gameName; }
			set {
				if (_gameName != value) {
					_gameName = value;
					DirtyEditor();
				}
			}
		}
		
		public string GamePhotoLink {
			get { return _gamePhotoLink; }
			set {
				if (_gamePhotoLink != value) {
					_gamePhotoLink = value;
					DirtyEditor();
				}
			}
		}
		
		public string GameLink {
			get { return _gameLink; }
			set {
				if (_gameLink != value) {
					_gameLink = value;
					DirtyEditor();
				}
			}
		}
		
		public bool EnableAutoLogin {
			get { return _enableAutoLogin; }
			set {
				if (_enableAutoLogin != value) {
					_enableAutoLogin = value;
					DirtyEditor();
				}
			}
		}
		
		public string BundleId {
			get { return _bundleId; }
		}
		
		public string BundleVersion {
			get { return _bundleVersion; }
		}
		
		public void SetBundleIdAndVersion () {
			#if UNITY_EDITOR
			_bundleId = PlayerSettings.bundleIdentifier;
			_bundleVersion = PlayerSettings.bundleVersion;
			DirtyEditor();
			#endif
		}
		
		public static void SetIdAndVersion () {
			Instance.SetBundleIdAndVersion();
		}
		
		public bool EnableKadoSakuLogging {
			get { return _enableKadoSakuLogging; }
			set {
				if (_enableKadoSakuLogging != value) {
					_enableKadoSakuLogging = value;
					DirtyEditor();
				}
			}
		}

		public string InviteTitle {
			get { return _inviteTitle; }
			set {
				if (_inviteTitle != value) {
					_inviteTitle = value;
					DirtyEditor();
				}
			}
		}
		
		public string InviteMessage {
			get { return _inviteMessage; }
			set {
				if (_inviteMessage != value)  {
					_inviteMessage = value;
					DirtyEditor();
				}
			}
		}
		#endregion
	}
}

