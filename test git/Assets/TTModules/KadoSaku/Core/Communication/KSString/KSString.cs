using System;
using System.Collections;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using KadoSaku.Api;

namespace KadoSaku.Core.Communication
{
	public class KSString : IEquatable<KSString>
	{
		[JsonProperty("action")] 		public string Action  		{ get; set; }
		[JsonProperty("callbackId")] 	public string CallbackId 	{ get; set; }
		[JsonProperty("request")] 		public JToken Request		{ get; set; }
		[JsonProperty("response")] 		public JToken Response 		{ get; set; }
		[JsonProperty("cancelled")] 	public bool? Cancelled		{ get; set; }
		[JsonProperty("error")] 		public IError Error 		{ get; set; }

		public class KSError : IEquatable<KSError>, IError
		{
			[JsonProperty("code")] 		public int Code 		{ get; private set; }
			[JsonProperty("message")]	public string Message 	{ get; private set; }
			
			[JsonConstructor]
			public KSError (int code, string message) {
				this.Code = code;
				this.Message = message;
			}
			
			#region IEquatable implementation
			public bool Equals (KSError other)
			{
				if (other == null) return false;
				
				return 	this.Code.Equals(other.Code) &&
						this.Message.Equals(other.Message);
			}
			#endregion

			public override string ToString ()
			{
				return string.Format ("[KSError: Code={0}, Message={1}]", Code, Message);
			}
		}

		public KSString (string jsonString) {
			string action = KSAction.Unknown;
			string callbackId = null;
			bool? cancelled = null;
			KSError error = null;

			if (!string.IsNullOrEmpty(jsonString)) {
				JToken token = JToken.Parse(jsonString);
				if (token != null) {
					this.Request = GetRequest(token);
					this.Response = GetResponse(token);

					action = GetAction(token);
					callbackId = GetCallbackId(token);
					cancelled = GetCancelled(token);
					error = GetError(token);
				}
			}

			Init(action, callbackId, cancelled, error);
		}

		public KSString () {}

		public KSString (KSString other) {
			this.Action = other.Action;
			this.CallbackId = other.CallbackId;
			this.Request = other.Request;
			this.Response = other.Response;
			this.Cancelled = other.Cancelled;
			this.Error = other.Error;
		}

		protected void Init (string action,
		                  	 string callbackId,
		                  	 bool? cancelled,
		                  	 KSError error)
		{
			this.CallbackId = callbackId;
			this.Action = action;
			this.Cancelled = cancelled;
			this.Error = error;
		}

		public string ToJson () {
			return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
		}

		public override string ToString ()
		{
			return string.Format ("[KSString: Action={0}, CallbackId={1}, Request={2}, Response={3}, Cancelled={4}, Error={5}]", Action, CallbackId, Request, Response, Cancelled, Error);
		}


		#region IEquatable implementation
		public bool Equals (KSString other)
		{
			if (other == null) return false;

			var err = this.Error as KSError;
			var otherErr = other.Error as KSError;

			bool _action = (this.Action == other.Action || this.Action != null && this.Action.Equals(other.Action));
			bool _callbackId = (this.CallbackId == other.CallbackId || this.CallbackId != null && this.CallbackId.Equals(other.CallbackId));
			bool _cancelled = (this.Cancelled == other.Cancelled || this.Cancelled != null && this.Cancelled.Equals(other.Cancelled));
			bool _error = (err == otherErr || err != null && err.Equals(otherErr));
			bool _request = JToken.DeepEquals(this.Request, other.Request);
			bool _response = JToken.DeepEquals(this.Response, other.Response);

			return _action && _callbackId && _cancelled && _error && _request && _response;
		}
		#endregion


		#region JSON Parser Utility
		const string actionKey = "action";
		const string callbackIdKey = "callbackId";
		const string cancelledKey = "cancelled";
		const string errorKey = "error";
		const string responseKey = "response";
		const string requestKey = "request";

		static string GetAction (JToken token) {
			if (token == null) return KSAction.Unknown;
			return (string)token[actionKey];
		}

		static string GetCallbackId (JToken token) {
			if (token == null) return null;
			if (token[callbackIdKey] == null) return null;
			return (string)token[callbackIdKey];
		}

		static bool? GetCancelled (JToken token) {
			if (token == null) return null;
			if (token[cancelledKey] == null) return null;
			return (bool)token[cancelledKey];
		}

		static KSError GetError (JToken token) {
			if (token == null) return null;
			if (token[errorKey] == null) return null;
			return token[errorKey].ToObject<KSError>();
		}

		static JToken GetResponse (JToken token) {
			if (token == null) return null;
			if (token[responseKey] == null) return null;
			return token[responseKey];
		}

		static JToken GetRequest (JToken token) {
			if (token == null) return null;
			if (token[requestKey] == null) return null;
			return token[requestKey];
		}
		#endregion

		public static IError CreateInternalError (string errorMessage) {
			return new KSError(-1, errorMessage);
		}

		public static IError CreateError (int errorCode, string errorMessage) {
			return new KSError(errorCode, errorMessage);
		}
	}
}

