using System;
using System.Collections.Generic;
using KadoSaku.Api;

namespace KadoSaku.Core.Communication
{
    public class CallbackManager
    {
        private IDictionary<string, object> kadosakuDelegates = new Dictionary<string, object>();
        private int nextAsyncId;

        public string AddKadoSakuDelegate<T>(KadoSakuDelegate<T> callback) where T : IResult
        {
            if (callback == null)
            {
                return null;
            }

            this.nextAsyncId++;
            this.kadosakuDelegates.Add(this.nextAsyncId.ToString(), callback);
            return this.nextAsyncId.ToString();
        }

        public void OnKadoSakuResponse(IInternalResult result)
        {
            if (result == null || result.CallbackId == null)
            {
                return;
            }

            object callback;
            if (this.kadosakuDelegates.TryGetValue(result.CallbackId, out callback))
            {
                CallCallback(callback, result);
                this.kadosakuDelegates.Remove(result.CallbackId);
            }
        }

		public void OnKadoSakuInternalError (IError error, bool cancelled, string CallbackId)
		{
			object callback;
			if (this.kadosakuDelegates.TryGetValue(CallbackId, out callback))
			{
				CallErrorCallback(callback, error, cancelled);
				this.kadosakuDelegates.Remove(CallbackId);
			}
		}

        // Since unity mono doesn't support covariance and contravariance use this hack
        private static void CallCallback(object callback, IResult result)
        {
            if (callback == null || result == null)
            {
                return;
            }

            if (CallbackManager.TryCallCallback<ILeaderboardScoresResult>(callback, result) ||
                CallbackManager.TryCallCallback<ILeadeboardInfoResult>(callback, result) ||
			    CallbackManager.TryCallCallback<IActionResult>(callback, result) ||
			    CallbackManager.TryCallCallback<IDownloadSaveResult>(callback, result) ||
			    CallbackManager.TryCallCallback<IDownloadMetaDataResult>(callback, result) ||
			    CallbackManager.TryCallCallback<ITimeResult>(callback, result) ||
			    CallbackManager.TryCallCallback<IMailResult>(callback, result) ||
			    CallbackManager.TryCallCallback<IPlayerProfileResult>(callback, result) ||
				CallbackManager.TryCallCallback<IAchievementTransactionResult>(callback, result) ||
				CallbackManager.TryCallCallback<IAchievementListResult>(callback, result) ||
				CallbackManager.TryCallCallback<IGiftNotificationResult>(callback, result) ||
				CallbackManager.TryCallCallback<IMatchNotificationResult>(callback, result) ||
				CallbackManager.TryCallCallback<IGiftCodeResult>(callback, result) ||
				CallbackManager.TryCallCallback<IFriendsResult>(callback, result) ||
				CallbackManager.TryCallCallback<IResult>(callback, result) 
			    )
            {
                return;
            }

            throw new NotSupportedException("Unexpected result type: " + callback.GetType().FullName);
        }

		public static void CallErrorCallback (object callback, IError error, bool cancelled) 
		{
			if (callback == null)
			{
				return;
			}

			if (CallbackManager.TryCallCallback<ILeaderboardScoresResult>(callback, new LeaderboardScoreResult(error, cancelled)) ||
			    CallbackManager.TryCallCallback<ILeadeboardInfoResult>(callback, new LeaderboardInfoResult(error, cancelled)) ||
			    CallbackManager.TryCallCallback<IActionResult>(callback, new ActionResult(error, cancelled)) ||
			    CallbackManager.TryCallCallback<IDownloadSaveResult>(callback, new DownloadSaveResult(error, cancelled)) ||
			    CallbackManager.TryCallCallback<IDownloadMetaDataResult>(callback, new DownloadMetaDataResult(error, cancelled)) ||
			    CallbackManager.TryCallCallback<ITimeResult>(callback, new TimeResult(error, cancelled)) ||
			    CallbackManager.TryCallCallback<IMailResult>(callback, new MailResult(error, cancelled)) ||
			    CallbackManager.TryCallCallback<IPlayerProfileResult>(callback, new PlayerProfileResult(error, cancelled)) ||
				CallbackManager.TryCallCallback<IAchievementTransactionResult>(callback, new AchievementTransactionResult(error, cancelled)) ||
				CallbackManager.TryCallCallback<IAchievementListResult>(callback, new AchievementListResult(error, cancelled)) ||
				CallbackManager.TryCallCallback<IGiftNotificationResult>(callback, new GiftNotificationResult(error, cancelled)) ||
				CallbackManager.TryCallCallback<IMatchNotificationResult>(callback, new MatchNotificationResult(error, cancelled)) ||
				CallbackManager.TryCallCallback<IGiftCodeResult>(callback, new GiftCodeResult(error, cancelled)) ||
				CallbackManager.TryCallCallback<IFriendsResult>(callback, new FriendsResult(error, cancelled)) ||
				CallbackManager.TryCallCallback<IResult>(callback, new BaseResult(error, cancelled)) 
			    )
			{
				return;
			}
			
			throw new NotSupportedException("Unexpected result type: " + callback.GetType().FullName);
		}

        private static bool TryCallCallback<T>(object callback, IResult result) where T : IResult
        {
            var castedCallback = callback as KadoSakuDelegate<T>;
            if (castedCallback != null)
            {
				castedCallback((T)result);
                return true;
            }

            return false;
        }
    }
}
