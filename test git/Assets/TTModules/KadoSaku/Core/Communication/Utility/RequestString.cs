using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using KadoSaku.Utility;

namespace KadoSaku.Core.Communication
{
	public class RequestString : IRequestString
	{
		#region IRequestString implementation
		[JsonProperty("action")]  public string Action { get; set; }
		[JsonProperty("queries")] public Dictionary<string, string> Queries { get; set; }
		[JsonProperty("headers")] public Dictionary<string, string> Headers { get; set; }
		[JsonProperty("body")] 	  public string Body { get; set; }
		#endregion

		public static IRequestString Create (JToken token) {
			return new RequestString();
		}
	}
}

