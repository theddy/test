using System;

namespace KadoSaku.Core.Communication
{
	public static class KSAction
	{
		public const string Unknown = "Unknown";
		public const string Init = "Init";	
		public const string BackButton = "BackButton";
		public const string ShowView = "ShowView";

		public static class Api {
			// - Authentication
			public const string AutoLogin = "AutoLogin";
			public const string Unlink = "Unlink";
			
			// - Leaderboard
			public const string GetLeaderboardInfo = "GetLeaderboardInfo";
			public const string GetLeaderboardGlobalScores = "GetLeaderboardGlobalScores";
			public const string GetLeaderboardSocialScores = "GetLeaderboardSocialScores";
			public const string SubmitLeaderboardScore = "SubmitLeaderboardScore";
			
			// - Achievement
			public const string TriggerAchievement = "TriggerAchievement";
			public const string GetAchievementList = "GetAchievementList";

			// - Cloud Save
			public const string UploadSave = "UploadSave";
			public const string DownloadSave = "DownloadSave";
			public const string DeleteSave = "DeleteSave";
			public const string DownloadMetaData = "DownloadMetaData";
			
			// - Utility
			public const string GetServerTime = "GetServerTime";
			public const string CheckSDKVersion = "CheckSDKVersion";
			public const string GetKadoSakuFriends = "GetKadoSakuFriends";

			// - User
			public const string UpdatePlayerProfile = "UpdatePlayerProfile";

			// - Mail
			public const string CheckInbox = "CheckInbox";
			public const string CheckAnnouncements = "CheckAnnouncements";
			public const string MarkMailAsRead = "MarkMailAsRead";

			// - Sync
			public const string SyncAchievement = "SyncAchievement";
			public const string SyncScore = "SyncScore";

			// - Amazon
			public const string RegisterAmazonCognitoId = "RegisterAmazonCognitoId";
			public const string RegisterAmazonSessionId = "RegisterAmazonSessionId";

			// - Push Notification
			public const string RegisterPushNotification = "RegisterPushNotification";
		
			// - Gifting
			public const string CreateGiftRequest = "CreateGiftRequest";
			public const string AcceptGift = "AcceptGift";
			public const string DenyGift = "DenyGift";
			public const string ArchiveGift = "ArchiveGift";
			public const string GetGiftNotification = "GetGiftNotification";

			// - Gift Code
			public const string RedeemGiftCode = "RedeemGiftCode";

			// - Matchmaking
			public const string CreateNewMatch = "CreateNewMatch";
			public const string SubmitMatchScore = "SubmitMatchScore";
			public const string CancelMatch = "CancelMatch";
			public const string ArchiveMatch = "ArchiveMatch";
			public const string GetMatchNotification = "GetMatchNotification";
		}

			
		public static class WebRequest {
			// - Action
			public const string LoginFacebook = "LoginFacebook";
			public const string GetSocialProfile = "GetSocialProfile";
			
			// - Headers
			public const string GetAppHeaders = "GetAppHeaders";
			public const string GetBearerHeaders = "GetBearerHeaders";
			public const string GetMembershipHeaders = "GetMembershipHeaders";
			
			// - Encryption
			public const string EncryptBodyMembership = "EncryptBodyMembership";
			public const string EncryptBodyBearer = "EncryptBodyBearer";
			public const string EncryptBodyApp = "EncryptBodyApp";

			// - Event: Login
			public const string SaveMembershipToken = "SaveMembershipToken";
			public const string SavePlayerProfile = "SavePlayerProfile";
			public const string LoginComplete = "LoginComplete";
			public const string CloseLoginSuccessView = "CloseLoginSuccessView";
			public const string CancelLoginView = "CancelLoginView";

			// - Event: Logout
			public const string LogoutUser = "LogoutUser";

			// - Event: Invite Friends
			public const string InviteFriends = "InviteFriends";
		}
	}
}

