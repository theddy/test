using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace KadoSaku.Core.Communication
{
	public interface IRequestString
	{
		string Action { get; set; }
		Dictionary<string, string> Queries { get; set; }
		Dictionary<string, string> Headers { get; set; }
		string Body { get; set; }
	}
}