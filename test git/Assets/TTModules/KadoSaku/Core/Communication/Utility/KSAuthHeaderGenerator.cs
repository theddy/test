using System;
using KadoSaku.Utility;

namespace KadoSaku.Core.Communication
{
	public static class KSAuthHeaderGenerator
	{
		public static string GetAuthHeader (string action, string accessToken) {
			string authHeader = string.Empty;
			switch (action) 
			{
			// Bearer
			case KSAction.Api.TriggerAchievement:
			case KSAction.Api.Unlink:
			case KSAction.Api.DeleteSave:
			case KSAction.Api.DownloadMetaData:
			case KSAction.Api.DownloadSave:
			case KSAction.Api.UploadSave:
			case KSAction.Api.GetLeaderboardGlobalScores:
			case KSAction.Api.GetLeaderboardInfo:
			case KSAction.Api.GetLeaderboardSocialScores:
			case KSAction.Api.SubmitLeaderboardScore:
			case KSAction.Api.CheckInbox:
			case KSAction.Api.MarkMailAsRead:
			case KSAction.Api.UpdatePlayerProfile:
			case KSAction.Api.SyncAchievement:
			case KSAction.Api.SyncScore:
			case KSAction.Api.AcceptGift:
			case KSAction.Api.ArchiveGift:
			case KSAction.Api.CreateGiftRequest:
			case KSAction.Api.DenyGift:
			case KSAction.Api.GetGiftNotification:
			case KSAction.Api.ArchiveMatch:
			case KSAction.Api.CancelMatch:
			case KSAction.Api.CreateNewMatch:
			case KSAction.Api.GetMatchNotification:
			case KSAction.Api.SubmitMatchScore:
			case KSAction.Api.RedeemGiftCode:
			case KSAction.Api.GetKadoSakuFriends:
				authHeader = "Bearer " + KSConfig.MembershipToken;
				break;

				// App
			case KSAction.Api.CheckAnnouncements:
			case KSAction.Api.GetServerTime:
			case KSAction.Api.RegisterAmazonCognitoId:
			case KSAction.Api.RegisterPushNotification:
			case KSAction.Api.GetAchievementList:
				authHeader = "App " + KSConfig.App;
				break;

				// Membership
			case KSAction.Api.AutoLogin:
				authHeader = "Membership " + KSConfig.AppSecret + ":" + accessToken;
				break;
			}
			return authHeader;
		}
	}
}

