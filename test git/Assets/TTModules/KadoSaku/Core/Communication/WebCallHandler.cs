using System;
using Touchten.Social;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using KadoSaku.Utility;
using KadoSaku.Api;
using KadoSaku.Core.View;
using KadoSaku.Core.Platform;
using KadoSaku.Core.Crypto;
using KadoSaku.Settings;
using Touchten.Analytics;

namespace KadoSaku.Core.Communication
{
	public class WebCallHandler
	{
		ICommunicationService commService;
		IViewManagerService viewService;
		ITTSocialExtension socialService;
		IAnalyticsData analyticsData;

		public WebCallHandler (IKadoSakuPlatform commService, ITTSocialExtension socialService, IAnalyticsData analyticsData) {
			this.commService = commService;
			this.viewService = commService;
			this.socialService = socialService;
			this.analyticsData = analyticsData;
		}

		public void ProcessCall (string requestString) {
			KSString ksString = new KSString(requestString);

			switch (ksString.Action) {
				// - Action
			case KSAction.WebRequest.LoginFacebook:
				LoginFacebook (ksString);
				break;
			case KSAction.WebRequest.GetSocialProfile:
				GetsocialServiceProfile (ksString);
				break;

				// - Headers
			case KSAction.WebRequest.GetMembershipHeaders:
				GetHeaders(ksString, GetMembershipHeaders);
				break;
			case KSAction.WebRequest.GetAppHeaders:
				GetHeaders(ksString, GetAppHeaders);
				break;
			case KSAction.WebRequest.GetBearerHeaders:
				GetHeaders(ksString, GetBearerHeaders);
				break;

				// - Encryption
			case KSAction.WebRequest.EncryptBodyMembership:
				EncryptRequestBody (ksString, GetMembershipHeaders);
				break;
			case KSAction.WebRequest.EncryptBodyBearer:
				EncryptRequestBody (ksString, GetBearerHeaders);
				break;
			case KSAction.WebRequest.EncryptBodyApp :
				EncryptRequestBody (ksString, GetAppHeaders);
				break;

				// - Event: Login
			case KSAction.WebRequest.SaveMembershipToken :
				SaveMembershipToken(ksString);
				break;
			case KSAction.WebRequest.SavePlayerProfile :
				SavePlayerProfile(ksString);
				break;
			case KSAction.WebRequest.LoginComplete :
				SaveMembershipToken(ksString);
				SavePlayerProfile(ksString);
				break;
			case KSAction.WebRequest.CancelLoginView :
				ProcessLoginCallback(true);
				CloseLoginView(ksString);
				break;
			case KSAction.WebRequest.CloseLoginSuccessView :
				ProcessLoginCallback(false);
				CloseLoginView(ksString);
				break;

				// - Event: Logout
			case KSAction.WebRequest.LogoutUser:
				ProcessLogout(ksString);
				break;
				
				// - Event: Invite Friends
			case KSAction.WebRequest.InviteFriends:
				InviteFriends(ksString);
				break;
			}
		}

		#region 3rd Party API Call
		void LoginFacebook (KSString ksString)
		{
			if (socialService == null) return;

			KadoSaku.Utility.KSDebug.Log("Login Facebook");
			KSString callbackString = GenerateCallbackString(ksString);
			socialService.LoginUsing (SocialService.Facebook, delegate(string accessToken) {
				if (string.IsNullOrEmpty (accessToken)) {
					callbackString.Error = KSString.CreateInternalError("Login Facebook Error");
				} 
				else {
					callbackString.Response = new JObject(new JProperty("accessToken", accessToken));
				}
				CallbackFromUnity(callbackString);
			});
		}
		
		void GetsocialServiceProfile (KSString ksString)
		{
			if (socialService == null) return;
	
			KSString callbackString = GenerateCallbackString(ksString);
			socialService.GetSocialProfile (SocialService.Facebook, delegate(string playerProfileJson) {
				if (string.IsNullOrEmpty (playerProfileJson)) {
					callbackString.Error = KSString.CreateInternalError("Facebook Get Player Profile Error");
				} else {
					callbackString.Response = new JObject(new JProperty ("socialProfile", JObject.Parse (playerProfileJson)));
				}
				CallbackFromUnity(callbackString);
			});
		}
		
		void InviteFriends (KSString ksString) {
			if (socialService == null) return;
			
			socialService.ShowInviteFriendsView(KSCoreSettings.INVITE_TITLE, KSCoreSettings.INVITE_MESSAGE, delegate(Touchten.Social.IActionResult result) {
				if (result.IsSuccess) {
					CallbackOK(ksString);
				} else {
					KSString callbackString = GenerateCallbackString(ksString);
					callbackString.Error = KSString.CreateInternalError("Invite Friend Error");
					CallbackFromUnity(callbackString);
				}
			});
		}
		#endregion
		
		
		#region Get Headers
		string GetMembershipHeaders {
			get {
				return string.Format ("Membership {0}:{1}", KSConfig.AppSecret, socialService.AccessToken);
			}
		}
		
		string GetAppHeaders {
			get {
				return string.Format ("App {0}", KSConfig.App);
			}
			
		}
		
		string GetBearerHeaders {
			get {
				return string.Format ("Bearer {0}", KSConfig.MembershipToken);
			}
		}

		void AddDefaultHeaders (JObject headers) {
			headers.Add (new JProperty ("Language", KSConfig.Language));
			headers.Add (new JProperty ("Location", KSConfig.Location));
			headers.Add (new JProperty ("Device-ID", KSConfig.DeviceId));
			headers.Add (new JProperty ("Content-Type", "application/json"));
		}
		
		void GetHeaders (KSString requestString, string authHeader)
		{
			KSString callbackString = GenerateCallbackString(requestString);
			JObject headers = GenerateHeaders(authHeader);
			callbackString.Response = new JObject(new JProperty("headers", headers));
			CallbackFromUnity(callbackString);
		}

		JObject GenerateHeaders (string authHeader) {
			JObject headers = new JObject();
			headers.Add (new JProperty ("Authorization", authHeader));
			AddDefaultHeaders(headers);
			if (KSConfig.IsEncrypted) EncryptHeaders.Encrypt(ref headers);
			return headers;
		}
		#endregion
		
		
		#region Ecnryption
		void EncryptRequestBody (KSString requestString, string authHeader)
		{
			KSString callbackString = GenerateCallbackString(requestString);

			JObject response = new JObject();
			JObject headers = GenerateHeaders(authHeader);
			JToken originalBody = requestString.Request["body"];

			// Encryption
			object encryptedBody;
			if (KSConfig.IsEncrypted) {
				encryptedBody = EncryptBody.Encrypt(originalBody);
			}
			else {
				encryptedBody = originalBody;
			}

			response.Add(new JProperty("headers", headers));
			response.Add(new JProperty("body", encryptedBody));
			callbackString.Response = response;
			
			CallbackFromUnity(callbackString);
		}
		#endregion


		#region Config
		void SaveMembershipToken (KSString ksString) {
			JToken token = ksString.Request;
			if (token["membershipToken"] == null) return;
			string membershipToken = (string)token["membershipToken"];
			KSConfig.SaveMembershipToken(membershipToken);

			KS.Authentication._OnLoginEvent(new KadoSaku.Api.ActionResult());
			CallbackOK(ksString);
		}

		void SavePlayerProfile (KSString ksString) {
			JToken token = ksString.Request;
			KSConfig.PlayerProfile = null;
			if (token["playerProfile"] == null) return;
			KadoSaku.Api.IPlayerProfileModel player = new KadoSaku.Api.PlayerModel(token["playerProfile"]);
			KSConfig.PlayerProfile = player;

			CallbackOK(ksString);
		}
		#endregion


		#region Login Callback
		void ProcessLoginCallback (bool cancelled) {
			KadoSaku.Api.IActionResult result = null;
			if (cancelled) result = new KadoSaku.Api.ActionResult(KSString.CreateError(-5,"Cancelled By Player"), cancelled);
			else {
				result = new KadoSaku.Api.ActionResult();
				KS.Sync.SyncAll();
			}

			KS.Authentication._OnLoginEvent(result);
			if (KS.Authentication.loginCallback != null) {
				KS.Authentication.loginCallback(result);
				KS.Authentication.loginCallback = null;
			}
		}

		void CloseLoginView (KSString ksString) {
			viewService.CloseView();
			CallbackOK(ksString);
		}
		#endregion


		#region Logout Callback
		void ProcessLogout (KSString ksString) {
			KS.Authentication.Logout((r) => {
				KS.Authentication._OnLogoutEvent(r);
				CallbackOK(ksString);
			});
		}
		#endregion

		#region Utility
		void CallbackFromUnity (KSString callbackString) {
			ParamCallback paramCallback = new ParamCallback (callbackString);
			commService.CallbackFromUnity(paramCallback);
		}

		void CallbackOK (KSString requestString) {
			KSString callbackString =  new KSString () {
				Action = requestString.Action,
				CallbackId = requestString.CallbackId,
				Response = new JObject(new JProperty("status", "ok"))
			};
			CallbackFromUnity(callbackString);
		} 

		static KSString GenerateCallbackString (KSString requestString) {
			return new KSString () {
				Action = requestString.Action,
				CallbackId = requestString.CallbackId
			};
		}
		#endregion
	}
}

