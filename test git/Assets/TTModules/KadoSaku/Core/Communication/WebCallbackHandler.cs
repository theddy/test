using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using KadoSaku.Utility;
using KadoSaku.Api;

namespace KadoSaku.Core.Communication
{
	public class WebCallbackHandler
	{
		CallbackManager callbackManager;

		public WebCallbackHandler (CallbackManager callbackManager) {
			this.callbackManager = callbackManager;
		}

		public void ProcessCallback (string responseString) {
			JObject jobject = JObject.Parse(responseString);
			string action = (string)jobject["action"];
			
			IInternalResult result = null;
			switch (action) 
			{
			// Core
			case KSAction.Init :
			case KSAction.Api.RegisterAmazonCognitoId :
			case KSAction.Api.RegisterPushNotification :
			case KSAction.Api.RegisterAmazonSessionId :
				result = new ActionResult(responseString);
				break;

			// Authentication
			case KSAction.Api.AutoLogin :
				ProcessLoginResult(new LoginResult(responseString));
				result = new ActionResult(responseString);
				break;
			case KSAction.Api.Unlink :
				result = new ActionResult(responseString);
				break;

			// Achievement
			case KSAction.Api.TriggerAchievement :
				result = new AchievementTransactionResult(responseString);
				break;
			case KSAction.Api.GetAchievementList :
				result = new AchievementListResult(responseString);
				break;

			// Leaderboard
			case KSAction.Api.GetLeaderboardInfo :
				result = new LeaderboardInfoResult(responseString);
				break;
			case KSAction.Api.GetLeaderboardGlobalScores :
			case KSAction.Api.GetLeaderboardSocialScores :
				result = new LeaderboardScoreResult(responseString);
				break;
			case KSAction.Api.SubmitLeaderboardScore :
				result = new ActionResult(responseString);
				break;	

			// Cloud Save
			case KSAction.Api.UploadSave :
				result = new ActionResult(responseString);
				break;
			case KSAction.Api.DownloadSave :
				result = new DownloadSaveResult(responseString);
				break;
			case KSAction.Api.DeleteSave :
				result = new ActionResult(responseString);
				break;
			case KSAction.Api.DownloadMetaData :
				result = new DownloadMetaDataResult(responseString);
				break;

			// Utility
			case KSAction.Api.GetServerTime :
				result = new TimeResult(responseString);
				break;
			case KSAction.Api.GetKadoSakuFriends:
				result = new FriendsResult(responseString);
				break;

			// Mail
			case KSAction.Api.CheckInbox :
				result = new MailResult(responseString);
				break;
			case KSAction.Api.CheckAnnouncements :
				result = new MailResult(responseString);
				break;
			case KSAction.Api.MarkMailAsRead :
				result = new ActionResult(responseString);
				break;

			// User
			case KSAction.Api.UpdatePlayerProfile :
				result = new PlayerProfileResult(responseString);
				break;

			// Sync
			case KSAction.Api.SyncAchievement :
			case KSAction.Api.SyncScore :
				result = new ActionResult(responseString);
				break;

			// Gifting
			case KSAction.Api.AcceptGift:
			case KSAction.Api.ArchiveGift:
			case KSAction.Api.CreateGiftRequest:
			case KSAction.Api.DenyGift:
				result = new ActionResult(responseString);
				break;
			case KSAction.Api.GetGiftNotification:
				result = new GiftNotificationResult(responseString);
				break;

			// Matchmaking
			case KSAction.Api.ArchiveMatch:
			case KSAction.Api.CancelMatch:
			case KSAction.Api.CreateNewMatch:
			case KSAction.Api.SubmitMatchScore:
				result = new ActionResult(responseString);
				break;
			case KSAction.Api.GetMatchNotification:
				result = new MatchNotificationResult(responseString);
				break;

			// Gift Code
			case KSAction.Api.RedeemGiftCode:
				result = new GiftCodeResult(responseString);
				break;
			}

			callbackManager.OnKadoSakuResponse(result);
		}

		static void ProcessLoginResult (ILoginResult loginResult) {
			if (loginResult.Error != null ||
			   (loginResult.Error != null && loginResult.Cancelled.Value == false)) 
			{
				KS.Authentication.Logout((r) => {});
			}
			else {
				KSConfig.SaveMembershipToken(loginResult.MembershipToken);
				KSConfig.PlayerProfile = loginResult.PlayerProfile;
			}
		}
	}
}

