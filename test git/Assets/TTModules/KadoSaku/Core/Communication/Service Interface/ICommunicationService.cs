using System;
using KadoSaku.Api;
using Touchten.Social;

namespace KadoSaku.Core.Communication
{
	public interface ICommunicationService
	{
		CallbackManager callbackManager { get; }
		ITTSocialExtension socialService { get; }

		void CallToWeb (ParamRequest param);
		void CallbackFromUnity (ParamCallback param);
	}
}

