using System;
using KadoSaku.Core.Platform;
using KadoSaku.Utility;
using UnityEngine;

namespace KadoSaku.Core.Communication
{
	public class WebCallString
	{
		public static string CreateApiCall (ParamBase param) {
			string callString = string.Format("{0}(\'{1}\')", 
			                                  KSConstants.apiCallMethod,
			                                  param.ParamString);
			KadoSaku.Utility.KSDebug.Log(callString);
			return callString;
		}

		public static string CreateMethodCall (ParamBase param, string methodName) {
			string callString = string.Format("{0}(\'{1}\')", 
			                                  methodName,
			                                  param.ParamString);
			KadoSaku.Utility.KSDebug.Log(callString);
			return callString;
		}
	}
}

