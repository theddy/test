using System;
using System.Collections.Generic;
using KadoSaku.Core.Communication;
using KadoSaku.Core.View;
using KadoSaku.Utility;
using Newtonsoft.Json.Linq;
using KadoSaku.Settings;

namespace KadoSaku.Core.Communication
{
	public class ParamView : ParamBase
	{
		const string VIEW_NAME_KEY = "viewName";
		const string VIEW_QUERY_KEY = "viewQuery";

		JObject requestObject;
		JObject viewQueryObject = new JObject();

		public ParamView (KadoSakuView viewName) : base (KSAction.ShowView) {
			requestObject = new JObject();
			requestObject.Add(new JProperty(VIEW_NAME_KEY, viewName.ToString()));
			requestObject.Add(new JProperty(VIEW_QUERY_KEY,viewQueryObject));
			this.ksString.Request = requestObject;
			AddViewQuery("membershipToken", KSConfig.MembershipToken);
			AddViewQuery("gameName", KSCoreSettings.GAME_NAME);
			AddViewQuery("gamePhoto", KSCoreSettings.GAME_PHOTO);
			AddViewQuery("gameLanguage", KSConfig.Language);
		}

		public void AddViewQuery (string name, string value) { 
			viewQueryObject.Add(new JProperty(name, value));
		}

		public void AddCallbackId (string callbackId) {
			this.ksString.CallbackId = callbackId;
		}
	}
}

