using System;
using KadoSaku.Core.Communication;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using KadoSaku.Utility;
using Touchten.Social;
using KadoSaku.Core.Crypto;

namespace KadoSaku.Core.Communication
{
	public class ParamRequest : ParamBase
	{
		JObject requestObject;
		JObject queryObject = new JObject();
		JObject bodyObject = new JObject();
		JObject headerObject = new JObject();

		string contentType;
		string accessToken;

		public ParamRequest (string action, string accessToken, string contentType): base (action) {
			Init(action, accessToken, contentType);
		}

		public ParamRequest (string action, string contentType = "application/json") : base (action) {
			Init (action, string.Empty, contentType);
		}

		void Init (string action, string accessToken, string contentType) {
			this.accessToken = accessToken;
			this.contentType = contentType;
			requestObject = new JObject();
			this.ksString.Request = requestObject;
			requestObject.Add(new JProperty("queries", queryObject));
			requestObject.Add(new JProperty("headers", headerObject));
			requestObject.Add(new JProperty("body", bodyObject));
			AddDefaultHeaders();
		}

		public string CallbackId {
			get { return ksString.CallbackId; }
		}

		public void AddCallbackId (string callbackId) {
			this.ksString.CallbackId = callbackId;
		}

		public void AddQuery (string name, object value) {
			queryObject.Add(new JProperty(name, value));
		}

		public void AddHeader (string name, object value) {
			headerObject.Add(new JProperty(name, value));
		}

		public void AddBody (string name, object value) {
			bodyObject.Add(new JProperty(name, value));
		}

		void AddDefaultHeaders () {
			AddAuthorizationHeader();
			headerObject.Add(new JProperty("Language", KSConfig.Language));
			headerObject.Add(new JProperty("Location", KSConfig.Location));
			headerObject.Add(new JProperty("Device-ID", KSConfig.DeviceId));
			headerObject.Add(new JProperty("Content-Type", contentType));
		}

		void AddAuthorizationHeader () {
			string authHeader = KSAuthHeaderGenerator.GetAuthHeader(Action, accessToken);
			headerObject.Add(new JProperty("Authorization", authHeader));
		}

		public override string ParamString {
			get {
				if (KSConfig.IsEncrypted) {
					EncryptHeaders.Encrypt(ref headerObject);
					string encryptedStringBody = EncryptBody.Encrypt(bodyObject);
					requestObject["body"] = encryptedStringBody;
				} 
				return base.ParamString;
			}
		}
	}
}

