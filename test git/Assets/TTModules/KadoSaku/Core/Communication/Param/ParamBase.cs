using System;

namespace KadoSaku.Core.Communication
{
	public class ParamBase
	{
		protected KSString ksString { get; private set; }

		public ParamBase (KSString ksString) {
			this.ksString = ksString;
		}

		public ParamBase (string action) {
			ksString = new KSString() {
				Action = action
			};
		}

		public string Action {
			get { return ksString.Action; }
		}

		public virtual string ParamString {
			get { return ksString.ToJson(); }
		}
	}
}

