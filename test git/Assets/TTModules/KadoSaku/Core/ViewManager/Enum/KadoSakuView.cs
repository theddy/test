using System;

namespace KadoSaku.Core.View
{
	public enum KadoSakuView
	{
		// Login
		LoginView,

		// Achievement
		AchievementListView,
		AchievementResultView,

		// Leaderboard
		LeaderboardView,

		// Reward
		RewardView
	}
}

