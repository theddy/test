using System;
using KadoSaku.Core.Communication;
using KadoSaku.Api;

namespace KadoSaku.Core.View
{
	public interface IViewManagerService
	{
		bool IsShown { get; }
		event DelegateViewState OnViewIsShownEvent;

		void ShowView (ParamView param);
		void CloseView ();
	}
}

