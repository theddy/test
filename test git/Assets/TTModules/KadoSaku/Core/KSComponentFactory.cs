using System;
using UnityEngine;

namespace KadoSaku.Core
{
	internal class KSComponentFactory
	{
		public const string GameObjectName = "KadoSakuSDK";
		
		private static GameObject kadosakuGameObject;
		
		internal enum IfNotExist
		{
			AddNew,
			ReturnNull
		}
		
		private static GameObject KadoSakuGameObject
		{
			get
			{
				if (kadosakuGameObject == null)
				{
					kadosakuGameObject = new GameObject(GameObjectName);
				}
				
				return kadosakuGameObject;
			}
		}
		
		/**
         * Gets one and only one component.  Lazy creates one if it doesn't exist
         */
		public static T GetComponent<T>(IfNotExist ifNotExist = IfNotExist.AddNew) where T : MonoBehaviour
		{
			var gameObject = KadoSakuGameObject;
			
			T component = gameObject.GetComponent<T>();
			if (component == null && ifNotExist == IfNotExist.AddNew)
			{
				component = gameObject.AddComponent<T>();
			}
			
			return component;
		}
		
		/**
         * Creates a new component on the Social object regardless if there is already one
         */
		public static T AddComponent<T>() where T : MonoBehaviour
		{
			return KadoSakuGameObject.AddComponent<T>();
		}
	}
}

