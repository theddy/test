using System;
using UnityEngine;
using KadoSaku.Core.Communication;
using KadoSaku.Core.View;
using KadoSaku.Api;
using KadoSaku.Core.Platform;

namespace KadoSaku.Core
{
	public class KadoSakuGameObject : MonoBehaviour 
	{
		public IKadoSakuPlatform kadoSakuPlatform { get; set; }

		public void Awake () {
			DontDestroyOnLoad(this);
        }

		void OnApplicationFocus (bool focus) {
			if(focus){
				if (kadoSakuPlatform != null) {
					if (kadoSakuPlatform.OnApplicationFocus != null) kadoSakuPlatform.OnApplicationFocus();
				}
			}		
		}
	}
}

