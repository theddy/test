using System;
using KadoSaku.Core.Communication;
using KadoSaku.Core.View;

namespace KadoSaku.Core.Platform
{
	public interface IKadoSakuPlatform : ICommunicationService, IViewManagerService {
		Action OnApplicationFocus { get; set; }
	}
}

