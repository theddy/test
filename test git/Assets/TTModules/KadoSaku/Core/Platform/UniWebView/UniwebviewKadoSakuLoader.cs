using System;
using KadoSaku.Api;
using KadoSaku.Core.Communication;
using Touchten.Social;
using Touchten.Analytics;

namespace KadoSaku.Core.Platform
{
	internal class UniwebviewKadoSakuLoader : KS.CompiledKadoSakuLoader
	{
		protected override void LoadServices (KadoSakuGameObject kadoSakuGO, ITTSocialExtension socialService, IAnalyticsData analyticsData)
		{
			kadoSakuGO.kadoSakuPlatform = new UniwebviewKadoSakuPlatform(kadoSakuGO.transform, socialService, analyticsData);
		}
	}
}

