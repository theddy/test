using System;
using KadoSaku.Core.Communication;
using KadoSaku.Utility;
using KadoSaku.Settings;
using UniRx;
using UnityEngine;
using Touchten.Social;
using Touchten.Analytics;

namespace KadoSaku.Core.Platform
{
	public class UniwebviewKadoSakuPlatform : KadoSakuPlatform
	{
		UniWebView _webView;
		bool isLoaded = false;
		bool isLoading = false;

		public UniwebviewKadoSakuPlatform (Transform root, ITTSocialExtension socialService, IAnalyticsData analyticsData) : base(root, socialService, analyticsData) {} 

		#region implemented abstract members of WebCommunicationService

		protected override void OnInit (Transform root)
		{
			KadoSaku.Utility.KSDebug.Log("OnInit");
			GameObject container =  new GameObject("KadoSaku UniWebView Plugin");
			_webView = container.AddComponent<UniWebView>();
			container.GetComponent<Transform>().SetParent(root);	
			MonoBehaviour.DontDestroyOnLoad(container);
			
			_webView.AddUrlScheme(KSConstants.urlScheme);
			_webView.CleanCache();
			_webView.CleanCookie();

			_webView.bouncesEnable = false;
			_webView.backButtonEnable = false;

			OnBackButtonReceived().Subscribe(_ => CloseView());
		}

		protected override IObservable<Unit> LoadStream ()
		{
			if (isLoaded) {
				return Observable.Return(Unit.Default);
			}

			var onLoadComplete = Observable.FromEvent<UniWebView.LoadCompleteDelegate, LoadResultWrapper>(
				handler => (UniWebView webView, bool success, string errorMessage) => {
					KadoSaku.Utility.KSDebug.Log("Load Callback: " + success);
					handler(new LoadResultWrapper (webView, success, errorMessage));
				},
				loadCompleteHandler => _webView.OnLoadComplete += loadCompleteHandler,
				loadCompleteHandler => _webView.OnLoadComplete -= loadCompleteHandler
			);

			return Observable.Create<Unit> (
				observer => {
					if(!isLoading) {
						onLoadComplete
							.Timeout(TimeSpan.FromSeconds(KSUtility.TIMEOUT_SECONDS))
							.Take(1)
							.Subscribe(
								res => {
									if (!res.success) {
										isLoaded = false;
										KadoSaku.Utility.KSDebug.LogError("Load Failed");
										if (IsShown) CloseView();
										observer.OnError(new Exception(res.errorMessage));
									}
									else {
										isLoaded = true;
										
										observer.OnNext(Unit.Default);
										observer.OnCompleted();
										KadoSaku.Utility.KSDebug.Log("Load Complete");
									}
									isLoading = false;
								},
								ex => { 
									isLoading = false;
									observer.OnError(ex);
								}
							);
						
						KadoSaku.Utility.KSDebug.Log("Load Webview");
						_webView.url = KSCoreSettings.API_URL;
						_webView.Load();
						isLoading = true;
					}
					return Disposable.Empty;
				}
			);
		}

		protected override IObservable<string> ApiCallStream (ParamRequest param)
		{
			var onCallComplete = Observable.FromEvent<UniWebView.ReceivedMessageDelegate, UniWebViewMessage> (
				handler => (UniWebView webview, UniWebViewMessage message) => {
					handler(message);
				},
				messageReceivedHandler => _webView.OnReceivedMessage += messageReceivedHandler,
				messageReceivedHandler => _webView.OnReceivedMessage -= messageReceivedHandler
			);
			
			return Observable.Create<string> (
				observer => {
					onCallComplete
						.Timeout(TimeSpan.FromSeconds(KSUtility.TIMEOUT_SECONDS))
						.Where(msg => msg.path == KSConstants.onResponseKey)
						.Select(msg => msg.args[KSConstants.resultKey])
						.Subscribe(
							res => {
								observer.OnNext(res);
								observer.OnCompleted();
							},
							ex => observer.OnError(ex)
						);
					
					string callString = WebCallString.CreateApiCall(param);
					KadoSaku.Utility.KSDebug.Log(callString);
					_webView.EvaluatingJavaScript(callString);
					return Disposable.Empty;
				}
			);
		}

		protected override IObservable<Unit> MethodCallStream (ParamBase param, string methodName)
		{
			var onEvalJSCompleted = Observable.FromEvent<UniWebView.EvalJavaScriptFinishedDelegate, string> (
				handler => (UniWebView webview, string result) => {
					handler(result);
				},
				messageReceivedHandler => _webView.OnEvalJavaScriptFinished += messageReceivedHandler,
				messageReceivedHandler => _webView.OnEvalJavaScriptFinished -= messageReceivedHandler
			);
			
			return Observable.Create<Unit> (
				observer => {
					onEvalJSCompleted
						.Timeout(TimeSpan.FromSeconds(KSUtility.TIMEOUT_SECONDS))
						.Subscribe(
							res => {
								observer.OnNext(Unit.Default);
								observer.OnCompleted();
							},
							ex => observer.OnError(ex)
						);
					
					string callString = WebCallString.CreateMethodCall(param, methodName);
					_webView.EvaluatingJavaScript(callString);
					return Disposable.Empty;
				}
			);
		}

		protected override IObservable<string> OnWebCallReceived ()
		{
			return OnWebViewMessageReceived(KSConstants.unityCallKey).Select(msg => msg.args[KSConstants.requestKey]);
		}

		protected override IObservable<Unit> OnCloseViewReceived ()
		{
			return OnWebViewMessageReceived(KSConstants.closeKey).Select(msg => Unit.Default);
		}

		protected override IObservable<string> OnOpenLinkReceived ()
		{
			return OnWebViewMessageReceived(KSConstants.openLinkKey).Select(msg => msg.args[KSConstants.linkKey]);
		}

		protected override void OnViewClosed ()
		{
			_webView.Hide();
			KadoSaku.Utility.KSDebug.Log("------ View Closed -------");
		}

		protected override void OnViewShown ()
		{
			_webView.Show();
		}
		#endregion

		IObservable<UniWebViewMessage> OnWebViewMessageReceived (string path) {
			var onMessageReceived = Observable.FromEvent<UniWebView.ReceivedMessageDelegate, UniWebViewMessage> (
				handler => (UniWebView webview, UniWebViewMessage message) => {
					handler(message);
				},
				messageReceivedHandler => _webView.OnReceivedMessage += messageReceivedHandler,
				messageReceivedHandler => _webView.OnReceivedMessage -= messageReceivedHandler
			);
			return onMessageReceived.Where( msg => msg.path == path);
		}

		private class LoadResultWrapper {
			public UniWebView webview { get; private set;}
			public bool success { get; private set; }
			public string errorMessage { get; private set; }
			
			public LoadResultWrapper (UniWebView webview, bool success, string errorMessage) {
				this.webview = webview;
				this.success = success;
				this.errorMessage = errorMessage;
			}
		}

		const int KEYCODE_BACK = 4;
		IObservable<Unit> OnBackButtonReceived () {
			var onKeyReceieved = Observable.FromEvent<UniWebView.ReceivedKeyCodeDelegate, int> (
				handler => (UniWebView webView, int keyCode) => {
					handler(keyCode);
				},
				messageReceivedHandler => _webView.OnReceivedKeyCode += messageReceivedHandler,
				messageReceivedHandler => _webView.OnReceivedKeyCode -= messageReceivedHandler
			);
			return onKeyReceieved.Where( key => key == KEYCODE_BACK)
								 .Select(_ => Unit.Default);
		}
	}
}

