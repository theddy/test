using System;
using UniRx;
using UnityEngine;
using Touchten.Social;
using Touchten.Analytics;
using KadoSaku.Utility;
using KadoSaku.Core.Communication;
using KadoSaku.Api;

namespace KadoSaku.Core.Platform
{
	public abstract class KadoSakuPlatform : IKadoSakuPlatform 
	{
		public WebCallHandler webCallHandler { get; private set; }
		public WebCallbackHandler webCallbackHandler { get; private set; }

		Transform root;
		bool isInitialized = false;

		#region IKadoSakuPlatform implementation

		public Action OnApplicationFocus { get; set; }

		#endregion


		#region ICommunicationService implementation

		public KadoSakuPlatform (Transform root, ITTSocialExtension socialService, IAnalyticsData analyticsData) {
			this.root = root;
			this.callbackManager = new CallbackManager();
			this.webCallHandler = new WebCallHandler(this, socialService, analyticsData);
			this.webCallbackHandler = new WebCallbackHandler(this.callbackManager);
			InitStream().Subscribe(_ => {});
		}

		public ITTSocialExtension socialService { get; private set; }
		public CallbackManager callbackManager { get; private set; }

		public void CallToWeb (ParamRequest param) {
			var query = from _init in InitStream()
						from _load in LoadStream()
						from _call in ApiCallStream(param)
						select (string)_call;
			
			query.Timeout(TimeSpan.FromSeconds(KSUtility.TIMEOUT_SECONDS)).Subscribe(
				response => webCallbackHandler.ProcessCallback(response),
				ex => {
					callbackManager.OnKadoSakuInternalError(KSString.CreateInternalError(ex.Message), false, param.CallbackId);
				}
			);
		}

		public void CallbackFromUnity (ParamCallback param) {
			var query = from _call in MethodCallStream(param, KSConstants.clientCallbackMethod)
						select (Unit)_call;
			
			query.Timeout(TimeSpan.FromSeconds(KSUtility.TIMEOUT_SECONDS)).Subscribe(
				_ => KadoSaku.Utility.KSDebug.Log("Callback Sent"),
				ex => {
					KadoSaku.Utility.KSDebug.LogError(ex.Message);
				}
			);
		}

		#endregion

		#region IViewManagerService implementation

		public event DelegateViewState OnViewIsShownEvent;

		bool _isShown;
		public bool IsShown {
			get { return _isShown; }
			private set {
				if (_isShown != value) {
					_isShown = value;
					if (OnViewIsShownEvent != null) OnViewIsShownEvent(value);
				}
			}
		}

		public void ShowView (ParamView param)
		{
			var query = from _init in InitStream()
						from _show in ShowViewStream()
						from _load in LoadStream()
						from _call in MethodCallStream(param, KSConstants.showViewMethod)
						select (Unit)_call;
			
			query.Timeout(TimeSpan.FromSeconds(KSUtility.TIMEOUT_SECONDS)).Subscribe(
				_ => {},
				ex => {
					if (ex is TimeoutException) {
						CloseView();
					}
				}
			);
		}

		public void CloseView () {
			OnViewClosed();
			IsShown = false;
		}
		#endregion

		protected abstract void OnInit (Transform root);
		protected abstract IObservable<Unit> LoadStream ();
		protected abstract IObservable<string> ApiCallStream (ParamRequest param);
		protected abstract IObservable<Unit> MethodCallStream (ParamBase param, string methodName);
		protected abstract IObservable<string> OnWebCallReceived ();

		protected abstract IObservable<Unit> OnCloseViewReceived ();
		protected abstract void OnViewShown ();
		protected abstract void OnViewClosed ();

		protected abstract IObservable<string> OnOpenLinkReceived ();

		IObservable<Unit> InitStream () {
			if (isInitialized) {
				return Observable.Return<Unit>(Unit.Default);
			}

			return Observable.Create<Unit> (
				observer => {
					OnInit(this.root);
					ListenToWebCall();
					ListenToCloseView();
					ListenToOpenLinkCall();
					isInitialized = true;

					observer.OnNext(Unit.Default);
					observer.OnCompleted();
					return Disposable.Empty;
				}
			);
		}

		private void ListenToWebCall () {
			var onCallReceived = OnWebCallReceived();
			onCallReceived.Subscribe(
				message => {
					webCallHandler.ProcessCall(message);
				},
				ex => KadoSaku.Utility.KSDebug.LogError(ex.Message)
			);
		}

		private void ListenToCloseView () {
			var onCallReceived = OnCloseViewReceived();
			onCallReceived.Subscribe(
				message => {
					CloseView();
				},
				ex => KadoSaku.Utility.KSDebug.LogError(ex.Message)
			);
		}

		private IObservable<Unit> ShowViewStream() {
			OnViewShown();
			IsShown = true;
			return Observable.Return<Unit>(Unit.Default);
		}

		private void ListenToOpenLinkCall () {
			var onOpenLinkReceived = OnOpenLinkReceived();
			onOpenLinkReceived.Subscribe(
				link => {
					Application.OpenURL(link);
				},
				ex => KadoSaku.Utility.KSDebug.LogError(ex.Message)
			);
		}
	}
}

