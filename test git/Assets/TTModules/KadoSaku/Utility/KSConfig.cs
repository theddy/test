using System;
using KadoSaku.Settings;
using UnityEngine;
using KadoSaku.Api;
using Touchten.Core;

namespace KadoSaku.Utility
{
	public class KSConfig
	{
		public static bool IsEncrypted { get; set; }

		public static ISharedSettings sharedSettings;

		public static string Language { 
			get {
				return sharedSettings.Language;
			} 
		} 

		public static string Location { 
			get {
				return sharedSettings.Location;
			} 
		}

		public static string DeviceId {
			get { return sharedSettings.DeviceId; }
		}

		public static bool IsLogin {
			get { 
				if (string.IsNullOrEmpty(MembershipToken)) return false;
				return true;
			}
		}

		public static string MembershipToken { get; private set; }

		public static string App {
			get { return AppId + ":" + AppSecret; }
		}

		public static string AppId {
			get { return KSCoreSettings.GAME_APP_ID; }
		}

		public static string AppSecret {
			get { return KSCoreSettings.GAME_APP_SECRET; }
		}

		public static string PublicKey { get; set; }

		public static void SaveMembershipToken (string token) {
			MembershipToken = token;
			PlayerPrefs.SetString(KSConstants.membershipToken, token);
		}

		public static void DeleteMembershipToken () {
			MembershipToken = null;
			PlayerPrefs.DeleteKey(KSConstants.membershipToken);
		}

		public static void LoadMembershipToken () {
			if (PlayerPrefs.HasKey(KSConstants.membershipToken)) {
				MembershipToken = PlayerPrefs.GetString(KSConstants.membershipToken);
			}
			else {
				MembershipToken = null;
			}
		}

		public static IPlayerProfileModel PlayerProfile { get; set; }
	}
}

