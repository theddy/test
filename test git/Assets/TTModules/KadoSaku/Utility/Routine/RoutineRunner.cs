using System;
using UnityEngine;
using System.Collections;

namespace KadoSaku.Utility
{
	public class RoutineRunner : IRoutineRunner
	{

		public GameObject contextView{ get; set; }
		
		private RoutineRunnerBehaviour mb;
		

		public void PostConstruct()
		{
			KadoSaku.Utility.KSDebug.Log("qweqweqweqw");
			mb = contextView.AddComponent<RoutineRunnerBehaviour> ();
		}
		
		public Coroutine StartCoroutine(IEnumerator routine)
		{
			return mb.StartCoroutine(routine);
		}

		public void StopCoroutine (IEnumerator routine)
		{
			mb.StopCoroutine(routine);
		}
	}
	
	public class RoutineRunnerBehaviour : MonoBehaviour
	{
	}
}




