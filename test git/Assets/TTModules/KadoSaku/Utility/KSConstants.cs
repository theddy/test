using System;

namespace KadoSaku.Utility
{
	public class KSConstants
	{
		public const string Content = "content";
		public const string Queries = "query";
		public const string Headers = "header";
		public const string Body = "body";

		public const string membershipToken = "membershipToken";

		public const string showViewMethod = "showView";
		public const string clientCallbackMethod = "clientCallback";
		public const string apiCallMethod = "apiCall";

		public const string urlScheme = "kadosaku";
		public const string onResponseKey = "onResponse";
		public const string resultKey = "result";
		public const string requestKey = "request";
		public const string closeKey = "closeView";
		public const string unityCallKey = "unityCall";
		public const string openLinkKey = "openLink";
		public const string linkKey = "link";

		public class Error {
			public const int NOT_LOGGED_IN_CODE = 404;
			public const string NOT_LOGGED_IN_MESSAGE = "Unathorized, Require Login";
		}
	}
}

