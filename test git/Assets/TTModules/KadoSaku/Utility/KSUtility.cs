using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.ComponentModel;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using KadoSaku.Api;
using KadoSaku.Core.Communication;
using System.Text;

namespace KadoSaku.Utility
{
	public static class KSUtility
	{
		public const double TIMEOUT_SECONDS = 30;

		private const string WarningMissingParameter = "Did not find expected value '{0}' in dictionary";

		public static bool TryGetValue<T>(
			this IDictionary<string, object> dictionary,
			string key,
			out T value)
		{
			object resultObj;
			if (dictionary.TryGetValue(key, out resultObj) && resultObj is T)
			{
				value = (T)resultObj;
				return true;
			}
			
			value = default(T);
			return false;
		}

		public static T ToEnum<T>(this string value)
		{
			return (T) Enum.Parse(typeof(T), value, true);
		}

		public static string SplitCamelCase( this string str )
		{
			return Regex.Replace( 
					Regex.Replace( 
			          str, 
			          @"(\P{Ll})(\P{Ll}\p{Ll})", 
			          "$1 $2" 
			          ), 
			                 @"(\p{Ll})(\P{Ll})", 
			                 "$1 $2" 
					);
		}

		public static T GetValue<T>(JToken token, string key, T defaultValue) {
			if (token[key] != null) {
				try {
					return token[key].ToObject<T>();
				}
				catch (ArgumentException ex) {
					KadoSaku.Utility.KSDebug.LogError(ex.Message);
					return defaultValue;
				}
				catch (JsonReaderException ex) {
					KadoSaku.Utility.KSDebug.LogError(ex.Message);
					return defaultValue;
				}
			}
			return defaultValue;
		}

		public static void CallbackNotLoggedInError<T> (KadoSakuDelegate<T> callback) where T : IResult
		{
			CallbackManager.CallErrorCallback(callback, KSString.CreateError(KSConstants.Error.NOT_LOGGED_IN_CODE, KSConstants.Error.NOT_LOGGED_IN_MESSAGE), false);
		}


		/// <summary>
		///  Match Custom Data Encoding
		/// </summary>
		static readonly char[] padding = { '=' };

		public static string EncodeCustomData (string plainCustomData) {
			var plainTextBytes = Encoding.UTF8.GetBytes(plainCustomData);
			return Convert.ToBase64String(plainTextBytes).TrimEnd(padding).Replace('+', '-').Replace('/', '_');	
		}

		public static string DecodeCustomData (string encodedCustomData) {
			var base64String = encodedCustomData.Replace('_', '/').Replace('-', '+');
			switch(encodedCustomData.Length % 4) {
				case 2: base64String += "=="; break;
				case 3: base64String += "="; break;
			}
			var base64EncodedBytes = Convert.FromBase64String(base64String);
			return Encoding.UTF8.GetString(base64EncodedBytes);
		}
	}
}

