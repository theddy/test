using UnityEngine;
using System.IO;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Touchten.Ads.AppODeal
{
	public enum BannerAdPosition : int {
		BOTTOM = 8,
		TOP = 16,
		CENTER = 32
	}

	public enum AdTypes {
		BANNER,
		INTERSTITIAL,
		VIDEO,
		REWARDED_VIDEO
	}

	#if UNITY_EDITOR
	[InitializeOnLoad]
	#endif
	public class TouchtenAppODealSettings : ScriptableObject
	{
		const string touchtenAODSettingsAssetName = "TouchtenAppODealSettings";
		const string touchtenAODSettingsPath = "Touchten Configuration/Resources";
		const string touchtenAODSettingsAssetExtension = ".asset";
		
		private static TouchtenAppODealSettings _instance;
		
		static TouchtenAppODealSettings Instance 
		{
			get 
			{
				if (_instance == null) 
				{
					_instance = Resources.Load(touchtenAODSettingsAssetName) as TouchtenAppODealSettings;
					if (_instance == null) {
						_instance = CreateInstance<TouchtenAppODealSettings>();
#if UNITY_EDITOR
						string properPath = Path.Combine(Application.dataPath, touchtenAODSettingsPath);
						if (!Directory.Exists(properPath))
						{
							AssetDatabase.CreateFolder("Assets", "Touchten Configuration");
							AssetDatabase.CreateFolder("Assets/Touchten Configuration", "Resources");
						}
						
						string fullPath = Path.Combine(Path.Combine("Assets", touchtenAODSettingsPath),
						                               touchtenAODSettingsAssetName + touchtenAODSettingsAssetExtension);
						AssetDatabase.CreateAsset(_instance, fullPath);
#endif
					}
				}
				return _instance;
			}
		}
		
#if UNITY_EDITOR
		[MenuItem("Touchten/Ads/Edit AppODeal Settings", false, 5)]
		public static void EditSettings () {
			Selection.activeObject = Instance;
			EditorApplication.ExecuteMenuItem("Window/Inspector");
		}
#endif
		
		#region Settings
		[SerializeField] bool _appodealTestMode = false;
		[SerializeField] string _appodealAndroidKey = string.Empty;
		[SerializeField] string _appodealIosKey = string.Empty;
		[SerializeField] BannerAdPosition _appodealAdsPosition = BannerAdPosition.BOTTOM;
		[SerializeField] bool _appodealBannerActive = true;
		[SerializeField] bool _appodealInterstitialActive = true;
		[SerializeField] bool _appodealVideoActive = true;
		[SerializeField] bool _appodealRewardedVideoActive = true;

		public static string ApiKey {
			get { 
				string id = "unused";
#if UNITY_ANDROID
				id = Instance._appodealAndroidKey;
#elif UNITY_IPHONE
				id = Instance._appodealIosKey;
#endif
				return id;
			}
		}

		public static BannerAdPosition BannerPosition {
			get {
				return Instance._appodealAdsPosition;
			}
		}

		public static bool TestingMode {
			get {
				return Instance._appodealTestMode;
			}
		}

		public static Dictionary<AdTypes, bool> ActiveAds {
			get {
				Dictionary<AdTypes, bool> activeAds = new Dictionary<AdTypes, bool>();
				activeAds.Add(AdTypes.BANNER, Instance._appodealBannerActive);
				activeAds.Add(AdTypes.INTERSTITIAL, Instance._appodealInterstitialActive);
				activeAds.Add(AdTypes.VIDEO, Instance._appodealVideoActive);
				activeAds.Add(AdTypes.REWARDED_VIDEO, Instance._appodealRewardedVideoActive);

				return activeAds;
			}
		}
		#endregion
	}
}
