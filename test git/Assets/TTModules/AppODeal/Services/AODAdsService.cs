using System;
using System.Collections.Generic;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine;
using System.Collections;

namespace Touchten.Ads.AppODeal
{
	public class AODAdsService : MonoBehaviour, IAdsBannerService, IBannerAdListener, IAdsInterstitialService, IInterstitialAdListener, IAdsVideoService, ISkippableVideoAdListener, IAdsRewardedVideoService, IRewardedVideoAdListener
	{
		public event EventHandler<AdsResult> onInitComplete;
		public event EventHandler<AdsResult> onCachedBanner;
		public event EventHandler<AdsResult> onShownBanner;
		public event Action onInteractBanner;
		public event EventHandler<AdsResult> onCachedInterstitial;
		public event EventHandler<AdsResult> onShownInterstitial;
		public event Action onClosedInterstitial;
		public event Action onInteractInterstitial;
		public event EventHandler<AdsResult> onCachedVideo;
		public event EventHandler<AdsResult> onShownVideo;
		public event Action onFinishedVideo;
		public event Action onClosedVideo;
		public event EventHandler<AdsResult> onCachedRewardedVideo;
		public event EventHandler<AdsResult> onShownRewardedVideo;
		public event Action<AdsReward> onFinishedRewardedVideo;
		public event Action onClosedRewardedVideo;

		public bool IsInitialized { get; private set; }
		public bool IsBannerCached { 
			get{
				return Appodeal.isLoaded (Appodeal.BANNER);
			} 
		}
		public bool IsInterstitialCached { 
			get{
				return Appodeal.isLoaded (Appodeal.INTERSTITIAL);
			}
		}
		public bool IsVideoCached { 
			get{
				return Appodeal.isLoaded (Appodeal.SKIPPABLE_VIDEO);
			} 
		}
		public bool IsRewardedVideoCached { 
			get{
				return Appodeal.isLoaded (Appodeal.REWARDED_VIDEO);
			} 
		}

		static GameObject _gameObject;
		public static AODAdsService Instance {
			get {
				if (_gameObject == null) {
					_gameObject = new GameObject ("AppODeal");
					MonoBehaviour.DontDestroyOnLoad (_gameObject);
				}
				var instance = _gameObject.GetComponent<AODAdsService> ();
				if(instance == null) {
					instance = _gameObject.AddComponent<AODAdsService> ();
				}
				return instance;
			}
		}
			
		DateTime bannerTimeout = new DateTime(0);
		DateTime interstitialTimeout = new DateTime (0);
		DateTime videoTimeout = new DateTime(0);
		DateTime rewardedVideoTimeout = new DateTime(0);

		public void InitAds ()
		{
			if(!IsInitialized){
				int activeAds = 0;
				foreach(KeyValuePair<AdTypes, bool> activeAd in TouchtenAppODealSettings.ActiveAds) {
					switch(activeAd.Key){
					case AdTypes.BANNER:
						if(activeAd.Value) activeAds |= Appodeal.BANNER;
						break;
					case AdTypes.INTERSTITIAL:
						if(activeAd.Value) activeAds |= Appodeal.INTERSTITIAL;
						break;
					case AdTypes.VIDEO:
						if(activeAd.Value) activeAds |= Appodeal.SKIPPABLE_VIDEO;
						break;
					case AdTypes.REWARDED_VIDEO:
						if(activeAd.Value) activeAds |= Appodeal.REWARDED_VIDEO;
						break;
					}
				}

			#if UNITY_IPHONE
				Appodeal.setAutoCache(activeAds, true);
			#elif UNITY_ANDROID
				Appodeal.setAutoCache(activeAds, false);
			#endif
				Appodeal.setLogging(true);
				Appodeal.setTesting (TouchtenAppODealSettings.TestingMode);
				Appodeal.confirm (Appodeal.SKIPPABLE_VIDEO);
				Appodeal.setBannerCallbacks(this);
				Appodeal.setInterstitialCallbacks(this);
				Appodeal.setSkippableVideoCallbacks(this);
				Appodeal.setRewardedVideoCallbacks (this);
				AdsDebug.Log("[AppODeal Ads] Init");
				Appodeal.initialize(TouchtenAppODealSettings.ApiKey, activeAds);

				StartCoroutine (RunTimer());
			}
			IsInitialized = true;

			if(onInitComplete != null) onInitComplete(this, new AODResult());
		}

		IEnumerator RunTimer(){
			while(true){
				var defaultTime = new DateTime (0);
				if(bannerTimeout > defaultTime && DateTime.Now > bannerTimeout){
					OnBannerTimeout ();
				}
				if(interstitialTimeout > defaultTime && DateTime.Now > interstitialTimeout){
					OnInterstitialTimeout ();
				}
				if(videoTimeout > defaultTime && DateTime.Now > videoTimeout){
					OnVideoTimeout ();
				}
				if(rewardedVideoTimeout > defaultTime && DateTime.Now > rewardedVideoTimeout){
					OnRewardedVideoTimeout ();
				}

				yield return new WaitForSeconds (1f);
			}
		}

		public void ShowBanner (string adsId)
		{
			AdsDebug.Log ("[AppODeal Ads] Banner show");
			bannerTimeout = DateTime.Now.AddSeconds (10);
			Appodeal.show ((int)TouchtenAppODealSettings.BannerPosition);
		}

		void OnBannerTimeout()
		{
			AdsDebug.Log ("[AppODeal Ads] Banner show timeout");
			bannerTimeout = new DateTime(0);
			if (onShownBanner != null) onShownBanner(this, new AODResult(new Exception("Banner show timeout reached")));
		}

		public void DestroyBanner (string adsId) { 
			AdsDebug.Log ("[AppODeal Ads] Banner hide");
			Appodeal.hide ((int)TouchtenAppODealSettings.BannerPosition);
		}

		public void CacheBanner (string adsId) { 
			AdsDebug.Log ("[AppODeal Ads] Banner creating");
			Appodeal.cache(Appodeal.BANNER);
		}

		public void CacheInterstitial (string adsId)
		{
			AdsDebug.Log("[AppODeal Ads] Interstitial caching");
			Appodeal.cache(Appodeal.INTERSTITIAL);
		}

		public void ShowInterstitial (string adsId)
		{
			AdsDebug.Log ("[AppODeal Ads] Interstitial show");
			interstitialTimeout = DateTime.Now.AddSeconds (10);
			Appodeal.show(Appodeal.INTERSTITIAL);
		}

		void OnInterstitialTimeout()
		{
			AdsDebug.Log ("[AppODeal Ads] Interstitial show timeout");
			interstitialTimeout = new DateTime(0);
			if (onShownInterstitial != null) onShownInterstitial(this, new AODResult(new Exception("Interstitial show timeout reached")));
		}

		public void CacheVideo (string adsId)
		{
			AdsDebug.Log ("[AppODeal Ads] Video caching");
			Appodeal.cache(Appodeal.SKIPPABLE_VIDEO);
		}

		public void ShowVideo (string adsId)
		{
			AdsDebug.Log ("[AppODeal Ads] Video show");
			videoTimeout = DateTime.Now.AddSeconds (10);
			Appodeal.show(Appodeal.SKIPPABLE_VIDEO);
		}

		void OnVideoTimeout()
		{
			AdsDebug.Log ("[AppODeal Ads] Video show timeout");
			videoTimeout = new DateTime(0);
			if (onShownVideo != null) onShownVideo(this, new AODResult(new Exception("Video show timeout reached")));
		}

		public void CacheRewardedVideo (string adsId)
		{
			AdsDebug.Log ("[AppODeal Ads] Rewarded video caching");
			Appodeal.cache(Appodeal.REWARDED_VIDEO);
		}

		public void ShowRewardedVideo (string adsId)
		{
			AdsDebug.Log ("[AppODeal Ads] Rewarded video show");
			rewardedVideoTimeout = DateTime.Now.AddSeconds (10);
			Appodeal.show(Appodeal.REWARDED_VIDEO);
		}

		void OnRewardedVideoTimeout()
		{
			AdsDebug.Log ("[AppODeal Ads] Rewarded video show timeout");
			rewardedVideoTimeout = new DateTime(0);
			if (onShownRewardedVideo != null) onShownRewardedVideo(this, new AODResult(new Exception("Rewarded video show timeout reached")));
		}


		#region IRewardedVideoAdListener implementation

		public void onRewardedVideoLoaded ()
		{
			AdsDebug.Log ("[AppODeal Ads] Rewarded video cached");
			if (onCachedRewardedVideo != null) onCachedRewardedVideo (this, new AODResult());
		}

		public void onRewardedVideoFailedToLoad ()
		{
			AdsDebug.Log ("[AppODeal Ads] Rewarded video cache failed");
			if (onCachedRewardedVideo != null) onCachedRewardedVideo (this, new AODResult(new Exception("Rewarded video failed to cache")));
		}

		public void onRewardedVideoShown ()
		{
			AdsDebug.Log("[AppODeal Ads] Rewarded video showing");
			rewardedVideoTimeout = new DateTime(0);
			if (onShownRewardedVideo != null) onShownRewardedVideo(this, new AODResult());
		}

		public void onRewardedVideoFinished (int amount, string name)
		{
			AdsDebug.Log("[AppODeal Ads] Rewarded video finished");
			if (onFinishedRewardedVideo != null) onFinishedRewardedVideo(new AODAdsReward (amount, name));
		}

		public void onRewardedVideoClosed ()
		{
			AdsDebug.Log("[AppODeal Ads] Rewarded video closed");
			if (onClosedRewardedVideo != null) onClosedRewardedVideo();
		}

		#endregion

		#region ISkippableVideoAdListener implementation

		public void onSkippableVideoLoaded() {
			AdsDebug.Log ("[AppODeal Ads] Video cached");
			if (onCachedVideo != null) onCachedVideo (this, new AODResult());
		}

		public void onSkippableVideoFailedToLoad() { 
			AdsDebug.Log ("[AppODeal Ads] Video cache failed");
			if (onCachedVideo != null) onCachedVideo (this, new AODResult(new Exception("Video failed to cache")));
		}

		public void onSkippableVideoShown() { 
			AdsDebug.Log("[AppODeal Ads] Video showing");
			videoTimeout = new DateTime(0);
			if (onShownVideo != null) onShownVideo (this, new AODResult());
		}

		public void onSkippableVideoFinished() { 
			AdsDebug.Log("[AppODeal Ads] Video finished");
			if (onFinishedVideo != null) onFinishedVideo();
		}

		public void onSkippableVideoClosed() { 
			AdsDebug.Log("[AppODeal Ads] Video closed");
			if (onClosedVideo != null) onClosedVideo();
		}

		#endregion

		#region IInterstitialAdListener implementation

		public void onInterstitialLoaded() {
			AdsDebug.Log("[AppODeal Ads] Interstitial cached");
			if (onCachedInterstitial != null) onCachedInterstitial(this, new AODResult());

		}
		public void onInterstitialFailedToLoad() { 
			AdsDebug.Log("[AppODeal Ads] Interstitial cache failed");
			if (onCachedInterstitial != null) onCachedInterstitial(this, new AODResult(new Exception("Failed in caching interstitial")));
		}
		public void onInterstitialShown() { 
			AdsDebug.Log("[AppODeal Ads] Interstitial showing");
			interstitialTimeout = new DateTime(0);
			if (onShownInterstitial != null) onShownInterstitial(this, new AODResult());
		}
		public void onInterstitialClosed() { 
			AdsDebug.Log("[AppODeal Ads] Interstitial closing");
			if (onClosedInterstitial != null) onClosedInterstitial();
		}
		public void onInterstitialClicked() { 
			AdsDebug.Log("[AppODeal Ads] Interstitial clicked");
			if (onInteractInterstitial != null) onInteractInterstitial();
		}

		#endregion

		#region IBannerAdListener implementation

		public void onBannerLoaded ()
		{
			AdsDebug.Log("[AppODeal Ads] Banner created");
			if(onCachedBanner != null) onCachedBanner(this, new AODResult());
		}

		public void onBannerFailedToLoad ()
		{
			AdsDebug.Log("[AppODeal Ads] Banner create failed");
			if(onCachedBanner != null) onCachedBanner(this, new AODResult(new Exception("Banner failed to load")));
		}

		public void onBannerShown ()
		{
			AdsDebug.Log("[AppODeal Ads] Banner showing");
			bannerTimeout = new DateTime(0);
			if (onShownBanner != null) onShownBanner(this, new AODResult());
		}

		public void onBannerClicked ()
		{
			AdsDebug.Log("[AppODeal Ads] Banner clicked");
			if (onInteractBanner != null) onInteractBanner ();
		}

		#endregion
	}
}