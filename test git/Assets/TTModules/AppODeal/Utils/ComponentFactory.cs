using UnityEngine;

namespace Touchten.Ads.AppODeal
{
	internal class AODComponentFactory
	{
		public const string GameObjectName = "AppODeal";
		private static GameObject ttAdsGameObject;
		
		private static GameObject TTAdsGameObject {
			get {
				if (ttAdsGameObject == null) {
					ttAdsGameObject = new GameObject (GameObjectName);
				}
				
				return ttAdsGameObject;
			}
		}
		
		/**
         * Gets one and only one component.  Lazy creates one if it doesn't exist
         */
		public static T GetComponent<T> () where T : MonoBehaviour
		{
			var ttAdsGameObject = TTAdsGameObject;
			
			T component = ttAdsGameObject.GetComponent<T> ();
			if (component == null) {
				component = ttAdsGameObject.AddComponent<T> ();
			}
			
			return component;
		}
		
		/**
         * Creates a new component on the TTIAP object regardless if there is already one
         */
		public static T AddComponent<T> () where T : MonoBehaviour
		{
			return TTAdsGameObject.AddComponent<T> ();
		}
	}
}