using System;
using System.Collections.Generic;
using Touchten.Ads;

namespace Touchten.Ads.AppODeal {
	public class CallbackManager {
		Queue<object> callbackDelegates = new Queue<object>();

		public void AddDelegate<T>(EventHandler<T> callback) where T : AdsResult {
			if (callback == null)
				return;

			this.callbackDelegates.Enqueue (callback);
		}

		public void OnResponse(object sender, AdsResult result){
			if (result == null)
				throw new Exception ("Result cannot be null");

			while(this.callbackDelegates.Count > 0){
				var callback = this.callbackDelegates.Dequeue ();
				CallbackManager.CallCallback (callback, sender, result);
			}
		}

		static void CallCallback(object callback, object sender, AdsResult result) {
			if (callback == null)
				return;

			if (CallbackManager.TryCallCallback<AODResult> (callback, sender, result))
				return;

			throw new NotSupportedException("Unexpected result type: " + callback.GetType().FullName);
		}

		static bool TryCallCallback<T>(object callback, object sender, AdsResult result) where T : AdsResult {
			var castedCallback = callback as EventHandler<AdsResult>;
			if (castedCallback != null){
				castedCallback (sender, (T)result);
				return true;
			}

			return false;
		}
	}

}