using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public interface ITestModule
{

	IList<string> MenuButtonResourcePath{ get; }

	IList<string> ModuleMenuResourcePath{ get; }


}
