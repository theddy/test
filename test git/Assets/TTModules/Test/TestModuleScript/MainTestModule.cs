using UnityEngine;
using System.Collections;
using System.Reflection;
using System;
using System.Linq;
using System.Collections.Generic;

public class MainTestModule : MonoBehaviour
{

	// Use this for initialization

	public Transform ButtonParent;
	public Transform MenuParent;

	void Start ()
	{
		var type = typeof(ITestModule);
		var types = Assembly.Load ("Assembly-CSharp").GetTypes ().Where (x => type.IsAssignableFrom (x));
	
		InitTypes (types);
		InitMainMenuButton (types);
		InitModuleMenus (types);

	}

	void InitTypes (IEnumerable<Type> types)
	{
		foreach (var type in types) {
			MethodInfo method = type.GetMethod ("Init");
			if (method != null) {
				method.Invoke (method, null);

			}
		}

	}

	void InitMainMenuButton (IEnumerable<Type> types)
	{
		foreach (var type in types) {
			MethodInfo method = type.GetMethod ("GetMenuButtonResourcePath");
			if (method != null) {
				IList<string> buttonPaths = (IList<string>)method.Invoke (method, null);

				if (buttonPaths != null && buttonPaths.Count > 0) {
					foreach (var path in buttonPaths) {
						GameObject go = Instantiate<GameObject> (Resources.Load (path) as GameObject);
						go.transform.SetParent (ButtonParent);
						go.transform.localScale = Vector3.one;
					}
				}
			}
		}
	}

	void InitModuleMenus (IEnumerable<Type> types)
	{
		foreach (var type in types) {
			MethodInfo method = type.GetMethod ("GetModuleMenuResourcePath");
			if (method != null) {
				IList<string> buttonPaths = (IList<string>)method.Invoke (method, null);
				if (buttonPaths != null && buttonPaths.Count > 0) {
					foreach (var path in buttonPaths) {
						GameObject go = Instantiate<GameObject> (Resources.Load (path) as GameObject);
						go.transform.SetParent (MenuParent);
						RectTransform rectTransform = go.GetComponent<RectTransform> ();
						rectTransform.offsetMax = Vector2.zero;
						rectTransform.offsetMin = Vector2.zero;
						go.transform.localScale = Vector3.one;
					}
				}
			}
		}
	}


}