using UnityEngine;
using System.Collections;
namespace Touchten.TestScene{
public class TestSceneEvent
{

	public delegate void OnModuleInitComplete (bool success);

	public static event OnModuleInitComplete OnTTCoreInitCompleteE;
	public static event OnModuleInitComplete OnAdsInitCompleteE;
	public static event OnModuleInitComplete OnGameServiceInitCompleteE;
	public static event OnModuleInitComplete OnSocialServiceInitCompleteE;
	public static event OnModuleInitComplete OnIAPServiceInitCompleteE;
	public static event OnModuleInitComplete OnLocalNotifServiceInitComplete;
	public static event OnModuleInitComplete OnKadoSakuInitCompleteE;


	public static void OnTTCoreInitComplete (bool success)
	{		
		BroadcastModuleInitComplete (OnTTCoreInitCompleteE, success);
	}

	public static void OnAdsInitComplete (bool success)
	{
		BroadcastModuleInitComplete (OnAdsInitCompleteE, success);
	}

	public static void OnGameServiceInitComplete (bool success)
	{
		BroadcastModuleInitComplete (OnGameServiceInitCompleteE, success);
	}

	public static void OnSocialServiceInitComplete (bool success)
	{
		BroadcastModuleInitComplete (OnSocialServiceInitCompleteE, success);
	}

	public static void OnIAPServiceInitComplete (bool success)
	{
		BroadcastModuleInitComplete (OnIAPServiceInitCompleteE, success);
	}

	public static void OnLocalNotifInitComplete (bool success)
	{
		BroadcastModuleInitComplete (OnLocalNotifServiceInitComplete, success);
	}

	public static void OnKadoSakuInitComplete (bool success)
	{
		BroadcastModuleInitComplete (OnKadoSakuInitCompleteE, success);
	}

	static void BroadcastModuleInitComplete (OnModuleInitComplete e, bool status)
	{
		if (e != null) {
			e (status);
		}

	}

}
}