using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text;

namespace Touchten.Utilities
{
	public class DebugConsoleCanvas : MonoBehaviour
	{

		static DebugConsoleCanvas instance;

		static DebugConsoleCanvas Instance {
			get {
				if (instance != null) {
					return instance;
				} else {
				
					instance = SetupNewDebugConsole ();
					return instance;
				}
			}
		}


		public GameObject TextPrefab;
		public Transform ConsoleParent;
		public GameObject ContentHolder;


		/// <summary>
		/// The hotkey to show and hide the console window.
		/// </summary>
		public KeyCode toggleKey = KeyCode.S;

		[SerializeField]
		List<Log> logs = new List<Log> ();
		Vector2 scrollPosition;
		public bool show = false;
		bool collapse = false;
		bool stackTrace = false;
		GUIStyle labelStyle = new GUIStyle ();
		// Visual elements:
		List<Text> textLog;
		static readonly Dictionary<LogType, Color> logTypeColors = new Dictionary<LogType, Color> () {
			{ LogType.Assert, Color.white },
			{ LogType.Error, Color.red },
			{ LogType.Exception, Color.red },
			{ LogType.Log, Color.white },
			{ LogType.Warning, Color.yellow },
		};


		static DebugConsoleCanvas SetupNewDebugConsole ()
		{
			GameObject go = Instantiate<GameObject> (Resources.Load ("Console/Console")as GameObject);
			instance = go.GetComponent<DebugConsoleCanvas> ();

			GameObject parent = Instantiate<GameObject> (Resources.Load ("Console/ConsoleCanvas")as GameObject);
			go.transform.SetParent (parent.transform);
			go.transform.localScale = Vector3.one;
			RectTransform rectTransform = go.GetComponent<RectTransform> ();

			rectTransform.anchorMax = new Vector2 (0f, 1f);
			rectTransform.anchorMin = new Vector2 (0f, 1f);

			rectTransform.offsetMax = Vector2.zero;
			rectTransform.offsetMin = Vector2.zero;
			return go.GetComponent<DebugConsoleCanvas> ();
		}

		void Awake ()
		{
			instance = this;
			show = gameObject.activeSelf;
			Application.logMessageReceived += HandleLog;
		}

		void OnDestroy ()
		{
			instance = null;
			Application.logMessageReceived -= HandleLog;
		}

		void Start ()
		{
			textLog = new List<Text> ();

		}


		void Update ()
		{
			if (Input.GetKeyDown (toggleKey)) {
				show = !show;
			}
		}

		public static void ToggleConsole ()
		{
			Instance._ToggleConsole ();
		}

		public static void ToggleStackTrace ()
		{
			Instance._ToggleStackTrace ();
		}

		public void _ToggleConsole ()
		{
			show = !show;
			ContentHolder.SetActive (show);
		}

		public void _ToggleStackTrace ()
		{
			stackTrace = !stackTrace;

			if (stackTrace) {
				for (int i = 0; i < logs.Count - 1; i++) {
					StringBuilder builder = new StringBuilder (logs [i].message);
					builder.AppendLine (logs [i].stackTrace);
					builder.AppendLine ();
					logs [i].textRef.text = builder.ToString ();				
				}
			} else {
				for (int i = 0; i < logs.Count - 1; i++) {

					logs [i].textRef.text = logs [i].message;				
				}
			}

		}

		public void Clear ()
		{
			for (int i = logs.Count - 1; i >= 0; i--) {

				GameObject.Destroy (logs [i].textRef.gameObject);
				logs.Remove (logs [i]);
			}
		}

		/// <summary>
		/// Records a log from the log callback.
		/// </summary>
		/// <param name="message">Message.</param>
		/// <param name="stackTrace">Trace of where the message came from.</param>
		/// <param name="type">Type of message (error, exception, warning, assert).</param>
		void HandleLog (string message, string stackTrace, LogType type)
		{
			GameObject go = Instantiate<GameObject> (TextPrefab);
			Text text = go.GetComponent<Text> ();
			logs.Add (new Log () {
				textRef = text,
				message = message,
				stackTrace = stackTrace,
				type = type,
			});

			if (!this.stackTrace) {
				text.text = message;
			} else {
				text.text = message + "\n" + stackTrace + "\n";
			}
			text.color = logTypeColors [type];


			go.transform.SetParent (ConsoleParent);
			go.transform.localScale = Vector3.one;
		}

		[System.Serializable]
		struct Log
		{
			public Text textRef;
			public string message;
			public string stackTrace;
			public LogType type;
		}
	}
}
