using UnityEngine;
using System.Collections;
using System;
namespace Touchten.TestScene{
public class MenuBase : MonoBehaviour
{
	public bool EnableBackButton;
	public string MenuName;
	[SerializeField]
	GameObject content;

	public Action OnActivated;

	public void ActivateMenu ()
	{
		content.SetActive (true);
		MenuManager.Instance.StackMenu (this);
		if(OnActivated!=null)OnActivated();

	}

	public void DeactivateMenu ()
	{
		content.SetActive (false);
	}
	public void ActivateContent()
	{
		content.SetActive (true);
		if(OnActivated!=null)OnActivated();
	}

}
}