using UnityEngine;
using System.Collections;
#if KADOSAKU_TEST
using KadoSaku.Api;
#endif
using UnityEngine.UI;

namespace Touchten.TestScene
{
	public class KadoSakuViewInputDisabler : MonoBehaviour
	{

		Canvas raycaster;
		bool subscribed = false;

		void Awake ()
		{
			#if KADOSAKU_TEST
			if (KS.IsInitComplete) {

				KS.onViewStateChangedEvent += KS_onViewStateChangedEvent;
				subscribed = true;
			} else {
				TestSceneEvent.OnKadoSakuInitCompleteE += TestSceneEvent_OnKadoSakuInitCompleteE;
				subscribed = true;
			}
			raycaster = GetComponent<Canvas> ();
			#endif
		}

		void TestSceneEvent_OnKadoSakuInitCompleteE (bool success)
		{
			#if KADOSAKU_TEST
			if (!subscribed)
				KS.onViewStateChangedEvent += KS_onViewStateChangedEvent;
		
			#endif
		}

		void KS_onViewStateChangedEvent (bool isViewShown)
		{
			#if KADOSAKU_TEST
			gameObject.SetActive (!isViewShown);
			#endif
		}


		void OnDestroy ()
		{
			#if KADOSAKU_TEST
			if (subscribed) {
				TestSceneEvent.OnKadoSakuInitCompleteE -= TestSceneEvent_OnKadoSakuInitCompleteE;
				KS.onViewStateChangedEvent -= KS_onViewStateChangedEvent;

			}
			#endif
		}
	}
}