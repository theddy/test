using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Touchten.TestScene
{
	[RequireComponent (typeof(Image))]
	public class StateIndicator : MonoBehaviour
	{
		public Image indicator;

		Color ErrorColor = new Color (1, 0, 0);
		Color IdleColor = new Color (1, 174 / 255f, 0);
		Color SuccessColor = new Color (61 / 255f, 240 / 255f, 4 / 255f);

		void Start ()
		{
			if (indicator == null) {
				indicator = GetComponent<Image> ();
			}
			SetStatus (IndicatorStatus.Idle);
		}

		public void SetStatus (IndicatorStatus status)
		{
			switch (status) {
			case IndicatorStatus.Error:
				indicator.color = ErrorColor;
				break;
			case IndicatorStatus.Idle: 
				indicator.color = IdleColor;
				break;
			case IndicatorStatus.Success:
				indicator.color = SuccessColor;
				break;
			}
		}

		public void SetStatus (bool status)
		{
			IndicatorStatus state = status == true ? IndicatorStatus.Success : IndicatorStatus.Error;
			SetStatus (state);
		}
	}

	public enum IndicatorStatus
	{
		Error,
		Idle,
		Success
	}
}