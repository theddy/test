using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Touchten.TestScene{
	
public class MenuManager : MonoBehaviour
{
	public static MenuManager Instance{ get; private set; }

	public Text MenuName;
	public Button backButton;
	public Stack<MenuBase> MenuQueue;

	void Awake ()
	{
		Instance = this;
	}

	void Start ()
	{
		MenuQueue = new Stack<MenuBase> ();
		MenuEvent.OpenMainMenu ();
		backButton.onClick.AddListener(delegate() {
			PreviousMenu();	
		});
	}

	public void StackMenu (MenuBase menu)
	{
		if (MenuQueue.Count > 0)
			MenuQueue.Peek ().DeactivateMenu ();
		MenuQueue.Push (menu);
		backButton.gameObject.SetActive(menu.EnableBackButton);
		MenuName.text = menu.MenuName;
	}

	public void PreviousMenu ()
	{
		MenuQueue.Peek ().DeactivateMenu ();
		MenuQueue.Pop ();

		MenuQueue.Peek ().ActivateContent ();
		MenuName.text = MenuQueue.Peek ().MenuName;
		backButton.gameObject.SetActive(MenuQueue.Peek().EnableBackButton);
	}


}

public class MenuEvent
{
	public delegate void OpenNewMenuHandler ();

	public static event OpenNewMenuHandler OpenMainMenuE;
	public static event OpenNewMenuHandler OpenAdsMenuE;
	public static event OpenNewMenuHandler OpenGameServiceE;
	public static event OpenNewMenuHandler OpenSocialServiceE;
	public static event OpenNewMenuHandler OpenIAPMenuE;
	public static event OpenNewMenuHandler OpenLocaNotifMenuE;
	public static event OpenNewMenuHandler OpenKadoSakuMenuE;

	public static event OpenNewMenuHandler OpenAchievementMenuE;
	public static event OpenNewMenuHandler OpenLeaderboardMenuE;
	public static event OpenNewMenuHandler OpenMatchmakingMenuE;


	public static void OpenAchievementMenu()
	{
		OpenMenu(OpenAchievementMenuE);
	}
	public static void OpenLeaderboardMenu()
	{
		OpenMenu(OpenLeaderboardMenuE);
	}

	public static void OpenMatchmakingMenu()
	{
		OpenMenu(OpenMatchmakingMenuE);
	}

	public static void OpenMainMenu ()
	{		
		OpenMenu (OpenMainMenuE);
	}

	public static void OpenAdsMenu ()
	{
		OpenMenu (OpenAdsMenuE);
	}
	public static void OpenGameService()
	{
		OpenMenu(OpenGameServiceE);
	}

	public static void OpenSocialService()
	{
		OpenMenu(OpenSocialServiceE);
	}

	public static void OpenIAPMenu()
	{
		OpenMenu(OpenIAPMenuE);
	}

	public static void OpenLocalNotifMenu()
	{
		OpenMenu(OpenLocaNotifMenuE);
	}

	public static void OpenKadoSakuMenu()
	{
		OpenMenu(OpenKadoSakuMenuE);
	}
	static void OpenMenu (OpenNewMenuHandler menu)
	{
		if (menu != null) {
			menu ();
		}
	}


}


}
