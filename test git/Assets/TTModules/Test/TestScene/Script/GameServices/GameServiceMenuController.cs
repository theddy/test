using UnityEngine;
using System.Collections;
using Touchten.Utilities;
using Touchten.Core;
namespace Touchten.TestScene
{
	[RequireComponent (typeof(GameServiceMenuView))]
	public class GameServiceMenuController : MonoBehaviour
	{
		GameServiceMenuView menuView;


		void Start ()
		{
			menuView = GetComponent<GameServiceMenuView> ();
		}

		public void Login ()
		{
		
			if (UnityEngine.Social.localUser.authenticated) {
				menuView.OnLoggedIn (true);
			} else {
				menuView.OnLoggingIn ();
				UnityEngine.Social.Active.localUser.Authenticate (delegate(bool obj) {
					menuView.OnLoggedIn (obj);
				});
			}
		}



		public void AchievementUI ()
		{
			if (UnityEngine.Social.localUser.authenticated) {
				UnityEngine.Social.Active.ShowAchievementsUI ();
			} else {
				TTCoreDebug.Log ("Not authenticated, login first");
			}
		}

		public void LeaderboardUI ()
		{
			if (UnityEngine.Social.localUser.authenticated) {
				UnityEngine.Social.Active.ShowLeaderboardUI ();
			} else {
				TTCoreDebug.Log ("Not authenticated, login first");
			}

		}
	}
}