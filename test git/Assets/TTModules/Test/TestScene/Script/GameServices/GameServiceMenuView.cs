using UnityEngine;
using System.Collections;
namespace Touchten.TestScene{
public class GameServiceMenuView : MenuBase
{
	public StateIndicator AuthenticatedStatus;
	void Awake ()
	{
		MenuEvent.OpenGameServiceE += MenuEvent_OpenGameServiceE;
	}

	void OnDestroy ()
	{
		MenuEvent.OpenGameServiceE -= MenuEvent_OpenGameServiceE;
	}

	void MenuEvent_OpenGameServiceE ()
	{
		ActivateMenu();
	}

	public void OnLoggingIn()
	{

	}

	public void OnLoggedIn(bool status)
	{
		AuthenticatedStatus.SetStatus(status);
	}

}
}