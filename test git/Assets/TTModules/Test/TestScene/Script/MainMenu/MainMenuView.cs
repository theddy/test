using UnityEngine;
using System.Collections;
using UnityEngine.UI;
namespace Touchten.TestScene{
public class MainMenuView : MenuBase
{

	public StateIndicator TTCoreIndicator;
	public StateIndicator AdsIndicator;
	public StateIndicator GameServiceIndicator;
	public StateIndicator SocialIndicator;
	public StateIndicator IAPIndicator;
	public StateIndicator LocalNotifIndicator;
	public StateIndicator KadoSakuIndicator;

	void Awake()
	{
		MenuEvent.OpenMainMenuE += MenuEvent_OpenMainMenuE;
	}
	void OnDestroy()
	{
		MenuEvent.OpenMainMenuE -= MenuEvent_OpenMainMenuE;
	}
	void OnEnable ()
	{
		TestSceneEvent.OnTTCoreInitCompleteE += TestSceneEvent_OnTTCoreInitCompleteE;
		TestSceneEvent.OnLocalNotifServiceInitComplete += TestSceneEvent_OnLocalNotifServiceInitComplete;
		TestSceneEvent.OnGameServiceInitCompleteE += TestSceneEvent_OnGameServiceInitCompleteE;
	}

	void MenuEvent_OpenMainMenuE ()
	{
		ActivateMenu();
	}
	void OnDisable()
	{
		TestSceneEvent.OnTTCoreInitCompleteE -= TestSceneEvent_OnTTCoreInitCompleteE;
		TestSceneEvent.OnLocalNotifServiceInitComplete -= TestSceneEvent_OnLocalNotifServiceInitComplete;
		TestSceneEvent.OnGameServiceInitCompleteE -= TestSceneEvent_OnGameServiceInitCompleteE;

	}
	void TestSceneEvent_OnGameServiceInitCompleteE (bool success)
	{
		IndicatorStatus status = success == true ? IndicatorStatus.Success : IndicatorStatus.Error;
		GameServiceIndicator.SetStatus (status);
	}

	void TestSceneEvent_OnLocalNotifServiceInitComplete (bool success)
	{
		IndicatorStatus status = success == true ? IndicatorStatus.Success : IndicatorStatus.Error;
		LocalNotifIndicator.SetStatus (status);
	}

	void TestSceneEvent_OnTTCoreInitCompleteE (bool success)
	{
		IndicatorStatus status = success == true ? IndicatorStatus.Success : IndicatorStatus.Error;
		TTCoreIndicator.SetStatus (status);
	}


}
}
