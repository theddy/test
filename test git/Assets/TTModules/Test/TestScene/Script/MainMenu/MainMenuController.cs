using UnityEngine;
using System.Collections;
using Touchten.Core;
using UnityEngine.UI;
using System.Collections.Generic;
using Touchten.Utilities;

namespace Touchten.TestScene
{
	public class MainMenuController : MonoBehaviour
	{

		public void InitTTCore ()
		{
			TTCore.Init (delegate(bool arg1, System.Exception arg2) {
				TestSceneEvent.OnTTCoreInitComplete (arg1);
				TestSceneEvent.OnGameServiceInitComplete (true);
				TestSceneEvent.OnLocalNotifInitComplete (true);
			});
		}

		public void OpenIAPMenu ()
		{
			MenuEvent.OpenIAPMenu ();
		}

		public void OpenGameServiceMenu ()
		{
			MenuEvent.OpenGameService ();
		}

		public void OpenSocialMenu ()
		{
			MenuEvent.OpenSocialService ();
		}

		public void OpenAdsMenu ()
		{
			MenuEvent.OpenAdsMenu ();
		}

		public void OpenKadoSaku ()
		{
			MenuEvent.OpenKadoSakuMenu ();
		}

		public void ToggleConsole ()
		{
			DebugConsoleCanvas.ToggleConsole ();
		}

	
	}
}
