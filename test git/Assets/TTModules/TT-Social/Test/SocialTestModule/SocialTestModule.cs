using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Touchten.Social;

namespace Touchten.TestScene
{
	public class SocialTestModule : ITestModule
	{
		static SocialTestModule _instance;

		static SocialTestModule Instance {
			get {
				if (_instance == null) {
					_instance = new SocialTestModule ();
				}
				return _instance;
			}
		}

		public static void Init ()
		{
			TTSocial.InitSocialComplete = delegate(bool obj) {
				TestSceneEvent.OnSocialServiceInitComplete (obj);	
			};

			Instance.MenuButtonResourcePath = new List<string> (){ "SocialMenuButton" };
			Instance.ModuleMenuResourcePath = new List<string> (){ "SocialServiceMenu" };


		}
		public static IList<string> GetMenuButtonResourcePath ()
		{
			return Instance.MenuButtonResourcePath;
		}

		public static IList<string> GetModuleMenuResourcePath ()
		{
			return Instance.ModuleMenuResourcePath;
		}

		public IList<string>  MenuButtonResourcePath {
			get ;
			private set;
		}

		public IList<string>  ModuleMenuResourcePath {
			get;
			private set;
		}
	}
}