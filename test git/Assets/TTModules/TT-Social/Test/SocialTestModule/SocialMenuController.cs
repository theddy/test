using UnityEngine;
using System.Collections;
using Touchten.Social;

namespace Touchten.TestScene
{

	public class SocialMenuController : MonoBehaviour
	{

		#region Facebook

		public void PostView ()
		{
			TTSocial.Facebook.ShowPostView ("customMessage", delegate(IActionResult result) {
				SocialDebug.Log (result.ToString ());
			});
		}

		public void ShareScreenShot ()
		{
			TTSocial.Facebook.PostScreenshot ("custom message post screenshot", delegate(IActionResult result) {
				SocialDebug.Log (result.ToString ());
			});
		}

		public void ShareImageWithDialog ()
		{
			TTSocial.Facebook.PostImageWithDialog ("message", RandomTexture (), delegate(IActionResult result) {
				SocialDebug.Log (result.ToString ());
			});

		}


		#endregion

		#region Instagram

		public void ShareImageInstagram ()
		{
			TTSocial.Instagram.PostImage ("Message", RandomTexture (), delegate(IActionResult result) {
				SocialDebug.Log (result.ToString ());
			});
		}

		public void InstagramAuthenticate()
		{
			TTSocial.Instagram.Login(delegate(ITTLoginResult result) {
				SocialDebug.Log(result.ToString());	
			});

		}
	
		public void InstagramGetFollowing ()
		{
			TTSocial.Instagram.GetFollowing (delegate(IUserListResult result) {
				if (result.IsSuccess) {
					foreach (var user in result.UserList) {
						SocialDebug.Log (user.Fullname + "," + user.Id);
					}
				}	
			});
		}

		public void InstagramGetFollower ()
		{
			TTSocial.Instagram.GetFollower (delegate(IUserListResult result) {
				if (result.IsSuccess) {
					foreach (var user in result.UserList) {
						SocialDebug.Log (user.Fullname + "," + user.Id);
					}
				}
			});
		}

		public void InstagramCheckFollow (string id)
		{
			TTSocial.Instagram.CheckFollowUser (id, delegate(ICheckFollowResult result) {
				SocialDebug.Log (result.ToString ());	
			});
		}


		public void InstagramFollowId (string id)
		{
			TTSocial.Instagram.FollowUser (id, delegate(IActionResult result) {
				SocialDebug.Log (result.ToString ());	
			});
		}

		public void InstagramUnfollowId (string id)
		{
			TTSocial.Instagram.UnfollowUser (id, delegate(IActionResult result) {
				SocialDebug.Log (result.ToString ());	
			});
		}

		#endregion



		#region Twitter

		public void ShareImageTwitter ()
		{
			TTSocial.Twitter.PostImage ("message", RandomTexture (), delegate(IActionResult result) {
				SocialDebug.Log (result.ToString ());
			});
		}

		public void Logout ()
		{
			TTSocial.Twitter.Logout (delegate(IActionResult result) {
				SocialDebug.Log (result.ToString ());
			});
		}

		#endregion


		public Texture2D RandomTexture ()
		{

			Texture2D tex = new Texture2D (256, 256);

			Color[] col = new Color[256 * 256];
			for (int i = 0; i < col.Length; i++) {
				col [i] = new Color (UnityEngine.Random.Range (0f, 1f),
					UnityEngine.Random.Range (0f, 1f), UnityEngine.Random.Range (0f, 1f));
			}

			tex.SetPixels (col);
			return tex;
		}
	}
}