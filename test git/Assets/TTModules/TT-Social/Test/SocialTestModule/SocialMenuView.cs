using UnityEngine;
using System.Collections;
namespace Touchten.TestScene{
public class SocialMenuView : MenuBase
{


	void Awake ()
	{
		MenuEvent.OpenSocialServiceE += MenuEvent_OpenSocialServiceE;
	}


	void OnDestroy ()
	{
		MenuEvent.OpenSocialServiceE -= MenuEvent_OpenSocialServiceE;
	}

	void MenuEvent_OpenSocialServiceE ()
	{
		ActivateMenu();
	}
}
}