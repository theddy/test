using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace Touchten.TestScene
{
	public class SocialMenuButton : MonoBehaviour
	{


		StateIndicator indicator;
		// Use this for initialization
		void Start ()
		{
			indicator = GetComponent<StateIndicator>();
			TestSceneEvent.OnSocialServiceInitCompleteE+= TestSceneEvent_OnSocialServiceInitCompleteE;
			GetComponent<Button>().onClick.AddListener(delegate() {
				MenuEvent.OpenSocialService();	
			});
		
		}

		void TestSceneEvent_OnSocialServiceInitCompleteE (bool success)
		{
			indicator.SetStatus(success);
		}
	
		void OnDestroy()
		{
			TestSceneEvent.OnSocialServiceInitCompleteE-= TestSceneEvent_OnSocialServiceInitCompleteE;

		}
	}
}