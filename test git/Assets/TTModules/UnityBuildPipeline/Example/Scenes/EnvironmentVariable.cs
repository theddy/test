using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnvironmentVariable : MonoBehaviour {

	public Text buildVersion;
	public Text buildEnvironment;
	public Text buildDeviceFilter;


	// Use this for initialization
	void Start () 
	{
		
		#if KS_PRODUCTION
			buildEnvironment.text = "Environment : Production";
		#elif KS_STAGING
			buildEnvironment.text = "Environment : Staging";
		#else 
			buildEnvironment.text = "Environment : Local";
		#endif
	}

}
