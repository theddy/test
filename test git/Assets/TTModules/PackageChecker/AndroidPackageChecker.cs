using UnityEngine;
using System.Collections;
using System;
using Touchten.Core;
namespace Touchten.Utilities
{
	public class AndroidPackageChecker:IPackageChecker
	{
		#region IPackageChecker implementation

		public bool isPackageInstalled (string packageName)
		{
			#if UNITY_ANDROID
			AndroidJavaClass up = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
			AndroidJavaObject ca = up.GetStatic<AndroidJavaObject> ("currentActivity");
			AndroidJavaObject packageManager = ca.Call<AndroidJavaObject> ("getPackageManager");

			AndroidJavaObject launchIntent = null;

			try {
				launchIntent = packageManager.Call<AndroidJavaObject> ("getLaunchIntentForPackage", packageName);
			} catch (Exception ex) {
				TTCoreDebug.Log ("exception" + ex.Message);
			}

			if (launchIntent == null)
				return false;
		
			return true;
			#else
		return false;
			#endif

		}

		#endregion
	
	
	
	}
}