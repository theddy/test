using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using Touchten.Core;
namespace Touchten.Utilities
{
	public class iOSPackageChecker : IPackageChecker
	{

		[DllImport ("__Internal")]
		private static extern bool isAppInstalled (string appID);

		#region IPackageChecker implementation

		public bool isPackageInstalled (string identifier)
		{
			bool ret = false;
			string urlScheme = identifier + "://";
			TTCoreDebug.Log ("URL Scheme = " + urlScheme);
			if (Application.platform == RuntimePlatform.IPhonePlayer) {
				ret = isAppInstalled (urlScheme);
			}
			return ret;
		}

		#endregion
	

	}
}