using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Touchten.Utilities
{
	#if UNITY_EDITOR
	[InitializeOnLoad]
	#endif
	public class PackageCheckerSettings : ScriptableObject
	{
		const string PackageCheckerSettingsAssetName = "PackageCheckerSettings";
		const string PackageCheckerSettingsPath = "Touchten Configuration/Resources";
		const string PackageCheckerSettingsAssetExtension = ".asset";


		private static PackageCheckerSettings _instance;

		static PackageCheckerSettings Instance {
			get {
				if (_instance == null) {
					_instance = Resources.Load (PackageCheckerSettingsAssetName) as PackageCheckerSettings;
					if (_instance == null) {
						_instance = CreateInstance<PackageCheckerSettings> ();
						#if UNITY_EDITOR
						string properPath = Path.Combine (Application.dataPath, PackageCheckerSettingsPath);
						if (!Directory.Exists (properPath)) {
							AssetDatabase.CreateFolder ("Assets", "Touchten Configuration");
							AssetDatabase.CreateFolder ("Assets/Touchten Configuration", "Resources");
						}

						string fullPath = Path.Combine (Path.Combine ("Assets", PackageCheckerSettingsPath),
							                  PackageCheckerSettingsAssetName + PackageCheckerSettingsAssetExtension);
						AssetDatabase.CreateAsset (_instance, fullPath);
						#endif
					}
				}
				return _instance;
			}
		}


		[SerializeField] List<PackageRecord> lockedPackages;
		[SerializeField] List<PackageRecord> editablePackages = new List<PackageRecord> ();
		#if UNITY_EDITOR
		[MenuItem ("Touchten/Edit Package Checker Settings", false, 9)]
		public static void EditSettings ()
		{
			Selection.activeObject = Instance;
			EditorApplication.ExecuteMenuItem ("Window/Inspector");
		}
		#endif

		#if UNITY_EDITOR
		[InitializeOnLoadMethod]
		#endif
		static void fillDefaultRecord ()
		{
			Instance.lockedPackages = new List<PackageRecord> ();
			Instance.lockedPackages.Add (new PackageRecord ("BraveWarrior", "com.touchten.bravewarrior", "fb1649792111972094"));
			Instance.lockedPackages.Add (new PackageRecord ("EndlessTank", "com.touchten.touchtankgame", "fb1677338989195131"));
			Instance.lockedPackages.Add (new PackageRecord ("WarungChain", "com.touchten.cookingblitz", "fb550952325075760"));
			Instance.lockedPackages.Add (new PackageRecord ("TapKoruptor", "com.touchten.tapkoruptor", "fb545864155577630"));
			Instance.lockedPackages.Add (new PackageRecord ("SmashyDashy", "com.touchten.smashydashy", "fb193169057711817"));
			Instance.lockedPackages.Add (new PackageRecord ("TekaTekiSaku", "com.touchten.tts", "fb453269891468077"));
			Instance.lockedPackages.Add (new PackageRecord ("TargetAcquired", "com.touchten.targetacquired", "fb919369254774666"));
		}

		public static bool TryGetPackageIdentifier (string key, out string identifier)
		{
            identifier = string.Empty;

			#if UNITY_ANDROID
			identifier = Instance.lockedPackages.FirstOrDefault
					(item => item.key.Equals (key, StringComparison.CurrentCultureIgnoreCase)).bundleID;
			if (string.IsNullOrEmpty (identifier)) {
				identifier = Instance.editablePackages.FirstOrDefault
						(item => item.key.Equals (key, StringComparison.CurrentCultureIgnoreCase)).bundleID;
			}
			#elif UNITY_IPHONE||UNITY_IOS
			identifier = Instance.lockedPackages.FirstOrDefault
				(item => item.key.Equals(key,StringComparison.CurrentCultureIgnoreCase)).urlScheme;
			if (string.IsNullOrEmpty (identifier)) {
				identifier = Instance.editablePackages.FirstOrDefault
					(item => item.key.Equals (key, StringComparison.CurrentCultureIgnoreCase)).urlScheme;
			}
			#endif


			if (string.IsNullOrEmpty (identifier)) {
				identifier = string.Empty;
				return false;
			} else {
				return true;
			}
		}


		public static List<PackageRecord> GetAllPackages {

			get {
				List<PackageRecord> ret = new List<PackageRecord>();			
				ret = Instance.lockedPackages.Concat(Instance.editablePackages).ToList();
				return ret;
			}
		}

		public List<PackageRecord> LockedPackages {
			get{ return lockedPackages; }
		}

		public List<PackageRecord> EditablePackages {
			get {
				return editablePackages;
			}
		}



		[Serializable]
		public class PackageRecord
		{
			public string key;
			//android
			public string bundleID;
			//iOS
			public string urlScheme;

			public PackageRecord ()
			{
			}

			public PackageRecord (string key, string bundleID, string urlScheme)
			{
				this.key = key;
				this.bundleID = bundleID;
				this.urlScheme = urlScheme;
			}
		}
	}
}