using UnityEngine;
using System.Collections;


namespace Touchten.Utilities
{
	public class PackageChecker
	{
		private static IPackageChecker checker;

		/// <summary>
		/// Check whether a package is installed
		/// </summary>
		/// <returns><c>true</c>, if package installed is installed, <c>false</c> otherwise.</returns>
		/// <param name="appKey">App key as specified in PackageCheckerSettings</param>

		public static bool isPackageInstalled (string appKey)
		{
			string id = string.Empty;
			if (PackageCheckerSettings.TryGetPackageIdentifier (appKey, out id)) {
				return getChecker ().isPackageInstalled (id);
			} else {
				return false;
			}
		}

		static IPackageChecker getChecker ()
		{
			if (checker == null) {
				#if UNITY_EDITOR 
				checker = new EditorPackageChecker ();
				#elif UNITY_ANDROID
			checker = new AndroidPackageChecker();
				#elif UNITY_IOS
			checker = new iOSPackageChecker();
				#else
			checker = new EditorPackageChecker();
				#endif
			}
			return checker;
		}

	}
}

