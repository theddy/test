namespace Touchten.Utilities
{
	public interface IPackageChecker
	{
		bool isPackageInstalled(string identifier);
	}
}