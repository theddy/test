<?php

$method = $_SERVER['REQUEST_METHOD'];

set_error_handler("warning_handler", E_WARNING);

switch ($method) {
	case 'POST':
	case 'DELETE':
		handleRequest($method);
		break;
}

function handleRequest($method) {
	$url = $_GET['url'];
	
	$options = array(
		'http' => array(
			'header'  => 'Content-type: application/x-www-form-urlencoded',
			'method'  => $method
		),
	);
	
	if ($method == 'POST') {
		$options['http']['content'] = http_build_query($_POST);
	}
		
	$context = stream_context_create($options);

	try {
		echo file_get_contents($url, false, $context);
	} catch (Exception $e) {
		setError($e->getCode(), $e->getMessage());
    }
}

function warning_handler($code, $message) { 
	setError($code, $message);
}

function setError($code, $message) {
	$error = array();
	$error['meta'] = array();
	$error['meta']['error_type'] = 'Unknown_error';
	$error['meta']['code'] = $code;
	$error['meta']['error_message'] = $message;
	$error['data'] = null;
	
	echo json_encode($error);
}