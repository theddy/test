using System.Linq;
using UnityEngine;

public class ActivateOnAwake : MonoBehaviour {

    [SerializeField]
    private GameObject[] targets;

    void Awake() {
        targets.ToList().ForEach(t => t.SetActive(true));
    }
}
