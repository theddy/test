using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class EndpointCategoryButton : MonoBehaviour, IPointerClickHandler {

    private static List<EndpointCategoryButton> buttons = new List<EndpointCategoryButton>();

    private static EndpointCategoryButton currentButton;

    private Button button;

    [SerializeField]
    private GameObject content;

    void Awake() {
        button = GetComponent<Button>();

        buttons.Add(this);
    }

    public void OnPointerClick(PointerEventData eventData) {
        if (currentButton) {
            if (currentButton == this) {
                return;
            } else {
                Unselect(currentButton);
            }
        }

        Select(this);
    }

    public static void Select(int index) {
        Select(buttons[index]);
    }

    public static void Select(EndpointCategoryButton button) {
        button.button.interactable = false;

        button.content.SetActive(true);

        currentButton = button;
    }

    public static void Unselect(int index) {
        Unselect(buttons[index]);
    }

    public static void Unselect(EndpointCategoryButton button) {
        button.button.interactable = true;

        button.content.SetActive(false);

        currentButton = null;
    }
}
