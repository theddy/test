using Instagram;
using System.Linq;

public class MediaCategory : Category {

    protected override void Start() {
        base.Start();

        InstagramAPI.Instance.OnMediaRetrieved += (sender, media) => {
            Log("Media retrieved: " + media);
        };

        InstagramAPI.Instance.OnMediaSearchRetrieved += (sender, media) => {
            Log("Media search retrieved");

            media.ToList().ForEach(m => Log(m));
        };
    }

    public override void Handle(int index) {
        switch (index) {
            case 0:
                OverlayView.Instance.Show(OverlayView.InputType.Id,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.GetMedia(id);
                    }
                );
                break;
            case 1:
                OverlayView.Instance.Show(OverlayView.InputType.Query,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.GetMediaByShortcode(query);
                    }
                );
                break;
            case 2:
                OverlayView.Instance.Show(OverlayView.InputType.Latitude | OverlayView.InputType.Longitude | OverlayView.InputType.Distance,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.SearchMedia(latitude, longitude, distance);
                    }
                );
                break;
        }
    }
}
