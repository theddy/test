using Instagram;
using System.Linq;

public class TagsCategory : Category {

    protected override void Start() {
        base.Start();

        InstagramAPI.Instance.OnTagRetrieved += (sender, tag) => {
            Log("Tag retrieved: " + tag);
        };

        InstagramAPI.Instance.OnTagMediaRecentRetrieved += (sender, media) => {
            Log("Tag media recent retrieved:");

            media.ToList().ForEach(m => Log(m));
        };

        InstagramAPI.Instance.OnTagSearchRetrieved += (sender, tags) => {
            Log("Tag search retrieved:");

            tags.ToList().ForEach(t => Log(t));
        };
    }

    public override void Handle(int index) {
        switch (index) {
            case 0:
                OverlayView.Instance.Show(OverlayView.InputType.Query,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.GetTag(query);
                    }
                );
                break;
            case 1:
                OverlayView.Instance.Show(OverlayView.InputType.Query | OverlayView.InputType.Count,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.GetTagRecentMedia(query, resultCount);
                    }
                );
                break;
            case 2:
                OverlayView.Instance.Show(OverlayView.InputType.Query,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.TagSearch(query);
                    }
                );
                break;
        }
    }
}
