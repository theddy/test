using Instagram;
using System.Linq;

public class RelationshipsCategory : Category {

    protected override void Start() {
        base.Start();

        InstagramAPI.Instance.OnUserFollowsRetrieved += (sender, users) => {
            Log("User follows retrieved");

            users.ToList().ForEach(m => Log(m));
        };

        InstagramAPI.Instance.OnUserFollowedByRetrieved += (sender, users) => {
            Log("User followed by retrieved");

            users.ToList().ForEach(m => Log(m));
        };

        InstagramAPI.Instance.OnUserRequestedByRetrieved += (sender, users) => {
            Log("User requested by retrieved");

            users.ToList().ForEach(m => Log(m));
        };

        InstagramAPI.Instance.OnUserRelationshipRetrieved += (sender, relationship) => {
            Log("User relationship retrieved: " + relationship);
        };
    }

    public override void Handle(int index) {
        switch(index) {
            case 0:
                InstagramAPI.Instance.GetUserFollows();
                break;
            case 1:
                InstagramAPI.Instance.GetUserFollowedBy();
                break;
            case 2:
                InstagramAPI.Instance.GetUserRequestedBy();
                break;
            case 3:
                OverlayView.Instance.Show(OverlayView.InputType.Id, 
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.GetUserRelationship(id);
                    }
                );
                break;
            case 4:
                OverlayView.Instance.Show(OverlayView.InputType.Id | OverlayView.InputType.Query,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.SetUserRelationship(id, query);
                    }
                );
                break;
        }
    }
}
