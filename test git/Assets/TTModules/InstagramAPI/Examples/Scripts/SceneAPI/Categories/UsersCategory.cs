using Instagram;
using System.Linq;

public class UsersCategory : Category {

    protected override void Start() {
        base.Start();

        InstagramAPI.Instance.OnUserRetrieved += (sender, user) => {
            Log("User retrieved: " + user);
        };

        InstagramAPI.Instance.OnUserMediaRecentRetrieved += (sender, media) => {
            Log("User self media recent retrieved");

            media.ToList().ForEach(m => Log(m));
        };

        InstagramAPI.Instance.OnUserMediaLikedRetrieved += (sender, media) => {
            Log("User self media liked retrieved");

            media.ToList().ForEach(m => Log(m));
        };

        InstagramAPI.Instance.OnUserSearchRetrieved += (sender, users) => {
            Log("User search retrieved");

            users.ToList().ForEach(u => Log(u));
        };
    }

    public override void Handle(int index) {
        switch (index) {
            case 0:
                InstagramAPI.Instance.GetUser();
                break;
            case 1:
                OverlayView.Instance.Show(OverlayView.InputType.Id, 
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.GetUser(id);
                    }
                );
                break;
            case 2:
                OverlayView.Instance.Show(OverlayView.InputType.Count,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.GetUserRecentMedia(resultCount);
                    }
                );
                break;
            case 3:
                OverlayView.Instance.Show(OverlayView.InputType.Id | OverlayView.InputType.Count,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.GetUserRecentMedia(id, resultCount);
                    }
                );
                break;
            case 4:
                OverlayView.Instance.Show(OverlayView.InputType.Count,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.GetUserMediaLiked(resultCount);
                    }
                );
                break;
            case 5:
                OverlayView.Instance.Show(OverlayView.InputType.Query | OverlayView.InputType.Count,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.SearchUsers(query, resultCount);
                    }
                );
                break;
        }
    }
}
