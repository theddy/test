using Instagram;
using System.Linq;

public class LikesCategory : Category {

    protected override void Start() {
        base.Start();

        InstagramAPI.Instance.OnMediaLikesRetrieved += (sender, likes) => {
            Log("Likes retrieved");

            likes.ToList().ForEach(l => Log(l));
        };

        InstagramAPI.Instance.OnMediaLikeCreated += (sender) => {
            Log("Like created");
        };

        InstagramAPI.Instance.OnMediaLikeRemoved += (sender) => {
            Log("Like removed");
        };
    }

    public override void Handle(int index) {
        switch (index) {
            case 0:
                OverlayView.Instance.Show(OverlayView.InputType.Id,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.GetLikes(id);
                    }
                );
                break;
            case 1:
                OverlayView.Instance.Show(OverlayView.InputType.Id,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.CreateLike(id);
                    }
                );
                break;
            case 2:
                OverlayView.Instance.Show(OverlayView.InputType.Id,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.RemoveLike(id);
                    }
                );
                break;
        }
    }
}
