using Instagram;
using System.Linq;

public class CommentsCategory : Category {

    protected override void Start() {
        base.Start();

        InstagramAPI.Instance.OnMediaCommentsRetrieved += (sender, comments) => {
            Log("Comments retrieved");

            comments.ToList().ForEach(c => Log(c));
        };

        InstagramAPI.Instance.OnMediaCommentCreated += (sender) => {
            Log("Comment created");
        };

        InstagramAPI.Instance.OnMediaCommentRemoved += (sender) => {
            Log("Comment removed");
        };
    }

    public override void Handle(int index) {
        switch (index) {
            case 0:
                OverlayView.Instance.Show(OverlayView.InputType.Id,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.GetComments(id);
                    }
                );
                break;
            case 1:
                OverlayView.Instance.Show(OverlayView.InputType.Id | OverlayView.InputType.Query,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.CreateComment(id, query);
                    }
                );
                break;
            case 2:
                OverlayView.Instance.Show(OverlayView.InputType.Id | OverlayView.InputType.Query,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.RemoveComment(id, query);
                    }
                );
                break;
        }
    }
}
