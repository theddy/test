using Instagram;
using System.Linq;

public class LocationsCategory : Category {

    protected override void Start() {
        base.Start();

        InstagramAPI.Instance.OnLocationRetrieved += (sender, location) => {
            Log("Location retrieved: " + location);
        };

        InstagramAPI.Instance.OnLocationMediaRecentRetrieved += (sender, media) => {
            Log("Location media recent retrieved:");

            media.ToList().ForEach(m => Log(m));
        };

        InstagramAPI.Instance.OnLocationSearchRetrieved += (sender, locations) => {
            Log("Location search retrieved:");

            locations.ToList().ForEach(l => Log(l));
        };
    }

    public override void Handle(int index) {
        switch (index) {
            case 0:
                OverlayView.Instance.Show(OverlayView.InputType.Id,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.GetLocation(id);
                    }
                );
                break;
            case 1:
                OverlayView.Instance.Show(OverlayView.InputType.Id,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        InstagramAPI.Instance.GetLocationRecentMedia(id);
                    }
                );
                break;
            case 2:
                OverlayView.Instance.Show(OverlayView.InputType.Latitude | OverlayView.InputType.Longitude | OverlayView.InputType.Distance,
                    (id, query, latitude, longitude, distance, resultCount) => {
                        LocationSearchConfig config = new LocationSearchConfig(distance, latitude, longitude);

                        InstagramAPI.Instance.LocationsSearch(config);
                    }
                );
                break;
        }
    }
}
