using Instagram;
using UnityEngine;

public abstract class Category : MonoBehaviour {

    protected virtual void Start() {
    }

    public abstract void Handle(int index);

    protected void Log(string text, params object[] args) {
        Log(string.Format(text, args));
    }

    protected void Log(object obj) {
        Log(obj.ToString());
    }

    protected void Log(string text) {
        InstagramAPI.Instance.LogInfo(text);
    }
}
