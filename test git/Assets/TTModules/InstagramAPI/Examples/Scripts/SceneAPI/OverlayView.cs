using UnityEngine;
using UnityEngine.UI;
using System;

public class OverlayView : OverlayBaseView {

    [Flags]
    public enum InputType {
        Id = 1,
        Query = 2,
        Latitude = 4,
        Longitude = 8,
        Distance = 16,
        Count = 32
    }


    private static OverlayView instance;

    public static OverlayView Instance {
        get { return instance; }
    }

    [SerializeField]
    private InputField idInput;

    private string Id {
        get { return idInput.text; }
        set { idInput.text = value; }
    }

    [SerializeField]
    private InputField queryInput;

    private string Query {
        get { return queryInput.text; }
        set { queryInput.text = value; }
    }

    [SerializeField]
    private InputField latitudeInput;

    private string Latitude {
        get { return latitudeInput.text; }
        set { latitudeInput.text = value; }
    }

    [SerializeField]
    private InputField longitudeInput;

    private string Longitude {
        get { return longitudeInput.text; }
        set { longitudeInput.text = value; }
    }

    [SerializeField]
    private InputField distanceInput;

    private string Distance {
        get { return distanceInput.text; }
        set { distanceInput.text = value; }
    }

    [SerializeField]
    private InputField countInput;

    private string ResultCount {
        get { return countInput.text; }
        set { countInput.text = value; }
    }

    public delegate void OverlayHandler(string id, string query, double latitude, double longitude, float distance, int resultCount);

    private OverlayHandler acceptCallback;
    private Action cancelCallback;

    protected override void Awake() {
        instance = this;

        base.Awake();
    }

    protected override bool OnAccept() {
        if (idInput.interactable && string.IsNullOrEmpty(idInput.text)) {
            idInput.ActivateInputField();
            return false;
        } else if (queryInput.interactable && string.IsNullOrEmpty(queryInput.text)) {
            queryInput.ActivateInputField();
            return false;
        } else if (latitudeInput.interactable && string.IsNullOrEmpty(latitudeInput.text)) {
            latitudeInput.ActivateInputField();
            return false;
        } else if (longitudeInput.interactable && string.IsNullOrEmpty(longitudeInput.text)) {
            longitudeInput.ActivateInputField();
            return false;
        } else if (distanceInput.interactable && string.IsNullOrEmpty(distanceInput.text)) {
            distanceInput.ActivateInputField();
            return false;
        } else if (countInput.interactable && string.IsNullOrEmpty(countInput.text)) {
            countInput.ActivateInputField();
            return false;
        }

        if (acceptCallback != null) {
            double lat = !string.IsNullOrEmpty(Latitude) ? double.Parse(Latitude) : 0;
            double lng = !string.IsNullOrEmpty(Longitude) ? double.Parse(Longitude) : 0;
            float distance = !string.IsNullOrEmpty(Distance) ? float.Parse(Distance) : 0;
            int resultCount = !string.IsNullOrEmpty(ResultCount) ? int.Parse(ResultCount) : 0;

            acceptCallback(Id, Query, lat, lng, distance, resultCount);
        }

        return true;
    }

    protected override void OnCancel() {
        if (cancelCallback != null) {
            cancelCallback();
        }
    }

    public void Show(InputType flags, OverlayHandler acceptCallback) {
        Show(flags, acceptCallback, null);
    }

    public void Show(InputType flags, OverlayHandler acceptCallback, Action cancelCallback) {
        this.acceptCallback = acceptCallback;
        this.cancelCallback = cancelCallback;

        idInput.interactable = (flags & InputType.Id) == InputType.Id;
        queryInput.interactable = (flags & InputType.Query) == InputType.Query;
        latitudeInput.interactable = (flags & InputType.Latitude) == InputType.Latitude;
        longitudeInput.interactable = (flags & InputType.Longitude) == InputType.Longitude;
        distanceInput.interactable = (flags & InputType.Distance) == InputType.Distance;
        countInput.interactable = (flags & InputType.Count) == InputType.Count;

        Id = "";
        Query = "";
        Latitude = "";
        Longitude = "";
        Distance = distanceInput.interactable ? 1000.ToString() : "";
        ResultCount = countInput.interactable ? 10.ToString() : "";

        Show();

        if (idInput.interactable) {
            idInput.ActivateInputField();
        } else if (queryInput.interactable) {
            queryInput.ActivateInputField();
        } else if (latitudeInput.interactable) {
            latitudeInput.ActivateInputField();
        } else if (longitudeInput.interactable) {
            longitudeInput.ActivateInputField();
        } else if (distanceInput.interactable) {
            distanceInput.ActivateInputField();
        } else if (countInput.interactable) {
            countInput.ActivateInputField();
        }
    }
}
