using UnityEngine;
using UnityEngine.UI;

public class EndpointSubcategoryButton : MonoBehaviour {

    [SerializeField]
    private int id;

    private Category category;

    private Button button;

    void Awake() {
        category = GetComponentInParent<Category>();

        button = GetComponentInChildren<Button>();
        button.onClick.AddListener(() => {
            category.Handle(id);
        });
    }
}
