using UnityEngine;
using UnityEngine.UI;

public abstract class OverlayBaseView : MonoBehaviour {

    [SerializeField]
    private Button acceptButton;

    [SerializeField]
    private Button cancelButton;

    protected virtual void Awake() {
        acceptButton.onClick.AddListener(() => {
            if (OnAccept()) {
                Hide();
            }
        });

        cancelButton.onClick.AddListener(() => {
            Hide();

            OnCancel();
        });

        Hide();
    }

    protected virtual void Update() {
        if (Input.GetKeyUp(KeyCode.Escape)) {
            Hide();

            OnCancel();
        }
    }

    protected void Show() {
        gameObject.SetActive(true);
    }

    protected void Hide() {
        gameObject.SetActive(false);
    }

    protected abstract bool OnAccept();
    protected abstract void OnCancel();
}
