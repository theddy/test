using Instagram;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Example : MonoBehaviour {

    [SerializeField]
    private Button authButton;

    [SerializeField]
    private CanvasGroup canvasGroup;

    [SerializeField]
    private string redirect;

    [SerializeField]
    private string redirectWebGL;

    public string Redirect {
        get {
#if !UNITY_EDITOR && UNITY_WEBGL
            return redirectWebGL;
#else
            return redirect;
#endif
        }
    }

    [SerializeField]
    private string[] scope = new string[] { "public_content", "follower_list", "comments", "relationships", "likes" };

    void Awake() {
        authButton.interactable = true;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;

        authButton.onClick.AddListener(() => {
            InstagramAPI.Instance.Auth(Redirect, scope);
        });
    }

    IEnumerator Start() {
        yield return null;

        InstagramAPI.Instance.OnLogin += (sender) => {
            InstagramAPI.Instance.LogInfo("On Login");

            authButton.interactable = false;
            canvasGroup.interactable = true;
            canvasGroup.blocksRaycasts = true;

            EndpointCategoryButton.Select(0);
        };

        InstagramAPI.Instance.OnLoginCanceled += (sender) => {
            InstagramAPI.Instance.LogInfo("Login cancel");

            authButton.interactable = true;
            canvasGroup.interactable = false;
            canvasGroup.blocksRaycasts = false;
        };
    }
}
