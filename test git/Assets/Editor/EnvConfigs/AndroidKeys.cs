using System;

namespace Touchten.Build
{
    public class AndroidKeys
    {
        public string keystoreName;
        public string keystorePass;
        public string keyaliasName;
        public string keyaliasPass;

        public AndroidKeys(string keystoreName, string keystorePass, string keyaliasName, string keyaliasPass)
        {
            this.keystoreName = keystoreName;
            this.keystorePass = keystorePass;
            this.keyaliasName = keyaliasName;
            this.keyaliasPass = keyaliasPass;
        }

        public AndroidKeys()
        {
        }
    }
}

