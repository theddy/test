using System;

namespace Touchten.Build
{
	[Serializable]
	public class Android
	{		
		public string[] defines;

		public string[] Defines {
			get {
				return this.defines;
			}
			set {
				defines = value;
			}
		}
	}
}