using System;

namespace Touchten.Build
{
	[Serializable]
	public class IOS
	{
		public string[] defines;

		public string[] Defines {
			get {
				return this.defines;
			}
			set {
				defines = value;
			}
		}
	}
}