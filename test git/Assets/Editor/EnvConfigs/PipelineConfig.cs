
namespace Touchten.Build
{
	public class PipelineConfig
	{
		public string name;
		public string version;
		public string buildIndentifier;

		public bool developmentBuild; 

		public IOS ios;
		public Android android;


		public string Name {
			get {
				return this.name;
			}
			set {
				name = value;
			}
		}

		public string Version {
			get {
				return this.version;
			}
			set {
				version = value;
			}
		}

		public string BuildIndentifier {
			get {
				return this.buildIndentifier;
			}
			set {
				buildIndentifier = value;
			}
		}

		public bool DevelopmentBuild {
			get {
				return this.developmentBuild;
			}
			set {
				developmentBuild = value;
			}
		}

		public IOS Ios {
			get {
				return this.ios;
			}
			set {
				ios = value;
			}
		}

		public Android Android {
			get {
				return this.android;
			}
			set {
				android = value;
			}
		}
	}
}