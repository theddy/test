using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using KadoSaku.Settings;

[CustomEditor(typeof(KSAchievementSettings))]
public class KSAchievementSettingsEditor : Editor
{
	private KSAchievementSettings instance;

	public override void OnInspectorGUI ()
	{
		instance = (KSAchievementSettings)target;
		SerializedAchievementGUI();
	}

	private static GUILayoutOption miniButtonWidth = GUILayout.Width(20f);

	void SerializedAchievementGUI () {
		serializedObject.Update();

		EditorGUILayout.HelpBox("Set Achievement Id and Key", MessageType.None);
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Achievement Key", GUILayout.Width(150));
		EditorGUILayout.LabelField("Achievement Id", GUILayout.Width(150));
		EditorGUILayout.EndHorizontal();

		ShowList(serializedObject.FindProperty("_kadosakuAchievements"));

		if (GUILayout.Button("Add List", GUILayout.Width(500))) instance.kadosakuAchievements.Add(new KadoSakuAchievementRecord());

		serializedObject.ApplyModifiedProperties();
	}

	void ShowList (SerializedProperty list) {
		for (int i = 0; i < list.arraySize; i++) {
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("achievementKey"), GUIContent.none, GUILayout.Width(150));
			EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("achievementId"), GUIContent.none, GUILayout.Width(150));
			if (GUILayout.Button("-", miniButtonWidth)) {
				int oldSize = list.arraySize;
				list.DeleteArrayElementAtIndex(i);
				if (list.arraySize == oldSize) list.DeleteArrayElementAtIndex(i);
			} 
			EditorGUILayout.EndHorizontal();
		}
	}
}


