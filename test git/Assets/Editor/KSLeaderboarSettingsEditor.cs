using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using KadoSaku.Settings;

[CustomEditor(typeof(KSLeaderboardSettings))]
public class KSLeaderboarSettingsEditor : Editor
{
	private KSLeaderboardSettings instance;
	
	public override void OnInspectorGUI ()
	{
		instance = (KSLeaderboardSettings)target;
		SerializedLeaderboardGUI();
	}
	
	private static GUILayoutOption miniButtonWidth = GUILayout.Width(20f);
	
	void SerializedLeaderboardGUI () {
		serializedObject.Update();
		
		EditorGUILayout.HelpBox("Set Leadeboard Id and Matchmaking Scene", MessageType.None);
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Leaderboard Key", GUILayout.Width(150));
		EditorGUILayout.LabelField("Leaderboard Id", GUILayout.Width(150));
		EditorGUILayout.LabelField("Matchmaking Scene", GUILayout.Width(150));
		EditorGUILayout.EndHorizontal();
		
		ShowList(serializedObject.FindProperty("_kadosakuLeaderboard"));
		
		if (GUILayout.Button("Add List", GUILayout.Width(500))) instance.kadosakuLeaderboard.Add(new KadoSakuLeaderboardRecord());
		
		serializedObject.ApplyModifiedProperties();
	}
	
	void ShowList (SerializedProperty list) {
		for (int i = 0; i < list.arraySize; i++) {
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("leaderboardKey"), GUIContent.none, GUILayout.Width(150));
			EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("leaderboardID"), GUIContent.none, GUILayout.Width(150));
			EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i).FindPropertyRelative("leaderboardMatchmakingLoadedSceneName"), GUIContent.none, GUILayout.Width(150));
			if (GUILayout.Button("-", miniButtonWidth)) {
				int oldSize = list.arraySize;
				list.DeleteArrayElementAtIndex(i);
				if (list.arraySize == oldSize) list.DeleteArrayElementAtIndex(i);
			} 
			EditorGUILayout.EndHorizontal();
		}
	}
}


