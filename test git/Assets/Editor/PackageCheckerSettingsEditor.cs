using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Touchten.Utilities
{
	[CustomEditor (typeof(PackageCheckerSettings))]
	public class PackageCheckerSettingsEditor : Editor
	{
		private static GUILayoutOption miniButtonWidth = GUILayout.Width (20f);
		private PackageCheckerSettings instance;

		public override void OnInspectorGUI ()
		{
			instance = (PackageCheckerSettings)target;
			SerializedGUI ();
		}


		void SerializedGUI ()
		{
			serializedObject.Update ();


			EditorGUILayout.HelpBox ("Set package name and url scheme", MessageType.None);
			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.LabelField ("App Key", GUILayout.Width (150));
			EditorGUILayout.LabelField ("Bundle Id (Android)", GUILayout.Width (150));
			EditorGUILayout.LabelField ("Url Scheme (iOS)", GUILayout.Width (150));
			EditorGUILayout.EndHorizontal ();

			ShowLockedList (serializedObject.FindProperty ("lockedPackages"));
			ShowList (serializedObject.FindProperty ("editablePackages"));
			if (GUILayout.Button ("Add List", GUILayout.Width (500)))
				instance.EditablePackages.Add (new PackageCheckerSettings.PackageRecord ());
			serializedObject.ApplyModifiedProperties ();

		}



		void ShowLockedList (SerializedProperty list)
		{
			GUI.enabled = false;
			for (int i = 0; i < list.arraySize; i++) {
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.PropertyField (list.GetArrayElementAtIndex (i).FindPropertyRelative ("key"), GUIContent.none, GUILayout.Width (150));
				EditorGUILayout.PropertyField (list.GetArrayElementAtIndex (i).FindPropertyRelative ("bundleID"), GUIContent.none, GUILayout.Width (150));
				EditorGUILayout.PropertyField (list.GetArrayElementAtIndex (i).FindPropertyRelative ("urlScheme"), GUIContent.none, GUILayout.Width (150));
				EditorGUILayout.EndHorizontal ();
			}
			GUI.enabled = true;


		}

		void ShowList (SerializedProperty list)
		{
			for (int i = 0; i < list.arraySize; i++) {
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.PropertyField (list.GetArrayElementAtIndex (i).FindPropertyRelative ("key"), GUIContent.none, GUILayout.Width (150));
				EditorGUILayout.PropertyField (list.GetArrayElementAtIndex (i).FindPropertyRelative ("bundleID"), GUIContent.none, GUILayout.Width (150));
				EditorGUILayout.PropertyField (list.GetArrayElementAtIndex (i).FindPropertyRelative ("urlScheme"), GUIContent.none, GUILayout.Width (150));
				if (GUILayout.Button ("-", miniButtonWidth)) {
					int oldSize = list.arraySize;
					list.DeleteArrayElementAtIndex (i);
					if (list.arraySize == oldSize)
						list.DeleteArrayElementAtIndex (i);
				} 
				EditorGUILayout.EndHorizontal ();
			}


		}



	}
}