
namespace Touchten.Utilities
{
	using UnityEngine;
	using System.Collections.Generic;
	using UnityEditor;
	using UnityEditor.Callbacks;
	using System.IO;
	using System.Xml;
	using System.Xml.Linq;
	using System.Text;
	using System;
	using Touchten.Core;

	#if UNITY_IOS
	using UnityEditor.iOS.Xcode;
	#endif

	public static class URLSchemePostProcessor
	{
		private const string LSApplicationQueriesSchemesKey = "LSApplicationQueriesSchemes";

		[PostProcessBuild (100)]
		public static void OnPostProcessBuild (BuildTarget target, string path)
		{
			#if UNITY_IOS
			TTCoreDebug.Log ("URL Scheme Post Process Build...");

			string plistPath = path + "/Info.plist";
			PlistDocument plist = new PlistDocument ();
			plist.ReadFromString (File.ReadAllText (plistPath));


			PlistElementDict rootDict = plist.root;

			var schemeKey = LSApplicationQueriesSchemesKey;
			var array = rootDict.CreateArray (schemeKey);
	
			TTCoreDebug.Log ("URL Scheme Post Process Build... Count : " + PackageCheckerSettings.GetAllPackages.Count.ToString());

			foreach (var package in PackageCheckerSettings.GetAllPackages) {
				array.AddString (package.urlScheme);
			}

			File.WriteAllText(plistPath, plist.WriteToString());
			#endif
		}

	
	}

}