using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using KadoSaku.Settings;
using KadoSaku.Utility;

[CustomEditor(typeof(KSCoreSettings))]
public class KSCoreSettingsEditor : Editor
{
	private KSCoreSettings instance;
	
	public override void OnInspectorGUI ()
	{
		instance = (KSCoreSettings)target;
		instance.SetBundleIdAndVersion();
		
		EditorGUILayout.HelpBox("Set Touchten Platform", MessageType.None);
		GameAppIdAndSecretGUI();
		GamePublicKeyGUI();
		GameNamePhotoLinkGUI();
		EnableAutoLoginGUI();
		InviteStringsGUI();
		GameBundleIdandVersionGUI();
		DebugConfiguratorGUI();
	}
	
	void GameAppIdAndSecretGUI () {
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Game App ID");
		EditorGUILayout.LabelField("Game App Secret");
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.BeginHorizontal();
		instance.GameAppID = EditorGUILayout.TextField(instance.GameAppID);
		instance.GameAppSecret = EditorGUILayout.TextField(instance.GameAppSecret);
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();
		EditorGUILayout.Space();
	}

	void GamePublicKeyGUI () {
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Game Public Key : ",  GUILayout.MaxWidth(150));
		instance.GamePublicKey = EditorGUILayout.TextField(instance.GamePublicKey);
		EditorGUILayout.EndHorizontal();
	}
	
	void GameNamePhotoLinkGUI () {
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Game Name : ",  GUILayout.MaxWidth(100));
		instance.GameName = EditorGUILayout.TextField(instance.GameName);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Game Photo Link : ",  GUILayout.MaxWidth(100));
		instance.GamePhotoLink = EditorGUILayout.TextField(instance.GamePhotoLink);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Game Link : ",  GUILayout.MaxWidth(100));
		instance.GameLink = EditorGUILayout.TextField(instance.GameLink);
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
	}
	
	void EnableAutoLoginGUI () {
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Enable Auto Login",  GUILayout.MaxWidth(110));
		instance.EnableAutoLogin = EditorGUILayout.Toggle(instance.EnableAutoLogin);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.Space();
		EditorGUILayout.Space();
	}
	
	void GameBundleIdandVersionGUI () {
		EditorGUILayout.HelpBox("Game Bundle Id & Bundle Version", MessageType.None);
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Bundle Id: ",  GUILayout.MaxWidth(110));
		EditorGUILayout.LabelField(instance.BundleId,  GUILayout.MaxWidth(300));
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Bundle Version: ",  GUILayout.MaxWidth(110));
		EditorGUILayout.LabelField(instance.BundleVersion,  GUILayout.MaxWidth(110));
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.Space();
		EditorGUILayout.Space();
	}
	
	void InviteStringsGUI () {
		EditorGUILayout.HelpBox("KadoSaku Invite Strings", MessageType.None);
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Invite Title : ", GUILayout.MaxWidth(100));
		instance.InviteTitle = EditorGUILayout.TextField(instance.InviteTitle);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Invite Message : ", GUILayout.MaxWidth(100));
		instance.InviteMessage = EditorGUILayout.TextField(instance.InviteMessage);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.Space();
		EditorGUILayout.Space();
	}
	
	void DebugConfiguratorGUI () {		
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Enable Touchten Debug Message", GUILayout.MaxWidth(200));
		instance.EnableKadoSakuLogging = EditorGUILayout.Toggle(instance.EnableKadoSakuLogging);
		EditorGUILayout.EndHorizontal();
	}
	
	
	[MenuItem("Touchten/KadoSaku/KadoSaku Unity ver - " + KSCoreSettings.SDK_VER, false, 51)]
	public static void TouchtenFrameworkVersion () {}
	
	[MenuItem("Touchten/KadoSaku/Clear Editor Player Prefs", false, 102)]
	public static void ClearEditorPlayerPrefs () {
		KadoSaku.Utility.KSDebug.Log("Clear Membership Token in Player Prefs");
		PlayerPrefs.DeleteKey(KSConstants.membershipToken);
	}
}


