/// <summary>
/// Builder script tweaked based on our own requirement. Based on Nordeus' Unity Build Pipeline script.
/// </summary>

using System;
using System.Collections.Generic;
using Nordeus.Build.Reporters;
using Nordeus.Build;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Linq;

namespace Touchten.Build
{
	public static partial class CommandLine
	{
		#region Methods
		[MenuItem ("Touchten/Build Automation/Build From Config")]
		public static void BuildFromConfig ()
		{
            buildConfigValue = "pipelineConfig";
            buildTargetValue = "Android";
            buildReporterValue = "TeamCity";

            BuildWithArguments();
		}

		[MenuItem ("Touchten/Build Automation/Build APK")]
		static void BuildAPK ()
        {
            buildTargetValue = "Android";
            buildReporterValue = "TeamCity";
            buildAppendNameValue = "";
            buildNumberValue = "XXX";

            BuildWithArguments();
		}

		[MenuItem ("Touchten/Build Automation/Build XCode")]
		static void BuildXCode ()
        {
            buildTargetValue = "iOS";
            buildReporterValue = "TeamCity";
            buildAppendNameValue = "";
            buildNumberValue = "XXX";

            BuildWithArguments();
		}

        [MenuItem ("Touchten/Build Automation/Default CommandLine Build")]
        static void BuildDefaultCommandLine ()
        {
            Build();
        }

        private const string PlayEnvironmentLocal = "ks_local";
        private const string PlayEnvironmentStaging = "ks_staging";
        private const string PlayEnvironmentProduction = "ks_production";

//		[MenuItem ("Touchten/KS Automation/Play//Local")]
		public static void PlayLocal ()
		{
			Play (PlayEnvironmentLocal);
		}

//		[MenuItem ("Touchten/KS Automation/Play//Staging")]
		public static void PlayStaging ()
		{
			Play (PlayEnvironmentStaging);
		}

//		[MenuItem ("Touchten/KS Automation/Play//Production")]
		public static void PlayProduction ()
		{
			Play (PlayEnvironmentProduction);
		}

		public static void Play (string playEnvironment)
		{
			BuildTarget platform = EditorUserBuildSettings.activeBuildTarget;

			BuildTargetGroup parsedBuildTarget = (BuildTargetGroup)Enum.Parse (typeof(BuildTargetGroup), platform.ToString ());

			string define = PlayerSettings.GetScriptingDefineSymbolsForGroup (parsedBuildTarget);

			List<string> defined = new List<string> (define.Split (';'));


			defined.Remove (PlayEnvironmentLocal.ToUpper ());
			defined.Remove (PlayEnvironmentStaging.ToUpper ());
			defined.Remove (PlayEnvironmentProduction.ToUpper ());

			defined.Add (playEnvironment.ToUpper ());

			define = string.Join (";", defined.ToArray<string> ());

			PlayerSettings.SetScriptingDefineSymbolsForGroup (parsedBuildTarget, define.ToUpper ());

			EditorApplication.isPlaying = true;
		}

		#endregion
	}
}