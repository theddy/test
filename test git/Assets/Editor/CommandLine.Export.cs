/// <summary>
/// Builder script tweaked based on our own requirement. Based on Nordeus' Unity Build Pipeline script.
/// </summary>

using System;
using System.Collections.Generic;
using Nordeus.Build.Reporters;
using Nordeus.Build;
using UnityEditor;
using UnityEngine;
using System.IO;

namespace Touchten.Build
{
	public static partial class CommandLine
	{

		#region Constants

		private const string ExportNameCommand = "-buildName";
		private const string ExportAssetPathsCommand = "-assetPaths";

		private const string EXPORT_TARGET_DIR = "AutomatedPackages";

		#endregion

		#region Methods

		public static void Export ()
		{

			string buildName, sourcePaths, buildReporter;
			string[] assetPaths = new string[] { "Assets/Dist" };

			Dictionary<string, string> commandToValueDictionary = GetCommandLineArguments ();

			commandToValueDictionary.TryGetValue (ExportNameCommand, out buildName);
			commandToValueDictionary.TryGetValue (ExportAssetPathsCommand, out sourcePaths);
			commandToValueDictionary.TryGetValue (BuildReporterCommand, out buildReporter);

			try {				

				if (!string.IsNullOrEmpty (buildReporter))
					BuildReporter.Current = BuildReporter.CreateReporterByName (buildReporter);

				if (string.IsNullOrEmpty (buildName))
					buildName = "kadosaku-v6-sdk"; 	

				if (!string.IsNullOrEmpty (sourcePaths))
					assetPaths = sourcePaths.Split (';');

				string properPath = Path.Combine ("Assets", EXPORT_TARGET_DIR);
				if (!Directory.Exists (properPath)) {
					AssetDatabase.CreateFolder ("Assets", EXPORT_TARGET_DIR);
				}

				string packagePath = properPath + "/" + buildName + ".unitypackage";
				ExportPackageOptions options = ExportPackageOptions.Recurse;
				AssetDatabase.ExportPackage (assetPaths, packagePath, options);

				BuildReporter.Current.IndicateSuccessfulBuild();
			} catch (Exception e) {
				BuildReporter.Current.Log (e.Message, BuildReporter.MessageSeverity.Error);
			}


		}

		#endregion
	}
}