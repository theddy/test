using System;
using UnityEngine;
using UnityEditor;
using System.IO;

namespace Touchten.Social.Instagram.Editor
{
	[CustomEditor (typeof(TouchtenInstagramSettings))]
	public class TouchtenInstagramSettingsEditor:UnityEditor.Editor
	{

		static TouchtenInstagramSettings _instance;

		public static TouchtenInstagramSettings Instance {
			get {
				if (_instance == null) {
					_instance = Resources.Load (TouchtenInstagramSettings.SettingsAssetName) as TouchtenInstagramSettings;
					if (_instance == null) {
						_instance = ScriptableObject.CreateInstance<TouchtenInstagramSettings> ();

						string properPath = Path.Combine (Application.dataPath, TouchtenInstagramSettings.SettingsPath);
						if (!Directory.Exists (properPath)) {
							AssetDatabase.CreateFolder ("Assets", "Touchten Configuration");
							AssetDatabase.CreateFolder ("Assets/Touchten Configuration", "Resources");
						}

						string fullPath = Path.Combine (Path.Combine ("Assets", TouchtenInstagramSettings.SettingsPath),
							                  TouchtenInstagramSettings.SettingsAssetName + TouchtenInstagramSettings.SettingsAssetExtension);
						AssetDatabase.CreateAsset (_instance, fullPath);

					}
				}
				return _instance;
			}
		}

		static InstagramSettings instagramAPIObject;

		[InitializeOnLoadMethod]
		static void Init ()
		{
			TouchtenInstagramSettings temp = Instance;
			temp = null;
		}

		[MenuItem ("Touchten/Social Module/Edit Instagram Setting", false, 10)]
		public static void EditSettings ()
		{
			TouchtenInstagramSettingsEditor.instagramAPIObject = Resources.Load<InstagramSettings> (InstagramSettings.RelativePath);
			Selection.activeObject = Instance;
			EditorApplication.ExecuteMenuItem ("Window/Inspector");
		}

		public override void OnInspectorGUI ()
		{
			if (instagramAPIObject == null) {
				TouchtenInstagramSettingsEditor.instagramAPIObject = Resources.Load<InstagramSettings> (InstagramSettings.RelativePath);
			}
			DrawTouchtenInstagramEditor ();
			DrawInstagramAPIEditor ();

			EditorUtility.SetDirty (Instance);
			EditorUtility.SetDirty (instagramAPIObject);
		}

		void DrawTouchtenInstagramEditor ()
		{
			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.LabelField ("Redirect URL", GUILayout.MaxWidth (150f));
			TouchtenInstagramSettings.Instance.RedirectURI = EditorGUILayout.TextField (TouchtenInstagramSettings.Instance.RedirectURI);
			EditorGUILayout.EndHorizontal ();
		}

		void DrawInstagramAPIEditor ()
		{
			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.LabelField ("Client ID", GUILayout.MaxWidth (150f));
			instagramAPIObject.ClientId = EditorGUILayout.TextField (instagramAPIObject.ClientId);
			EditorGUILayout.EndHorizontal ();			

			EditorGUILayout.HelpBox ("To retrieve your debug access token \n" +
			"1. Fill in client id\n" +
			"2. Authenticate using test scene in editor\n" +
			"3. You will be redirected to a redirect page you filled earlier with access token in your url : https://www.touchten.com/#access_token=<YOUR_ACCESS_TOKEN>\n" +
			"4. Copy <YOUR_ACCESS_TOKEN> to debug access token field", MessageType.Info);

			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.LabelField ("Debug Access Token", GUILayout.MaxWidth (150f));
			instagramAPIObject.DebugAccessToken = EditorGUILayout.TextField (instagramAPIObject.DebugAccessToken);
			EditorGUILayout.EndHorizontal ();
		}
	}
}

