/// <summary>
/// Builder script tweaked based on our own requirement. Based on Nordeus' Unity Build Pipeline script.
/// </summary>

using System;
using System.Collections.Generic;
using Nordeus.Build.Reporters;
using Nordeus.Build;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Linq;

namespace Touchten.Build
{
	public static partial class CommandLine
	{
		#region constants
        private const char CommandStartCharacter = '-';
        private const string BuildTargetCommand = "-target";
        private const string BuildConfigCommand = "-config";
        private const string BuildReporterCommand = "-reporter";
        private const string BuildAppendNameCommand = "-appendName";
        private const string DevelopmentBuildCommand = "-developmentBuild";
        private const string BuildNumberCommand = "-buildNumber";

		private const string AndroidKeystoreNameCommand = "-androidKeystoreName";
		private const string AndroidKeystorePassCommand = "-androidKeystorePass";
		private const string AndroidKeyaliasNameCommand = "-androidKeyaliasName";
		private const string AndroidKeyaliasPassCommand = "-androidKeyaliasPass";
        #endregion

        #region variables
        static string buildConfigValue, buildAppendNameValue, developmentBuildValue, buildNumberValue;
        static string androidKeystoreNameValue, androidKeystorePassValue, androidKeyaliasNameValue, androidKeyaliasPassValue;
        static string buildReporterValue = "Unity";
        static string buildTargetValue = "Android";
        static Dictionary<string, string> commandToValueDictionary = new Dictionary<string, string>();
        #endregion

        #region Methods

        /// <summary>
        /// Performs the command line build by using the passed command line arguments.
        /// </summary>
        public static void Build ()
        {
            ExtractArguments();
            BuildWithArguments();
        }

        static void BuildWithArguments()
        {
            PrepareEditorAndPlatform();

            AndroidKeys androidKeys = new AndroidKeys(androidKeystoreNameValue, androidKeystorePassValue, androidKeyaliasNameValue, androidKeyaliasPassValue);

            if (!string.IsNullOrEmpty(buildConfigValue))
                BuildWithConfig(buildConfigValue, buildTargetValue, buildReporterValue, androidKeys);
            else
                BuildWithDefaultName(buildTargetValue, buildReporterValue, buildNumberValue, BuildPipelineSettings.Milestone, BuildPipelineSettings.IsDebug, buildAppendNameValue, androidKeys);
        }

        static void PrepareEditorAndPlatform()
        {
            BuildReporter.Current.Log("Preparing editor and switching platform...");
            EditorUserBuildSettings.SwitchActiveBuildTarget(ParseBuildTarget(buildTargetValue));
            EditorUserBuildSettings.development = string.IsNullOrEmpty(developmentBuildValue) ? true : bool.Parse(developmentBuildValue);
        }

        static void ExtractArguments()
        {
            commandToValueDictionary = GetCommandLineArguments ();
            commandToValueDictionary.TryGetValue (BuildTargetCommand, out buildTargetValue);
            commandToValueDictionary.TryGetValue (BuildConfigCommand, out buildConfigValue);
            commandToValueDictionary.TryGetValue (BuildReporterCommand, out buildReporterValue);
            commandToValueDictionary.TryGetValue (BuildAppendNameCommand, out buildAppendNameValue);
            commandToValueDictionary.TryGetValue (DevelopmentBuildCommand, out developmentBuildValue);
            commandToValueDictionary.TryGetValue (BuildNumberCommand, out buildNumberValue);

            commandToValueDictionary.TryGetValue (AndroidKeystoreNameCommand, out androidKeystoreNameValue);
            commandToValueDictionary.TryGetValue (AndroidKeystorePassCommand, out androidKeystorePassValue);
            commandToValueDictionary.TryGetValue (AndroidKeyaliasNameCommand, out androidKeyaliasNameValue);
            commandToValueDictionary.TryGetValue (AndroidKeyaliasPassCommand, out androidKeyaliasPassValue);
        }

		/// <summary>
		/// Switchs the platform.
		/// </summary>
		public static void Platform ()
		{
			string buildTarget, buildReporter;
			
			Dictionary<string, string> commandToValueDictionary = GetCommandLineArguments ();
			commandToValueDictionary.TryGetValue (BuildTargetCommand, out buildTarget);
			commandToValueDictionary.TryGetValue (BuildReporterCommand, out buildReporter);

			if (!string.IsNullOrEmpty (buildReporter))
				BuildReporter.Current = BuildReporter.CreateReporterByName (buildReporter);

			if (!string.IsNullOrEmpty (buildTarget)) {
				try {				
					BuildTarget parseBuildTarget = (BuildTarget)Enum.Parse (typeof(BuildTarget), buildTarget);

					EditorUserBuildSettings.SwitchActiveBuildTarget (parseBuildTarget);
				} catch (Exception e) {
					BuildReporter.Current.Log (e.Message, BuildReporter.MessageSeverity.Error);
				}
			}

			BuildReporter.Current.IndicateSuccessfulBuild();
        }

        /// <summary>
        /// Gets all the command line arguments relevant to the build process. All commands that don't have a value after them have their value at string.Empty.
        /// </summary>
        static Dictionary<string, string> GetCommandLineArguments ()
        {
            Dictionary<string, string> commandToValueDictionary = new Dictionary<string, string> ();

            string[] args = System.Environment.GetCommandLineArgs ();

            for (int i = 0; i < args.Length; i++) {
                if (args [i].StartsWith (CommandStartCharacter.ToString ())) {
                    string command = args [i];
                    string value = string.Empty;

                    if (i < args.Length - 1 && !args [i + 1].StartsWith (CommandStartCharacter.ToString ())) {
                        value = args [i + 1];
                        i++;
                    }

                    if (!commandToValueDictionary.ContainsKey (command)) {
                        commandToValueDictionary.Add (command, value);
                    } else {
                        BuildReporter.Current.Log ("Duplicate command line argument " + command, BuildReporter.MessageSeverity.Warning);
                    }
                }
            }

            return commandToValueDictionary;
        }

		#endregion
	}
}