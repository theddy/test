using System;
using UnityEngine;

namespace Instagram {

    [Serializable]
    public class Image {

        [SerializeField]
        private string url;

        public string Url {
            get { return url; }
        }

        [SerializeField]
        private uint width;

        public uint Width {
            get { return width; }
        }

        [SerializeField]
        private uint height;

        public uint Height {
            get { return height; }
        }

        public override string ToString() {
            return base.ToString() + " -> " + string.Format("Url: {0} Width: {1} Height: {2}", Url, Width, Height);
        }
    }
}