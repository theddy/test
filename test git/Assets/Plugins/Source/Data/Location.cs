using System;
using UnityEngine;

namespace Instagram {

    [Serializable]
    public class Location {

        [SerializeField]
        private string id;

        public string Id {
            get { return id; }
            set { id = value; }
        }

        [SerializeField]
        private string name;

        public string Name {
            get { return name; }
            set { name = value; }
        }

        [SerializeField]
        private string street_address;

        public string StreetAddress {
            get { return street_address; }
            set { street_address = value; }
        }

        [SerializeField]
        private double latitude;

        public double Latitude {
            get { return latitude; }
            set { latitude = value; }
        }

        [SerializeField]
        private double longitude;

        public double Longitude {
            get { return longitude; }
            set { longitude = value; }
        }

        public override string ToString() {
            return base.ToString() + " -> " + string.Format("Id: {0} Name: {1} StreetAddress: {2} Latitude: {3} Longitude {4}", Id, Name, StreetAddress, Latitude, Longitude);
        }
    }
}