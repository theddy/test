using System;
using UnityEngine;

namespace Instagram {

    [Serializable]
    public class UserFrom {

        [SerializeField]
        private string username;

        public string Username {
            get { return username; }
        }

        [SerializeField]
        private string profile_picture;

        public string ProfilePicture {
            get { return profile_picture; }
        }

        [SerializeField]
        private string id;

        public string Id {
            get { return id; }
        }

        [SerializeField]
        private string full_name;

        public string Fullname {
            get { return full_name; }
        }

        public override string ToString() {
            return base.ToString() + " -> " + string.Format("Id: {0} Username: {1}", Id, Username);
        }
    }
}