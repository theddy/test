using System;
using UnityEngine;

namespace Instagram {

    [Serializable]
    public class UserLike {

        [SerializeField]
        private string username;

        public string Username {
            get { return username; }
        }

        [SerializeField]
        private string first_name;

        public string FirstName {
            get { return first_name; }
        }

        [SerializeField]
        private string last_name;

        public string LastName {
            get { return last_name; }
        }

        [SerializeField]
        private string type;

        public string Type {
            get { return type; }
        }

        [SerializeField]
        private string id;

        public string Id {
            get { return id; }
        }

        public override string ToString() {
            return base.ToString() + " -> " + string.Format("Id: {0} Username: {1}", Id, Username);
        }
    }
}