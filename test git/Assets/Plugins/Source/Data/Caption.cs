using UnityEngine;
using System;

namespace Instagram {

    [Serializable]
    public class Caption {

        [SerializeField]
        private string created_time;

        public string CreatedTime {
            get { return created_time; }
        }

        [SerializeField]
        private string text;

        public string Text {
            get { return text; }
        }

        [SerializeField]
        private CaptionFrom from;

        public CaptionFrom From {
            get { return from; }
        }

        [SerializeField]
        private string id;

        public string Id {
            get { return id; }
        }

        [Serializable]
        public class CaptionFrom {

            [SerializeField]
            private string username;

            public string Username {
                get { return username; }
            }

            [SerializeField]
            private string full_name;

            public string Fullname {
                get { return full_name; }
            }

            [SerializeField]
            private string type;

            public string Type {
                get { return type; }
            }

            [SerializeField]
            private string id;

            public string Id {
                get { return id; }
            }

            public override string ToString() {
                return base.ToString() + " -> " + string.Format("Id: {0} Username: {1}", Id, Username);
            }
        }

        public override string ToString() {
            return base.ToString() + " -> " + string.Format("Id: {0} CreatedTime: {1} Text: {2}", Id, CreatedTime, Text);
        }
    }
}