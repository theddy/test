using System;
using UnityEngine;

namespace Instagram {

    [Serializable]
    public class Response {

        [SerializeField]
        private string error_type;

        public string Type {
            get { return error_type; }
        }

        [SerializeField]
        private uint code;

        public uint Code {
            get { return code; }
        }

        [SerializeField]
        private string error_message;

        public string Message {
            get { return error_message; }
        }

        public override string ToString() {
            return base.ToString() + " -> " + string.Format("Type: {0} Code: {1} Message: {2}", Type, Code, Message);
        }
    }
}