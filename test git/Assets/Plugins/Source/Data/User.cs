using System;
using UnityEngine;

namespace Instagram {

    [Serializable]
    public class User {

        [SerializeField]
        private string id;

        public string Id {
            get { return id; }
            set { id = value; }
        }

        [SerializeField]
        private string username;

        public string Username {
            get { return username; }
            set { username = value; }
        }

        [SerializeField]
        private string full_name;

        public string Fullname {
            get { return full_name; }
            set { full_name = value; }
        }

        [SerializeField]
        private string profile_picture;

        public string ProfilePicture {
            get { return profile_picture; }
            set { profile_picture = value; }
        }

        [SerializeField]
        private string bio;

        public string Bio {
            get { return bio; }
            set { bio = value; }
        }

        [SerializeField]
        private string website;

        public string Website {
            get { return website; }
            set { website = value; }
        }

        [SerializeField]
        [HideInInspector]
        private Counts counts;

        public int MediaCount {
            get { return counts.media; }
        }

        public int FollowsCount {
            get { return counts.follows; }
        }

        public int FollowedByCount {
            get { return counts.followed_by; }
        }

        [Serializable]
        public class Counts {
            public int media;
            public int follows;
            public int followed_by;
        }

        public override string ToString() {
            return base.ToString() + " -> " + string.Format("Id: {0} Username: {1}", Id, Username);
        }
    }
}