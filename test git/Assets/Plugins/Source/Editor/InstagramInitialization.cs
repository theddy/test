using System.IO;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class InstagramInitialization {

    static InstagramInitialization() {
        string relativeFilePath = string.Format("Resources/{0}.asset", InstagramSettings.RelativePath);
        string filePath = Path.Combine(Application.dataPath, relativeFilePath);

        if (!File.Exists(filePath)) {
            string dirPath = Path.GetDirectoryName(filePath);

            if (!AssetDatabase.IsValidFolder(dirPath)) {
                AssetDatabase.CreateFolder("Assets", "Resources");
            }

            InstagramSettings settings = ScriptableObject.CreateInstance<InstagramSettings>();

            AssetDatabase.CreateAsset(settings, string.Format("Assets/{0}", relativeFilePath));
        }
    }
}
