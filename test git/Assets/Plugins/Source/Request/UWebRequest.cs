using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_5_3
using UnityEngine.Experimental.Networking;
#else
using UnityEngine.Networking;
#endif

namespace Instagram {

    public class UWebRequest : IRequest {

        protected UnityWebRequest request;

        protected string error;

        public string Error {
            get { return error; }
        }

        protected string text;

        public string Text {
            get { return text; }
        }

        public event ErrorHandler OnError;
        public event CompleteHandler OnComplete;

        public virtual void Get(MonoBehaviour context, string path, ErrorHandler OnError, CompleteHandler OnComplete) {
            context.StartCoroutine(DoGet(path, OnError, OnComplete));
        }

        IEnumerator DoGet(string path, ErrorHandler OnError, CompleteHandler OnComplete) {
            if (request != null) {
                request.Dispose();
                request = null;
            }

            request = UnityWebRequest.Get(path);

            yield return request.Send();

            error = request.error;
            text = request.downloadHandler.text;

            if (!string.IsNullOrEmpty(Error)) {
                OnError(Error, Text);
            } else {
                OnComplete(Text);
            }
        }

        public virtual void Post(MonoBehaviour context, string path, Dictionary<string, string> parms, ErrorHandler OnError, CompleteHandler OnComplete) {
            context.StartCoroutine(DoPost(path, parms, OnError, OnComplete));
        }

        IEnumerator DoPost(string path, Dictionary<string, string> parms, ErrorHandler OnError, CompleteHandler OnComplete) {
            if (request != null) {
                request.Dispose();
                request = null;
            }

            request = UnityWebRequest.Post(path, parms);
            request.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            yield return request.Send();

            error = request.error;
            text = request.downloadHandler.text;

            if (!string.IsNullOrEmpty(Error)) {
                if (!string.IsNullOrEmpty(Error)) {
                    OnError(Error, Text);
                } else {
                    OnComplete(Text);
                }
            } else {
                if (OnComplete != null) {
                    OnComplete(Text);
                }
            }
        }

        public virtual void Delete(MonoBehaviour context, string path, ErrorHandler OnError, CompleteHandler OnComplete) {
            context.StartCoroutine(DoDelete(path, OnError, OnComplete));
        }

        IEnumerator DoDelete(string path, ErrorHandler OnError, CompleteHandler OnComplete) {
            if (request != null) {
                request.Dispose();
                request = null;
            }

            request = UnityWebRequest.Delete(path);

            if (request.downloadHandler == null) {
                request.downloadHandler = new DownloadHandlerBuffer();
            }

            yield return request.Send();

            error = request.error;
            text = request.downloadHandler.text;

            if (!string.IsNullOrEmpty(Error)) {
                if (!string.IsNullOrEmpty(Error)) {
                    OnError(Error, Text);
                } else {
                    OnComplete(Text);
                }
            } else {
                if (OnComplete != null) {
                    OnComplete(Text);
                }
            }
        }
    }
}