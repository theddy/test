using UnityEngine;

public class InstagramSettings : ScriptableObject
{

	public const string RelativePath = "InstagramSettings";

	[SerializeField]
	private string clientId;

	public string ClientId {
		get { return clientId; }
		set{ clientId = value; }
	}

	#if UNITY_EDITOR
	[SerializeField]
	private string debugAccessToken;

	public string DebugAccessToken {
		get { return debugAccessToken; }
		set { debugAccessToken = value; }

	}
	#endif

	[SerializeField]
	private string webGLProxyEndpoint;

	public string WebGLProxyEndpoint {
		get { return webGLProxyEndpoint; }
	}
}
