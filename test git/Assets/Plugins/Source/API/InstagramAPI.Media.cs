using System.Collections.Generic;
using UnityEngine;

namespace Instagram {

    public partial class InstagramAPI {

        private class MediaData {

            [SerializeField]
            private Media data;

            public Media Data {
                get { return data; }
            }
        }

        private class MediaSearchData {

            [SerializeField]
            private Media[] data;

            public Media[] Data {
                get { return data; }
            }
        }

        public delegate void MediaHandler(InstagramAPI sender, Media media);
        public event MediaHandler OnMediaRetrieved;

        public delegate void MediaSearchHandler(InstagramAPI sender, Media[] media);
        public event MediaSearchHandler OnMediaSearchRetrieved;

        #region GET /media/media-id
        private static readonly string MediaUrl = string.Format(ApiUrl, "media/{0}");

        /// <summary>
        /// Get information about a media object. Use the type field to differentiate between image and video media in the response. You will also receive the user_has_liked field which tells you whether the owner of the access_token has liked this media.
        /// The public_content permission scope is required to get a media that does not belong to the owner of the access_token.
        /// Scope: basic, public_content
        /// </summary>
        public void GetMedia(string id) {
            string path = BuildUri(string.Format(MediaUrl, id));

            Get<MediaData>(path,
                (response) => {
                    if (OnMediaRetrieved != null) {
                        OnMediaRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion

        #region GET /media/shortcode/shortcode
        private static readonly string MediaShortcodeUrl = string.Format(ApiUrl, "media/shortcode/{0}");

        /// <summary>
        /// This endpoint returns the same response as GET /media/media-id.
        /// A media object's shortcode can be found in its shortlink URL. An example shortlink is http://instagram.com/p/tsxp1hhQTG/. Its corresponding shortcode is tsxp1hhQTG.
        /// Scope: basic, public_content
        /// </summary>
        public void GetMediaByShortcode(string shortcode) {
            string path = BuildUri(string.Format(MediaShortcodeUrl, shortcode));

            Get<MediaData>(path,
                (response) => {
                    if (OnMediaRetrieved != null) {
                        OnMediaRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion

        #region GET /media/search
        private static readonly string MediaSearchUrl = string.Format(ApiUrl, "media/search");

        /// <summary>
        /// Search for recent media in a given area.
        /// Scope: public_content
        /// </summary>
        public void SearchMedia(double lat, double lng, float distance) {
            string path = BuildUri(MediaSearchUrl, new List<QueryItem>() {
            new QueryItem("lat", lat.ToString()),
            new QueryItem("lng", lng.ToString()),
            new QueryItem("distance", distance.ToString())
        });

            Get<MediaSearchData>(path,
                (response) => {
                    if (OnMediaSearchRetrieved != null) {
                        OnMediaSearchRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion
    }
}