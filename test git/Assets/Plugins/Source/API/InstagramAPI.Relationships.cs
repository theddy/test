using UnityEngine;
using System.Collections.Generic;

namespace Instagram {

    public partial class InstagramAPI {

        private class UserRelationshipData {

            [SerializeField]
            private UserRelationship data;

            public UserRelationship Data {
                get { return data; }
            }
        }

        public delegate void UserSelfFollowsHandler(InstagramAPI sender, User[] users);
        public event UserSelfFollowsHandler OnUserFollowsRetrieved;

        public delegate void UserSelfFollowedByHandler(InstagramAPI sender, User[] users);
        public event UserSelfFollowedByHandler OnUserFollowedByRetrieved;

        public delegate void UserSelfRequestedByHandler(InstagramAPI sender, User[] users);
        public event UserSelfRequestedByHandler OnUserRequestedByRetrieved;

        public delegate void UserRelationshipHandler(InstagramAPI sender, UserRelationship relationship);
        public event UserRelationshipHandler OnUserRelationshipRetrieved;

        #region GET /users/self/follows
        private static readonly string UserSelfFollowsUrl = string.Format(ApiUrl, "users/self/follows");

        /// <summary>
        /// Get the list of users this user follows.
        /// Scope: public_content
        /// </summary>
        public void GetUserFollows() {
            string path = BuildUri(UserSelfFollowsUrl);

            Get<UserSearchData>(path,
                (response) => {
                    if (OnUserFollowsRetrieved != null) {
                        OnUserFollowsRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion

        #region GET /users/self/follows
        private static readonly string UserSelfFollowedByUrl = string.Format(ApiUrl, "users/self/followed-by");

        /// <summary>
        /// Get the list of users this user is followed by.
        /// Scope: follower_list
        /// </summary>
        public void GetUserFollowedBy() {
            string path = BuildUri(UserSelfFollowedByUrl);

            Get<UserSearchData>(path,
                (response) => {
                    if (OnUserFollowedByRetrieved != null) {
                        OnUserFollowedByRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion

        #region GET /users/self/requested-by
        private static readonly string UserSelfRequestedByUrl = string.Format(ApiUrl, "users/self/requested-by");

        /// <summary>
        /// List the users who have requested this user's permission to follow.
        /// Scope: follower_list
        /// </summary>
        public void GetUserRequestedBy() {
            string path = BuildUri(UserSelfRequestedByUrl);

            Get<UserSearchData>(path,
                (response) => {
                    if (OnUserRequestedByRetrieved != null) {
                        OnUserRequestedByRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion

        #region GET/POST /users/user-id/relationship
        private static readonly string UserRelationshipUrl = string.Format(ApiUrl, "users/{0}/relationship");

        /// <summary>
        /// Get information about a relationship to another user. Relationships are expressed using the following terms in the response:
        /// outgoing_status: Your relationship to the user.Can be 'follows', 'requested', 'none'.
        /// incoming_status: A user's relationship to you. Can be 'followed_by', 'requested_by', 'blocked_by_you', 'none'.
        /// Scope: follower_list
        /// </summary>
        public void GetUserRelationship(string userId) {
            string path = BuildUri(string.Format(UserRelationshipUrl, userId));

            Get<UserRelationshipData>(path,
                (response) => {
                    if (OnUserRelationshipRetrieved != null) {
                        OnUserRelationshipRetrieved(this, response.Data);
                    }
                }
            );
        }

        /// <summary>
        /// Modify the relationship between the current user and the target user. You need to include an action parameter to specify the relationship action you want to perform. Valid actions are: 'follow', 'unfollow' 'approve' or 'ignore'. Relationships are expressed using the following terms in the response:
        /// outgoing_status: Your relationship to the user.Can be 'follows', 'requested', 'none'.
        /// incoming_status: A user's relationship to you. Can be 'followed_by', 'requested_by', 'blocked_by_you', 'none'.
        /// Scope: follower_list
        /// </summary>
        /// <param name="action">follow | unfollow | approve | ignore</param>
        public void SetUserRelationship(string userId, string action) {
            string path = BuildUri(string.Format(UserRelationshipUrl, userId));

            Dictionary<string, string> parms = new Dictionary<string, string>();
            parms["action"] = action;

            Post<UserRelationshipData>(path, parms,
                (response) => {
                    if (OnUserRelationshipRetrieved != null) {
                        OnUserRelationshipRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion
    }
}