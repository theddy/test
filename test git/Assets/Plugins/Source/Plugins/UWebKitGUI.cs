using UnityEngine;

public class UWebKitGUI : MonoBehaviour {

#if USE_UWEBKIT
    private const int buttonSize = 50;
    private const int buttonPadding = 10;

    private GameObject listener;

    private UWKWebView view;

    private Vector2 Position;

    private GUIStyle buttonStyle;

    void Awake() {
        view = GetComponent<UWKWebView>();
    }

    void OnGUI() {
        if (buttonStyle == null) {
            buttonStyle = new GUIStyle(GUI.skin.button);
            buttonStyle.normal.background = CreateTexture(buttonSize, buttonSize, new Color(0.2f, 0.2f, 0.2f, 1f));
            buttonStyle.hover.background = CreateTexture(buttonSize, buttonSize, new Color(0f, 0f, 0f, 1f));
        }

        if (view && view.Visible()) {
            Rect r = new Rect(Position.x, Position.y, view.CurrentWidth, view.CurrentHeight);
            view.DrawTexture(r);

            if (GUI.Button(new Rect(Screen.width - buttonSize - buttonPadding, buttonPadding, buttonSize, buttonSize), "X", buttonStyle)) {
                UWebKitInsta.Instance.Hide();

                if (!listener) {
                    listener = GameObject.Find(Insta.ListenerName);
                }

                listener.SendMessage(Insta.CancelMethodName, SendMessageOptions.RequireReceiver);
            }

            Vector3 mousePos = Input.mousePosition;
            mousePos.y = Screen.height - mousePos.y;

            mousePos.x -= Position.x;
            mousePos.y -= Position.y;

            view.ProcessMouse(mousePos);

            if (Event.current.isKey)
                view.ProcessKeyboard(Event.current);
        }
    }

    private Texture2D CreateTexture(int width, int height, Color color) {
        Color[] pixels = new Color[width * height];

        for (int i = 0; i < pixels.Length; i++) {
            pixels[i] = color;
        }

        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pixels);
        result.Apply();

        return result;
    }
#endif
}