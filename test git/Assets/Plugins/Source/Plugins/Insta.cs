using Instagram;
using System;

public class Insta : Plugin<Insta> {

    public const string ListenerName = "InstaEventsListener";

    public const string RequestProxyMethodName = "HandleRequestProxy";
    public const string TokenMethodName = "HandleToken";
    public const string CancelMethodName = "HandleCancel";

#if USE_UWEBKIT
    private UWebKitInsta plugin;
#elif UNITY_ANDROID
    private AndroidInsta plugin;
#elif UNITY_IOS
    private iOSInsta plugin;
#elif UNITY_WEBGL
    private WebGLInsta plugin;
#endif

    public delegate void LoginHandler(Insta sender);
    public event LoginHandler OnLogin;
    public event LoginHandler OnLoginCanceled;

    public Insta() {
#if USE_UWEBKIT
        plugin = UWebKitInsta.Instance;
#elif UNITY_ANDROID
        plugin = AndroidInsta.Instance;
#elif UNITY_IOS
        plugin = iOSInsta.Instance;
#elif UNITY_WEBGL
        plugin = WebGLInsta.Instance;
#endif

        CreateListener<InstaEventsListener>(ListenerName);
    }

    public void ShowAuth(string redirect, string url) {
#if USE_UWEBKIT || UNITY_WEBGL || UNITY_ANDROID || UNITY_IOS
        plugin.ShowAuth(redirect, url);
#else
        throw new System.Exception("Only available on Window/MacOSX (UWebKit), WebGL or Android or iOS");
#endif
    }

    public override string GetLastErrorMessage() {
#if !USE_UWEBKIT && UNITY_ANDROID
        return plugin.GetLastErrorMessage();
#else
        throw new System.Exception("Only available on Android");
#endif
    }

    public void CallOnToken(string url) {
        Uri uri = new Uri(url);

        string[] parts = uri.Fragment.Split('=', '&');

        if (parts.Length == 2) {
            InstagramAPI.Instance.AccessToken = parts[1];

            if (OnLogin != null) {
                OnLogin(this);
            }
        } else {
            InstagramAPI.Instance.LogError("Token URL not well formed: " + url);
        }
    }

    public void CallOnCancel() {
        if (OnLoginCanceled != null) {
            OnLoginCanceled(this);
        }
    }
}
