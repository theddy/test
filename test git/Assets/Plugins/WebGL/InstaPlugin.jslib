var InstaPlugin = {
	$listenerName: null,
	$tokenCallback: null,
	$cancelCallback: null,
	instaPluginRequest: function(url) {
		var script = document.createElement("script");
		script.src = Pointer_stringify(url);
		
		document.body.appendChild(script);
	},
	instaPluginShowAuth: function(redirect, url) {
		var getHost = function(url) {
			var l = document.createElement("a");
			l.href = url;
			
			return l.hostname;
		};
		
		var instaPluginGetToken = function(param) {
			var regex = new RegExp("(?:&|#)" + param + "=([a-z0-9._-]+)", "i");

			var matches = window.location.hash.match(regex);
			
			var user_token = "";
			
			if (matches) {
				var removeRegex = new RegExp("(?:&|#)" + param + "=" + matches[1], "i");
				window.location.hash = window.location.hash.replace(removeRegex, '');

				user_token = matches[1];				
			}
			
			return user_token;
		};
	
		var hostMatch = window.location.hostname == getHost(Pointer_stringify(redirect));
		var href = window.location.href;
		var user_token = instaPluginGetToken('access_token');
		
		if (hostMatch && user_token != "") {		
			SendMessage(listenerName, tokenCallback, href);
		} else {	
			window.location = Pointer_stringify(url);
		}
	},
	instaPluginSetListenerName: function(listener) {
		listenerName = Pointer_stringify(listener);
	},
	instaPluginSetSubmitCallback: function(callbackName) {
		tokenCallback = Pointer_stringify(callbackName);
	},
	instaPluginSetCancelCallback: function(callbackName) {
		cancelCallback = Pointer_stringify(callbackName);
	}
};

mergeInto(LibraryManager.library, InstaPlugin);