#import "InstaPlugin.h"

@implementation InstaPlugin

- (id)init
{
    self = [super init];
    
    controller = [[WebBrowserController alloc] init];
    
    return self;
}

- (void)showAuth:(const char *)submitUrl url:(const char *)url
{
    NSString *submitUrlString = [NSString stringWithUTF8String:submitUrl];
    
    [controller setSubmitUrl:submitUrlString];
    
    UIViewController *rootViewController = UnityGetGLViewController();
    
    [rootViewController.view addSubview:controller.view];
    
    NSString *urlString = [NSString stringWithUTF8String:url];
    
    [controller loadRequest:urlString];
}

- (void)setListenerName:(const char *)listenerName
{
    NSString *listenerNameString = [NSString stringWithUTF8String:listenerName];
    
    [controller setListenerName:listenerNameString];
}

- (void)setSubmitCallback:(const char *)callbackName
{
    NSString *callbackNameString = [NSString stringWithUTF8String:callbackName];
    
    [controller setSubmitCallback:callbackNameString];
}

- (void)setCancelCallback:(const char *)callbackName
{
    NSString *callbackNameString = [NSString stringWithUTF8String:callbackName];
    
    [controller setCancelCallback:callbackNameString];
}

- (void)dispose
{
    [controller dispose];
    controller = nil;
}
@end

static InstaPlugin *instaPlugin;

extern "C" UIViewController *UnityGetGLViewController();

extern "C" {
    
    void instaPluginInit() {
        instaPlugin = [[InstaPlugin alloc] init];
    }
    
    void instaPluginShowAuth(const char *submitUrl, const char *url) {
        [instaPlugin showAuth:submitUrl url:url];
    }
    
    void instaPluginSetListenerName(const char *listenerName) {
        [instaPlugin setListenerName:listenerName];
    }
    
    void instaPluginSetSubmitCallback(const char *callbackName) {
        [instaPlugin setSubmitCallback:callbackName];
    }
    
    void instaPluginSetCancelCallback(const char *callbackName) {
        [instaPlugin setCancelCallback:callbackName];
    }
}