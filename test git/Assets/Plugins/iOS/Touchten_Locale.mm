#import "Touchten_Locale.h"

char* TTMakeStringCopy (const char* string)
{
    if (string == NULL)
        return NULL;
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    return res;
}

const char* DetectLanguageiOS ()
{
    // By default mono string marshaler creates .Net string for returned UTF-8 C string
    // and calls free for returned value, thus returned strings should be allocated on heap
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    return TTMakeStringCopy([language UTF8String]);
}