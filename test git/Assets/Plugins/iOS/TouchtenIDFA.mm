//
//  Created by Touchten Mac 1 on 12/18/15.
//  Copyright © 2015 Touchten. All rights reserved.
//

#import "TouchtenIDFA.h"

const char* GetOpenUDID()
{
    NSString *identifier = [[UIDevice currentDevice].identifierForVendor UUIDString];
        
    char* res = (char*)malloc(strlen([identifier UTF8String]) + 1);
    strcpy(res, [identifier UTF8String]);
        
    return res;
}