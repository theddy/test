#import "WebBrowserController.h"

@implementation WebBrowserController
int const headerHeight = 50;

- (void)setListenerName:(NSString*)listenerName_
{
    listenerName = listenerName_;
}

- (void)setSubmitUrl:(NSString*)url
{
    submitUrl = url;
}

- (void)setSubmitCallback:(NSString*)callbackName
{
    submitCallback = callbackName;
}

- (void)setCancelCallback:(NSString*)callbackName
{
    cancelCallback = callbackName;
}

- (void)loadRequest:(NSString*)url
{
    NSURL *nsurl = [[NSURL alloc] initWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:nsurl];
    
    [mWebView loadRequest:request];
    
    [self updateButtons];
}

-(void)loadView
{
    [super loadView];
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    toolbar.frame = CGRectMake(0, 0, self.view.bounds.size.width, 50);
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    mBack = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRewind target:self action:@selector(buttonBackClick)];
    mForward = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFastForward target:self action:@selector(buttonForwardClick)];
    mRefresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(buttonRefreshClick)];
    mClose = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(buttonCloseClick)];
    
    toolbar.items = @[mBack, mForward, mRefresh, flexibleSpace, flexibleSpace, mClose];
    
    CGRect frame = [UIApplication sharedApplication].keyWindow.bounds;
    frame.origin.y = headerHeight;
    frame.size.height -= headerHeight;
    
    mWebView = [[UIWebView alloc] initWithFrame:frame];
    mWebView.delegate = self;
    mWebView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    mWebView.scalesPageToFit = YES;
    
    [self.view addSubview:mWebView];
    
    [self.view addSubview:toolbar];
}

- (void)updateButtons
{
    mForward.enabled = self->mWebView.canGoForward;
    mBack.enabled = self->mWebView.canGoBack;
}

- (void)buttonBackClick
{
    [self->mWebView goBack];
}

- (void)buttonForwardClick
{
    [self->mWebView goForward];
}

- (void)buttonRefreshClick
{
    [self->mWebView reload];
}

- (void)buttonCloseClick
{
    [self dispose];
    
    int count = 0;
    char *dummy = (char*)calloc(count, sizeof(char));
    
    UnitySendMessage([listenerName UTF8String], [cancelCallback UTF8String], dummy);
}

- (void)dispose
{
    [self.view removeFromSuperview];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [self updateButtons];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [self updateButtons];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [self updateButtons];
}

- (bool)webView:(UIWebView *)uiWebView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (mWebView == nil)
        return YES;
    
    NSURL *requestNSUrl = [request URL];
    NSURL *submitNSUrl = [NSURL URLWithString:self->submitUrl];
    
    if ([[requestNSUrl host] isEqualToString: [submitNSUrl host]] ) {
        NSString *url = [[request URL] absoluteString];
        
        [self dispose];
        
        UnitySendMessage([listenerName UTF8String], [submitCallback UTF8String], [url UTF8String]);
        
        return NO;
    } else {
        return YES;
    }
}
@end

extern "C" void UnitySendMessage(const char *, const char *, const char *);