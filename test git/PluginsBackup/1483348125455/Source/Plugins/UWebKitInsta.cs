using UnityEngine;
using System;

#if USE_UWEBKIT
public class UWebKitInsta : PlatformPlugin<UWebKitInsta> {

    private GameObject UWebKitAuth;

    private UWKWebView view;

    private GameObject listener;

    public UWebKitInsta() {
    }

    public void ShowAuth(string redirect, string url) {
        if (!listener) {
            listener = GameObject.Find(Insta.ListenerName);
        }

        if (!UWebKitAuth) {
            UWebKitAuth = new GameObject("UWebKitAuth");

            view = UWKWebView.AddToGameObject(UWebKitAuth, url, Screen.width, Screen.height);
            view.URLChanged += (sender, newUrl) => {
                if (string.IsNullOrEmpty(newUrl)) return;

                Uri requestUri = new Uri(newUrl);
                Uri redirectUri = new Uri(redirect);

                if (requestUri.Host.Equals(redirectUri.Host)) {
                    Hide();

                    listener.SendMessage(Insta.TokenMethodName, newUrl, SendMessageOptions.RequireReceiver);
                }
            };

            UWebKitAuth.AddComponent<UWebKitGUI>();
        } else {
            Show();
        }

        view.MaxWidth = Screen.width;
        view.MaxHeight = Screen.width;

        view.CurrentWidth = Screen.width;
        view.CurrentHeight = Screen.height;

        view.LoadURL(url);
    }

    private void Show() {
        UWebKitAuth.SetActive(true);

        view.Show();
    }

    public void Hide() {
        view.Hide();

        UWebKitAuth.SetActive(false);
    }
}
#endif