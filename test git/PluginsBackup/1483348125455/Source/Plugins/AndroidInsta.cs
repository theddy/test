public class AndroidInsta : AndroidPlugin<AndroidInsta> {

    protected override string GetJavaClassName() {
        return "com.bdovaz.instaplugin.Plugin";
    }

    protected override void Reset() {
        base.Reset();

        SetListenerName(Insta.ListenerName);

        SetTokenCallback(Insta.TokenMethodName);
        SetCancelCallback(Insta.CancelMethodName);
    }

    private void SetTokenCallback(string callbackName) {
        Call("setTokenCallback", callbackName);
    }

    private void SetCancelCallback(string callbackName) {
        Call("setCancelCallback", callbackName);
    }

    public void ShowAuth(string redirect, string url) {
        Call("showAuth", redirect, url);
    }
}