using System;

#if !UNITY_EDITOR && UNITY_WEBGL
using System.Runtime.InteropServices;
using Instagram;
using UnityEngine;
#endif

public class WebGLInsta : PlatformPlugin<WebGLInsta> {

#if !UNITY_EDITOR && UNITY_WEBGL
    private const string jsonpCallbackName = "instagramPluginRequestProxy";

    [DllImport("__Internal")]
    private static extern void instaPluginRequest(string url);

    [DllImport("__Internal")]
    private static extern void instaPluginShowAuth(string redirect, string url);

    [DllImport("__Internal")]
    private static extern void instaPluginSetListenerName(string listenerName);

    [DllImport("__Internal")]
    private static extern void instaPluginSetSubmitCallback(string callbackName);

    [DllImport("__Internal")]
    private static extern void instaPluginSetCancelCallback(string callbackName);

    private class Request {
        public Action<string> OnError;
        public Action<string> OnComplete;

        public Request(Action<string> OnError, Action<string> OnComplete) {
            this.OnError = OnError;
            this.OnComplete = OnComplete;
        }
    }

    private Request currentRequest;
#endif

    public WebGLInsta() {
#if !UNITY_EDITOR && UNITY_WEBGL
        instaPluginSetListenerName(Insta.ListenerName);

        SetTokenCallback(Insta.TokenMethodName);
        SetCancelCallback(Insta.CancelMethodName);

        string code = "var script = document.createElement('script');" +
                      "script.type='text/javascript';" +
                      "script.innerHTML=\"" +
                      "function " + jsonpCallbackName + " (json) { var response = JSON.stringify(json); return SendMessage('" + Insta.ListenerName + "', '" + Insta.RequestProxyMethodName + "', response); }" +
                      "\";" +
                      "document.getElementsByTagName('head')[0].appendChild(script);";

        Application.ExternalEval(code);
#endif
    }

    private void SetTokenCallback(string callbackName) {
#if !UNITY_EDITOR && UNITY_WEBGL
        instaPluginSetSubmitCallback(callbackName);
#endif
    }

    private void SetCancelCallback(string callbackName) {
#if !UNITY_EDITOR && UNITY_WEBGL
        instaPluginSetCancelCallback(callbackName);
#endif
    }

    public void MakeRequest(string url, Action<string> OnError, Action<string> OnComplete) {
#if !UNITY_EDITOR && UNITY_WEBGL
        string jsonpUrl = string.Format("{0}&callback={1}", url, jsonpCallbackName);

        currentRequest = new Request(OnError, OnComplete);

        instaPluginRequest(jsonpUrl);
#endif
    }

    public void HandleRequest(string json) {
#if !UNITY_EDITOR && UNITY_WEBGL
        Action<string> handler = currentRequest.OnComplete;

        currentRequest = null;

        handler(json);
#endif
    }

    public void ShowAuth(string redirect, string url) {
#if !UNITY_EDITOR && UNITY_WEBGL
        instaPluginShowAuth(redirect, url);
#endif
    }
}