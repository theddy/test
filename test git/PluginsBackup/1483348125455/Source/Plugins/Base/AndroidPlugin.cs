using UnityEngine;

public abstract class AndroidPlugin<T> : PlatformPlugin<T> where T : class, new() {

#if !UNITY_EDITOR && UNITY_ANDROID
    private const string UnityPackageName = "com.unity3d.player.UnityPlayer";

    protected AndroidJavaObject unityActivity;
    private AndroidJavaObject plugin;
#endif

    public AndroidPlugin() {
#if !UNITY_EDITOR && UNITY_ANDROID
        AndroidJNI.AttachCurrentThread();

        unityActivity = new AndroidJavaClass(UnityPackageName).GetStatic<AndroidJavaObject>("currentActivity");
        
        Reset();
#endif
    }

    protected virtual void Reset() {
#if !UNITY_EDITOR && UNITY_ANDROID
        plugin = Create();
#endif
    }

#if !UNITY_EDITOR && UNITY_ANDROID
    protected virtual AndroidJavaObject Create() {
        return new AndroidJavaObject(GetJavaClassName(), unityActivity);
    }
#endif

    protected abstract string GetJavaClassName();

    protected void SetListenerName(string name) {
        Call("setListenerName", name);
    }

    public string GetLastErrorMessage() {
        return Call<string>("getLastErrorMessage");
    }

    protected void Call(string method, params object[] args) {
#if !UNITY_EDITOR && UNITY_ANDROID
        plugin.Call(method, args);
#endif
    }

    protected U Call<U>(string method, params object[] args) {
#if !UNITY_EDITOR && UNITY_ANDROID
        return plugin.Call<U>(method, args);
#else
        return default(U);
#endif

    }

}
