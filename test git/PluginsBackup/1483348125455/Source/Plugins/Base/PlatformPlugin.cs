public abstract class PlatformPlugin<T> where T : class, new() {

    private static T instance = new T();

    public static T Instance {
        get { return instance; }
    }
}