using UnityEngine;

public abstract class Plugin<T> where T : class, new() {

    private static T instance = new T();

    public static T Instance {
        get { return instance; }
    }

    protected void CreateListener<U>(string name) where U : MonoBehaviour {
        GameObject listener = new GameObject(name);
        listener.hideFlags = HideFlags.HideInHierarchy;
        listener.AddComponent<U>();

        Object.DontDestroyOnLoad(listener);
    }

    /// <summary>
    /// Gets the last error (only on Android).
    /// </summary>
    /// <returns></returns>
    public abstract string GetLastErrorMessage();
}
