using UnityEngine;

public class InstaEventsListener : MonoBehaviour {

#if UNITY_WEBGL
    void HandleRequestProxy(string json) {
        WebGLInsta.Instance.HandleRequest(json);
    }
#endif

    void HandleToken(string url) {
        Insta.Instance.CallOnToken(url);
    }

    void HandleCancel() {
        Insta.Instance.CallOnCancel();
    }
}
