using System.Runtime.InteropServices;

public class iOSInsta : iOSPlugin<iOSInsta> {

	[DllImport("__Internal")]
    private static extern void instaPluginInit();

	[DllImport("__Internal")]
    private static extern void instaPluginShowAuth(string redirect, string url);

	[DllImport("__Internal")]
    private static extern void instaPluginSetListenerName(string listenerName);

	[DllImport("__Internal")]
    private static extern void instaPluginSetSubmitCallback(string callbackName);

	[DllImport("__Internal")]
	private static extern void instaPluginSetCancelCallback(string callbackName);	

	public iOSInsta() {
#if !UNITY_EDITOR
		instaPluginInit();
		
        instaPluginSetListenerName(Insta.ListenerName);

        SetTokenCallback(Insta.TokenMethodName);
        SetCancelCallback(Insta.CancelMethodName);
#endif
    }

    private void SetTokenCallback(string callbackName) {
		instaPluginSetSubmitCallback(callbackName);
    }

    private void SetCancelCallback(string callbackName) {
		instaPluginSetCancelCallback(callbackName);
    }

    public void ShowAuth(string redirect, string url) {
        instaPluginShowAuth(redirect, url);
    }
}