using System.Collections.Generic;
using UnityEngine;

namespace Instagram {

    public partial class InstagramAPI {

        private class MediaCommentsData {

            [SerializeField]
            private Comment[] data;

            public Comment[] Data {
                get { return data; }
            }
        }

        public delegate void MediaCommentsHandler(InstagramAPI sender, Comment[] comments);
        public event MediaCommentsHandler OnMediaCommentsRetrieved;

        public delegate void MediaCommentCreateHandler(InstagramAPI sender);
        public event MediaCommentCreateHandler OnMediaCommentCreated;

        public delegate void MediaCommentRemoveHandler(InstagramAPI sender);
        public event MediaCommentRemoveHandler OnMediaCommentRemoved;

        #region GET /media/media-id/comments
        private static readonly string MediaCommentsUrl = string.Format(ApiUrl, "media/{0}/comments");

        /// <summary>
        /// Get a list of recent comments on a media object. The public_content permission scope is required to get comments for a media that does not belong to the owner of the access_token.
        /// Scope: basic, public_content
        /// </summary>
        public void GetComments(string mediaId) {
            string path = BuildUri(string.Format(MediaCommentsUrl, mediaId));

            Get<MediaCommentsData>(path,
                (response) => {
                    if (OnMediaCommentsRetrieved != null) {
                        OnMediaCommentsRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion

        #region POST /media/media-id/comments
        /// <summary>
        /// Create a comment on a media object with the following rules:
        /// The total length of the comment cannot exceed 300 characters.
        /// The comment cannot contain more than 4 hashtags.
        /// The comment cannot contain more than 1 URL.
        /// The comment cannot consist of all capital letters.
        /// The public_content permission scope is required to create comments on a media that does not belong to the owner of the access_token.
        /// Scope: comments
        /// </summary>
        public void CreateComment(string mediaId, string text) {
            string path = BuildUri(string.Format(MediaCommentsUrl, mediaId));

            Dictionary<string, string> parms = new Dictionary<string, string>();
            parms["text"] = text;

            Post(path, parms,
                () => {
                    if (OnMediaCommentCreated != null) {
                        OnMediaCommentCreated(this);
                    }
                }
            );
        }
        #endregion

        #region DEL /media/media-id/comments/comment-id
        private static readonly string MediaCommentDeleteUrl = string.Format(ApiUrl, "media/{0}/comments/{1}");

        /// <summary>
        /// Remove a comment either on the authenticated user's media object or authored by the authenticated user.
        /// Scope: comments
        /// </summary>
        public void RemoveComment(string mediaId, string commentId) {
            string path = BuildUri(string.Format(MediaCommentDeleteUrl, mediaId, commentId));

            Delete(path,
                () => {
                    if (OnMediaCommentRemoved != null) {
                        OnMediaCommentRemoved(this);
                    }
                }
            );
        }
        #endregion
    }
}