using System.Collections.Generic;
using UnityEngine;

namespace Instagram {

    public partial class InstagramAPI {

        private class UserData {

            [SerializeField]
            private User data;

            public User Data {
                get { return data; }
            }
        }

        private class UserSearchData {

            [SerializeField]
            private User[] data;

            public User[] Data {
                get { return data; }
            }
        }

        public delegate void UserHandler(InstagramAPI sender, User user);
        public event UserHandler OnUserRetrieved;

        public delegate void UserMediaRecentHandler(InstagramAPI sender, Media[] media);
        public event UserMediaRecentHandler OnUserMediaRecentRetrieved;

        public delegate void UserMediaLikedHandler(InstagramAPI sender, Media[] media);
        public event UserMediaLikedHandler OnUserMediaLikedRetrieved;

        public delegate void UserSearchHandler(InstagramAPI sender, User[] users);
        public event UserSearchHandler OnUserSearchRetrieved;

        #region GET /users/self
        private static readonly string UserSelfUrl = string.Format(ApiUrl, "users/self");

        /// <summary>
        /// Get information about the owner of the access_token.
        /// Scope: basic
        /// </summary>
        public void GetUser() {
            string path = BuildUri(UserSelfUrl);

            DoGetUser(path);
        }

        private void DoGetUser(string path) {
            Get<UserData>(path,
                (response) => {
                    if (OnUserRetrieved != null) {
                        OnUserRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion

        #region GET /users/user-id
        private static readonly string UserUrl = string.Format(ApiUrl, "users/{0}");

        /// <summary>
        /// Get information about a user. This endpoint requires the public_content scope if the user-id is not the owner of the access_token.
        /// Scope: public_content
        /// </summary>
        public void GetUser(string id) {
            string path = BuildUri(string.Format(UserUrl, id));

            DoGetUser(path);
        }
        #endregion

        #region GET /users/self/media/recent
        private static readonly string UserSelfMediaRecentUrl = string.Format(ApiUrl, "users/self/media/recent");

        /// <summary>
        /// Get the most recent media published by the owner of the access_token.
        /// Scope: basic
        /// </summary>
        public void GetUserRecentMedia() {
            GetUserRecentMedia(-1);
        }

        /// <summary>
        /// Get the most recent media published by the owner of the access_token.
        /// Scope: basic
        /// </summary>
        public void GetUserRecentMedia(int count) {
            string path = BuildUri(UserSelfMediaRecentUrl, new List<QueryItem>() {
            new QueryItem("count", count.ToString())
        });

            DoGetUserRecentMedia(path, count, new List<Media>());
        }

        private void DoGetUserRecentMedia(string path, int count, List<Media> result) {
            GetPaginatedData<MediaQueryData, Media>(path, count, result, (data) => {
                if (OnUserMediaRecentRetrieved != null) {
                    OnUserMediaRecentRetrieved(this, data);
                }
            });
        }
        #endregion

        #region GET /users/user-id/media/recent
        private static readonly string UserMediaRecentUrl = string.Format(ApiUrl, "users/{0}/media/recent");

        /// <summary>
        /// Get the most recent media published by a user. This endpoint requires the public_content scope if the user-id is not the owner of the access_token.
        /// Scope: basic
        /// </summary>
        public void GetUserRecentMedia(string id) {
            GetUserRecentMedia(id, -1);
        }

        /// <summary>
        /// Get the most recent media published by a user. This endpoint requires the public_content scope if the user-id is not the owner of the access_token.
        /// Scope: basic
        /// </summary>
        public void GetUserRecentMedia(string id, int count) {
            string path = BuildUri(string.Format(UserMediaRecentUrl, id),
                new List<QueryItem>() { new QueryItem("count", count.ToString())
            });

            DoGetUserRecentMedia(path, count, new List<Media>());
        }
        #endregion

        #region GET /users/self/media/liked
        private static readonly string UserSelfMediaLikedUrl = string.Format(ApiUrl, "users/self/media/liked");

        /// <summary>
        /// Get the list of recent media liked by the owner of the access_token.
        /// Scope: public_content
        /// </summary>
        public void GetUserMediaLiked(int count) {
            string path = BuildUri(UserSelfMediaLikedUrl, new List<QueryItem>() {
            new QueryItem("count", count.ToString())
        });

            DoGetUserMediaLiked(path, count, new List<Media>());
        }

        private void DoGetUserMediaLiked(string path, int count, List<Media> result) {
            GetPaginatedData<MediaQueryData, Media>(path, count, result, (data) => {
                if (OnUserMediaLikedRetrieved != null) {
                    OnUserMediaLikedRetrieved(this, data);
                }
            });
        }
        #endregion

        #region GET /users/search
        private static readonly string UsersSearchUrl = string.Format(ApiUrl, "users/search");

        /// <summary>
        /// Get a list of users matching the query.
        /// Scope: public_content
        /// </summary>
        public void SearchUsers(string query, int count) {
            string path = BuildUri(UsersSearchUrl, new List<QueryItem>() {
            new QueryItem("q", query),
            new QueryItem("count", count.ToString())
        });

            Get<UserSearchData>(path,
                (response) => {
                    if (OnUserSearchRetrieved != null) {
                        OnUserSearchRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion
    }
}