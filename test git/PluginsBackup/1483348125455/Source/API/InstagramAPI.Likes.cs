using UnityEngine;

namespace Instagram {

    public partial class InstagramAPI {

        private class MediaLikesData {

            [SerializeField]
            private UserLike[] data;

            public UserLike[] Data {
                get { return data; }
            }
        }

        public delegate void MediaLikeHandler(InstagramAPI sender, UserLike[] likes);
        public event MediaLikeHandler OnMediaLikesRetrieved;

        public delegate void MediaLikeCreateHandler(InstagramAPI sender);
        public event MediaLikeCreateHandler OnMediaLikeCreated;

        public delegate void MediaLikeRemoveHandler(InstagramAPI sender);
        public event MediaLikeRemoveHandler OnMediaLikeRemoved;

        #region GET /media/media-id/likes
        private static readonly string LikesUrl = string.Format(ApiUrl, "media/{0}/likes");

        /// <summary>
        /// Get a list of users who have liked this media.
        /// Scope: basic, public_content
        /// </summary>
        public void GetLikes(string mediaId) {
            string path = BuildUri(string.Format(LikesUrl, mediaId));

            Get<MediaLikesData>(path,
                (response) => {
                    if (OnMediaLikesRetrieved != null) {
                        OnMediaLikesRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion

        #region POST /media/media-id/likes
        /// <summary>
        /// Set a like on this media by the currently authenticated user. The public_content permission scope is required to create likes on a media that does not belong to the owner of the access_token.
        /// Scope: likes
        /// </summary>
        public void CreateLike(string mediaId) {
            string path = BuildUri(string.Format(LikesUrl, mediaId));

            Post(path,
                () => {
                    if (OnMediaLikeCreated != null) {
                        OnMediaLikeCreated(this);
                    }
                }
            );
        }
        #endregion

        #region DEL /media/media-id/likes
        /// <summary>
        /// Remove a like on this media by the currently authenticated user. The public_content permission scope is required to delete likes on a media that does not belong to the owner of the access_token.
        /// Scope: likes
        /// </summary>
        public void RemoveLike(string mediaId) {
            string path = BuildUri(string.Format(LikesUrl, mediaId));

            Delete(path,
                () => {
                    if (OnMediaLikeRemoved != null) {
                        OnMediaLikeRemoved(this);
                    }
                }
            );
        }
        #endregion
    }
}