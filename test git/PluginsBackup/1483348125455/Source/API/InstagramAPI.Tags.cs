using System.Collections.Generic;
using UnityEngine;

namespace Instagram {

    public partial class InstagramAPI {

        private class TagData {

            [SerializeField]
            private Tag data;

            public Tag Data {
                get { return data; }
            }
        }

        private class TagSearchData {

            [SerializeField]
            private Tag[] data;

            public Tag[] Data {
                get { return data; }
            }
        }

        public delegate void TagHandler(InstagramAPI sender, Tag tag);
        public event TagHandler OnTagRetrieved;

        public delegate void TagMediaRecentHandler(InstagramAPI sender, Media[] media);
        public event TagMediaRecentHandler OnTagMediaRecentRetrieved;

        public delegate void TagsHandler(InstagramAPI sender, Tag[] tags);
        public event TagsHandler OnTagSearchRetrieved;

        #region GET /tags/tag-name
        private static readonly string TagUrl = string.Format(ApiUrl, "tags/{0}");

        /// <summary>
        /// Get information about a tag object.
        /// Scope: public_content
        /// </summary>
        public void GetTag(string name) {
            string path = BuildUri(string.Format(TagUrl, name));

            Get<TagData>(path,
                (response) => {
                    if (OnTagRetrieved != null) {
                        OnTagRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion

        #region GET /tags/tag-name/media/recent
        private static readonly string TagRecentUrl = string.Format(ApiUrl, "tags/{0}/media/recent");

        /// <summary>
        /// Get a list of recently tagged media.
        /// Scope: public_content
        /// </summary>
        public void GetTagRecentMedia(string name, int count) {
            string path = BuildUri(string.Format(TagRecentUrl, name), new List<QueryItem>() {
            new QueryItem("count", count.ToString())
        });

            DoGetTagRecentMedia(path, count, new List<Media>());
        }

        private void DoGetTagRecentMedia(string path, int count, List<Media> result) {
            GetPaginatedData<MediaQueryData, Media>(path, count, result, (data) => {
                if (OnTagMediaRecentRetrieved != null) {
                    OnTagMediaRecentRetrieved(this, data);
                }
            });
        }
        #endregion

        #region GET /tags/search
        private static readonly string TagSearchUrl = string.Format(ApiUrl, "tags/search");

        /// <summary>
        /// Search for tags by name.
        /// Scope: public_content
        /// </summary>
        public void TagSearch(string query) {
            string path = BuildUri(TagSearchUrl, new List<QueryItem>() {
            new QueryItem("q", query)
        });

            Get<TagSearchData>(path,
                (response) => {
                    if (OnTagSearchRetrieved != null) {
                        OnTagSearchRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion
    }
}