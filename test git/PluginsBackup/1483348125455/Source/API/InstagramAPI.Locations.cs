using System.Collections.Generic;
using UnityEngine;

namespace Instagram {

    public partial class InstagramAPI {

        private class LocationData {

            [SerializeField]
            private Location data;

            public Location Data {
                get { return data; }
            }
        }

        private class LocationSearchData {

            [SerializeField]
            private Location[] data;

            public Location[] Data {
                get { return data; }
            }
        }

        public delegate void LocationHandler(InstagramAPI sender, Location location);
        public event LocationHandler OnLocationRetrieved;

        public delegate void LocationMediaRecentHandler(InstagramAPI sender, Media[] media);
        public event LocationMediaRecentHandler OnLocationMediaRecentRetrieved;

        public delegate void LocationsHandler(InstagramAPI sender, Location[] locations);
        public event LocationsHandler OnLocationSearchRetrieved;

        #region GET /locations/location-id
        private static readonly string LocationUrl = string.Format(ApiUrl, "locations/{0}");

        /// <summary>
        /// Get information about a location. 
        /// Scope: public_content
        /// </summary>
        public void GetLocation(string locationId) {
            string path = BuildUri(string.Format(LocationUrl, locationId));

            Get<LocationData>(path,
                (response) => {
                    if (OnLocationRetrieved != null) {
                        OnLocationRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion

        #region GET /locations/location-id/media/recent
        private static readonly string LocationRecentUrl = string.Format(ApiUrl, "locations/{0}/media/recent");

        /// <summary>
        /// Get a list of recent media objects from a given location.
        /// Scope: public_content
        /// </summary>
        public void GetLocationRecentMedia(string locationId) {
            GetLocationRecentMedia(locationId, -1);
        }

        /// <summary>
        /// Get a list of recent media objects from a given location.
        /// Scope: public_content
        /// </summary>
        public void GetLocationRecentMedia(string locationId, int count) {
            string path = BuildUri(string.Format(LocationRecentUrl, locationId));

            DoGetLocationRecentMedia(path, count, new List<Media>());
        }

        private void DoGetLocationRecentMedia(string path, int count, List<Media> result) {
            GetPaginatedData<MediaQueryData, Media>(path, count, result, (data) => {
                if (OnLocationMediaRecentRetrieved != null) {
                    OnLocationMediaRecentRetrieved(this, data);
                }
            });
        }
        #endregion

        #region GET /locations/search
        private static readonly string LocationSearchUrl = string.Format(ApiUrl, "locations/search");

        /// <summary>
        /// REQUIREMENTS 
        /// Scope: public_content
        /// </summary>
        public void LocationsSearch(LocationSearchConfig config) {
            List<QueryItem> items = new List<QueryItem>();

            if (!string.IsNullOrEmpty(config.FacebookPlacesId)) {
                items.Add(new QueryItem("facebook_places_id", config.FacebookPlacesId));
            } else if (!string.IsNullOrEmpty(config.FoursquareId)) {
                items.Add(new QueryItem("foursquare_v2_id", config.FoursquareId));
            } else {
                items.Add(new QueryItem("lat", config.Latitude.ToString()));
                items.Add(new QueryItem("lng", config.Longitude.ToString()));
            }

            string path = BuildUri(LocationSearchUrl, items);

            Get<LocationSearchData>(path,
                (response) => {
                    if (OnLocationSearchRetrieved != null) {
                        OnLocationSearchRetrieved(this, response.Data);
                    }
                }
            );
        }
        #endregion
    }
}