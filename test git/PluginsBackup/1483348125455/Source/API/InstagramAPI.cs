using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Instagram {

    public partial class InstagramAPI : MonoBehaviour {

        private const string ApiUrl = "https://api.instagram.com/v1/{0}";
        private const string AuthUrl = "https://api.instagram.com/oauth/authorize/?client_id={0}&redirect_uri={1}&response_type=token";

        #region Singleton
        private static InstagramAPI instance;

        public static InstagramAPI Instance {
            get { return instance; }
        }
        #endregion

        private static string ClientId;

#if UNITY_EDITOR
        public static bool endpointLogEnabled = false;
        public static bool jsonLogEnabled = false;
#endif

        private static Dictionary<string, string> dummyPostData;

        public static Dictionary<string, string> DummyPostData {
            get {
                if (dummyPostData == null) {
                    dummyPostData = new Dictionary<string, string>();
                    dummyPostData["dummy"] = "0";
                }

                return dummyPostData;
            }
        }

        private abstract class PaginatedData {

            [SerializeField]
            private Pagination pagination;

            public Pagination Pagination {
                get { return pagination; }
            }

            public abstract T[] GetData<T>() where T : class, new();

            protected T[] DoGetData<T>(object value) {
                return (T[])Convert.ChangeType(value, typeof(T[]));
            }
        }

        private class MediaQueryData : PaginatedData {

            [SerializeField]
            private Media[] data;

            public Media[] Data {
                get { return data; }
            }

            public override T[] GetData<T>() {
                return DoGetData<T>(data);
            }
        }

        private const string AccessTokenKey = "InstaAPI_AccessToken";

        [SerializeField]
        [Multiline]
        private string accessToken;

        public string AccessToken {
            get { return accessToken; }
            set {
                accessToken = value;

#if !UNITY_EDITOR
            PlayerPrefs.SetString(AccessTokenKey, value);
            PlayerPrefs.Save();
#endif
            }
        }

        public bool IsLoggedIn {
            get { return !string.IsNullOrEmpty(AccessToken); }
        }

#if !UNITY_EDITOR && UNITY_WEBGL
        private IRequest request = new WebGLWebRequest();
#else
        private IRequest request = new UWebRequest();
#endif

        public IRequest Request {
            private get { return request; }
            set { request = value; }
        }

        private IJsonSerializer serializer = new UJsonSerializer();

        public IJsonSerializer Serializer {
            private get { return serializer; }
            set { serializer = value; }
        }

        public delegate void LogHandler(InstagramAPI sender, string text);
        public event LogHandler OnMessageLogged;

        public delegate void ErrorHandler(InstagramAPI sender, Response response);
        public event ErrorHandler OnError;

        public delegate void LoginHandler(InstagramAPI sender);
        public event LoginHandler OnLogin;
        public event LoginHandler OnLoginCanceled;

        private class ResponseData {

            [SerializeField]
            private Response meta;

            public Response Meta {
                get { return meta; }
            }
        }

        private class QueryItem {
            public string name;
            public string value;

            public QueryItem(string name, string value) {
                this.name = name;
                this.value = value;
            }

            public override string ToString() {
                return string.Format("{0}={1}", name, value);
            }
        }

#if UNITY_EDITOR
        [SerializeField]
        private bool logEndpoint;

        [SerializeField]
        private bool logJson;
#endif

        private InstagramSettings settings;

        public string WebGLProxyEndpoint {
            get { return settings.WebGLProxyEndpoint; }
        }

        void Awake() {
            instance = this;

#if UNITY_EDITOR
            endpointLogEnabled = logEndpoint;
            jsonLogEnabled = logJson;
#endif

            settings = Resources.Load<InstagramSettings>(InstagramSettings.RelativePath);

            if (string.IsNullOrEmpty(settings.ClientId)) {
                LogError("You must define a \"ClientId\" (https://www.instagram.com/developer/clients/manage/) in \"Resources > InstagramSettings.asset\"");
            }

#if UNITY_EDITOR
            if (string.IsNullOrEmpty(settings.DebugAccessToken)) {
                LogError("You must define a \"DebugAccessToken\" in \"Resources > InstagramSettings.asset\". In order to obtain it, you need to call 'Auth' method will open your WebBrowser, you need to grab then your \"access_token\".");
            }
#endif

            ClientId = settings.ClientId;

#if !UNITY_EDITOR
        AccessToken = PlayerPrefs.GetString(AccessTokenKey);
#endif
        }

        void Start() {
            Insta.Instance.OnLogin += (sender) => {
                if (OnLogin != null) {
                    OnLogin(this);
                }
            };

            Insta.Instance.OnLoginCanceled += (sender) => {
                if (OnLoginCanceled != null) {
                    OnLoginCanceled(this);
                }
            };
        }

        public void Auth(string redirect, string[] scope) {
            string path = string.Format(AuthUrl, ClientId, redirect);
            path += string.Format("&scope={0}", string.Join("+", scope));

#if !USE_UWEBKIT && UNITY_EDITOR
            if (!string.IsNullOrEmpty(settings.DebugAccessToken)) {
                AccessToken = settings.DebugAccessToken;

                if (OnLogin != null) {
                    OnLogin(this);
                }
            } else {
                LogWarning("Log in with a SandBox user (https://www.instagram.com/developer/clients/manage/) on Instagram, grab the \"access_token\" value from the previous url and paste it in \"Resources > InstagramSettings.asset > DebugAccessToken\"");

                Application.OpenURL(path);

                if (OnLoginCanceled != null) {
                    OnLoginCanceled(this);
                }
            }
#elif USE_UWEBKIT || UNITY_WEBGL || UNITY_ANDROID || UNITY_IOS
        if (!string.IsNullOrEmpty(AccessToken)) {
            if (OnLogin != null) {
                OnLogin(this);
            }
        } else {
            Insta.Instance.ShowAuth(redirect, path);
        }
#else
        LogError("Not supported on this platform: " + Application.platform);
#endif
        }

        void GetPaginatedData<T, U>(string path, int count, List<U> result, Action<U[]> callback) where T : PaginatedData where U : class, new() {
            Get<T>(path,
                (response) => {
                    if (count == -1) {
                        result.AddRange(response.GetData<U>());

                        if (response.Pagination != null && !string.IsNullOrEmpty(response.Pagination.NextUrl)) {
                            GetPaginatedData<T, U>(response.Pagination.NextUrl, count, result, callback);
                        } else {
                            if (callback != null) {
                                callback(result.ToArray());
                            }
                        }
                    } else {
                        foreach (U item in response.GetData<U>()) {
                            result.Add(item);

                            if (result.Count == count) {
                                break;
                            }
                        }

                        if (result.Count < count && response.Pagination != null && !string.IsNullOrEmpty(response.Pagination.NextUrl)) {
                            GetPaginatedData<T, U>(response.Pagination.NextUrl, count, result, callback);
                        } else {
                            if (callback != null) {
                                callback(result.ToArray());
                            }
                        }
                    }
                }
            );
        }

        void Get(string path, Action OnComplete) {
            DoGet(path, (json) => {
                if (OnComplete != null) {
                    OnComplete();
                }
            });
        }

        void Get<T>(string path, Action<T> OnComplete) {
            DoGet(path, (json) => {
                T response = serializer.Deserialize<T>(json);

                if (OnComplete != null) {
                    OnComplete(response);
                }
            });
        }

        void DoGet(string path, Action<string> OnCompleted) {
#if UNITY_EDITOR
            if (endpointLogEnabled) {
                LogInfo(path);
            }
#endif

#if UNITY_WEBGL
            path = string.Format("{0}&callback=?", path);
#endif

            Request.Get(this, path,
                (errorMessage, errorJson) => { HandleError(errorMessage, errorJson); },
                (json) => { HandleComplete(json, OnCompleted); }
            );
        }

        void Post(string path, Action OnComplete) {
            Post(path, DummyPostData, OnComplete);
        }

        void Post(string path, Dictionary<string, string> parms, Action OnComplete) {
            DoPost(path, parms, (json) => {
                if (OnComplete != null) {
                    OnComplete();
                }
            });
        }

        void Post<T>(string path, Action<T> OnComplete) {
            Post<T>(path, DummyPostData, OnComplete);
        }

        void Post<T>(string path, Dictionary<string, string> parms, Action<T> OnComplete) {
            DoPost(path, parms, (json) => {
                T response = serializer.Deserialize<T>(json);

                if (OnComplete != null) {
                    OnComplete(response);
                }
            });
        }

        void DoPost(string path, Dictionary<string, string> parms, Action<string> OnComplete) {
#if UNITY_EDITOR
            if (endpointLogEnabled) {
                LogInfo(path);
            }
#endif

#if UNITY_WEBGL
            path = string.Format("{0}&callback=?", path);
#endif

            Request.Post(this, path, parms,
                (errorMessage, errorJson) => { HandleError(errorMessage, errorJson); },
                (json) => { HandleComplete(json, OnComplete); }
            );
        }

        void Delete(string path, Action OnComplete) {
            DoDelete(path, (json) => {
                if (OnComplete != null) {
                    OnComplete();
                }
            });
        }

        void Delete<T>(string path, Action<T> OnComplete) {
            DoDelete(path, (json) => {
                T response = serializer.Deserialize<T>(json);

                if (OnComplete != null) {
                    OnComplete(response);
                }
            });
        }

        void DoDelete(string path, Action<string> OnComplete) {
#if UNITY_EDITOR
            if (endpointLogEnabled) {
                LogInfo(path);
            }
#endif

#if UNITY_WEBGL
            path = string.Format("{0}&callback=?", path);
#endif

            Request.Delete(this, path,
                (errorMessage, errorJson) => { HandleError(errorMessage, errorJson); },
                (json) => { HandleComplete(json, OnComplete); }
            );
        }

        void HandleError(string errorJson) {
            HandleError("", errorJson);
        }

        void HandleError(string errorMessage, string errorJson) {
#if UNITY_EDITOR
            if (jsonLogEnabled) {
                LogError(errorJson);
            }
#endif

            ResponseData response = serializer.Deserialize<ResponseData>(errorJson);

            string error = "";

            if (string.IsNullOrEmpty(errorMessage)) {
                error = response.Meta.ToString();
            } else {
                error = string.Format("{0} -> {1}", errorMessage, response.Meta);
            }

            if (OnError != null) {
                OnError(this, response.Meta);
            } else {
                LogError(error);
            }
        }

        void HandleComplete(string json, Action<string> OnComplete) {
            ResponseData response = serializer.Deserialize<ResponseData>(json);

            if (response.Meta.Code != 200) {
                HandleError(json);
            } else {
#if UNITY_EDITOR
                if (jsonLogEnabled) {
                    LogInfo(json);
                }
#endif

                OnComplete(json);
            }
        }

        void HandleComplete<T>(string json, Action<T> OnComplete) {
            ResponseData response = serializer.Deserialize<ResponseData>(json);

            if (response.Meta.Code != 200) {
                HandleError(json);
            } else {
                T completeResponse = serializer.Deserialize<T>(json);

                OnComplete(completeResponse);
            }
        }

        private string BuildUri(string path) {
            return BuildUri(path, null);
        }

        private string BuildUri(string path, List<QueryItem> items) {
            if (items == null) {
                items = new List<QueryItem>();
            }

            items.Add(new QueryItem("access_token", AccessToken));

            string query = BuildQuery(items);

            return string.Format("{0}?{1}", path, query);
        }

        private string BuildQuery(List<QueryItem> items) {
            return string.Join("&", items.Select(i => i.ToString()).ToArray());
        }

        public void LogInfo(string text) {
            string message = string.Format("{0}", text);

            Debug.Log(message);

            if (OnMessageLogged != null) {
                OnMessageLogged(this, message);
            }
        }

        public void LogWarning(string text) {
            string message = string.Format("Warning: {0}", text);

            Debug.LogWarning(message);

            if (OnMessageLogged != null) {
                OnMessageLogged(this, message);
            }
        }

        public void LogError(string text) {
            string message = string.Format("Error: {0}", text);

            Debug.LogError(message);

            if (OnMessageLogged != null) {
                OnMessageLogged(this, message);
            }
        }
    }
}