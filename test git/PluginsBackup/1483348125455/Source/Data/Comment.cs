using System;
using UnityEngine;

namespace Instagram {

    [Serializable]
    public class Comment {

        [SerializeField]
        private string created_time;

        public string CreatedTime {
            get { return created_time; }
        }

        [SerializeField]
        private string text;

        public string Text {
            get { return text; }
        }

        [SerializeField]
        private UserFrom from;

        public UserFrom From {
            get { return from; }
        }

        [SerializeField]
        private string id;

        public string Id {
            get { return id; }
        }

        public override string ToString() {
            return base.ToString() + " -> " + string.Format("Id: {0} CreatedTime: {1} Text: {2}", Id, CreatedTime, Text);
        }
    }
}