using System;
using UnityEngine;

namespace Instagram {

    [Serializable]
    public class Tag {

        [SerializeField]
        private string name;

        public string Name {
            get { return name; }
            set { name = value; }
        }

        [SerializeField]
        private int media_count;

        public int MediaCount {
            get { return media_count; }
            set { media_count = value; }
        }

        public override string ToString() {
            return base.ToString() + " -> " + string.Format("Name: {0} MediaCount: {1}", Name, MediaCount);
        }
    }
}