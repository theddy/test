using System;
using UnityEngine;

namespace Instagram {

    [Serializable]
    public class Pagination {

        [SerializeField]
        private string next_url;

        public string NextUrl {
            get { return next_url; }
        }

        [SerializeField]
        private string next_min_id;

        public string NextMinId {
            get { return next_min_id; }
        }

        [SerializeField]
        private string next_max_id;

        public string NextMaxId {
            get { return next_max_id; }
        }

        [SerializeField]
        private string min_tag_id;

        public string MinTagId {
            get { return min_tag_id; }
        }

        [SerializeField]
        private string max_tag_id;

        public string MaxTagId {
            get { return max_tag_id; }
        }
    }
}