using System;
using UnityEngine;

namespace Instagram {

    [Serializable]
    public class UserRelationship {

        [SerializeField]
        private string outgoing_status;

        public string OutgoingStatus {
            get { return outgoing_status; }
        }

        [SerializeField]
        private string incoming_status;

        public string IncomingStatus {
            get { return incoming_status; }
        }

        [SerializeField]
        private bool target_user_is_private;


        public bool TargetUserIsPrivate {
            get { return target_user_is_private; }
        }

        public override string ToString() {
            return base.ToString() + " -> " + string.Format("OutgoingStatus: {0} IncomingStatus: {1} TargetUserIsPrivate: {2}", OutgoingStatus, IncomingStatus, TargetUserIsPrivate);
        }
    }
}