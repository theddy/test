using System;

namespace Instagram {

    [Serializable]
    public class LocationSearchConfig {

        public enum Type {
            FacebookPlaces = 0,
            Foursquare = 1
        }

        private float distance;

        public float Distance {
            get { return distance; }
        }

        private double latitude;

        public double Latitude {
            get { return latitude; }
        }

        private double longitude;

        public double Longitude {
            get { return longitude; }
        }

        private string facebookPlacesId;

        public string FacebookPlacesId {
            get { return facebookPlacesId; }
        }

        private string foursquareId;

        public string FoursquareId {
            get { return foursquareId; }
        }

        private LocationSearchConfig(float distance) {
            this.distance = distance;
        }

        public LocationSearchConfig(float distance, double latitude, double longitude) : this(distance) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public LocationSearchConfig(float distance, string id, Type type) : this(distance) {
            switch (type) {
                case Type.FacebookPlaces:
                    facebookPlacesId = id;
                    break;
                case Type.Foursquare:
                    foursquareId = id;
                    break;
            }
        }
    }
}