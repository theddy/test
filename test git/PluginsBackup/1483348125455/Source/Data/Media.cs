using UnityEngine;
using System;

namespace Instagram {

    [Serializable]
    public class Media {

        [SerializeField]
        private MediaAttribution attribution;

        public MediaAttribution Attribution {
            get { return attribution; }
        }

        [SerializeField]
        private string[] tags;

        public string[] Tags {
            get { return tags; }
        }

        [SerializeField]
        private string type;

        public string Type {
            get { return type; }
        }

        [SerializeField]
        private Location location;

        public Location Location {
            get { return location; }
        }

        [SerializeField]
        [HideInInspector]
        private CountData comments;

        public int CommentsCount {
            get { return comments.Count; }
        }

        [SerializeField]
        private string filter;

        public string Filter {
            get { return filter; }
        }

        [SerializeField]
        private string created_time;

        public string CreatedTime {
            get { return created_time; }
        }

        [SerializeField]
        private string link;

        public string Link {
            get { return link; }
        }

        [SerializeField]
        [HideInInspector]
        private CountData likes;

        public int LikesCount {
            get { return likes.Count; }
        }

        [SerializeField]
        private MediaImagesData images;

        public MediaImagesData Images {
            get { return images; }
        }

        [SerializeField]
        private MediaVideosData videos;

        public MediaVideosData Videos {
            get { return videos; }
        }

        [SerializeField]
        private UserInPhoto[] users_in_photo;

        public UserInPhoto[] UsersInPhoto {
            get { return users_in_photo; }
        }

        [SerializeField]
        private Caption caption;

        public Caption Caption {
            get { return caption; }
        }

        [SerializeField]
        private bool user_has_liked;

        public bool UserHasLiked {
            get { return user_has_liked; }
        }

        [SerializeField]
        private string id;

        public string Id {
            get { return id; }
        }

        [SerializeField]
        private UserFrom user;

        public UserFrom User {
            get { return user; }
        }

        [Serializable]
        public class MediaAttribution {

            [SerializeField]
            private string website;

            public string Website {
                get { return website; }
            }

            [SerializeField]
            private string itunes_url;

            public string ItunesUrl {
                get { return itunes_url; }
            }

            [SerializeField]
            private string name;

            public string Name {
                get { return name; }
            }

            public override string ToString() {
                return base.ToString() + " -> " + string.Format("Name: {0} Website: {1} ItunesUrl: {2}", Name, Website, ItunesUrl);
            }
        }

        [Serializable]
        public class UserInPhoto {

            [Serializable]
            public class UserPosition {

                [SerializeField]
                private float y;

                public float Y {
                    get { return y; }
                }

                [SerializeField]
                private float x;

                public float X {
                    get { return x; }
                }

                public override string ToString() {
                    return base.ToString() + " -> " + string.Format("X: {0} Y: {1}", X, Y);
                }
            }

            [SerializeField]
            private UserPosition position;

            public UserPosition Position {
                get { return position; }
            }

            [SerializeField]
            private Caption.CaptionFrom user;

            public Caption.CaptionFrom User {
                get { return user; }
            }

            public override string ToString() {
                return base.ToString() + " -> " + string.Format("User: {0} Position: {1}", User, Position);
            }
        }

        [Serializable]
        public class MediaImagesData {

            [SerializeField]
            private Image low_resolution;

            public Image LowResolution {
                get { return low_resolution; }
            }

            [SerializeField]
            private Image thumbnail;

            public Image Thumbnail {
                get { return thumbnail; }
            }

            [SerializeField]
            private Image standard_resolution;

            public Image StandardResolution {
                get { return standard_resolution; }
            }

            public override string ToString() {
                return base.ToString() + " -> " + string.Format("LowResolution: {0} Thumbnail: {1} StandardResolution: {2}", LowResolution, Thumbnail, StandardResolution);
            }
        }

        [Serializable]
        public class MediaVideosData {

            [SerializeField]
            private Video low_bandwidth;

            public Video LowBandwidth {
                get { return low_bandwidth; }
            }

            [SerializeField]
            private Video low_resolution;

            public Video LowResolution {
                get { return low_resolution; }
            }

            [SerializeField]
            private Video standard_resolution;

            public Video StandardResolution {
                get { return standard_resolution; }
            }

            public override string ToString() {
                return base.ToString() + " -> " + string.Format("LowBandwidth: {0} LowResolution: {1} StandardResolution: {2}", LowBandwidth, LowResolution, StandardResolution);
            }
        }

        [Serializable]
        public class CountData {

            [SerializeField]
            private int count;

            public int Count {
                get { return count; }
            }
        }

        public override string ToString() {
            return base.ToString() + " -> " + string.Format("Id: {0} Link: {1}", Id, Link);
        }
    }
}