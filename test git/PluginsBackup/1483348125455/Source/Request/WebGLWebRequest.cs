using System;
using System.Collections.Generic;
using UnityEngine;

namespace Instagram {

    public class WebGLWebRequest : UWebRequest {

        protected string ProxyEndpoint {
            get { return InstagramAPI.Instance.WebGLProxyEndpoint; }
        }

        public override void Get(MonoBehaviour context, string path, ErrorHandler OnError, CompleteHandler OnComplete) {
            WebGLInsta.Instance.MakeRequest(path,
                (message) => {
                    error = message;

                    OnError(error, null);
                },
                (json) => {
                    text = json;

                    OnComplete(json);
                }
            );
        }

        public override void Post(MonoBehaviour context, string path, Dictionary<string, string> parms, ErrorHandler OnError, CompleteHandler OnComplete) {
            ProcessRequest("POST", path, (resultPath) => {
                base.Post(context, resultPath, parms, OnError, OnComplete);
            });
        }

        public override void Delete(MonoBehaviour context, string path, ErrorHandler OnError, CompleteHandler OnComplete) {
            ProcessRequest("DELETE", path, (resultPath) => {
                base.Delete(context, resultPath, OnError, OnComplete);
            });
        }

        private void ProcessRequest(string type, string path, Action<string> callback) {
            if (!string.IsNullOrEmpty(ProxyEndpoint)) {
                path = string.Format("{0}?url={1}", ProxyEndpoint, path);

                callback(path);
            } else {
                InstagramAPI.Instance.LogError("You need to set a proxy endpoint in \"Resources > InstagramSettings.asset > WebGLProxyEndpoint\" field to make " + type + " requests. Read the WebGL documentation.");
            }
        }
    }
}