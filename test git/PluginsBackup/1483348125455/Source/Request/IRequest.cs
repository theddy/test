using System.Collections.Generic;
using UnityEngine;

namespace Instagram {

    public delegate void ErrorHandler(string message, string json);
    public delegate void CompleteHandler(string text);

    public interface IRequest {

        event ErrorHandler OnError;
        event CompleteHandler OnComplete;

        string Error { get; }

        string Text { get; }

        void Get(MonoBehaviour context, string path, ErrorHandler OnError, CompleteHandler OnComplete);

        void Post(MonoBehaviour context, string path, Dictionary<string, string> parms, ErrorHandler OnError, CompleteHandler OnComplete);

        void Delete(MonoBehaviour context, string path, ErrorHandler OnError, CompleteHandler OnComplete);
    }
}