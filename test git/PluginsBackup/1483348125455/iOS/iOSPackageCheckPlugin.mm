extern "C"
{
    bool isAppInstalled(const char* appID)
    {
        NSString *id = [NSString stringWithUTF8String:appID];
        return [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:id]];
    }
}