#import <UIKit/UIKit.h>

@interface WebBrowserController : UIViewController<UIWebViewDelegate>
{
    UIWebView *mWebView;
    
    UIToolbar *mToolbar;
    UIBarButtonItem *mBack;
    UIBarButtonItem *mForward;
    UIBarButtonItem *mRefresh;
    UIBarButtonItem *mClose;
    
    NSString* listenerName;
    NSString* submitUrl;
    NSString* submitCallback;
    NSString* cancelCallback;
}

extern int const headerHeight;

- (void)setListenerName:(NSString*)listenerName;
- (void)setSubmitUrl:(NSString*)url;
- (void)setSubmitCallback:(NSString*)callbackName;
- (void)setCancelCallback:(NSString*)callbackName;
- (void)loadRequest:(NSString*)url;
- (void)updateButtons;
- (void)dispose;
@end