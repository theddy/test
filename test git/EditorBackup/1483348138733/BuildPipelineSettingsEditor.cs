using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEditor;

namespace Touchten.Build
{
    [CustomEditor(typeof(BuildPipelineSettings))]
    public class BuildPipelineSettingsEditor : Editor
    {
        [InitializeOnLoadMethod]
        static void Init()
        {
            BuildPipelineSettings temp = BuildPipelineSettings.Instance;
            temp = null;
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.HelpBox("Build Pipeline Settings", MessageType.None);
            DrawDefaultInspector();
            DirtyEditor();
        }

        private static void DirtyEditor()
        {
            EditorUtility.SetDirty(BuildPipelineSettings.Instance);
        }
    }
}


