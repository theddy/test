#if UNITY_EDITOR
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using UnityEditor;

namespace Touchten.Build
{
    [InitializeOnLoad]
    public class BuildPipelineSettings : ScriptableObject
    {
        const string SettingsAssetName = "BuildPipelineSettings";
        const string SettingsPath = "Touchten Configuration/Resources";
        const string touchtenVmaxSettingsAssetExtension = ".asset";

        private static BuildPipelineSettings _instance;

        public static BuildPipelineSettings Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = Resources.Load(SettingsAssetName) as BuildPipelineSettings;

                    if (_instance == null)
                    {
                        _instance = CreateInstance<BuildPipelineSettings>();

                        string properPath = Path.Combine(Application.dataPath, SettingsPath);
                        if (!Directory.Exists(properPath))
                        {
                            AssetDatabase.CreateFolder("Assets", "Touchten Configuration");
                            AssetDatabase.CreateFolder("Assets/Touchten Configuration", "Resources");
                        }

                        string fullPath = Path.Combine(Path.Combine("Assets", SettingsPath),
                                          SettingsAssetName + touchtenVmaxSettingsAssetExtension);
                        AssetDatabase.CreateAsset(_instance, fullPath);
                    }
                }

                return _instance;
            }
        }

        #if UNITY_EDITOR
        [MenuItem("Touchten/Build Automation/Edit Build Settings...", false, 5)]
        public static void EditSettings()
        {
            Selection.activeObject = Instance;
            EditorApplication.ExecuteMenuItem("Window/Inspector");
        }
        #endif

        #region settings

        [SerializeField] string _milestone = "Undefined";
        [SerializeField] BuildConfigName _buildConfig = BuildConfigName.Debug;

        public static string Milestone { get { return Instance._milestone; } set { Instance._milestone = value; } }

        public static bool IsDebug { get { return Instance._buildConfig == BuildConfigName.Debug; } }

        #endregion
    }

    public enum BuildConfigName
    {
        Debug,
        Release
    }
}
#endif