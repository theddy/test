using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using System.Diagnostics;
using System.IO;

public class AppodealPostProcess : MonoBehaviour
{
	[PostProcessBuild(1)]
	public static void OnPostProcessBuild (BuildTarget target, string pathToBuildProject)
	{
#if UNITY_IPHONE
		UnityEngine.Debug.Log("===================== appodeal postbuild start ====================");

		// !TODO: Put Appodeal iOS SDK folder to your project path (up one folder from Assets folder)
		string appodealSdkPath;
		int cutoff = Application.dataPath.LastIndexOf ("Assets");
		if(cutoff != -1){
			appodealSdkPath = Path.Combine (Application.dataPath.Substring (0, cutoff), "Appodeal/");
		}
		else{
			appodealSdkPath = string.Empty;
		}

		Process pythonProcess = new Process();
		pythonProcess.StartInfo.FileName = "python";
		pythonProcess.StartInfo.Arguments = string.Format("Assets/Editor/post_process.py \"{0}\" \"{1}\"", pathToBuildProject, appodealSdkPath);

		UnityEngine.Debug.Log(pythonProcess.StartInfo.Arguments);

		pythonProcess.StartInfo.UseShellExecute = false;
		pythonProcess.StartInfo.RedirectStandardOutput = false;
		pythonProcess.Start();
		pythonProcess.WaitForExit();

		UnityEngine.Debug.Log("====================== appodeal postbuild end =====================");
#endif
	}
}
