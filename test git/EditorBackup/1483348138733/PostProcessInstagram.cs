using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using System.IO;

#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif

public class PostProcessInstagram : MonoBehaviour
{
	[PostProcessBuild (99)]
	public static void InstagramPostProcess (BuildTarget buildTarget, string pathToBuiltProject)
	{		
		//PostProcess only for IOS
		#if UNITY_IOS				      
		// Get plist
		string plistPath = pathToBuiltProject + "/Info.plist";
		PlistDocument plist = new PlistDocument ();
		plist.ReadFromString (File.ReadAllText (plistPath));
			
		// Get root
		PlistElementDict rootDict = plist.root;
			
		// Change value of CFBundleVersion in Xcode plist
		var buildKey = "LSApplicationQueriesSchemes";

		PlistElementArray urlTypeArray = rootDict.CreateArray (buildKey);
			
			
		urlTypeArray.AddString ("instagram");             
		File.WriteAllText (plistPath, plist.WriteToString ());
		return;
		#endif

	}
}