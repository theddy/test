/// <summary>
/// Builder script tweaked based on our own requirement. Based on Nordeus' Unity Build Pipeline script.
/// </summary>

using System;
using System.Collections.Generic;
using Nordeus.Build.Reporters;
using Nordeus.Build;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Linq;

namespace Touchten.Build
{
	public static partial class CommandLine
	{
        #region Constants
        private const string TargetDirectory = "AutomatedBuilds";
        private const string EnvUnityBuildName = "env.UNITY_BUILD_NAME";
        private const string EnvXCodeProductName = "env.XCODE_PRODUCT_NAME";
		#endregion

        #region Methods
        static void BuildWithConfig(string configName, string buildTarget, string buildReporter, AndroidKeys androidKeys = null)
        {
            PipelineConfig config = GetConfig(configName);

            ResolveConfig(config, buildTarget);
            BuildProject(config.name, buildTarget, buildReporter, androidKeys);
        }

        static void BuildWithDefaultName (string buildTarget, string buildReporter, string buildNumber, string milestone, bool isDebug, string extraName,  AndroidKeys androidKeys = null)
        {
            string buildName = GetBuildName(buildTarget, buildNumber, milestone, isDebug, extraName);
            BuildProject(buildName, buildTarget, buildReporter, androidKeys);
        }

        static void BuildProject(string buildName, string buildTarget, string buildReporter, AndroidKeys androidKeys = null)
        {
            if (!string.IsNullOrEmpty (buildReporter))
                BuildReporter.Current = BuildReporter.CreateReporterByName (buildReporter);

            try 
            {
                BuildReporter.Current.SetEnvironmentParameter(EnvUnityBuildName, buildName);
                BuildReporter.Current.SetEnvironmentParameter(EnvXCodeProductName, PlayerSettings.bundleIdentifier.Substring(PlayerSettings.bundleIdentifier.LastIndexOf('.') + 1));

                // execute prebuild command, if any
                IPreBuild preBuild = new PreBuildProcess();
                preBuild.Prepare();

                BuildTarget parsedBuildTarget = ParseBuildTarget(buildTarget);
                switch (parsedBuildTarget) 
                {
                    case BuildTarget.Android:
                        if (androidKeys == null) androidKeys = new AndroidKeys();
                        Builder.BuildToAndroid (TargetDirectory, buildName, androidKeys.keystoreName, androidKeys.keystorePass, androidKeys.keyaliasName, androidKeys.keyaliasPass);
                        break;
                    case BuildTarget.iOS:                                                                   
                        Builder.BuildToXCode (TargetDirectory, buildName);
                        break;
                    default:
                        throw new ArgumentException (parsedBuildTarget + " is not a supported build target.");
                }
            } 
            catch (Exception e) 
            {
                BuildReporter.Current.Log (e.Message, BuildReporter.MessageSeverity.Error);
            }
        }

        /// <summary>
        /// Gets the name of the build.
        /// Refer to https://docs.google.com/document/d/1ejTCjxA6Q1sX4e4GbrxbfGBjRkTr4pXIORDJ1oPi5mM/edit for convention.
        /// </summary>
        static string GetBuildName(string target, string buildNumber, string milestone, bool isDebug, string extraName)
        {
            BuildTarget buildTarget = ParseBuildTarget(target);

            string date = DateTime.Now.ToString("yyMMdd");
            string version = PlayerSettings.bundleVersion;
            string bundleVerCode = GetBundleVersionCode(buildTarget);
            string buildNumString = "b" + buildNumber;
            string debugString = isDebug ? "Debug" : "Release";
            string bundleIdentifier = PlayerSettings.bundleIdentifier;

            string name = string.Format("{0}_{1}_{2}_{3}_{4}_{5}_{6}",
                date,
                version,
                bundleVerCode,
                buildNumString,
                milestone,
                debugString,
                bundleIdentifier);

            if (!string.IsNullOrEmpty(extraName))
                name = name + "_" + extraName;

            return name;
        }

        static string GetBundleVersionCode(BuildTarget buildTarget)
        {
            if (buildTarget == BuildTarget.Android)
                return PlayerSettings.Android.bundleVersionCode.ToString();
            else if (buildTarget == BuildTarget.iOS)
                return PlayerSettings.iOS.buildNumber;
            else
                return "";
        }

        static void ResolveConfig(PipelineConfig config, string buildTarget)
        {
            PlayerSettings.bundleVersion = config.version;
            PlayerSettings.bundleIdentifier = config.buildIndentifier;

            BuildTarget target = ParseBuildTarget(buildTarget);
            string buildDefined = ParseDefineSymbols(config, target);

            switch (target) 
            {
                case BuildTarget.iOS:
                    PlayerSettings.SetScriptingDefineSymbolsForGroup (BuildTargetGroup.iOS, buildDefined);
                    break;
                case BuildTarget.Android:
                    PlayerSettings.SetScriptingDefineSymbolsForGroup (BuildTargetGroup.Android, buildDefined);
                    break;
            }
        }

        static PipelineConfig GetConfig(string configName)
        {
            string configFiles = File.ReadAllText (Application.dataPath + "/" + configName + ".json");
            return JsonUtility.FromJson<PipelineConfig> (configFiles);
        }

        static string ParseDefineSymbols(PipelineConfig config, BuildTarget  buildTarget)
        {
            string buildDefined = "";

            switch (buildTarget) {
                case BuildTarget.Android:                       
                    buildDefined += (";" + string.Join (";", config.android.defines).ToUpper ());
                    buildDefined += (";" + PlayerSettings.GetScriptingDefineSymbolsForGroup (BuildTargetGroup.Android));
                    break;
                case BuildTarget.iOS:                       
                    buildDefined += (";" + string.Join (";", config.ios.defines).ToUpper ());
                    buildDefined += (";" + PlayerSettings.GetScriptingDefineSymbolsForGroup (BuildTargetGroup.iOS));
                    break;
            }

            List<string> defined = new List<string> (buildDefined.Split (';'));
            buildDefined = string.Join (";", defined.Distinct ().ToArray<string> ()); 

            return buildDefined;
        }

        static BuildTarget ParseBuildTarget(string buildTarget)
        {
            return (BuildTarget)Enum.Parse(typeof(BuildTarget), buildTarget);
        }

		#endregion
	}
}