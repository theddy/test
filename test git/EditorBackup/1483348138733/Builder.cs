using System;
using System.Collections.Generic;
using System.IO;
using Nordeus.Build.Reporters;
using UnityEditor;
using UnityEngine;
using System.Collections;

namespace Touchten.Build
{
	public static class Builder
	{
        static void BuildUnity(BuildTarget target, string buildFullPath)
        {
            string buildMessage = BuildPipeline.BuildPlayer(GetEnabledScenePaths().ToArray(), buildFullPath, target, BuildOptions.None);

            if (string.IsNullOrEmpty(buildMessage)) 
                BuildReporter.Current.IndicateSuccessfulBuild();
            else
                BuildReporter.Current.Log(buildMessage, BuildReporter.MessageSeverity.Error);
        }

		/// <summary>
		/// Returns a list of all the enabled scenes.
		/// </summary>
		private static List<string> GetEnabledScenePaths()
		{
			List<string> scenePaths = new List<string>();

			foreach (var scene in EditorBuildSettings.scenes)
			{
                if (scene.enabled) scenePaths.Add(scene.path);
			}

			return scenePaths;
		}

		/// <summary>
		/// Builds the specified build target.
		/// </summary>
		/// <param name="parsedBuildTarget">Build target to build.</param>
		/// <param name="buildPath">Output path for the build.</param>
                                                                                             		/// <param name="parsedTextureSubtarget">Texture compression subtarget for Android.</param>

        public static void BuildToAndroid(string directory, string buildName, string keystoreName, string keystorePass, string keyaliasName, string keyaliasPass)
        {
            if (string.IsNullOrEmpty(keystoreName)) PlayerSettings.Android.keystoreName = keystoreName;
            if (string.IsNullOrEmpty(keystorePass)) PlayerSettings.Android.keystorePass = keystorePass;
            if (string.IsNullOrEmpty(keyaliasName)) PlayerSettings.Android.keyaliasName = keyaliasName;
            if (string.IsNullOrEmpty(keyaliasPass)) PlayerSettings.Android.keyaliasPass = keyaliasPass;

            string buildPath =  PrepareBuildPath(directory, buildName) + ".apk";
            BuildReporter.Current.Log(string.Format("Building android with path = {0}, keystore: {1}, keyalias: {2}", buildPath, keystoreName, keyaliasName));

            BuildUnity(BuildTarget.Android, buildPath);
        }

        public static void BuildToXCode(string directory, string buildName)
        {
            string buildPath =  PrepareBuildPath(directory, buildName);
            BuildReporter.Current.Log(string.Format("Building to Xcode with path = {0}", buildPath));

            BuildUnity(BuildTarget.iOS, buildPath);
        }

        static string PrepareBuildPath(string directory, string buildName)
        {
            FileUtil.DeleteFileOrDirectory(directory);
            Directory.CreateDirectory(directory);

            string filePath = Path.Combine(directory, buildName);
            if (File.Exists(filePath)) {
                Debug.Log("Previous file exists. Deleting previous file.");
                File.Delete(filePath);
            }

            return filePath;

        }
	}
}